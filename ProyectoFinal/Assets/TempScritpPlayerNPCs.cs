using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;

public class TempScritpPlayerNPCs : MonoBehaviour
{
    RaycastHit lastHit = new RaycastHit();
    public GameObject child;
    public GameObject NPCInteractText;
    public GameObject Camera;
    public bool isPlayerAbleToMove = true; // This variable will be used to limit the player's movement when interacting with NPC or cinematics are running
    public bool isPlayerTalking = false; // This variable will be used to know when the player is talking and limit its action capacities

    public SettingsScriptableObject settingsScriptableObject;

    public QuestSO activeQuest;

    void Update()
    {
        RaycastHit hit;
        if (activeQuest == null)
        {
            print("NO active quest");
        }
        else
        {
            print(activeQuest.QuestName);
        }
        print("Player talking?? " + isPlayerTalking);
        // Throw RayCast to see if the player could interact with something
        if (Physics.Raycast(Camera.transform.position, Camera.transform.TransformDirection(Vector3.forward), out hit, 6))
        {
            if (hit.transform.tag == "NPC") // If the player has hit an NPC entity
            {
                lastHit = hit;
                // Enable Outline Script
                if (hit.transform.GetComponent<Outline>() != null)
                {
                    hit.transform.GetComponent<Outline>().enabled = true;
                }
                // We show a text to help the player interact with the NPC
                PopUpToInteract();

                // If the player is facing an NPC and press E and is not talking already do things
                if (Input.GetKeyDown("e") && !isPlayerTalking)
                {
                    // Animate the camera playing an animation
                    PlayInConversationAnimation();
                    // Call the interact() function of that interactuable object -> NPCScript.cs
                    hit.transform.GetComponent<PassiveNPCScript>().Interact();
                }
            }
            else if (hit.transform.tag == "ITEM")
            {
                lastHit = hit;
                // Enable Outline Script
                if (hit.transform.GetComponent<Outline>() != null)
                {
                    hit.transform.GetComponent<Outline>().enabled = true;
                }
                // We show a text to help the player interact with the item
                PopUpToInteract();
                // If the player is facing an item and press E
                if (Input.GetKeyDown("e"))
                {
                    // Call the interact() function of that interactuable object -> ItemScript.cs
                    hit.transform.GetComponent<ItemScript>().Interact();
                }
            }
        }
        else
        {
            // Remove the little help text of 'Press E to interact'
            RemovePopUpToInteract();
            /*
             * if (lastHit.transform.gameObject != null && lastHit.transform.GetComponent<Outline>() != null)
            {
                lastHit.transform.GetComponent<Outline>().enabled = false;
            }
             */
        }

        if (isPlayerTalking)
        {
            // If the player is talking with something/someone and press Esc key the conversation will end
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                // Remove the little help text of 'Press E to interact'
                RemovePopUpToInteract();
                // Play the animation to restore the camera position
                // Set the talking variable and free the player's movement
                // Call the function to close all panels available from the conversation
                CloseConversation();
            }
        }

        if (isPlayerAbleToMove)
        {
            if (Input.GetKey("space"))
            {
                child.GetComponent<Rigidbody>().velocity = child.transform.up * 5;
            }
            if (Input.GetKey("w"))
            {
                child.GetComponent<Rigidbody>().velocity = child.transform.forward * 2;
            }
            if (Input.GetKey("s"))
            {
                child.GetComponent<Rigidbody>().velocity = -child.transform.forward * 2;
            }
            if (Input.GetKey("a"))
            {
                child.transform.Rotate(new Vector3(0, -1, 0) * 1);
            }
            if (Input.GetKey("d"))
            {
                child.transform.Rotate(new Vector3(0, 1, 0) * 1);
            }
        }
    }

    private void PopUpToInteract() // Change the text to be the settings key binding
    {
        NPCInteractText.SetActive(true);
    }
    private void RemovePopUpToInteract()
    {
        NPCInteractText.SetActive(false);
    }

    // Do all stuff that involves finish a conversation with npc
    public void CloseConversation()
    {
        print("Out conversation Animation");
        PlayOutConversationAnimation();
        print("Setting variables closing conversation");
        isPlayerTalking = false;
        isPlayerAbleToMove = true;
        print("Closing NPC Panels");
        print("HIT NAME " + lastHit.transform.name);
        lastHit.transform.GetComponent<PassiveNPCScript>().CloseNPCPanels();
    }

    public void PlayOutConversationAnimation()
    {
        Camera.GetComponent<Animator>().Play("OutConversation");
    }

    public void PlayInConversationAnimation()
    {
        Camera.GetComponent<Animator>().Play("InConversation");
    }
}
