using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialQuestAutoDestroyWall : MonoBehaviour
{
    public QuestSO quest;

    void Start()
    {
        if (quest.GetState() == QuestSO.QuestState.FINISHED)
        {
            Destroy(this.gameObject);
        }
    }
}
