using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteChildsOnDisable : MonoBehaviour
{
    Transform[] allChilds;

    private void OnEnable()
    {
        allChilds = GetComponentsInChildren<Transform>();
    }

    private void OnDisable()
    {
        print("DISABLING CHILDS ON " + this.gameObject.name);
        foreach(Transform child in allChilds)
        {
            print("NAME OF THE CHILD WHICH WILL BE DELETED: " + child.gameObject.name);
            if(child != this.GetComponent<Transform>())
                Destroy(child.gameObject);
        }
    }
}
