using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingZone_Skill : ActiveSkill
{
    [SerializeField] GameObject healingZonePrefab;
    [SerializeField] AGUSMOVEMENT m_PlayerBehaviour;
    [SerializeField] float minDistance;

    public override void Use()
    {
        base.Use();

        GameObject newTornado = Instantiate(healingZonePrefab, m_PlayerBehaviour.transform.position + new Vector3(0, -0.8f, 0), m_PlayerBehaviour.playerObj.rotation);
        newTornado.GetComponent<HealZone>().EnableHealZone(m_PlayerBehaviour.DoDamage().dmg / 100, skillDuration, m_PlayerBehaviour, minDistance);

    }
}
