using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using UnityEngine.Windows;

public class MapController : MonoBehaviour
{
    [SerializeField] GameObject Player;
    [SerializeField] Camera MapCamera;
    public Sprite CheckImage;
    public Sprite CrossImage;
    AGUSMOVEMENT agusMovement;
    bool isMovingMap = false;

    public float moveMapSensibility;

    public int MinZ = -175;
    public int MaxZ = 175;
    public int MinX = -112;
    public int MaxX = 90;

    private void Start()
    {
        agusMovement = Player.GetComponent<AGUSMOVEMENT>();
        agusMovement.m_InputMap.Player.MoveMap.performed += MovingMethod;
    }
    void MovingMethod(InputAction.CallbackContext context)
    {
        isMovingMap = true;
    }

    private void Update()
    {
        if (isMovingMap)
        {
            MoveMap();
        }

        if (MapCamera.transform.position.x > MaxX)
        {
            MapCamera.transform.position = new Vector3(MaxX, MapCamera.transform.position.y, MapCamera.transform.position.z);
        }
        if (MapCamera.transform.position.x < MinX)
        {
            MapCamera.transform.position = new Vector3(MinX, MapCamera.transform.position.y, MapCamera.transform.position.z);
        }

        if (MapCamera.transform.position.z > MaxZ)
        {
            MapCamera.transform.position = new Vector3(MapCamera.transform.position.x, MapCamera.transform.position.y, MaxZ);
        }
        if (MapCamera.transform.position.z < MinZ)
        {
            MapCamera.transform.position = new Vector3(MapCamera.transform.position.x, MapCamera.transform.position.y, MinZ);
        }
    }




    public void MoveMap()
    {
        Vector2 a2 = agusMovement.m_InputMap.Player.MoveMap.ReadValue<Vector2>();
        MapCamera.transform.position += new Vector3(a2.x, 0, a2.y).normalized * Time.deltaTime * moveMapSensibility;
        if (a2 == Vector2.zero) isMovingMap = false;
    }

    public void CenterThePlayer()
    {
        MapCamera.transform.position = new Vector3(Player.transform.position.x, this.transform.position.y, Player.transform.position.z);
    }

    public void Zoom()
    {
        if (this.gameObject.activeSelf)
        {
            float direction = GetScroll();
            if (direction > 0)
            {


                if (MapCamera.orthographicSize > 15)
                {
                    MapCamera.orthographicSize -= 5;
                    if (MapCamera.orthographicSize < 15)
                    {
                        MapCamera.orthographicSize = 15;
                    }
                }

            }
            else if (direction < 0)
            {
                if (MapCamera.orthographicSize < 50)
                {
                    MapCamera.orthographicSize += 5;
                    if (MapCamera.orthographicSize > 50)
                    {
                        MapCamera.orthographicSize = 50;
                    }
                }
            }
        }
    }
    public float GetScroll()
    {
        return Mouse.current.scroll.ReadValue().y;
    }

    public void ToggleFilters(string type)
    {
        // Get all objects in the layer of icons of the minimap
        GameObject[] allIconsLayer16;
        GameObject[] allIconsLayer17;
        allIconsLayer16 = FindGameObjectsInLayer(16);
        allIconsLayer17 = FindGameObjectsInLayer(17);

        var empty = Enumerable.Empty<GameObject>();

        GameObject[] allIcons = (allIconsLayer16 ?? empty).Concat(allIconsLayer17 ?? empty).ToArray();

        // Get the items of the type of the parameter and change the layers to see it or not
        foreach (GameObject icon in allIcons)
        {
            if (icon.name.Contains(type))
            {
                if (icon.layer == 16)
                {
                    print("Changing to layer 17 icon " + icon.name);
                    icon.layer = 17;
                    ToggleCheckOnGameObject(type);
                }
                else if (icon.layer == 17)
                {
                    print("Changing to layer 16 icon " + icon.name);
                    icon.layer = 16;
                    ToggleCheckOnGameObject(type);
                }
            }
        }
    }

    void ToggleCheckOnGameObject(string type)
    {
        GameObject FiltersGO = transform.GetChild(0).gameObject;
        Transform[] childs = FiltersGO.GetComponentsInChildren<Transform>();
        foreach (Transform child in childs)
        {
            print("toggling filters for child " + child.name + ". Comparing with name " + type);
            if (child.name.Contains(type))
            {
                if (child.GetComponent<Image>().sprite == CrossImage)
                    child.GetComponent<Image>().sprite = CheckImage;
                else
                    child.GetComponent<Image>().sprite = CrossImage;
                break;
            }
        }
    }

    GameObject[] FindGameObjectsInLayer(int layer)
    {
        GameObject[] goArray = FindObjectsOfType(typeof(GameObject)) as GameObject[];
        List<GameObject> goList = new List<GameObject>();
        for (int i = 0; i < goArray.Length; i++)
        {
            if (goArray[i].layer == layer)
            {
                goList.Add(goArray[i]);
            }
        }
        if (goList.Count == 0)
        {
            return null;
        }
        if (goList.Count > 0)
        {
            return goList.ToArray();
        }
        else
        {
            return (GameObject[])Enumerable.Empty<GameObject>();
        }
    }

}
