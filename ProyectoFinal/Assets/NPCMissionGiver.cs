using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static QuestSO;

public class NPCMissionGiver : Interactuable
{
    public NPCSentences SentencesSO;
    private NPC_QUEST_STATES currentState;
    public List<QuestSO> availableMissions;

    public GameObject CanvasDestroyedOnLoad;

    public GameObject Quests;
    public GameObject DialogPanel;
    public GameObject QuesterPanel;
    public TextMeshProUGUI DialogText;
    public GameObject QuestList;
    public GameObject QuestBTN;
    public GameObject QuestInfo;
    public GameObject QuestUIGroup;
    public TextMeshProUGUI UI_QuestTitle;
    public TextMeshProUGUI UI_QuestDescription;
    public TextMeshProUGUI UI_TasksText;
    public TextMeshProUGUI UI_RewardsText;
    public TextMeshProUGUI QuestNameLabel;
    public TextMeshProUGUI QuestDescLabel;
    public TextMeshProUGUI RequirementsLabel;
    public TextMeshProUGUI RewardsLabel;
    public Button AcceptQuestBTN;
    public GameObject AbandonBTN;
    public GameObject ClaimRewardsBTN;

    GameObject GetCanvasObject()
    {
        GameObject temp = new GameObject();
        UnityEngine.SceneManagement.Scene dontDestroyOnLoad = temp.scene;
        Object.DestroyImmediate(temp);
        temp = null;

        foreach (var rootObject in dontDestroyOnLoad.GetRootGameObjects())
        {
            if (rootObject.name == "Canvas")
            {
                return rootObject;
            }
        }

        return null;
    }

    private void Start()
    {
        this.ChangeCurrentStateTo(NPC_QUEST_STATES.IDLE);

        print("DOES THE CANVAS EXISTS OR NOT? " + GameManager.Instance.CanvasGO);
        CanvasDestroyedOnLoad = GameManager.Instance.CanvasGO;
        GetAllObjects();
        //SetAllObjectsInactive();
    }
    void GetAllObjects()
    {
        Quests = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().Quests;
        DialogPanel = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().DialogPanel;
        QuesterPanel = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().QuesterPanel;
        DialogText = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().DialogText;
        QuestList = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().QuestList;
        QuestBTN = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().QuestBTN;
        QuestInfo = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().QuestInfo;
        QuestUIGroup = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().QuestUIGroup;
        UI_QuestTitle = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().UI_QuestTitle;
        UI_QuestDescription = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().UI_QuestDescription;
        UI_TasksText = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().UI_TasksText;
        UI_RewardsText = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().UI_RewardsText;
        QuestNameLabel = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().QuestNameLabel;
        QuestDescLabel = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().QuestDescLabel;
        RequirementsLabel = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().RequirementsLabel;
        RewardsLabel = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().RewardsLabel;
        AcceptQuestBTN = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().AcceptQuestBTN;
        AbandonBTN = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().AbandonBTN;
        ClaimRewardsBTN = CanvasDestroyedOnLoad.GetComponent<CanvasSingleton>().ClaimRewardsBTN;
    }
    void SetAllObjectsInactive()
    {
        Quests.SetActive(false);
        DialogPanel.SetActive(false);
        QuesterPanel.SetActive(false);
        DialogText.gameObject.SetActive(false);
        QuestList.SetActive(false);
        QuestBTN.SetActive(false);
        QuestInfo.SetActive(false);
        QuestUIGroup.SetActive(false);
        UI_QuestTitle.gameObject.SetActive(false);
        UI_QuestDescription.gameObject.SetActive(false);
        UI_TasksText.gameObject.SetActive(false);
        UI_RewardsText.gameObject.SetActive(false);
        QuestNameLabel.gameObject.SetActive(false);
        QuestDescLabel.gameObject.SetActive(false);
        RequirementsLabel.gameObject.SetActive(false);
        RewardsLabel.gameObject.SetActive(false);
        AcceptQuestBTN.gameObject.SetActive(false);
        AbandonBTN.SetActive(false);
        ClaimRewardsBTN.SetActive(false);
    }

    public override bool Interact()
    {
        GameManager.Instance.menuOpened = true;
        GameManager.Instance.CanConsoleBeOpened = false;
        GameManager.Instance.currentInteractuableNPC = this.gameObject;
        OpenQuestPanel();

        return false;
    }

    void CheckIfPlayerQuestIsMine()
    {
        QuestSO quest = QuestManager.Instance.MisionActiva;
        foreach (QuestSO q in availableMissions)
        {
            if ((q.questState == QuestState.DOING_FINISHED || q.questState == QuestState.DOING_NOT_FINISHED) && q == quest)
                this.ChangeCurrentStateTo(NPC_QUEST_STATES.QUEST_IN_PROGRESS);
        }
    }

    void ShowAllAvailableQuests()
    {
        if (QuestList.transform.childCount > 0)
            foreach (Transform go in QuestList.transform)
                Destroy(go.gameObject);

        foreach (QuestSO quest in availableMissions)
        {
            // If quest is not completed list it in the panel, else do not list it because is useless
            if (quest.GetState() != QuestState.FINISHED)
            {
                // Bool to manage if we have to display the quest or break the loop and go to the next iteration
                bool displayQuest = true;

                // For each "item" in the list of quest dependencies, check if the current quest 'quest' is the same as any of the quest in the list of dependencies
                // Then check if the dependency quest of that one is completed. If so, list the actual quest. If not, just continue for the next quest to list and repeat the process
                if (quest.Dependencies.Count > 0)
                {
                    foreach (QuestSO dependency in quest.Dependencies)
                    {
                        if (dependency.GetState() != QuestState.FINISHED)
                        {
                            displayQuest = false;
                            break;
                        }
                    }
                }

                // if displayQuest value is setted to false, it means that the quest must not be displayed in the list, so continue;
                if (displayQuest == false)
                {
                    continue;
                }
                else
                {
                    // Instantiate a button/prefab with things about the quest
                    GameObject newQuestBTN = Instantiate(QuestBTN, QuestList.transform);

                    // Add listener to each button to open the correct info panel
                    //newQuestBTN.GetComponent<Button>().onClick.RemoveAllListeners();
                    newQuestBTN.GetComponent<Button>().onClick.AddListener(() => OpenQuestInfoPanel(quest));
                    newQuestBTN.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = quest.QuestName;
                }
            }
        }
    }

    public void OpenQuestInfoPanel(QuestSO quest)
    {
        GameManager.Instance.menuOpened = true;
        QuestInfo.SetActive(true);
        UI_QuestTitle.text = quest.QuestName;
        QuestNameLabel.text = quest.QuestName;
        UI_QuestDescription.text = quest.QuestDescription;
        QuestDescLabel.text = quest.QuestDescription;
        string requirementsText = FillReqText(quest);
        UI_TasksText.text = requirementsText;
        RequirementsLabel.text = requirementsText;
        string rewardsText = FillRewText(quest);
        UI_RewardsText.text = rewardsText;
        RewardsLabel.text = rewardsText;

        ClaimRewardsBTN.gameObject.SetActive(false);

        // If the player already has an active mission, set the button as 'disabled'. Else, set the button as enabled and add listener
        if (QuestManager.Instance.HasActiveMission() && QuestManager.Instance.MisionActiva == quest)
        {
            AcceptQuestBTN.gameObject.SetActive(false);
            ClaimRewardsBTN.SetActive(false);
            AbandonBTN.SetActive(true);
        }
        else if (QuestManager.Instance.HasActiveMission() && QuestManager.Instance.MisionActiva != quest)
        {
            AcceptQuestBTN.gameObject.SetActive(false);
            AbandonBTN.SetActive(false);
            ClaimRewardsBTN.SetActive(false);
        }
        else
        {
            AcceptQuestBTN.gameObject.SetActive(true);
            AbandonBTN.SetActive(false);
            AcceptQuestBTN.onClick.AddListener(() => AcceptMission(quest));
        }
    }

    string FillRewText(QuestSO quest)
    {
        string finalText = "REWARDS: \n";

        foreach (QuestReward reward in quest.rewards)
        {
            finalText += reward.rewardName + " " + reward.amount + "\n";
        }

        return finalText;
    }

    string FillReqText(QuestSO quest)
    {
        string finalText = "There are no requirements";
        if (quest is MisionRecolectaSO recQuest)
        {
            finalText = "Collect " + recQuest.itemObjetivo.ItemName + ": " + recQuest.progreso + "/" + recQuest.cantidadObjetivo;
        }
        else if (quest is MisionDungeonSO dungQuest)
        {
            finalText = "Complete the dungeon";
        }
        else if (quest is MisionPosicionSO posQuest)
        {
            finalText = "Go to " + posQuest.LugarALlegar.name;
        }

        return finalText;
    }

    void OpenQuestPanel()
    {

        QuestInfo.SetActive(false);
        QuesterPanel.SetActive(false);
        DialogPanel.SetActive(false);

        DialogPanel.SetActive(true);
        QuesterPanel.SetActive(true);

        CheckIfPlayerQuestIsMine();

        if (this.currentState == NPC_QUEST_STATES.IDLE) // If the player does not have any active quest from this NPC, The NPC shows all available quests
        {
            DialogText.text = SentencesSO.SayRandomQuestSentece();
            ShowAllAvailableQuests();
        }
        else if (this.currentState == NPC_QUEST_STATES.QUEST_IN_PROGRESS)
        {
            print("LMAOOO " + QuestManager.Instance.MisionActiva.GetState());
            // If the last time the player interacted with the NPC was the quest in progress, but now the quest has been completed, change the npc state and do things
            if (QuestManager.Instance.MisionActiva.GetState() == QuestState.DOING_FINISHED)
            {
                // Open the info panel with a button of 'ClaimRewards'
                OpenQuestInfoPanel(QuestManager.Instance.MisionActiva);
                AcceptQuestBTN.gameObject.SetActive(false);
                AbandonBTN.SetActive(false);
                ClaimRewardsBTN.gameObject.SetActive(true);
                ClaimRewardsBTN.GetComponent<Button>().onClick.AddListener(() => ClaimRewards(QuestManager.Instance.MisionActiva));
                if (HasAnyRemainingQuest()) // If there is any remaining quest in the availableMissions list, change the state to default/idle
                {
                    this.ChangeCurrentStateTo(NPC_QUEST_STATES.IDLE);
                }
                else
                {
                    this.ChangeCurrentStateTo(NPC_QUEST_STATES.QUEST_COMPLETED);
                }
                // Set the text as the CompletedDialogue because the last quest is actually completed, although there could be more available quests...
                DialogText.text = QuestManager.Instance.MisionActiva.CompletedDialogue;
            }
            else
            {
                OpenQuestInfoPanel(QuestManager.Instance.MisionActiva);

                // If the player currently has an active quest, do not show the accept button, instead the abandon button
                AcceptQuestBTN.gameObject.SetActive(false);
                AbandonBTN.SetActive(true);
                AbandonBTN.GetComponent<Button>().onClick.AddListener(() => AbandonMission());

                string sentence = QuestManager.Instance.MisionActiva.OnGoingDialogue;
                DialogText.text = sentence;
            }
        }
        else if (this.currentState == NPC_QUEST_STATES.QUEST_COMPLETED)
        {
            DialogText.text = QuestManager.Instance.MisionActiva.CompletedDialogue;
            DialogText.text = SentencesSO.SayRandomPassiveSentece(); // Say something random as this NPC is not useful anymore i believe
        }
    }

    void ClaimRewards(QuestSO quest)
    {
        // Add items to inventory
        // Loop through all amount of each items/rewards of the quest
        foreach (QuestReward reward in quest.rewards)
        {
            for (int i = 0; i < reward.amount; i++)
            {
                if (reward.rewardName == "GOLD")
                {
                    print("GOLD TO GIVE " + reward.amount);
                    PlayerMaxStatsRuntime.Instance.CURRENT_GOLD += reward.amount;
                    break;
                }
                else
                {
                    ObjectItem obj = GameManager.Instance.SearchItemInGame(reward.rewardName);
                    InventoryManager.Instance.AddItem(obj);
                }
            }
        }

        // Close all the panels and this kind of stuff
        QuestInfo.SetActive(false);
        QuesterPanel.SetActive(false);
        DialogPanel.SetActive(false);

        //Change the quest state and set the active quest to null, since the player has finished the last quest
        AcceptQuestBTN.onClick.RemoveAllListeners();
        QuestManager.Instance.MisionActiva.ChangeQuestState(QuestState.FINISHED);
        QuestManager.Instance.MisionActiva = null;

        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    public void AcceptMission(QuestSO mission) // Esta funcion se llama cuando el jugador acepta la mision. Comprueba que no tenga una misi�n activa ya
    {
        QuestManager.Instance.MisionActiva = null;
        if (QuestManager.Instance.HasActiveMission() == false) // If the player has no active mission, set this mission as active
        {
            QuestManager.Instance.AgregarMisionActiva(mission);
            this.ChangeCurrentStateTo(NPC_QUEST_STATES.QUEST_IN_PROGRESS);
            mission.ChangeQuestState(QuestState.DOING_NOT_FINISHED);
            // Close all the panels and this kind of stuff
            QuestInfo.SetActive(false);
            QuesterPanel.SetActive(false);
            DialogPanel.SetActive(false);


            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            GameManager.Instance.menuOpened = false;
        }
        else // If the player already has a quest, do not accept it. TODO show some kind of visual feedback and this kind of things i guess
        {
            Debug.Log("You can't accept another mission until you complete your current one. Your quest is " + QuestManager.Instance.MisionActiva);
        }
    }

    bool HasAnyRemainingQuest()
    {
        foreach (QuestSO quest in availableMissions)
        {
            if (quest.GetState() != QuestState.FINISHED) return true;
        }
        return false;
    }

    public void AbandonMission()
    {
        QuestManager.Instance.EliminarMisionActiva();
        // Close all the panels and this kind of stuff
        QuestInfo.SetActive(false);
        QuesterPanel.SetActive(false);
        DialogPanel.SetActive(false);

        this.ChangeCurrentStateTo(NPC_QUEST_STATES.IDLE);


        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    void ChangeCurrentStateTo(NPC_QUEST_STATES state)
    {
        this.currentState = state;
    }

    public enum NPC_QUEST_STATES
    {
        IDLE,
        QUEST_IN_PROGRESS,
        QUEST_COMPLETED
    }
}
