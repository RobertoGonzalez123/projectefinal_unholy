using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalonCallFatherShoot : MonoBehaviour
{
    public void ToCallFather()
    {
        GetComponentInParent<EnemyBehaviourMetalon>().ToShootProjectile();
    }
}
