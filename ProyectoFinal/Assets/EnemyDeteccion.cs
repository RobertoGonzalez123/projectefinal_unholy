using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeteccion : MonoBehaviour
{
    [SerializeField] EnemiesDetection_Skill skill;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Enemy")
        {
            other.GetComponent<Outline>().enabled = true;
            skill.enemigosRevelados.Add(other.gameObject);
        }
    }
}
