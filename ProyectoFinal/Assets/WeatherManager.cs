using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeatherManager : MonoBehaviour
{
    public static WeatherManager Instance { get; private set; }
    public WeatherState currentWeatherState = WeatherState.Sunny;
    public float sunnyTimePercentage = 0.4f;
    public float cloudyTimePercentage = 0.2f;
    public float windyTimePercentage = 0.1f;
    public float rainyTimePercentage = 0.1f;
    [Tooltip("This is the multiplier for the above values. The result will be the seconds each state will be active")]
    public float totalGameTimeMultiplier = 1000f;

    public GameObject sunnyVFX;
    public GameObject cloudyVFX;
    public GameObject windyVFX;
    public GameObject rainyVFX;

    public AudioClip sunnyAudioClip;
    public AudioClip cloudyAudioClip;
    public AudioClip windyAudioClip;
    public AudioClip rainyAudioClip;

    private float currentTime = 0f;
    private float totalTime = 0f;

    public delegate void WeatherChanged();
    public event WeatherChanged OnWeatherChanged;

    void Awake()
    {
        if (Instance != null && Instance != this) Destroy(this);
        else Instance = this;
    }

    private void Start()
    {
        totalTime = CalculateTotalTime();
    }

    private void Update()
    {
        currentTime += Time.deltaTime;

        if (currentTime >= totalTime)
        {
            currentTime = 0f;
            totalTime = CalculateTotalTime();
            ChangeWeatherState();
        }
    }

    private float CalculateTotalTime()
    {
        float total = 0f;
        total += sunnyTimePercentage;
        total += cloudyTimePercentage;
        total += windyTimePercentage;
        total += rainyTimePercentage;
        return total * totalGameTimeMultiplier; // Convert to seconds
    }

    private void ChangeWeatherState()
    {

        WeatherState previousWeatherState = currentWeatherState;
        float randomValue = Random.value;

        if (randomValue < sunnyTimePercentage)
        {
            currentWeatherState = WeatherState.Sunny;
            PlaySunnyEffects();
        }
        else if (randomValue < sunnyTimePercentage + cloudyTimePercentage)
        {
            currentWeatherState = WeatherState.Cloudy;
            PlayCloudyEffects();
        }
        else if (randomValue < sunnyTimePercentage + cloudyTimePercentage + windyTimePercentage)
        {
            currentWeatherState = WeatherState.Windy;
            PlayWindyEffects();
        }
        else
        {
            currentWeatherState = WeatherState.Rainy;
            PlayRainyEffects();
        }
        // TODO make something more interesting behaving different when the weather changes idk
        if(OnWeatherChanged != null) OnWeatherChanged.Invoke();
    }

    private void PlayWeatherEffects(GameObject activeVFX, AudioClip audioClip)
    {
        sunnyVFX.SetActive(false);
        cloudyVFX.SetActive(false);
        windyVFX.SetActive(false);
        rainyVFX.SetActive(false);
        activeVFX.SetActive(true);

        AudioSource audioSource = GetComponent<AudioSource>();
        audioSource.Stop();
        audioSource.clip = audioClip;
        audioSource.Play();
    }

    private void PlaySunnyEffects()
    {
        PlayWeatherEffects(sunnyVFX, sunnyAudioClip);
    }

    private void PlayCloudyEffects()
    {
        PlayWeatherEffects(cloudyVFX, cloudyAudioClip);
    }

    private void PlayWindyEffects()
    {
        PlayWeatherEffects(windyVFX, windyAudioClip);
    }

    private void PlayRainyEffects()
    {
        PlayWeatherEffects(rainyVFX, rainyAudioClip);
    }

    public enum WeatherState
    {
        Sunny,
        Windy,
        Cloudy,
        Rainy
    }
}
