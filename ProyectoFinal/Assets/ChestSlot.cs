using UnityEngine;
using UnityEngine.UI;

public class ChestSlot : MonoBehaviour
{
    public ObjectItem item;
    public Image itemIcon;
    public ChestController sackWhereIBelong;

    public void SetItemToSlot(ObjectItem itemToLoad, ChestController sack)
    {
        this.sackWhereIBelong = sack;
        this.item = itemToLoad;
        this.itemIcon.sprite = this.item.itemModel.SlotIcon;
    }
    public void AddItemToInventory()
    {
        if (InventoryManager.Instance.AddItem(this.item))
        {
            sackWhereIBelong.UnloadItemByID(this.item);
            RemoveItemFromUI();
        }
    }

    public void RemoveItemFromUI()
    {
       Destroy(this.gameObject);
    }
}
