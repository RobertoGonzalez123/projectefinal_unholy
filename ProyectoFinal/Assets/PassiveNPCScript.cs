using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PassiveNPCScript : Interactuable
{
    public NPCSentences SO_NPCsComments;
    public GameObject player;
    public GameObject DialogPanel;

    public override bool Interact()
    {
        // Set the variables of playerTalking and playerAbleToMove correctly
        player.transform.GetComponent<AGUSMOVEMENT>().isPlayerTalking = true;
        player.transform.GetComponent<AGUSMOVEMENT>().isPlayerAbleToMove = false;
        // Open the panel setting it to SetActive true
        OpenDialogPanel();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;

        // Pq roberto quiere
        return false;
    }

    private void OpenDialogPanel()
    {
        print("OPENING DIALOG PANEL");
        DialogPanel.SetActive(true);
        GameObject panelContent = DialogPanel.transform.GetChild(0).gameObject;
        // Set active the section for the Passive NPC
        panelContent.SetActive(true);
        // Select a random string/comment from the list and set it to the text object of the panel
        //SetAllChildObjectsActive(panelContent);
        string NpcComment = sayPassiveRandomComment();
        panelContent.transform.GetChild(0).gameObject.SetActive(true);
        panelContent.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = NpcComment;
    }

    public void CloseNPCPanels()
    {
        try
        {
            for (int i = 0; i < DialogPanel.transform.childCount; i++)
            {
                print("break?");
                // Set al childs inactive
                if (DialogPanel.transform.GetChild(i).gameObject.activeSelf)
                {
                    SetAllChildObjectsInactive(DialogPanel.transform.GetChild(i).gameObject);
                    DialogPanel.transform.GetChild(i).gameObject.SetActive(false);
                }
            }
        }
        catch (NullReferenceException nre)
        {
            print("NullReferenceException on CLoseNPCPanels() method. " + nre.Message);
        }
        // Set the whole panel inactive
        DialogPanel.gameObject.SetActive(false);
    }

    public void SetAllChildObjectsActive(GameObject parentObject)
    {
        // Loop through all child objects
        foreach (Transform child in parentObject.transform)
        {
            // Set child object to active
            child.gameObject.SetActive(true);

            // Recursively call function to set child object's children to active
            SetAllChildObjectsActive(child.gameObject);
        }
    }
    public void SetAllChildObjectsInactive(GameObject parentObject)
    {
        // Loop through all child objects
        foreach (Transform child in parentObject.transform)
        {
            if (child.gameObject.activeSelf)
            {

                // Set child object to inactive
                child.gameObject.SetActive(false);

                // Recursively call function to set child object's children to inactive
                SetAllChildObjectsInactive(child.gameObject);
            }
        }
    }

    public string sayPassiveRandomComment()
    {
        // Select a random index from the list and return the value on that position
        int randomPositionCommentList = UnityEngine.Random.Range(0, SO_NPCsComments.PassiveSentences.Count
        );
        return SO_NPCsComments.PassiveSentences[randomPositionCommentList];
    }
}
