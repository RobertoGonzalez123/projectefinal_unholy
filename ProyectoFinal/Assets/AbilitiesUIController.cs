using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AbilitiesUIController : MonoBehaviour
{

    [SerializeField] GameObject PassiveSkillsList;
    [SerializeField] PlayerSkillsSO playerSkillsSO;

    void OnEnable()
    {
        UpdatePassiveSkills();
    }

    void UpdatePassiveSkills()
    {
        for (int i = 0; i < PassiveSkillsList.transform.childCount; i++)
        {
            GameObject child = PassiveSkillsList.transform.GetChild(i).gameObject;
            TextMeshProUGUI skillName = child.transform.GetChild(1).GetComponent<TextMeshProUGUI>();
            skillName.text = playerSkillsSO.playerSkills[i].skill.ToString();
            TextMeshProUGUI skillDescription = child.transform.GetChild(2).GetComponent<TextMeshProUGUI>();
            skillDescription.text = playerSkillsSO.playerSkills[i].skillDescription.ToString();
            TextMeshProUGUI skillLevel = child.transform.GetChild(3).GetComponent<TextMeshProUGUI>();
            skillLevel.text = playerSkillsSO.playerSkills[i].CurrentSkillLevel.ToString();

            Slider skillSlider = child.transform.GetChild(4).gameObject.GetComponent<Slider>();
            int MaxValue, CurrentValue;
            MaxValue = playerSkillsSO.playerSkills[i].MaxPointsNeededForNextLevel;
            CurrentValue = playerSkillsSO.playerSkills[i].CurrentSkillPoints;
            skillSlider.maxValue = MaxValue;
            skillSlider.value = CurrentValue;
        }
    }

}
