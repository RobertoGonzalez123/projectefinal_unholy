using UnityEngine;
using UnityEngine.UI;

public class TamingMinigame : MonoBehaviour
{
    [SerializeField] Transform topPivot;
    [SerializeField] Transform bottomPivot;
    [SerializeField] Transform horseMarker;

    float horsePosition;
    float horseDestination;

    float horseTimer;
    [SerializeField] float timerMultiplicator = 3f;

    float fishSpeed;
    [SerializeField] float smoothMotion = 1f;

    [SerializeField] Transform hookArea;
    float hookPosition;
    [SerializeField] float hookSize = 0.1f;
    [SerializeField] float hookPower = 5f;
    float hookProgress;
    float hookPullVelocity;
    [SerializeField] float hookPullPower = 0.01f;
    [SerializeField] float hookGravityPower = 0.005f;
    [SerializeField] float hookProgressDegradationPower = 0.1f;
    [SerializeField] Image hookImage;

    [SerializeField] Transform progressBarContainer;

    public bool pause = false;

    [SerializeField] float failTime = 15f;

    [SerializeField] GameObject HorseGameObject;
    [SerializeField] MountSO mount;

    private void Start()
    {
        pause = false;
    }



    private void Update()
    {
        if (pause) return;
        HorseMovement();
        Hook();
        ProgressCheck();
    }

    void ProgressCheck()
    {
        Vector3 ls = progressBarContainer.localScale;
        ls.y = hookProgress;
        progressBarContainer.localScale = ls;

        float min = hookPosition - hookSize / 2;
        float max = hookPosition + hookSize / 2;

        if (min < horsePosition && horsePosition < max)
        {
            hookProgress += hookPower * Time.deltaTime;
        }
        else
        {
            hookProgress -= hookProgressDegradationPower * Time.deltaTime;
            failTime -= Time.deltaTime;
            if (failTime < 0f)
            {
                Lose();
            }
        }

        if (hookProgress >= 1f)
        {
            Win();
        }

        hookProgress = Mathf.Clamp(hookProgress, 0f, 1f);
    }

    void Lose()
    {
        pause = true;
        // Deactivate the parent object, not this one
        this.transform.parent.gameObject.SetActive(false);
        GameManager.Instance.Player.GetComponent<AGUSMOVEMENT>().m_FSM.ChangeState<Idle>();
        GameManager.Instance.Player.GetComponent<AGUSMOVEMENT>().isPlayerAbleToMove = true;
    }

    void Win()
    {
        pause = true;
        // Deactivate the parent object, not this one
        InteractionManager.Instance.UnregisterInteractuable(HorseGameObject.GetComponent<Interactuable>());
        GetComponentInParent<Transform>().gameObject.SetActive(false);
        // Destroy the parent, since I want to destroy this object, and it has the same result destroying the parent
        Destroy(HorseGameObject);
        InventoryManager.Instance.AddItem(new ObjectMount(mount));
        GameManager.Instance.Player.GetComponent<AGUSMOVEMENT>().m_FSM.ChangeState<Idle>();
        GameManager.Instance.Player.GetComponent<AGUSMOVEMENT>().isPlayerAbleToMove = true;
    }

    void Hook()
    {
        // Check if the 'C' key is being held down
        if (Input.GetKey(KeyCode.C))
        {
            // Increase the hook's pulling velocity based on the hookPullPower and the time that has passed since the last frame
            hookPullVelocity += hookPullPower * Time.deltaTime;
        }

        // Decrease the hook's pulling velocity based on the hookGravityPower and the time that has passed since the last frame
        hookPullVelocity -= hookGravityPower * Time.deltaTime;

        // Update the hook's position by adding the current hookPullVelocity
        hookPosition += hookPullVelocity;

        // Check if the hook's position is at the lower boundary (hookSize / 2) and the hookPullVelocity is negative
        if (hookPosition - hookSize / 2 <= 0f && hookPullVelocity < 0f)
        {
            // If so, set the hookPullVelocity to 0 to stop further downward movement
            hookPullVelocity = 0f;
        }

        // Check if the hook's position is at the upper boundary (1f - hookSize / 2) and the hookPullVelocity is positive
        if (hookPosition + hookSize / 2 >= 1f && hookPullVelocity > 0f)
        {
            // If so, set the hookPullVelocity to 0 to stop further upward movement
            hookPullVelocity = 0f;
        }

        // Clamp the hook's position within the range of hookSize / 2 to (1 - hookSize / 2)
        hookPosition = Mathf.Clamp(hookPosition, hookSize / 2, 1 - hookSize / 2);

        // Update the position of the hookArea based on the interpolated hookPosition
        // The position is calculated by interpolating between the bottomPivot and topPivot positions
        hookArea.position = Vector3.Lerp(bottomPivot.position, topPivot.position, hookPosition);
    }


    void HorseMovement()
    {
        // Decrease the horseTimer each frame
        horseTimer -= Time.deltaTime;

        // Check if the horseTimer has reached 0
        if (horseTimer < 0f)
        {
            // Generate a random value between 0 and 1 and scale it by timerMultiplicator
            horseTimer = Random.value * timerMultiplicator;

            // Generate a random value between 0 and 1 as the horse's destination
            horseDestination = Random.value;
        }

        // Gradually interpolate the horse's current position towards the target position
        // The interpolation is done smoothly using the SmoothDamp function
        horsePosition = Mathf.SmoothDamp(horsePosition, horseDestination, ref fishSpeed, smoothMotion);

        // Update the position of the horseMarker based on the interpolated horsePosition
        // The position is calculated by interpolating between the bottomPivot and topPivot positions
        horseMarker.position = Vector3.Lerp(bottomPivot.position, topPivot.position, horsePosition);
    }

}
