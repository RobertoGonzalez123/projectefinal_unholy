using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using QFSW.QC;
using Cinemachine;
using System.IO;
using FifisSaver.OdinSerializer;
using System.Text;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public bool AutoSave = false;

    [Header("Pools")]
    public Pool DamageNumberTextPrefabPool;
    public Pool WalkingDustEffectPool;

    [Header("Don't Destroy On Load")]
    public GameObject QuestManagerGO;
    public GameObject SoundManagerGO;
    public GameObject WeatherManagerGO;
    public GameObject CanvasGO;
    public GameObject Player;
    public GameObject CamerasGroup;
    public Camera mainCamera;
    public GameObject CameraFreeLook;
    public GameObject FocusEnemyCamera;
    public GameObject EventSystem;
    public GameObject DungeonsPortalsController;

    [Header("Settings & Dependencies")]
    public SettingsScriptableObject settingsSO;

    [Header("Menu Stuff")]
    public MenuManager menu;
    public GameObject SettingsMenu;
    private bool MenuOpened = false;
    public bool MainMenuOpened = false;
    public bool pauseMenuOpened = false;

    [Tooltip("Contador utilizado para asegurar que cada objeto ObjectItem Creado tenga un id distinto")]
    [SerializeField] public static int contadorIdObjeto = 0;

    [Header("Loading Screen Stuff")]
    [SerializeField] Slider LoadingSlider;
    [SerializeField] TextMeshProUGUI LoadingPercentage;
    AsyncOperation loadingOperation;
    [SerializeField] GameObject LoadingScreen;
    [SerializeField] CanvasGroup canvasGroup;


    [Header("Trader Stuff")]
    [SerializeField] GameObject BuyPanel;
    [SerializeField] GameObject SellPanel;

    [Header("Tutorial Stuff")]
    public bool IsTutorialDungeonCompleted = false;
    public GameObject TutorialWall;
    //public UnityEvent TutorialDungeonCompletedEvent;

    [Header("Dungeon Stuff")]
    [Tooltip("Do not change this value in the inspector")]
    public DungeonDifficulty DungeonInteractedDifficulty;
    public enum DungeonDifficulty { EASY, MEDIUM, HARD }

    GameController gameController;

    public GameObject AllGUICloseable;

    public GameObject PauseMenu;

    [Header("HorseSpawn Explosion")]
    public GameObject ExplosionHorseSpawn;

    [Header("Objects Database")]
    public ObjectsDatabase objectsDatabase;

    public QuantumConsole quantumConsole;

    public bool CanConsoleBeOpened = true;
    public bool ConsoleOpened;

    [Header("Clips Musica")]
    public AudioClip mapSceneAudio;
    public AudioClip dungeonSceneAudio;

    public bool menuOpened
    {
        get => MenuOpened;
        set
        {
            MenuOpened = value;
            AGUSMOVEMENT.Instance.m_FSM.ChangeState<Idle>();
            AGUSMOVEMENT.Instance.isPlayerAbleToMove = !value;
            AGUSMOVEMENT.Instance.shouldPlayerObjectRotate = !value;
            ShowOrHideCursor(value);
            if (value)
            {
                CameraFreeLook.GetComponent<CinemachineFreeLook>().m_YAxis.m_MaxSpeed = 0;
                CameraFreeLook.GetComponent<CinemachineFreeLook>().m_XAxis.m_MaxSpeed = 0;
            }
            else
            {
                CameraFreeLook.GetComponent<CinemachineFreeLook>().m_YAxis.m_MaxSpeed = 2;
                CameraFreeLook.GetComponent<CinemachineFreeLook>().m_XAxis.m_MaxSpeed = 300;
            }
        }

    }

    private float IgnoreMouseInput()
    {
        return 0f;
    }


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        SceneManager.sceneLoaded += OnSceneLoaded;
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(QuestManagerGO);
        DontDestroyOnLoad(SoundManagerGO);
        DontDestroyOnLoad(WeatherManagerGO);
        DontDestroyOnLoad(CanvasGO);
        DontDestroyOnLoad(Player);
        DontDestroyOnLoad(CamerasGroup);
        DontDestroyOnLoad(EventSystem);
        DontDestroyOnLoad(quantumConsole.gameObject);
    }

    public void ShowOrHideCursor(bool value)
    {
        if (value)
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        Cursor.visible = value;
    }

    public void ChangeTutDungeonCompleted()
    {
        IsTutorialDungeonCompleted = true;
        TutorialWall.SetActive(false);
        if (QuestManager.Instance.MisionActiva is MisionDungeonSO)
            QuestManager.Instance.MisionActiva.CompletarMision();
    }

    void consoleOpened()
    {
        ConsoleOpened = true;
    }
    void consoleClosed()
    {
        ConsoleOpened = false;
    }
    void CallOnActiveShowOrHideCursor()
    {
        ShowOrHideCursor(true);
    }
    void CallOnDeactivateShowOrHideCursor()
    {
        ShowOrHideCursor(false);
    }
    private void Start()
    {
        quantumConsole.OnActivate += CallOnActiveShowOrHideCursor;
        quantumConsole.OnActivate += consoleOpened;

        quantumConsole.OnDeactivate += CallOnDeactivateShowOrHideCursor;
        quantumConsole.OnActivate += consoleClosed;

        if (AutoSave)
        {
            StartCoroutine("KeepSaving");
        }

        //TutorialDungeonCompletedEvent.AddListener(ChangeTutDungeonCompleted);

        Player.GetComponent<AGUSMOVEMENT>().isPlayerAbleToMove = true;

        menuOpened = false;
        gameController = GetComponent<GameController>();

        if (QuestManager.Instance.HasActiveMission())
        {
            QuestManager.Instance.NoQuestAcceptedText.SetActive(false);
            QuestManager.Instance.QuestsUIGroup.SetActive(true);
        }
        else
        {
            QuestManager.Instance.NoQuestAcceptedText.SetActive(true);
            QuestManager.Instance.QuestsUIGroup.SetActive(false);
        }
    }

    IEnumerator KeepSaving()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);
            gameController.SaveGame();
        }
    }

    [Header("QUESTS SO")]
    public QuestSO questDungeonTutorial;
    public QuestSO questRecolectaItems;
    public QuestSO questPosition;
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        // Verifica si la escena cargada es la escena "MapCreation"
        if (scene.name == "MapCreation")
        {
            SoundManagerGO.GetComponent<AudioSource>().clip = mapSceneAudio;
            SoundManagerGO.GetComponent<AudioSource>().Play();
            try
            {
                string newBase64 = File.ReadAllText(Application.persistentDataPath + "lastGameState.json");
                byte[] serializedData = System.Convert.FromBase64String(newBase64);

                LastGameState lastGameState = SerializationUtility.DeserializeValue<LastGameState>(serializedData, DataFormat.JSON);
                if (!lastGameState.isNewGame)
                {
                    questDungeonTutorial.questState = lastGameState.questdungeonTutorial;
                    questRecolectaItems.questState = lastGameState.questrecolectaItems;
                    questPosition.questState = lastGameState.questtutorialPosition;
                    if (lastGameState.wasInDungeon)
                    {
                        GetComponent<GameController>().LoadItemsToRestore();
                        AGUSMOVEMENT.Instance.gameObject.transform.position = new Vector3(-231.22f, 1.36f, -235.02f);
                        Debug.Log("LUGAR");
                        lastGameState.wasInDungeon = false;
                        byte[] serializedData2 = SerializationUtility.SerializeValue<LastGameState>(lastGameState, DataFormat.JSON);
                        string base64 = System.Convert.ToBase64String(serializedData2);
                        File.WriteAllText(Application.persistentDataPath + "lastGameState.json", base64, Encoding.UTF8);
                    }
                    else
                    {
                        GetComponent<GameController>().LoadGame();
                    }
                }
                else
                {
                    // if file found but a new game, set the new game bool to false
                    lastGameState.isNewGame = false;
                    questDungeonTutorial.questState = QuestSO.QuestState.NOT_TAKEN;
                    questRecolectaItems.questState = QuestSO.QuestState.NOT_TAKEN;
                    questPosition.questState = QuestSO.QuestState.NOT_TAKEN;
                    byte[] serializedData2 = SerializationUtility.SerializeValue<LastGameState>(lastGameState, DataFormat.JSON);
                    string base64 = System.Convert.ToBase64String(serializedData2);
                    File.WriteAllText(Application.persistentDataPath + "lastGameState.json", base64, Encoding.UTF8);
                }

            }
            catch (System.Exception e)
            {
                Debug.Log(e.Message);
            }


        }

        else if (scene.name == "Mazmorras")
        {
            SoundManagerGO.GetComponent<AudioSource>().clip = dungeonSceneAudio;
            SoundManagerGO.GetComponent<AudioSource>().Play();
            GetComponent<GameController>().LoadItemsToRestore();
        }

    }
    public void SimpleChangeScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void ChangeToScene(string sceneName)
    {
        if (sceneName == "Mazmorras")
            GetComponent<GameController>().SaveItemsToRestore();
        SceneManager.LoadScene(sceneName);
        //StartCoroutineFadeLoadingScreen(1, 0, 1);
    }


    public void ChangeToScene(string sceneName, Vector3 SpawnPoint)
    {
        SceneManager.LoadScene(sceneName);
        AGUSMOVEMENT.Instance.gameObject.transform.position = SpawnPoint;
        //StartCoroutineFadeLoadingScreen(1, 0, 1);
    }

    public void StartCoroutineFadeLoadingScreen(float duration, int startAlpha, int endAlpha)
    {
        StartCoroutine(FadeLoadingScreen(duration, startAlpha, endAlpha));

        loadingOperation.allowSceneActivation = true;
        do
        {
            LoadingSlider.value = loadingOperation.progress * 110;
            LoadingPercentage.text = loadingOperation.progress.ToString("0.#");
        } while (loadingOperation.progress < 0.9f);
    }

    IEnumerator FadeLoadingScreen(float duration, int startAlpha, int endAlpha)
    {
        float time = 0;
        while (time < duration)
        {
            canvasGroup.alpha = Mathf.Lerp(startAlpha, endAlpha, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        canvasGroup.alpha = endAlpha;
    }

    public ObjectItem SearchItemInGame(string itemName)
    {
        foreach (ItemSO item in objectsDatabase.objectes)
        {
            print("Checking item in game by name " + item.ItemName + ". The name to compare with is " + itemName);
            if (item.ItemName == itemName)
            {
                if (item is ArmorSO)
                {
                    ObjectArmor newItem = new ObjectArmor((ArmorSO)item);
                    return newItem;
                }
                else if (item is ConsumableSO)
                {
                    ObjectConsumable newItem = new ObjectConsumable((ConsumableSO)item);
                    return newItem;
                }
                else if (item is SimpleItemSO)
                {
                    ObjectSimpleItem newItem = new ObjectSimpleItem((SimpleItemSO)item);
                    return newItem;
                }
                else if (item is WeaponSO)
                {
                    ObjectWeapon newItem = new ObjectWeapon((WeaponSO)item);
                    return newItem;
                }
            }
        }
        return null;
    }
    public ObjectItem SearchItemInGame(int itemID)
    {
        foreach (ItemSO item in objectsDatabase.objectes)
        {
            if (item.DictionaryID == itemID)
            {
                if (item is ArmorSO)
                {
                    ObjectArmor newItem = new ObjectArmor((ArmorSO)item);
                    return newItem;
                }
                else if (item is ConsumableSO)
                {
                    ObjectConsumable newItem = new ObjectConsumable((ConsumableSO)item);
                    return newItem;
                }
                else if (item is SimpleItemSO)
                {
                    ObjectSimpleItem newItem = new ObjectSimpleItem((SimpleItemSO)item);
                    return newItem;
                }
                else if (item is WeaponSO)
                {
                    ObjectWeapon newItem = new ObjectWeapon((WeaponSO)item);
                    return newItem;
                }
            }
        }
        return null;
    }

    void Update()
    {
        /*
        if (loadingOperation != null)
        {
            print("PROGRESS LOADING OPERATION " + loadingOperation.progress);
            LoadingSlider.value = Mathf.Clamp01(loadingOperation.progress / 0.9f);
            float progressValue = Mathf.Clamp01(loadingOperation.progress / 0.9f);
            LoadingPercentage.text = Mathf.Round(progressValue * 100) + "%";
            if (loadingOperation.progress > 90)
            {
                print("loading ended");
                FadeLoadingScreen(1, 1, 0);
            }
        }
        */

        if (Input.GetKeyDown(KeyCode.O))
        {
            gameController.SaveGame();
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            gameController.LoadGame();
        }
        //print("UPDATE IS PLAYER ABLE TO MOVE UPDATE GAME MANAGER: " + Player.GetComponent<AGUSMOVEMENT>().isPlayerAbleToMove);
    }

    // Get a new unique item ID
    public int GetNewItemID()
    {
        contadorIdObjeto++;
        return contadorIdObjeto;
    }

    public void OnMenuOpen(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            ShowOrHideCursor(true);
            menu.gameObject.SetActive(true);
            menuOpened = true;
        }
    }

    public void OnMenuClose(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            menu.GetComponent<MenuManager>().CloseMenu();
            ShowOrHideCursor(false);
            menu.gameObject.SetActive(false);
            menuOpened = false;
        }
    }

    public GameObject currentInteractuableNPC = null;
    public void OpenTraderBuyPanel()
    {
        InteractionManager.Instance.currentInteractuable.transform.GetComponent<NPCBuySell>().ToggleBuyPanel(BuyPanel);
    }
    public void OpenTraderSellPanel()
    {
        InteractionManager.Instance.currentInteractuable.transform.GetComponent<NPCBuySell>().ToggleSellPanel(SellPanel);
    }

    // Do all stuff that involves close panels in the interface
    public void CloseConversation(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            CallCloseConversation();
        }
    }

    public void CallCloseConversation()
    {
        if (!menuOpened && !MainMenuOpened)
        {
            OpenPauseMenu();
        }
        else if (pauseMenuOpened)
        {
            ResumeGame();
        }
        if (!MainMenuOpened && !pauseMenuOpened)
        {
            foreach (Transform child in AllGUICloseable.GetComponentsInChildren<Transform>())
            {
                if (child.gameObject.activeInHierarchy)
                {
                    if (child.gameObject != AllGUICloseable.gameObject)
                    {
                        if (child.name == "DialogPanel")
                        {
                            child.GetChild(0).gameObject.SetActive(false);
                            child.GetChild(1).gameObject.SetActive(false);
                            child.GetChild(2).gameObject.SetActive(false);
                        }
                        if (child.name == "Menu")
                        {
                            child.GetChild(0).gameObject.SetActive(false);
                            child.GetChild(1).gameObject.SetActive(false);
                            child.GetChild(2).gameObject.SetActive(false);
                            child.GetChild(3).gameObject.SetActive(false);
                            child.GetChild(4).gameObject.SetActive(false);
                            continue; // Because the 'Menu' object must be always enabled, but not the childs
                        }
                        child.gameObject.SetActive(false);
                    }
                }
            }
            menuOpened = false;
        }
        else if (!pauseMenuOpened && !MainMenuOpened)
        {
            menu.CloseMenu();
        }
    }

    // Activa el menu
    void OpenPauseMenu()
    {
        pauseMenuOpened = true;
        menuOpened = true;
        PauseMenu.SetActive(true);
    }

    // Deactivete the pause menu
    public void ResumeGame()
    {
        pauseMenuOpened = false;
        menuOpened = false;
        PauseMenu.SetActive(false);
        ShowOrHideCursor(false);
    }

    public void OpenSettingsMenu()
    {
        menuOpened = true;
        SettingsMenu.SetActive(true);
    }
}