using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.UI;

public class ButtonSelect : MonoBehaviour
{
    EventSystem eventSystem;
    InputSystemUIInputModule inputModule;
    // Start is called before the first frame update
    void Start()
    {
        eventSystem = GetComponent<EventSystem>();
        inputModule = GetComponent<InputSystemUIInputModule>();
        eventSystem.SetSelectedGameObject(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
