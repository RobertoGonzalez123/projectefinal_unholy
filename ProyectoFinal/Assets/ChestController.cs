using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChestController : Interactuable
{
    public List<ObjectItem> itemsAvailable;
    [SerializeField] GameObject SackSection;
    public GameObject Slot;
    public GameObject noInventorySpace;
    GameObject LootList;
    static Button GetAllItemsBTN;
    [SerializeField] GameEvent eventOpenSack;
    public List<ItemSO> lootTable;

    private void Awake()
    {
        SackSection = GameObject.Find("Canvas/AllGUICloseable/SackSection");
        LootList = SackSection.transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).gameObject;
        GetAllItemsBTN = SackSection.transform.GetChild(0).transform.GetChild(1).GetComponent<Button>();
        LoadItemsAvailables();
    }

    void LoadItemsAvailables()
    {
        foreach (ItemSO itemModel in lootTable)
        {
            if (itemModel is ArmorSO)
            {
                itemsAvailable.Add(new ObjectArmor((ArmorSO)itemModel));
            }
            else if (itemModel is WeaponSO)
            {
                itemsAvailable.Add(new ObjectWeapon((WeaponSO)itemModel));
            }
            else if (itemModel is ConsumableSO)
            {
                itemsAvailable.Add(new ObjectConsumable((ConsumableSO)itemModel));
            }
            else if (itemModel is SimpleItemSO)
            {
                itemsAvailable.Add(new ObjectSimpleItem((SimpleItemSO)itemModel));
            }
        }
    }

    public void UnloadAllItems()
    {
        for (int i = 0; i < LootList.transform.childCount; i++)
        {
            Destroy(LootList.transform.GetChild(i).gameObject);
        }
    }

    public override bool Interact()
    {
        GetAllItemsBTN.onClick.AddListener(GetAllItems);
        // Habilitar el cursor
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        // Abrir el menu de loot listando todos los items. Un boton para cerrar la ventana, un boton para recoger todo, si se clica un item se coge ese item
        SackSection.SetActive(true);
        UnloadAllItems();
        LoadAllItems();
        return false;
    }

    void LoadAllItems()
    {
        foreach (ObjectItem item in itemsAvailable)
        {
            GameObject newSlot = Instantiate(Slot, LootList.transform);
            newSlot.GetComponent<ChestSlot>().SetItemToSlot(item, this);
        }
    }

    public void UnloadItem(ObjectItem item)
    {
        try
        {
            for (int i = 0; i < LootList.transform.childCount; i++)
            {
                if (LootList.transform.GetChild(i).gameObject.activeInHierarchy && LootList.transform.GetChild(i).GetComponent<InventorySlot>().item == item)
                {
                    Destroy(LootList.transform.GetChild(i).gameObject);
                    break;
                }
            }
        }
        catch (NullReferenceException e)
        {
            Debug.Log(e.ToString());
        }
    }
    public void UnloadItemByID(ObjectItem item)
    {
        int index = 0;
        foreach (ObjectItem item2 in itemsAvailable)
        {
            if (item2.itemID == item.itemID) break;
            index++;
        }
        itemsAvailable.RemoveAt(index);

        // Si se ha looteado todo y la lista est� vac�a, se elimina el objecto del saco de la escena
        if (itemsAvailable.Count == 0) WhatToDoWhenSackIsEmpty();
    }
    private void WhatToDoWhenSackIsEmpty()
    {
        CloseSackPanel();
        InteractionManager.Instance.UnregisterInteractuable(this);
        Invoke("DestroyGO", 0.1f);
    }

    public void CloseSackPanel()
    {
        //LootList = SackSection.transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).gameObject;

        SackSection.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        GetAllItemsBTN.onClick.RemoveListener(GetAllItems);
    }

    public void GetAllItems()
    {
        List<ObjectItem> tmpItemsAvailable = new List<ObjectItem>(itemsAvailable);
        foreach (ObjectItem currentObj in itemsAvailable)
        {
            if (InventoryManager.Instance.AddItem(currentObj))
            {
                UnloadItem(currentObj);
                tmpItemsAvailable.Remove(currentObj);
            }
            else
            {
                break;
            }
        }
        itemsAvailable = tmpItemsAvailable;



        // Si se ha looteado todo y la lista est� vac�a, se elimina el objecto del saco de la escena
        if (itemsAvailable.Count == 0) WhatToDoWhenSackIsEmpty();
    }

    void DestroyGO()
    {
        Destroy(this.gameObject);
    }
}
