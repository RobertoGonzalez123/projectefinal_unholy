using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellPanelManager : MonoBehaviour
{
    int maxItemsToBuy = 5;
    List<ItemSO> itemsInCart;

    public delegate void OnCartChanged();
    public OnCartChanged onCartChanged;

    public delegate void OnCartFull();
    public OnCartFull onCartFull;

    public static SellPanelManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void AddItemToCart(ItemSO item)
    {
        // The shop has a cart with a maximum storage of 'maxItemToBuy', defaulted with 5. If you want to add an item and the max has not been reached yet, add it and return true, else return false
        if (itemsInCart.Count < maxItemsToBuy)
        {
            itemsInCart.Add(item);
            onCartChanged?.Invoke();
        }
        else
        {
            onCartFull?.Invoke();
        }
    }

    public void DeleteItemFromCart(ItemSO item)
    {
        onCartChanged?.Invoke();
        itemsInCart.Remove(item);
    }

}
