using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallSpikesTrapController : MonoBehaviour
{
    [SerializeField] string AnimationToPlay;
    void Start()
    {
        GetComponent<Animator>().Play(AnimationToPlay);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.transform.tag == "Player")
        {
            collision.transform.GetComponent<AGUSMOVEMENT>().ToDie();
        }
    }

}
