using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

[CreateAssetMenu]
[Serializable]
public class SkillsWTF : ScriptableObject
{

    public SKILL_TYPE skill;
    public string skillDescription;
    public int CurrentSkillLevel = 1; // Default 1
    public int MaxPointsNeededForNextLevel; // Default 2
    public int CurrentSkillPoints; // Default 0
    public int XPGivenPerAction = 1; // Default 1
    public float StaminaConsumed;

}

public enum SKILL_TYPE
{
    HP, JUMP, CRIT_RATE, DEXTERITY, DODGE, RUN, STRENGTH
}
