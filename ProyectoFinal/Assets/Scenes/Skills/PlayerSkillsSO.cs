using System;
using UnityEngine;

[CreateAssetMenu]
public class PlayerSkillsSO : ScriptableObject
{

    public SkillsWTF[] playerSkills;

    public SkillsWTF GetSkill(string name)
    {
        foreach (SkillsWTF obj in playerSkills)
        {
            bool contains = obj.name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0;
            if (contains)
            {
                return obj;
            }
        }

        return null; // Return null if no object with the specified name is found
    }

    public float GetSkillStamina(string name)
    {
        foreach (SkillsWTF obj in playerSkills)
        {
            bool contains = obj.name.IndexOf(name, StringComparison.OrdinalIgnoreCase) >= 0;
            if (contains)
            {
                return obj.StaminaConsumed;
            }
        }

        return 0.0f; // Return null if no object with the specified name is found
    }
}
