using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class playerSkillScript : MonoBehaviour
{
    public PlayerStats playerStats;
    public PlayerSkillsSO playerSkills;
    public GameObject levelUpParticles;

    void levelUp(SkillsWTF skill)
    {
        skill.CurrentSkillLevel += 1;
        skill.MaxPointsNeededForNextLevel += Mathf.RoundToInt(Mathf.Pow(skill.CurrentSkillLevel, 1.5f) * 0.5f + 0.5f);
        skill.CurrentSkillPoints = 0;

        switch (skill.skill)
        {
            case SKILL_TYPE.HP:
                PlayerMaxStatsRuntime.Instance.LevelUpHP();
                playerStats.MAX_HP *= 1.05f;
                PlayLevelUpAnimation();
                break;
            case SKILL_TYPE.JUMP:
                PlayerMaxStatsRuntime.Instance.LevelUpJump();
                PlayLevelUpAnimation();
                break;
            case SKILL_TYPE.RUN:
                PlayerMaxStatsRuntime.Instance.LevelUpRun();
                PlayLevelUpAnimation();
                break;
            case SKILL_TYPE.DODGE:
                PlayerMaxStatsRuntime.Instance.LevelUpDash();
                PlayLevelUpAnimation();
                break;
            case SKILL_TYPE.STRENGTH:
                PlayerMaxStatsRuntime.Instance.LevelUpStrength();
                PlayLevelUpAnimation();
                break;
            case SKILL_TYPE.CRIT_RATE:
                PlayerMaxStatsRuntime.Instance.LevelUpCrit();
                PlayLevelUpAnimation();
                break;
        }
    }
    void PlayLevelUpAnimation()
    {
        Instantiate(levelUpParticles, this.transform.position, Quaternion.identity);
    }

    public void addExperience(SkillsWTF skill)
    {
        skill.CurrentSkillPoints += skill.XPGivenPerAction;
        if (skill.CurrentSkillPoints >= skill.MaxPointsNeededForNextLevel)
        {
            // Mirar si el siguiente nivel es mayor al nivel maximo permitido
            // En ese caso subir nivel
            if (skill.CurrentSkillLevel + 1 <= 10)
            {
                levelUp(skill);
            }
        }
    }
}
