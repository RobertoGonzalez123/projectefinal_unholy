using System.Collections.Generic;
using UnityEngine;

public abstract class QuestSO : ScriptableObject
{

    public string QuestName;
    public string QuestDescription;
    public string StartingDialogue;
    public string OnGoingDialogue;
    public string CompletedDialogue;
    public string QuestAlreadyAccepted;
    public string RefuseDialogue;

    public List<QuestSO> Dependencies;

    [Header("REWARDS")]
    public QuestReward[] rewards;

    // NOT_TAKEN -> Quest has not been taken by the player
    // DOING_NOT_FINISHED -> Quest has been taken by the player and is still doing it
    // DOING_FINISHED -> Quest has been taken but the player has all the requirements met
    // FINISHED -> Quest has been delivered/finished and the quest is "Completed"
    public enum QuestState { NOT_TAKEN, DOING_NOT_FINISHED, DOING_FINISHED, FINISHED }
    public QuestState questState;

    public void ChangeQuestState(QuestState newQuestState)
    {
        questState = newQuestState;
    }

    public abstract void CompletarMision();
    public QuestState GetState() { return questState; }
}

[System.Serializable]
public class QuestReward
{
    public string rewardName;
    public int amount;
}

public enum QUEST_TYPE
{
    ITEM_COLLECTION, TEMPLE_COMPLETION, FIND_LOCATION
}
