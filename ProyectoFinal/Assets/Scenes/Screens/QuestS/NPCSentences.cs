using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class NPCSentences : ScriptableObject
{
    public List<string> PassiveSentences;
    public List<string> QuestSentences;
    public List<string> TraderSentences;

    public virtual string SayRandomPassiveSentece()
    {
        return PassiveSentences[UnityEngine.Random.Range(0, PassiveSentences.Count)];
    }
    public virtual string SayRandomQuestSentece()
    {
        return QuestSentences[UnityEngine.Random.Range(0, QuestSentences.Count)];
    }
    public virtual string SayRandomTraderSentece()
    {
        return TraderSentences[UnityEngine.Random.Range(0, TraderSentences.Count)];
    }
}
