using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Nueva Mision de Dungeon", menuName = "Mision/Dungeon")]
public class MisionDungeonSO : QuestSO
{
    public bool isCompleted = false;
    public DungeonDifficulty QuestDifficulty;

    public enum DungeonDifficulty { EASY, MEDIUM, HARD }

    public override void CompletarMision()
    {
        this.ChangeQuestState(QuestState.DOING_FINISHED);
        isCompleted = true;
    }
}
