using UnityEngine;

[CreateAssetMenu(fileName = "Nueva Mision de Recolecta", menuName = "Mision/Recolecta")]
public class MisionRecolectaSO : QuestSO
{
    public int cantidadObjetivo;
    public ItemSO itemObjetivo;
    public int progreso;

    public void AddProgress()
    {
        progreso++;
        if (progreso < cantidadObjetivo)
            this.ChangeQuestState(QuestState.DOING_NOT_FINISHED);
        else if (progreso >= cantidadObjetivo)
            this.ChangeQuestState(QuestState.DOING_FINISHED);
    }

    public override void CompletarMision()
    {
        // Actualizar el progreso de la mision
        progreso++;
        if (progreso == cantidadObjetivo)
        {
            QuestManager.Instance.MisionActiva.ChangeQuestState(QuestState.DOING_FINISHED);
        }
    }
}
