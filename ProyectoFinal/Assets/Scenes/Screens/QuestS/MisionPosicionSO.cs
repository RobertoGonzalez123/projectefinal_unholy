using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Nueva Mision de Posicion", menuName = "Mision/Posicion")]
public class MisionPosicionSO : QuestSO
{
    public GameObject LugarALlegar;
    public float distanciaMargen;
    public bool isCompleted = false;

    public override void CompletarMision()
    {
        this.ChangeQuestState(QuestState.DOING_FINISHED);
        isCompleted = true;
    }

}
