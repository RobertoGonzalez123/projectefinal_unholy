using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu]
public class SettingsScriptableObject : ScriptableObject
{
    // GRAFICOS
    [Header("GRAPHICS")]
    public CALIDAD Quality;
    public RESOLUCION Resolution;
    public bool FullScreen;
    public bool Particles;
    [Range(0, 100)]
    public int Brightness;
    public bool vSync;
    public FPSLimit fpsLimit;
    
    [Space(10)]

    // SONIDO
    [Header("SOUND")]
    [Range(0.0001f, 1)]
    public float GeneralVolume;
    [Range(0.0001f, 1)]
    public int MusicVolume;
}

public enum RESOLUCION
{
    RES4K, RES2K, RES1080, RES720
}

public enum FPSLimit
{
    FPS24, FPS30, FPS50, FPS59, FPS60, FPS90, FPS120, FPS140, FPS_NO_LIMIT
}

public enum CALIDAD
{
    MUY_ALTA, ALTA, MEDIA, BAJA, MUY_BAJA
}
