using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class KeyBinding
{
    public string KeyCodeName;
    public KeyCode keyCode;
}
