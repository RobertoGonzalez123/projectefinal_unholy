using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TamingHorse : Interactuable
{
    [SerializeField] GameObject TamingMiniGameParent;

    public override bool Interact()
    {
        print("interacting with horse");
        GameManager.Instance.Player.GetComponent<AGUSMOVEMENT>().m_FSM.ChangeState<Idle>();
        GameManager.Instance.Player.GetComponent<AGUSMOVEMENT>().isPlayerAbleToMove = false;

        TamingMiniGameParent.SetActive(true);

        return false;
    }
}
