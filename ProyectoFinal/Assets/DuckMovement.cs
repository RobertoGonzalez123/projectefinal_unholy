using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DuckMovement : MonoBehaviour
{
    public float movementRange = 0.5f; // The maximum distance the duck can move up and down
    public float movementSpeed = 1f;  // The speed at which the duck moves
    public float rotationSpeed = 20f; // The speed at which the duck rotates
    private float originalY;          // The original Y position of the duck


    // Start is called before the first frame update
    void Start()
    {
        originalY = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        // Calculate the new Y position based on a sine wave
        float newY = originalY + Mathf.Sin(Time.time * movementSpeed) * movementRange;

        // Calculate the new Z rotation based on time
        float newRotation = Time.time * rotationSpeed;

        // Update the position and rotation of the duck
        transform.position = new Vector3(transform.position.x, newY, transform.position.z);
        transform.rotation = Quaternion.Euler(-90, newRotation, -115);
    }
}
