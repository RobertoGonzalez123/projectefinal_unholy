using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakingFloorController : MonoBehaviour
{
    Vector3 initialPosition;
    // Start is called before the first frame update
    void Start()
    {
        initialPosition = this.transform.position;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Player")
        {
            StartCoroutine(PlayShakingAnimation());
        }
    }

    IEnumerator PlayShakingAnimation()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Animator>().Play("ShakingFloor");
        yield return new WaitForSeconds(5);
        this.transform.position = initialPosition;
    }
}
