using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonZone : OnlyDamaging
{
    [SerializeField] float time = 10;

    private void Start()
    {
        StartCoroutine(DestroyObject());
    }

    public void StartPoisonZone(float dmg, float time)
    {
        doDamageStats.dmg = dmg;
        this.time = time;
        doDamageStats.estados.Add(Estado.GetEstado(Estado.Estados.Burning, dmg));
    }



    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
