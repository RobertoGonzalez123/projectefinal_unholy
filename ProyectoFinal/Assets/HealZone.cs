using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealZone : MonoBehaviour
{

    [SerializeField] AGUSMOVEMENT m_PlayerBehaviour;
    [SerializeField] float tickRate;

    float time = 5f;
    float minDistance = 4.5f;
    float dmg = 0;

    public void EnableHealZone(float dmg, float time, AGUSMOVEMENT m_PlayerBehaviour, float minDistance)
    {
        this.dmg = dmg;
        this.time = time;
        this.m_PlayerBehaviour = m_PlayerBehaviour;
        this.minDistance = minDistance;

        StartCoroutine(HealingCoroutine());

    }
    private void Update()
    {
        time -= Time.deltaTime;
    }



    IEnumerator HealingCoroutine()
    {
        while (time > 0)
        {
            yield return new WaitForSeconds(tickRate);

            float distace = Vector3.Distance(m_PlayerBehaviour.transform.position, transform.position);
            if (distace < minDistance)
            {
                m_PlayerBehaviour.CurrentHP += dmg;
            }
        }
        Destroy(gameObject);

    }
}
