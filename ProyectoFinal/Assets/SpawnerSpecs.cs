using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerSpecs : MonoBehaviour
{
    [Tooltip("Can the spawn, spawn enemies?")]
    public bool isAbleToSpawn = true;
    [Tooltip("Is the spawner in the area of vision of the player? If so, the spawn does not spawn anything")]
    public bool isSeenByPlayer = false;

    [Tooltip("How many enemies to spawn")]
    public int enemiesToSpawn;
    [Tooltip("Which enemies to spawn")]
    public GameObject[] enemiesAbleToSpawn;
    public int enemiesSpawned;

    [Header("Patroling spots")]
    public List<Transform> patrolingSpots;

    [SerializeField] GameObject testPlaceWhereChecking;

    void Start()
    {
        StartCoroutine(SpawnEnemiesCoroutine());
    }

    IEnumerator SpawnEnemiesCoroutine()
    {
        while (true)
        {
            if(enemiesSpawned < enemiesToSpawn)
            {
                SpawnEnemies();
            }
            int randomSeconds = Random.Range(5, 11);
            yield return new WaitForSeconds(randomSeconds);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.name == "SpawneableCollider")
        {
            isAbleToSpawn = true;
        }
        else if (other.transform.name == "SafeZoneCollider")
        {
            isSeenByPlayer = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform.name == "SpawneableCollider")
        {
            isAbleToSpawn = false;
        }
        else if (other.transform.name == "SafeZoneCollider")
        {
            isSeenByPlayer = false;
        }
    }

    public void SpawnEnemies()
    {
        // If the spawner is able to spawn and is not seen by the player, proceed to spawn
        if (isAbleToSpawn && !isSeenByPlayer)
        {
            // Iterate 'enemiesToSpawn' times and Instantiate enemies prefabs witha  random number in case there is more than one prefab in the list
            for (int i = 0; i < enemiesToSpawn - enemiesSpawned; i++)
                InstantiateEnemy();
        }
    }

    void InstantiateEnemy()
    {
        // Get the enemy object from the list
        int prefabPosition = Random.Range(0, enemiesAbleToSpawn.Length);
        // Instantiate the enemy at a random position
        GameObject newEnemy = Instantiate(enemiesAbleToSpawn[prefabPosition], this.transform.position, Quaternion.identity, this.transform);

        Vector3 newPosition = GetRandomPositionCloseToSpawner() + this.transform.position;
        // Adjust the new position based on the irregular ground
        newPosition.y = GetGroundHeight(newPosition);

        newEnemy.transform.position = newPosition;

        int maxAttempts = 10; // Limit the number of attempts to avoid infinite loop
        int attempts = 0;
        while (IsOverlapping(newEnemy.GetComponent<Collider>()) && attempts < maxAttempts)
        {
            if (testPlaceWhereChecking != null) Instantiate(testPlaceWhereChecking, newPosition, Quaternion.identity);
            newPosition = GetRandomPositionCloseToSpawner() + this.transform.position;
            // Adjust the new position based on the irregular ground
            newPosition.y = GetGroundHeight(newPosition);

            newEnemy.transform.position = newPosition;

            attempts++;
        }

        newEnemy.GetComponent<EnemyBehaviour>().waypointList = this.patrolingSpots;

        enemiesSpawned++;
    }

    public Vector3 GetRandomPositionCloseToSpawner()
    {
        // Set a random position close to the spawner's position
        float randomX = Random.Range(-6, 7);
        float randomZ = Random.Range(-6, 7);
        Vector3 randomPosition = new Vector3(randomX, 0, randomZ);
        return randomPosition;
    }

    public float GetGroundHeight(Vector3 position)
    {
        // Raycast downward to find the ground height at the given position
        RaycastHit hit;
        //layer 6 = ground
        if (Physics.Raycast(position + Vector3.up * 10f, Vector3.down, out hit, Mathf.Infinity, 6))
        {
            return hit.point.y + 1f;
        }

        // If no ground is found, return a default value (0)
        return 1f;
    }

    public bool IsOverlapping(Collider colliderToCheck)
    {
        //LAYER 10 = ENEMY
        Collider[] colliders = Physics.OverlapBox(colliderToCheck.bounds.center, colliderToCheck.bounds.extents + new Vector3(1, 0, 1), Quaternion.identity, 10);

        foreach (Collider collider in colliders)
        {
            if (collider != colliderToCheck && collider.gameObject != gameObject)
            {
                // Collision detected, return true because the enemy is colliding with something
                return true;
            }

        }

        // No collision detected, return false
        return false;
    }

}
