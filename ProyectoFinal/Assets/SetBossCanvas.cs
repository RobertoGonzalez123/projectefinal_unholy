using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetBossCanvas : MonoBehaviour
{
    public GameObject HealthBar;
    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == "Player")
        {
            SetCanvasBoss();
        }
    }

    public void SetCanvasBoss()
    {
        print("intentando setear el canvas del boss");
        GameObject canvas = null;
        Canvas[] arrayCanvas = FindObjectsOfType(typeof(Canvas)) as Canvas[];
        foreach (Canvas canvasTmp in arrayCanvas)
        {
            if (canvasTmp.gameObject.name == "Canvas")
            {
                canvas = canvasTmp.gameObject;
                break;
            }
        }
        HealthBar.transform.SetParent(canvas.transform);
        HealthBar.GetComponent<RectTransform>().anchoredPosition.Set(Screen.width / 2, Screen.height - 15);
        HealthBar.transform.position = Vector3.zero;
        HealthBar.transform.localScale = new Vector3(3, 1.5f, 0);
    }
}
