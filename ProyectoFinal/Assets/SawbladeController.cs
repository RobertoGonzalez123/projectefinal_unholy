using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawbladeController : MonoBehaviour
{
    public int rotationVelocity = 5;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RotateSawblade());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<AGUSMOVEMENT>().ReceiveDamage(20);
        }
    }

    IEnumerator RotateSawblade()
    {
        while (true)
        {
            transform.Rotate(0, 0, transform.rotation.z + rotationVelocity);
            yield return null;
        }
    }
}
