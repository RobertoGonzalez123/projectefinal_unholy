using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem.Interactions;
using UnityEngine.UI;

public class NPCBuySell : Interactuable
{
    GameObject dialogPanel;
    GameObject traderPanel;
    GameObject ListItemsToBuy;
    GameObject ListItemsToSell;
    TextMeshProUGUI CurrentBuyCoinsText;
    TextMeshProUGUI CurrentSellCoinsText;
    GameObject TraderButtonPrefab;
    TextMeshProUGUI traderPanelComment;
    public NPCSentences SentencesSO;
    public List<ItemSO> itemListToSell;

    public PlayerMaxStatsRuntime PlayerStats;

    private void Start()
    {
        GetAllObjects();
    }

    void GetAllObjects()
    {
        dialogPanel = CanvasSingleton.Instance.DialogPanel;
        traderPanel = CanvasSingleton.Instance.TraderPanel;
        ListItemsToBuy = CanvasSingleton.Instance.ListItemsToBuy;
        ListItemsToSell = CanvasSingleton.Instance.ListItemsToSell;
        CurrentBuyCoinsText = CanvasSingleton.Instance.CurrentBuyCoinsText;
        CurrentSellCoinsText = CanvasSingleton.Instance.CurrentSellCoinsText;
        TraderButtonPrefab = CanvasSingleton.Instance.TraderButtonPrefab;
        traderPanelComment = CanvasSingleton.Instance.TraderPanelComment;
    }

    public override bool Interact()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        GameManager.Instance.menuOpened = true;
        GameManager.Instance.CanConsoleBeOpened = false;
        OpenDialogPanel();

        return false;
    }

    public void ToggleBuyPanel(GameObject buyPanel)
    {
        if (buyPanel.activeSelf) // if the buyPanel is active, set it inactive and the dialog panel active
        {
            dialogPanel.SetActive(true);
            buyPanel.SetActive(false);
        }
        else
        {
            dialogPanel.SetActive(false);
            buyPanel.SetActive(true);
            ListItemsToBuyInThePanel();
        }
    }
    public void ToggleSellPanel(GameObject sellPanel)
    {
        if (sellPanel.activeSelf) // if the buyPanel is active, set it inactive and the dialog panel active
        {
            dialogPanel.SetActive(true);
            sellPanel.SetActive(false);
        }
        else
        {
            dialogPanel.SetActive(false);
            sellPanel.SetActive(true);
            ListItemsToSellInThePanel();
        }
    }

    void ListItemsToSellInThePanel()
    {
        // Remove all items first and then load them back
        for (int i = ListItemsToSell.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(ListItemsToSell.transform.GetChild(i).gameObject);
        }

        List<ObjectItem> playerItems = InventoryManager.Instance.Items;
        foreach (ObjectItem playerItem in playerItems)
        {
            if (playerItem.itemModel.Sellable)
            {
                GameObject newItemButton = Instantiate(TraderButtonPrefab, ListItemsToSell.transform);
                newItemButton.GetComponent<TraderButtonBehaviour>().LoadInfo(playerItem);

                // Set the listener to buy the item when clicked
                newItemButton.GetComponent<Button>().onClick.AddListener(() => SellItem(playerItem));
            }
        }
        UpdateCashUI(CurrentSellCoinsText);
    }

    void SellItem(ObjectItem item)
    {
        // Add the money to the playerstats
        if (item.itemModel.Sellable)
        {
            if (item.itemModel is ConsumableSO)
            {
                PlayerStats.CURRENT_GOLD += item.itemModel.SellPrice * ((ObjectConsumable)item).currentQuantity;
            }
            else if (item.itemModel is SimpleItemSO)
            {
                PlayerStats.CURRENT_GOLD += item.itemModel.SellPrice * ((ObjectSimpleItem)item).currentQuantity;
            }
            else
            {
                PlayerStats.CURRENT_GOLD += item.itemModel.SellPrice;
            }
            InventoryManager.Instance.RemoveItem(item);
        }

        // Update UI
        ListItemsToSellInThePanel();
    }

    void ListItemsToBuyInThePanel()
    {
        // Remove all items first and then load them back
        for (int i = ListItemsToBuy.transform.childCount - 1; i >= 0; i--)
        {
            Destroy(ListItemsToBuy.transform.GetChild(i).gameObject);
        }


        // Pillar los items del npc con el que interactua el jugador. Si hay mas de un npc, estos tendran distintos items a vender. Se pillan los items con el que interactuas a partir del InteractionManager
        List<ItemSO> npcItems = InteractionManager.Instance.currentInteractuable.gameObject.GetComponent<NPCBuySell>().itemListToSell;
        foreach (ItemSO item in npcItems)
        {
            // Instantiate a button prefab with item information
            GameObject newItemButton = Instantiate(TraderButtonPrefab, ListItemsToBuy.transform);
            newItemButton.GetComponent<TraderButtonBehaviour>().LoadInfo(item);

            // Set the listener to buy the item when clicked
            newItemButton.GetComponent<Button>().onClick.AddListener(() => BuyItem(item));
        }
        UpdateCashUI(CurrentBuyCoinsText);
    }

    void BuyItem(ItemSO itemModel)
    {
        print("buy item");
        if (PlayerMaxStatsRuntime.Instance.CURRENT_GOLD >= itemModel.BuyPrice) // If the player has enough gold,
        {
            print("the player has gold");
            print("itemType: " + itemModel.GetType());
            if (itemModel is ArmorSO)
            {
                bool hasAddedItem = InventoryManager.Instance.AddItem(new ObjectArmor((ArmorSO)itemModel));
                if (hasAddedItem)
                {
                    print("bought armor");
                    PlayerStats.CURRENT_GOLD -= itemModel.BuyPrice;
                    UpdateCashUI(CurrentBuyCoinsText);
                }
                else
                {
                    print("Not enough space in inventory to buy this item");
                }
            }
            else if (itemModel is WeaponSO)
            {
                bool hasAddedItem = InventoryManager.Instance.AddItem(new ObjectWeapon((WeaponSO)itemModel));
                if (hasAddedItem)
                {
                    print("bought weapon");
                    PlayerStats.CURRENT_GOLD -= itemModel.BuyPrice;
                    UpdateCashUI(CurrentBuyCoinsText);
                }
                else
                {
                    print("Not enough space in inventory to buy this item");
                }
            }
            else if (itemModel is ConsumableSO)
            {
                print("consumable");
                bool hasAddedItem = InventoryManager.Instance.AddItem(new ObjectConsumable((ConsumableSO)itemModel));
                print("hasAddedConsumable? " + hasAddedItem);
                if (hasAddedItem)
                {
                    print("bought consumable");
                    print("before gold " + PlayerStats.CURRENT_GOLD);
                    PlayerStats.CURRENT_GOLD -= itemModel.BuyPrice;
                    print("after gold " + PlayerStats.CURRENT_GOLD);
                    UpdateCashUI(CurrentBuyCoinsText);
                }
                else
                {
                    print("Not enough space in inventory to buy this item");
                }
            }
        }
        else // The player has no gold to buy the item
        {
            print("Not enough gold to buy this item");
        }
    }

    void UpdateCashUI(TextMeshProUGUI CurrentCoinsText)
    {
        CurrentCoinsText.text = "GOLD: " + PlayerMaxStatsRuntime.Instance.CURRENT_GOLD.ToString();
    }

    private void OpenDialogPanel()
    {
        dialogPanel.SetActive(true);
        traderPanel.SetActive(true);

        traderPanelComment.text = SentencesSO.SayRandomTraderSentece();
    }
}
