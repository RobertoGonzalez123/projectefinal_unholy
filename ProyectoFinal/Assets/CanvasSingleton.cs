using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CanvasSingleton : MonoBehaviour
{
    public static CanvasSingleton Instance;

    // NPC STUFF
    public GameObject Quests;
    public GameObject DialogPanel;
    public GameObject QuesterPanel;
    public TextMeshProUGUI DialogText;
    public GameObject QuestList;
    public GameObject QuestBTN;
    public GameObject QuestInfo;
    [Header("Menu Objects")]
    public GameObject QuestUIGroup;
    public TextMeshProUGUI UI_QuestTitle;
    public TextMeshProUGUI UI_QuestDescription;
    public TextMeshProUGUI UI_TasksText;
    public TextMeshProUGUI UI_RewardsText;

    [Header("NPC Quest Info Panel")]
    public TextMeshProUGUI QuestNameLabel;
    public TextMeshProUGUI QuestDescLabel;
    public TextMeshProUGUI RequirementsLabel;
    public TextMeshProUGUI RewardsLabel;
    public Button AcceptQuestBTN;
    public GameObject AbandonBTN;
    public GameObject ClaimRewardsBTN;

    [Header("NPCBuySeller")]
    public GameObject TraderPanel;
    public GameObject ListItemsToBuy;
    public GameObject ListItemsToSell;
    public TextMeshProUGUI CurrentBuyCoinsText;
    public TextMeshProUGUI CurrentSellCoinsText;
    public GameObject TraderButtonPrefab;
    public TextMeshProUGUI TraderPanelComment;



    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}
