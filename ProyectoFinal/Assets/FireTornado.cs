using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTornado : OnlyDamaging
{
    [SerializeField] Collider objectCollider;
    [SerializeField] Rigidbody rigidBodyCollider;
    [SerializeField] float timeRemaining;
    [SerializeField] float tickRate;
    float time = 0;

    public void EnableFireTornado(float dmg, float time, float applyBurningTime)
    {
        this.doDamageStats.dmg = dmg;
        this.doDamageStats.estados.Add(Estado.GetEstado(Estado.Estados.Burning, dmg));
        this.time = time;
        StartCoroutine(TornadoCoroutine());

    }

    private void Update()
    {
        time -= Time.deltaTime;
    }

    IEnumerator TornadoCoroutine()
    {
        while (time > 0)
        {
            yield return new WaitForSeconds(tickRate);
            objectCollider.enabled = true;
            yield return new WaitForSeconds(tickRate / 3);
            objectCollider.enabled = false;

        }

    }

}
