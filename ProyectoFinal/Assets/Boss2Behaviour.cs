using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using UnityEngine.Events;
using Mono.Cecil;
using static InterfaceDamageable;
using Unity.VisualScripting;
using System.Linq;

public class Boss2Behaviour : EnemyBehaviour
{


    



    public UnityEvent EnemyDamaged = new UnityEvent();

   

    public GameObject MinionToSpawnPrefab;
    public GameObject[] GroundSpikesPrefabs;
    public GameObject RedAreaPrefab;

    public GameObject ProjectileToSpawnPrefab;




    private void ToAttack()
    {
        AttackDeltaTime = 0;
        selectedRandomAttack = Random.Range(0, attacks.Count);

        switch (attacks[selectedRandomAttack].AttackAnimationName)
        {
            case "NormalAttack":
                CastProjectile();
                break;
            case "SummonMinions":
                if (CurrentState == States.PHASE1)
                    SummonMinions();
                break;
            case "SpikesFromGround":
                if (CurrentState == States.PHASE1 || CurrentState == States.PHASE2)
                    SpikesFromGround();
                break;
        }


        animator.Play(attacks[selectedRandomAttack].AttackAnimationName);
    }
    void CastProjectile()
    {
        animator.Play("NormalSpell");
        print("casting projectile");
        GameObject newProjectile = Instantiate(ProjectileToSpawnPrefab, transform.position, Quaternion.identity);
    }
    void SummonMinions()
    {
        animator.Play("SummonMinions");
        for (int i = 0; i < 10; i++)
        {
            // Select a random prefab 10 times and spawn it underground
            float randomX = Random.Range(-15, 15);
            float randomZ = Random.Range(6, -26);
            GameObject newMinion = Instantiate(MinionToSpawnPrefab, new Vector3(randomX, -0.5f, randomZ), Quaternion.identity);
        }
    }
    void SpikesFromGround()
    {
        animator.Play("SpikesFromGround");
        for (int i = 0; i < 10; i++)
        {
            // Select a random prefab 10 times and spawn it underground
            int random = Random.Range(0, GroundSpikesPrefabs.Length);
            GameObject SpikeToSpawn = GroundSpikesPrefabs[random];
            float randomX = Random.Range(-15, 15);
            float randomZ = Random.Range(6, -26);
            GameObject newRedArea = Instantiate(RedAreaPrefab, new Vector3(randomX, 0.5f, randomZ), Quaternion.Euler(-90, 0, 0));
            StartCoroutine(SpawnRedArea(newRedArea));
            GameObject newSpike = Instantiate(SpikeToSpawn, new Vector3(randomX, -5, randomZ), Quaternion.identity);
            // Make a coroutine to rise the spikes faster and then destroy them
            StartCoroutine(SpikeRise(newSpike));
        }
    }

    IEnumerator SpawnRedArea(GameObject redArea)
    {
        while (true)
        {
            yield return null;
            redArea.transform.localScale += new Vector3(0.002f, 0.002f, 0.002f);
            if (redArea.transform.localScale == new Vector3(0.5f, 0.5f, 0.5f)) { Destroy(redArea); break; }
        }
    }

    IEnumerator SpikeRise(GameObject spike)
    {
        while (spike.transform.position.y < 4)
        {
            yield return new WaitForSeconds(0.01f);
            spike.transform.position = new Vector3(spike.transform.position.x, spike.transform.position.y + 0.2f, spike.transform.position.z);
        }
        yield return new WaitForSeconds(2.5f);
        while (spike.transform.position.y > -5)
        {
            yield return new WaitForSeconds(0.1f);
            spike.transform.position = new Vector3(spike.transform.position.x, spike.transform.position.y - 0.05f, spike.transform.position.z);
        }
        Destroy(spike.gameObject);
    }

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        target = GameObject.Find("swordman_lv1").transform;
        CurrentHP = EnemyStats.health;
        healthBar = transform.Find("EnemyCanvas").GetComponent<Canvas>().GetComponentsInChildren<Image>()[1];
        this.AddComponent<Outline>();
        GetComponent<Outline>().enabled = false;
    }
    private void Start()
    {
        InitState(States.RESTING);
        dmgReceivedMultiplier = 1;
        healthBar.fillAmount = CurrentHP / EnemyStats.health;
    }

    void Update()
    {
        UpdateState(CurrentState);
        print("current state: " + CurrentState);
    }



    protected override void InitState(States initState)
    {
        //print("ENTERING " + initState);
        CurrentState = initState;
        StateDeltaTime = 0;

        switch (CurrentState)
        {
            case States.RESTING:
                animator.Play("Idle");
                break;

            case States.PATROLING:
                animator.Play("Walk");
                agent.speed = EnemyStats.patrolingSpeed;
                GoToNewPatrolPoint();
                break;

            case States.CHASING:
                animator.Play("Walk");
                agent.speed = EnemyStats.chasingSpeed;
                break;
            case States.ATTACKING:
                ToAttack();
                break;
            case States.DYING:
                animator.Play("Death");
                break;
            case States.PHASE1:
                animator.Play("TransitionPhase1");
                ToAttack();
                break;
            case States.PHASE2:
                animator.Play("TransitionPhase2");
                ToAttack();
                break;
            default:
                break;
        }
    }

    protected override void UpdateState(States updateState)
    {
        StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {

            case States.RESTING:
                if (inRestingDetectDistance)
                {
                    print("RESTING: Player inside range, checking if I have vision");
                    if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, EnemyStats.restingDetectDistance, ~EnemyMask))
                    {
                        print("RESTING: RayCast Detected object -> " + hit.transform.name);
                        if (hit.transform.gameObject.tag == "Player")
                        {
                            print("RESTING: Player detected");
                            ChangeState(States.CHASING);
                        }
                    }
                }
                break;
            case States.PATROLING:
                if (ArrivedAtDestination())
                {
                    GoToNewPatrolPoint();
                }
                else if (inPatrolingDetectDistance)
                {
                    print("PATROLING: Player inside range, checking if I have vision");
                    //Debug.DrawRay(transform.position, (target.position - transform.position).normalized * BearStats.patrolingDetectDistance, Color.magenta, 2f);
                    if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, EnemyStats.patrolingDetectDistance, ~EnemyMask))
                    {
                        //print("PATROLING: Player detected");
                        //ChangeState(States.CHASING);
                        //Debug.DrawRay(transform.position, (target.position - transform.position).normalized * hit.distance, Color.magenta, 2f);
                        if (hit.transform.gameObject.tag == "Player")
                        {
                            print("PATROLING: Player detected");
                            ChangeState(States.CHASING);
                        }
                    }
                }
                break;
            case States.CHASING:
                agent.SetDestination(target.position);
                print("Target position: " + target.position);
                if (!inChasingMaxDistance)
                {
                    print("CHASING: Not in chase distance, entering PATROLING");
                    ChangeState(States.PATROLING);
                }
                else if (inAttackDistance)
                {
                    print("Changing from CHASING to ATTACKING");
                    agent.SetDestination(transform.position); // To make the boss stop on the spot when it sees the player
                    ChangeState(States.ATTACKING);
                }
                break;
            case States.ATTACKING:
                AttackDeltaTime += Time.deltaTime;
                if (!inAttackDistance && AttackDeltaTime >= attacks[selectedRandomAttack].AttackCooldown)
                {
                    ChangeState(States.CHASING);
                }
                else if (inAttackDistance && AttackDeltaTime >= attacks[selectedRandomAttack].AttackCooldown)
                {
                    ToAttack();
                }

                break;
            case States.DYING:
                if (StateDeltaTime >= 3.21f)
                {
                    Destroy(this.gameObject);
                }
                break;

            default:
                break;
        }
    }

    protected override void ExitState(States exitState)
    {
        //print("EXITING " + exitState);
        switch (exitState)
        {
            case States.RESTING:
                break;
            case States.PATROLING:
                break;
            case States.CHASING:
                break;
            case States.ATTACKING:
                break;
            default:
                break;
        }
    }





    public void ReceiveDamage(List<Estado> estados, float damageReceived)
    {

        EnemyDamaged.Invoke();
        ReceiveDmgToHealth(damageReceived);

        // TO-DO AddReceiveing Damage Animations and implement ItemDropSystem
        Instantiate(particlesReceivedDamage, this.transform.position, Quaternion.identity);
        if (CurrentHP < EnemyStats.health * 0.6f) CurrentState = States.PHASE1;
        if (CurrentHP < EnemyStats.health * 0.4f) CurrentState = States.PHASE2;

        if (estados != null)
        {
            for (int i = 0; i < estados.Count; i++)
            {
                Estado newEstado = currentStates.FirstOrDefault(objecte => objecte.GetType() == estados[i].GetType());
                if (newEstado == null)
                {
                    currentStates.Add(estados[i]);
                }


            }
        }
        CheckApplyStates();

    }

    public override void ToDie()
    {
        ChangeState(States.DYING);
    }



}
