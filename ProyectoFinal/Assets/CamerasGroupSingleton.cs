using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamerasGroupSingleton : MonoBehaviour
{
    public static CamerasGroupSingleton Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

}
