using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNPCAnimation : MonoBehaviour
{
    public enum WhatIsTheNPCDoing { SITTING, IDLE_STAND_UP, TALKING }
    public WhatIsTheNPCDoing whatIsTheNPCDoing;

}
