﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMoveScript : OnlyDamaging {

	public GameObject hitPrefab;
	public List<GameObject> trails;

	[SerializeField] Rigidbody rb;

    private void OnEnable()
    {
        StartCoroutine("destroyCoroutine");
    }
    IEnumerator destroyCoroutine()
    {
        yield return new WaitForSeconds(20f);
        Destroy(gameObject);
    }
    public void LoadDamage(AttackSO attackSettings)
    {
        doDamageStats.dmg = attackSettings.AttackDamage;
        foreach (Estado.Estados unEstado in attackSettings.estados)
        {
            doDamageStats.estados.Add(new Estado(unEstado, attackSettings.AttackDamage / 10, 10f));
        }

    }
    public void GoToPoint(Vector3 targetPoint, float speed, AttackSO attackSettings)
    {
        this.LoadDamage(attackSettings);
        Vector3 direction = (targetPoint - transform.position).normalized;
        rb.velocity = direction * speed;

    }
    private void OnTriggerEnter(Collider other)
    {
        if (trails.Count > 0)
        {
            for (int i = 0; i < trails.Count; i++)
            {
                trails[i].transform.parent = null;
                var ps = trails[i].GetComponent<ParticleSystem>();
                if (ps != null)
                {
                    ps.Stop();
                    Destroy(ps.gameObject, ps.main.duration + ps.main.startLifetime.constantMax);
                }
            }
        }

        Vector3 closestPoint = other.ClosestPointOnBounds(transform.position);
        Vector3 normal = (transform.position - closestPoint).normalized;

        Quaternion rot = Quaternion.FromToRotation(Vector3.up, normal);
        Vector3 pos = closestPoint;

        if (hitPrefab != null)
        {
            var hitVFX = Instantiate(hitPrefab, pos, rot) as GameObject;

            var ps = hitVFX.GetComponent<ParticleSystem>();
            if (ps == null)
            {
                var psChild = hitVFX.transform.GetChild(0).GetComponent<ParticleSystem>();
                Destroy(hitVFX, psChild.main.duration);
            }
            else
                Destroy(hitVFX, ps.main.duration);
        }

        StartCoroutine(DestroyParticle(0f));
    }


	public IEnumerator DestroyParticle (float waitTime) {

		if (transform.childCount > 0 && waitTime != 0) {
			List<Transform> tList = new List<Transform> ();

			foreach (Transform t in transform.GetChild(0).transform) {
				tList.Add (t);
			}		

			while (transform.GetChild(0).localScale.x > 0) {
				yield return new WaitForSeconds (0.01f);
				transform.GetChild(0).localScale -= new Vector3 (0.1f, 0.1f, 0.1f);
				for (int i = 0; i < tList.Count; i++) {
					tList[i].localScale -= new Vector3 (0.1f, 0.1f, 0.1f);
				}
			}
		}
		
		yield return new WaitForSeconds (waitTime);
		Destroy (gameObject);
	}

}
