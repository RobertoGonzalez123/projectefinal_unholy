using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bossProjectileController : MonoBehaviour
{
    public int speed = 5;
    void Start()
    {
        Vector3 direction = (GameObject.Find("swordman_lv1").transform.position - this.transform.position).normalized;
        this.GetComponent<Rigidbody>().velocity = direction * speed;
    }
}
