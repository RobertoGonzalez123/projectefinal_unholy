using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class GoToPoint_QuestController : MonoBehaviour
{

    [SerializeField]
    private Transform Player;
    [SerializeField]
    private LineRenderer Path;
    [SerializeField]
    private float PathHeightOffset = 1f;
    [SerializeField]
    private float PathUpdateSpeed = 0.10f;

    [Header("Cinematics")]
    public GameObject WallDestruction1Camera;
    public GameObject WallToDestroy1;
    public GameObject ExplosionEffects;
    public GameObject HarbourCamera;

    [SerializeField] QuestSO questGoToPoint;

    private void Start()
    {
        Invoke("ShouldDestroy", 1.3f);
    }
    void ShouldDestroy()
    {
        if (questGoToPoint!=null && (questGoToPoint.questState == QuestSO.QuestState.FINISHED || questGoToPoint.questState == QuestSO.QuestState.DOING_FINISHED))
        {
            Destroy(WallToDestroy1.gameObject);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (QuestManager.Instance.MisionActiva is MisionPosicionSO || questGoToPoint.questState == QuestSO.QuestState.DOING_NOT_FINISHED)
            {
                QuestManager.Instance.MisionActiva = questGoToPoint;
                GameManager.Instance.Player.GetComponent<AGUSMOVEMENT>().isPlayerAbleToMove = false;

                // Complete the quest and destroy the goal point
                QuestManager.Instance.MisionActiva.CompletarMision();

                // Stop the player. If all quests of position need the player to stop, keep the following line outside the if of checking the names.
                // If the stop of the player is specifically for each point, put this line on each if checking the names.
                GameManager.Instance.Player.GetComponent<AGUSMOVEMENT>().m_FSM.ChangeState<Idle>();
                GameManager.Instance.Player.GetComponent<Rigidbody>().velocity = Vector3.zero;

                print("the name of the object asdf is " + this.gameObject.name);

                if (this.gameObject.name.Contains("Tutorial"))
                {
                    // Play animation, explosion and let the player destroy the fences in the path
                    ExplodeTutorialWall();
                }
                else if (this.gameObject.name.Contains("Harbour"))
                {
                    PlayHarbourAnimation();
                }
            }
            
        }
    }

    void PlayHarbourAnimation()
    {
        // Play the animation and then allow the player to move again
        HarbourCamera.GetComponent<Animator>().Play("HarbourAnimation");
        StartCoroutine(RestartPlayerMovement(6.5f));
    }

    void ExplodeTutorialWall()
    {
        // Play the camera animation, restart the player movement and destroy the wall gameobject
        WallDestruction1Camera.GetComponent<Animator>().Play("DestructionWall1");
        StartCoroutine(RestartPlayerMovement(4));
        StartCoroutine(DestroyWall());
    }
    IEnumerator DestroyWall()
    {
        yield return new WaitForSeconds(1f);
        ExplosionEffects.GetComponent<ParticleSystem>().Play();
        Destroy(WallToDestroy1.gameObject);
    }

    IEnumerator RestartPlayerMovement(float secondsToRestart)
    {
        yield return new WaitForSeconds(secondsToRestart);
        print("waiting");
        GameManager.Instance.Player.GetComponent<AGUSMOVEMENT>().isPlayerAbleToMove = true;
        print("waited");
        Destroy(this.gameObject);
    }
}
