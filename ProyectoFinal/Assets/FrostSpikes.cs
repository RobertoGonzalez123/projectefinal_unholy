using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostSpikes : OnlyDamaging
{
    [SerializeField] GameObject objectCollider;
    [SerializeField] Rigidbody rigidBodyCollider;
    [SerializeField] float speed;
    [SerializeField] float time;
    [SerializeField] float timeCollider;

    public void EnableFrostSpikes(float dmg)
    {
        this.doDamageStats.dmg = dmg;
        StartCoroutine(DisableObject());
        StartCoroutine(DisableCollider());
        objectCollider.GetComponent<Rigidbody>().velocity = objectCollider.transform.forward * speed;
    }

    IEnumerator DisableObject()
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
    IEnumerator DisableCollider()
    {
        yield return new WaitForSeconds(timeCollider);
        objectCollider.SetActive(false);
    }

}