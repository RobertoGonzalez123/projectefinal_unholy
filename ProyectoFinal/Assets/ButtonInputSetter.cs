using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInputSetter : MonoBehaviour
{
    private KeyBinding keyBinding;

    private void Awake()
    {
        keyBinding = GetComponent<KeyBinding>();
    }

    private void OnEnable()
    {
        GetComponent<Button>().onClick.AddListener(SetNewKey);
    }

    private void OnDisable()
    {
        GetComponent<Button>().onClick.RemoveListener(SetNewKey);
    }

    private void SetNewKey()
    {
        StartCoroutine(WaitForKey());
    }

    IEnumerator WaitForKey()
    {
        bool keySet = false;
        while (!keySet)
        {
            foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKey(vKey))
                {
                    keyBinding.keyCode = vKey;
                    keySet = true;
                    break;
                }
            }
            yield return null;
        }
    }
}