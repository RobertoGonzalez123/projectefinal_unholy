using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ShipManager : MonoBehaviour
{
    public List<GameObject> targetPositions;

    void Start()
    {
        int targetPosition = Random.Range(0, targetPositions.Count - 1);
        this.GetComponent<NavMeshAgent>().SetDestination(targetPositions[targetPosition].transform.position);
        StartCoroutine(SetShipTargetLocation());
    }


    IEnumerator SetShipTargetLocation()
    {
        while (true)
        { 
            yield return new WaitForSeconds(Random.Range(30, 61));
            int targetPosition = Random.Range(0, targetPositions.Count - 1);
            this.GetComponent<NavMeshAgent>().SetDestination(targetPositions[targetPosition].transform.position);
        }
    }
}
