using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScript : MonoBehaviour, InteractuableInterface
{
    [SerializeField]
    GameObject Player;
    // Item item = which item is that?
    public bool Interact()
    {
        //Player.inventory.AddItem(this.item);
        // Play little particles/effects/animation
        GetComponent<Animator>().Play("PickUp");
        // Sound of picking up items
        GetComponent<AudioSource>().Play();

        // Probably there will be some kind of alert in the screen showing something like "Apple +1/+2"

        StartCoroutine("DestroyObject");
        return false;
    }
    IEnumerator DestroyObject()
    {
        yield return new WaitForSeconds(0.2f);
        Destroy(gameObject);
    }
}
