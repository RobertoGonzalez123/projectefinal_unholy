using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UpdateEquipment : MonoBehaviour
{
    [SerializeField] GameObject equipmentSlotsContainer;
    EquipmentSlot[] equipmentSlots;
    void Start()
    {
        EquipmentManager.Instance.onEquipmentChanged += UpdateEquipmentUI;
        
        equipmentSlots = equipmentSlotsContainer.GetComponentsInChildren<EquipmentSlot>();
        UpdateEquipmentUI();
    }

    // coloca el equipamiento en el UI de equipamiento
    public void UpdateEquipmentUI()
    {
        for (int i = 0; i < equipmentSlots.Length; i++)
        {
            if (EquipmentManager.Instance.currentEquipment[i]?.itemModel?.name != null)
            {
                //Debug.Log("hay algo aqui existe"+ EquipmentManager.Instance.currentEquipment[i].itemModel.name);
                equipmentSlots[i].AddEquipmentToUI(EquipmentManager.Instance.currentEquipment[i]);
            }
            else
            {
                //Debug.Log("no hay nada aqui");
                equipmentSlots[i].RemoveEquipmentToUI();
            }
        }
    }

}
