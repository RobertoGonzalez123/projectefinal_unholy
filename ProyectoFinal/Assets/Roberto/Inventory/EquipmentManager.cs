using System.Collections.Generic;
using UnityEngine;
using static EquipSO;

public class EquipmentManager : MonoBehaviour
{

    [SerializeField] public List<ObjectEquip> currentEquipment;

    [SerializeField] InventoryManager inventory;

    public delegate void OnEquipmentChanged();
    public OnEquipmentChanged onEquipmentChanged;

    public static EquipmentManager Instance;
    void Awake()
    {
        if (Instance == null) Instance = this;
        else Destroy(this.gameObject);
    }

    private void Start()
    {
        inventory = GetComponent<InventoryManager>();
    }

    public void AddEquip(ObjectEquip equip)
    {
        int slotPositionInArray = (int)((EquipSO)equip.itemModel).equipmentType;
        if (currentEquipment[slotPositionInArray]?.itemModel?.name != null)
        {
            print("intentando equipar en el slot OCUPADO->" + slotPositionInArray);
            inventory.RemoveItem(equip);
            inventory.AddItem(currentEquipment[slotPositionInArray]);
            currentEquipment[slotPositionInArray] = equip;
            onEquipmentChanged?.Invoke();
        }
        else
        {
            print("intentando equipar en el slot LIBRE->" + slotPositionInArray);
            inventory.RemoveItem(equip);
            currentEquipment[slotPositionInArray] = equip;
            onEquipmentChanged?.Invoke();
        }
    }

    public void Unequip(ObjectEquip equip)
    {
        if (equip?.itemModel != null)
            if (inventory.AddItem(equip))
            {
                if (equip?.itemModel is MountSO && AGUSMOVEMENT.Instance.isRidingHorse) AGUSMOVEMENT.Instance.ToggleHorse();
                currentEquipment[(int)((EquipSO)equip.itemModel).equipmentType] = null;
                if (((EquipSO)equip.itemModel).equipmentType == EquipSO.EquipmentType.Weapon)
                {
                    AGUSMOVEMENT.Instance.StopCheckTemporalStates();
                    AGUSMOVEMENT.Instance.estadosTemporales.Clear();
                }
                onEquipmentChanged?.Invoke();
            }
    }


    public float getWeaponNormalAttack()
    {
        foreach (ObjectEquip equip in currentEquipment)
        {
            if (equip?.itemModel != null)
                if (((EquipSO)equip.itemModel).equipmentType == EquipSO.EquipmentType.Weapon)
                {
                    float weaponNormalDamage = ((WeaponSO)equip.itemModel).dmgNormalAttack;
                    weaponNormalDamage += (0.25f * (equip.currentLevel - 1)) * weaponNormalDamage;
                    // Calculate critical dmg and rate?
                    WeaponSO Equip = (WeaponSO)equip.itemModel;
                    int critChance = Equip.criticalHitChance;
                    int randomNumber = Random.Range(0, 100);
                    if (randomNumber <= critChance)
                    {
                        weaponNormalDamage += (PlayerMaxStatsRuntime.Instance.CRIT_SKILL_LEVEL - 1) * 2.5f;
                    }

                    weaponNormalDamage *= PlayerMaxStatsRuntime.Instance.STRENGTH_MODIFIER;
                    return weaponNormalDamage;
                }
        }
        return 5;
    }

    public float getWeaponHeavyAttack()
    {
        foreach (ObjectEquip equip in currentEquipment)
        {
            if (equip?.itemModel != null)
                if (((EquipSO)equip.itemModel).equipmentType == EquipSO.EquipmentType.Weapon)
                {
                    float weaponHeavyDamage = ((WeaponSO)equip.itemModel).dmgHeavyAttack;
                    weaponHeavyDamage += (0.25f * (equip.currentLevel - 1)) * weaponHeavyDamage;
                    // Calculate critical dmg and rate?
                    WeaponSO Equip = (WeaponSO)equip.itemModel;
                    int critChance = Equip.criticalHitChance;
                    int randomNumber = Random.Range(0, 100);
                    if (randomNumber <= critChance)
                    {
                        weaponHeavyDamage += (PlayerMaxStatsRuntime.Instance.CRIT_SKILL_LEVEL - 1) * 2.5f;
                    }

                    weaponHeavyDamage *= PlayerMaxStatsRuntime.Instance.STRENGTH_MODIFIER;
                    return weaponHeavyDamage;
                }
        }
        return 0;
    }


    public float getArmorDefense()
    {
        float defense = 0;
        foreach (ObjectEquip equip in currentEquipment)
        {
            if (equip?.itemModel != null
                && ((EquipSO)equip.itemModel).equipmentType != EquipSO.EquipmentType.Weapon
                && ((EquipSO)equip.itemModel).equipmentType != EquipSO.EquipmentType.Mount)
            {
                float baseDefense = ((ArmorSO)equip.itemModel).defense;
                baseDefense += (0.05f * (equip.currentLevel - 1)) * baseDefense;
                defense += baseDefense;
            }
        }
        return defense;
    }
    public float getArmorHP()
    {
        float hp = 0;
        foreach (ObjectEquip equip in currentEquipment)
        {
            if (((EquipSO)equip?.itemModel)?.equipmentType != null
                && ((EquipSO)equip?.itemModel)?.equipmentType != EquipSO.EquipmentType.Weapon
                && ((EquipSO)equip?.itemModel)?.equipmentType != EquipSO.EquipmentType.Mount)
            {
                Debug.Log("HA ENTRAD");
                float basehp = ((ArmorSO)equip.itemModel).hp;
                basehp += (0.15f * (equip.currentLevel - 1)) * basehp;
                hp += basehp;
            }
        }
        return hp;
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            print("ArmorHP->" + getArmorHP() + " Defense->" + getArmorDefense());
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            string linea = " ";
            currentEquipment.ForEach(e => linea += e?.itemModel?.ItemName + "(" + e?.itemID + ")");
            Debug.Log("CurrentEquipment->" + linea);
        }
    }

}
