using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentSlot : MonoBehaviour
{

    public ObjectEquip item;
    public Image itemIcon;
    public Image rarityGlow;

    public AccessibleDPADSlot anotherUIElement;

    public void AddEquipmentToUI(ObjectEquip newItem)
    {
        item = newItem;
        itemIcon.gameObject.SetActive(true);
        itemIcon.sprite = item.itemModel.SlotIcon;
        itemIcon.enabled = true;


        rarityGlow.gameObject.SetActive(true);
        rarityGlow.color = GetRarityColor(item.itemModel.Rarity);
        
        if(anotherUIElement!=null) anotherUIElement.ShowItemIcon(item.itemModel.SlotIcon);
    }

    public void RemoveEquipmentToUI()
    {
        item = null;
        itemIcon.gameObject.SetActive(false);
        itemIcon.sprite = null;
        rarityGlow.gameObject.SetActive(false);

        if (anotherUIElement != null) anotherUIElement.HideItemIcon();
    }

    public void CallUnequip()
    {
        Debug.Log("Called unequip");
        if (item?.itemModel != null)
        {
            EquipmentManager.Instance.Unequip(item);
        }
    }
    private Color GetRarityColor(Rarity rarity)
    {
        Color color;

        switch (rarity)
        {
            case Rarity.Common:
                color = Color.white;
                break;
            case Rarity.Uncommon:
                color = Color.green;
                break;
            case Rarity.Rare:
                color = Color.blue;
                break;
            case Rarity.Epic:
                color = Color.magenta;
                break;
            case Rarity.Legendary:
                color = Color.yellow;
                break;
            default:
                color = Color.white;
                break;
        }
        color.a = 0.1008f;
        return color;
    }


}
