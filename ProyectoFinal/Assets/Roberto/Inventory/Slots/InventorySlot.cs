using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public ObjectItem item;

    public Image itemIcon;
    public TextMeshProUGUI text;
    public Image rarityGlow;

    public void AddItemToUI(ObjectItem newItem)
    {
        item = newItem;
        itemIcon.gameObject.SetActive(true);
        text.color = Color.white;
        itemIcon.sprite = newItem.itemModel.SlotIcon;
        if (newItem is ObjectConsumable)
            text.text = "" + ((ObjectConsumable)newItem).currentQuantity;
        else if (newItem is ObjectSimpleItem)
            text.text = "" + ((ObjectSimpleItem)newItem).currentQuantity;
        else
            text.text = "";

        rarityGlow.gameObject.SetActive(true);
        rarityGlow.color = GetRarityColor(item.itemModel.Rarity);
    }
    public void RemoveItemFromUI()
    {
        itemIcon.sprite = null;
        text.text = "";
        itemIcon.gameObject.SetActive(false);
        rarityGlow.gameObject.SetActive(false);
        item = null;
    }

    public void UseItem()
    {
        Debug.Log("Called UseItem From Inventory Slot");
        if (item?.itemModel!=null)
        {
            Debug.Log("Item From Inventory Slot is not null");
            item.Use();
        }
    }

    private Color GetRarityColor(Rarity rarity)
    {
        Color color;

        switch (rarity)
        {
            case Rarity.Common:
                color = Color.white;
                break;
            case Rarity.Uncommon:
                color = Color.green;
                break;
            case Rarity.Rare:
                color = Color.blue;
                break;
            case Rarity.Epic:
                color = Color.magenta;
                break;
            case Rarity.Legendary:
                color = Color.yellow;
                break;
            default:
                color = Color.white;
                break;
        }
        color.a = 0.1008f;
        return color;
    }


}
