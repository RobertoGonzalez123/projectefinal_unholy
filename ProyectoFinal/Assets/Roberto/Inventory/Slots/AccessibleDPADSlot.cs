using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class AccessibleDPADSlot : MonoBehaviour
{
    public Image itemIcon;
    [SerializeField] Sprite optionalSpriteIfEmpty;

    public void ShowItemIcon(Sprite sprite)
    {
        itemIcon.gameObject.SetActive(true);
        itemIcon.sprite = sprite;
        itemIcon.enabled = true;
    }

    public void HideItemIcon()
    {
        
        if(optionalSpriteIfEmpty!=null)
            itemIcon.sprite = optionalSpriteIfEmpty;
        else
        {
            itemIcon.gameObject.SetActive(false);
            itemIcon.sprite = null;
        }
            
    }



}
