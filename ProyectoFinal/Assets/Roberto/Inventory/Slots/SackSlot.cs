using UnityEngine;
using UnityEngine.UI;

public class SackSlot : MonoBehaviour
{
    public ObjectItem item;
    public Image itemIcon;
    public SackManager sackWhereIBelong;

    public void SetItemToSlot(ObjectItem itemToLoad,SackManager sack)
    {
        this.sackWhereIBelong = sack;
        this.item = itemToLoad;
        this.itemIcon.sprite = this.item.itemModel.SlotIcon;
    }
    public void AddItemToInventory()
    {
        if (InventoryManager.Instance.AddItem(this.item))
        {
            sackWhereIBelong.UnloadItemByID(this.item);
            RemoveItemFromUI();
        }
    }

    public void RemoveItemFromUI()
    {
       Destroy(this.gameObject);
    }
}
