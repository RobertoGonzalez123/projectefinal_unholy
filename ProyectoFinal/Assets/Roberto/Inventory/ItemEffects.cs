using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemEffects : MonoBehaviour
{
    [SerializeField] PlayerStats PlayerStats;
    /* -------------------------------
     * CONSUMABLE MODIFIES FIXED VALUES
     * -------------------------------
     */

    // --------------HEALTH POINTS--------------
    public void RestoreSmallFixedHP() // Adds 10 hp instantly
    {
        AGUSMOVEMENT.Instance.CurrentHP += 10;
    }
    public void RestoreMediumFixedHP() // Adds 30 hp instantly
    {
        AGUSMOVEMENT.Instance.CurrentHP += 30;
    }
    public void RestoreLargeFixedHP() // Adds 75 hp instantly
    {
        AGUSMOVEMENT.Instance.CurrentHP += 75;
    }

    // --------------DEXTERITY--------------
    public void AddSmallDexterity()
    {
        StartCoroutine(AddDexterity(8, 90));
    } // Adds fixed dexterity percentage and removes it in 120 seconds
    public void AddMediumDexterity()
    {
        StartCoroutine(AddDexterity(13, 90));
    } // Adds fixed dexterity percentage and removes it in 120 seconds
    public void AddLargeDexterity()
    {
        StartCoroutine(AddDexterity(18, 90));
    } // Adds fixed dexterity percentage and removes it in 120 seconds

    // --------------ARMOUR--------------
    public void AddSmallArmour() // Adds instantly 10 armour points and removes it after 120 seconds
    {
        StartCoroutine(AddArmour(10, 120));
    }
    public void AddMediumArmour() // Adds instantly 15 armour points and removes it after 120 seconds
    {
        StartCoroutine(AddArmour(15, 120));
    }
    public void AddLargeArmour() // Adds instantly 25 armour points and removes it after 120 seconds
    {
        StartCoroutine(AddArmour(25, 120));
    }

    public void ConsumeLessStamina()
    {
        PlayerStats.STAMINA_CONSUME_MODIFIER = 0.5f;
        StartCoroutine(RestartStaminaModifier(120));
    }

    IEnumerator AddDexterity(float quantity, int seconds)
    {
        PlayerStats.MAX_DEXTERITY += quantity;
        yield return new WaitForSeconds(seconds);
        PlayerStats.MAX_DEXTERITY -= quantity;
    }  // Used to add fixed dexterity value

    IEnumerator AddArmour(int quantity, int seconds)
    {
        //PlayerStats.MAX_ARMOR += quantity;
        yield return new WaitForSeconds(seconds);
        //PlayerStats.MAX_ARMOR -= quantity;
    } // Used to add fixed armour value
    IEnumerator AddArmour(float quantity, int seconds)
    {
        //PlayerStats.MAX_ARMOR += quantity;
        yield return new WaitForSeconds(seconds);
        //PlayerStats.MAX_ARMOR -= quantity;
    } // Used to add armour by percentage
    IEnumerator RestartStaminaModifier(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        PlayerStats.STAMINA_CONSUME_MODIFIER = 1;
    }

    /*
     * -----------------------------------
     * CONSUMABLE MODIFIES VALUES OVER TIME
     * -----------------------------------
     */

    IEnumerator RestoreHealthOverTime(int healthToRestore, int seconds)
    {
        float healthPerSecond = healthToRestore / seconds;
        for (int i = 0; i < seconds; i++)
        {
            yield return new WaitForSeconds(1);
            AGUSMOVEMENT.Instance.CurrentHP += healthPerSecond;
        }
    }

    public void RestoreSmallTimeHP()
    {
        StartCoroutine(RestoreHealthOverTime(15, 5));
    } // Restore fixed hp value over time
    public void RestoreMediumTimeHP()
    {
        StartCoroutine(RestoreHealthOverTime(50, 7));
    } // Restore fixed hp value over time
    public void RestoreLargeTimeHP()
    {
        StartCoroutine(RestoreHealthOverTime(70, 4));
    } // Restore fixed hp value over time


    /*
     * --------------------------------------
     * CONSUMABLES MODIFIES VALUES PERCENTAGES
     * --------------------------------------
     */

    // --------------HEALTH POINTS--------------
    public void RestoreSmallPercentageHP()
    {
        // Restore 7%
        float totalHP = PlayerStats.MAX_HP;
        float hpToRecover = (float)(totalHP * 0.07);
        AGUSMOVEMENT.Instance.CurrentHP += hpToRecover;
    }
    public void RestoreMediumPercentageHP()
    {
        // Restore 18%
        float totalHP = PlayerStats.MAX_HP;
        float hpToRecover = (float)(totalHP * 0.18);
        AGUSMOVEMENT.Instance.CurrentHP += hpToRecover;
    }
    public void RestoreLargePercentageHP()
    {
        // Restore 36%
        float totalHP = PlayerStats.MAX_HP;
        float hpToRecover = (float)(totalHP * 0.36);
        AGUSMOVEMENT.Instance.CurrentHP += hpToRecover;
    }

    // --------------ARMOUR--------------
    public void AddSmallArmourPercentage()
    {
        // Add 7% and remove after 120 seconds
        //float totalArmour = PlayerStats.MAX_ARMOR;
        //float armourToAdd = (float)(totalArmour * 0.07);
        //player.playerStats.currentArmour += armourToAdd;
        //StartCoroutine(AddArmour(armourToAdd, 120));
    }
    public void AddMediumArmourPercentage()
    {
        // Add 18% and remove after 120 seconds
        //float totalArmour = PlayerStats.MAX_ARMOR;
        //float armourToAdd = (float)(totalArmour * 0.018);
        //player.playerStats.currentArmour += armourToAdd;
        //StartCoroutine(AddArmour(armourToAdd, 120));
    }
    public void AddLargeArmourPercentage()
    {
        // Add 36% and remove after 120 seconds
        //float totalArmour = PlayerStats.MAX_ARMOR;
        //float armourToAdd = (float)(totalArmour * 0.36);
        //player.playerStats.currentArmour += armourToAdd;
        //StartCoroutine(AddArmour(armourToAdd, 120));
    }


    /*
     * -------------------------------------------------
     * CONSUMABLES MODIFIES VALUES PERCENTAGES OVER TIME
     * -------------------------------------------------
     */

    public void SmallHealthPercentageOverTime()
    {
        StartCoroutine(AddHealthPercentageOverTime(10, 3));
    }
    public void MediumHealthPercentageOverTime()
    {
        StartCoroutine(AddHealthPercentageOverTime(20, 4));
    }
    public void LargeHealthPercentageOverTime()
    {
        StartCoroutine(AddHealthPercentageOverTime(40, 5));
    }

    IEnumerator AddHealthPercentageOverTime(float percentageHpToRestore, int seconds)
    {
        float totalHP = PlayerStats.MAX_HP;
        float hpToRecover = (float)(totalHP * percentageHpToRestore);

        hpToRecover /= seconds;

        for (int i = 0; i < seconds; i++)
        {
            yield return new WaitForSeconds(1);
            AGUSMOVEMENT.Instance.CurrentHP += hpToRecover;
        }
    }
}
