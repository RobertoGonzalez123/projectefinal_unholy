using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NewSimpleItem", menuName = "Inventory/Item/Simple Item")]

public class SimpleItemSO : ItemSO
{
    // The Use method is not used since this is a simple item, mostly used for quests and nothing more
    
}
