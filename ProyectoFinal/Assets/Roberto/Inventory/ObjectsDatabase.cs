using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "ObjectsDatabase", menuName = "Inventory/ObjectsDatabase")]
public class ObjectsDatabase : ScriptableObject
{
    public List<ItemSO> objectes;

    public ItemSO Get(int id)
    {
        return objectes.FirstOrDefault(objecte => objecte.DictionaryID == id);
    }
}



