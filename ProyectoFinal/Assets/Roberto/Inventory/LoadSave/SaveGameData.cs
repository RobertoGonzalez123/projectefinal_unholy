using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct SaveGameData
{

    public string currentScene;
    public int contadorMaxIdObjeto;


    public SaveObjectPositionRotation player;

    public List<ObjectItem> inventory;
    public List<ObjectEquip> equipment;

    [Serializable]
    public struct SaveObjectPositionRotation
    {
        public Vector3 position;
        public Quaternion rotation;
    }
}

