using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using FifisSaver.OdinSerializer;
using System.IO;
using System.Text;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Audio;
using System.Collections.Generic;

public class GameController : MonoBehaviour
{
    [SerializeField] bool encoded = false;
    [SerializeField] ObjectsDatabase m_ObjectsDatabase;

    [Header("Settings")]
    public SettingsScriptableObject settingsSO;

    [Header("Saved Things")]
    AGUSMOVEMENT player;

    public GameObject saveIcon;

    private SaveGameData m_CurrentSavedData;

    [Header("Settings Objects")]
    [SerializeField] TMP_Dropdown QualityDropdown;
    [SerializeField] TMP_Dropdown ResolutionDropdown;
    [SerializeField] TMP_Dropdown FPSLimitDropdown;
    [SerializeField] Button FullscreenBTN;
    [SerializeField] Button ParticlesBTN;
    [SerializeField] Button vsyncBTN;
    [SerializeField] Slider MusicSlider;
    [SerializeField] Slider GeneralSlider;
    [SerializeField] AudioMixer mixer;

    [SerializeField] Sprite CheckButton;
    [SerializeField] Sprite CrossButton;

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    private void Start()
    {
        player = FindObjectOfType<AGUSMOVEMENT>();
        SaveSettings();
    }

    public void StartNewGame()
    {
        // Maybe play some sound?
        SceneManager.LoadScene("MapCreation");
    }

    public void ChangeFPSLimitSettingsSO(FPSLimit value)
    {
        settingsSO.fpsLimit = value;
    }

    public void toggleFullScreen()
    {
        settingsSO.FullScreen = !settingsSO.FullScreen;
        if (settingsSO.FullScreen == false)
            FullscreenBTN.GetComponent<Image>().sprite = CrossButton;
        else
            FullscreenBTN.GetComponent<Image>().sprite = CheckButton;
    }

    public void toggleParticles()
    {
        settingsSO.Particles = !settingsSO.Particles;
        if (settingsSO.Particles == false)
            ParticlesBTN.GetComponent<Image>().sprite = CrossButton;
        else
            ParticlesBTN.GetComponent<Image>().sprite = CheckButton;
    }

    public void toggleVSync()
    {
        settingsSO.vSync = !settingsSO.vSync;
        if (settingsSO.vSync == false)
            vsyncBTN.GetComponent<Image>().sprite = CrossButton;
        else
            vsyncBTN.GetComponent<Image>().sprite = CheckButton;
    }

    public void SetMusicLevel(float sliderValue)
    {
        mixer.SetFloat("MusicVol", Mathf.Log10(MusicSlider.value) * 20);
    }
    public void SetGeneralLevel(float sliderValue)
    {
        mixer.SetFloat("GeneralVol", Mathf.Log10(GeneralSlider.value) * 20);
    }

    public void SaveSettings()
    {
        /* ---------------------- SAVE SETTINGS TO SCRIPTABLE OBJECT ----------------------------*/
        // QualityStuff
        switch (QualityDropdown.value.ToString())
        {
            case "4":
                settingsSO.Quality = CALIDAD.MUY_BAJA;
                break;
            case "3":
                settingsSO.Quality = CALIDAD.BAJA;
                break;
            case "2":
                settingsSO.Quality = CALIDAD.MEDIA;
                break;
            case "1":
                settingsSO.Quality = CALIDAD.ALTA;
                break;
            case "0":
                settingsSO.Quality = CALIDAD.MUY_ALTA;
                break;
        }

        // ResolutionStuff
        switch (ResolutionDropdown.value.ToString())
        {
            case "3":
                settingsSO.Resolution = RESOLUCION.RES720;
                break;
            case "2":
                settingsSO.Resolution = RESOLUCION.RES1080;
                break;
            case "1":
                settingsSO.Resolution = RESOLUCION.RES2K;
                break;
            case "0":
                settingsSO.Resolution = RESOLUCION.RES4K;
                break;
        }

        // FPSLimitStuff
        switch (FPSLimitDropdown.value.ToString())
        {
            case "0":
                settingsSO.fpsLimit = FPSLimit.FPS24;
                break;
            case "1":
                settingsSO.fpsLimit = FPSLimit.FPS30;
                break;
            case "2":
                settingsSO.fpsLimit = FPSLimit.FPS50;
                break;
            case "3":
                settingsSO.fpsLimit = FPSLimit.FPS59;
                break;
            case "4":
                settingsSO.fpsLimit = FPSLimit.FPS60;
                break;
            case "5":
                settingsSO.fpsLimit = FPSLimit.FPS90;
                break;
            case "6":
                settingsSO.fpsLimit = FPSLimit.FPS120;
                break;
            case "7":
                settingsSO.fpsLimit = FPSLimit.FPS140;
                break;
            case "8":
                settingsSO.fpsLimit = FPSLimit.FPS_NO_LIMIT;
                break;
        }

        // Fullscreen
        if (FullscreenBTN.GetComponent<Image>().sprite = CrossButton)
        {
            settingsSO.FullScreen = true;
        }
        else
        {
            settingsSO.FullScreen = false;
        }

        // MUSIC and SOUNDS
        settingsSO.MusicVolume = (int)MusicSlider.value;
        settingsSO.GeneralVolume = (int)GeneralSlider.value;
        LoadSettings();
    }

    void LoadSettings()
    {
        /* ---------------------- LOAD SETTINGS ----------------------------*/

        // MUSIC & SOUNDS
        MusicSlider.value = settingsSO.MusicVolume;
        GeneralSlider.value = settingsSO.GeneralVolume;

        // QUALITY
        switch (settingsSO.Quality)
        {
            case CALIDAD.MUY_ALTA:
                QualitySettings.SetQualityLevel(4, true);
                break;
            case CALIDAD.ALTA:
                QualitySettings.SetQualityLevel(3, true);
                break;
            case CALIDAD.MEDIA:
                QualitySettings.SetQualityLevel(2, true);
                break;
            case CALIDAD.BAJA:
                QualitySettings.SetQualityLevel(1, true);
                break;
            case CALIDAD.MUY_BAJA:
                QualitySettings.SetQualityLevel(0, true);
                break;
        }

        // RESOLUTION
        switch (settingsSO.Resolution)
        {
            case RESOLUCION.RES720:
                Screen.SetResolution(1280, 720, settingsSO.FullScreen);
                break;
            case RESOLUCION.RES1080:
                Screen.SetResolution(1920, 1080, settingsSO.FullScreen);
                break;
            case RESOLUCION.RES2K:
                Screen.SetResolution(2560, 1440, settingsSO.FullScreen);
                break;
            case RESOLUCION.RES4K:
                Screen.SetResolution(3840, 2160, settingsSO.FullScreen);
                break;
        }

        // FULLSCREEN
        switch (settingsSO.FullScreen)
        {
            case true:
                Screen.SetResolution(Screen.width, Screen.height, true);
                break;
            case false:
                Screen.SetResolution(Screen.width, Screen.height, false);
                break;
        }

        // FPS LIMIT
        print("FPSs in settings SO: " + settingsSO.fpsLimit);
        switch (settingsSO.fpsLimit)
        {
            case FPSLimit.FPS24:
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = 24;
                break;
            case FPSLimit.FPS30:
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = 30;
                break;
            case FPSLimit.FPS50:
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = 50;
                break;
            case FPSLimit.FPS59:
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = 59;
                break;
            case FPSLimit.FPS60:
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = 60;
                break;
            case FPSLimit.FPS90:
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = 90;
                break;
            case FPSLimit.FPS120:
                QualitySettings.vSyncCount = 0;
                print("changing fps to 120");
                Application.targetFrameRate = 120;
                break;
            case FPSLimit.FPS140:
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = 140;
                break;
            case FPSLimit.FPS_NO_LIMIT:
                QualitySettings.vSyncCount = 0;
                Application.targetFrameRate = 300;
                break;
        }

        // VSYNC (HAVE TO CREATE THE INTERFACE THING FOR THIS)
        switch (settingsSO.vSync)
        {
            case true:
                QualitySettings.vSyncCount = 1;
                break;
            case false:
                QualitySettings.vSyncCount = 0;
                break;
        }
        Application.targetFrameRate = Screen.currentResolution.refreshRate;
    }

    IEnumerator BlinkSaveIcon()
    {
        saveIcon.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        saveIcon.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        saveIcon.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        saveIcon.SetActive(false);
        yield return new WaitForSeconds(0.3f);
        saveIcon.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        saveIcon.SetActive(false);
    }

    [Header("QUESTS SO")]
    public QuestSO questDungeonTutorial;
    public QuestSO questRecolectaItems;
    public QuestSO questPosition;
    public void SaveGame()
    {
        //Al main menu no guardem
        if (SceneManager.GetActiveScene().name != "SaveGameINIT")
        {
            StartCoroutine(BlinkSaveIcon());

            SaveGameData saveGameData = new SaveGameData();
            saveGameData.currentScene = SceneManager.GetActiveScene().name;
            AGUSMOVEMENT player = AGUSMOVEMENT.Instance;

            saveGameData.player.position = player.transform.position;
            saveGameData.player.rotation = player.transform.rotation;
            saveGameData.inventory = InventoryManager.Instance.Items;
            saveGameData.equipment = EquipmentManager.Instance.currentEquipment;
            m_CurrentSavedData.contadorMaxIdObjeto = GameManager.contadorIdObjeto;

            byte[] serializedData = SerializationUtility.SerializeValue<SaveGameData>(saveGameData, DataFormat.JSON);
            if (!encoded)
                File.WriteAllBytes(Application.persistentDataPath + "savegame.json", serializedData);
            else
            {
                string base64 = System.Convert.ToBase64String(serializedData);
                File.WriteAllText(Application.persistentDataPath + "savegame.json", base64, Encoding.UTF8);
            }

            Debug.Log("Desant el fitxer savegame.json");
            Debug.Log(saveGameData);

            LastGameState lastGameState = new LastGameState();
            lastGameState.isNewGame = false;
            lastGameState.questdungeonTutorial = questDungeonTutorial.questState;
            lastGameState.questrecolectaItems = questRecolectaItems.questState;
            lastGameState.questtutorialPosition = questPosition.questState;
            byte[] serializedData3 = SerializationUtility.SerializeValue<LastGameState>(lastGameState, DataFormat.JSON);
            string base644 = System.Convert.ToBase64String(serializedData3);
            File.WriteAllText(Application.persistentDataPath + "lastGameState.json", base644, Encoding.UTF8);


        }
    }
    public void SaveItemsToRestore()
    {
        SaveGameData saveGameData = new SaveGameData();
        saveGameData.currentScene = SceneManager.GetActiveScene().name;
        AGUSMOVEMENT player = FindObjectOfType<AGUSMOVEMENT>();
        saveGameData.inventory = InventoryManager.Instance.Items;
        foreach(ObjectItem a in saveGameData.inventory)
        {
            a.itemModel = null;
        }
        saveGameData.equipment = EquipmentManager.Instance.currentEquipment;
        foreach (ObjectEquip a in saveGameData.equipment)
        {
            if(a)
                if(a?.itemModel)
                    a.itemModel = null;
        }
        m_CurrentSavedData.contadorMaxIdObjeto = GameManager.contadorIdObjeto;

        byte[] serializedData = SerializationUtility.SerializeValue<SaveGameData>(saveGameData, DataFormat.JSON);
        string base64 = System.Convert.ToBase64String(serializedData);
        File.WriteAllText(Application.persistentDataPath + "tmp.json", base64, Encoding.UTF8);
    }
    public void LoadItemsToRestore()
    {
        string newBase64 = File.ReadAllText(Application.persistentDataPath + "tmp.json");
        byte[] serializedData = System.Convert.FromBase64String(newBase64);

        SaveGameData m_CurrentSavedData = SerializationUtility.DeserializeValue<SaveGameData>(serializedData, DataFormat.JSON);

        GameManager.contadorIdObjeto = 0;
        InventoryManager.Instance.Items= new List<ObjectItem>();
        EquipmentManager.Instance.currentEquipment = new List<ObjectEquip>();
        foreach (ObjectEquip currentObject in m_CurrentSavedData.equipment)
        {
            if (currentObject?.DictionarySOID >= -1)
            {
                currentObject.itemModel = m_ObjectsDatabase.Get(currentObject.DictionarySOID);
                InventoryManager.Instance.AddItem(currentObject);
            }
            else
            {
                Debug.Log("InventoryLoad-Referencia :nula");
            }
        }

        foreach (ObjectItem currentObject in m_CurrentSavedData.inventory)
        {
            if (currentObject?.DictionarySOID >= -1)
            {
                currentObject.itemModel = m_ObjectsDatabase.Get(currentObject.DictionarySOID);
                InventoryManager.Instance.AddItem(currentObject);
            }
            else
            {
                Debug.Log("InventoryLoad-Referencia :nula");
            }
        }

        GameManager.contadorIdObjeto = m_CurrentSavedData.contadorMaxIdObjeto;

        InventoryManager.Instance.Items = m_CurrentSavedData.inventory;
        EquipmentManager.Instance.currentEquipment = m_CurrentSavedData.equipment;
        EquipmentManager.Instance.onEquipmentChanged.Invoke();
        InventoryManager.Instance.onInventoryChanged.Invoke();
    }

    public void LoadGame()
    {
        // Change to the game scene since this function must be only called from the starting menu 'Load' button.
        // SceneManager.LoadScene("MapCreation");

        try
        {
            //AGUSMOVEMENT.Instance.PlayerRespawn();

            Debug.Log("Carregant el fitxer: savegame.json");
            byte[] serializedData;
            if (!encoded)
                serializedData = File.ReadAllBytes(Application.persistentDataPath + "savegame.json");
            else
            {
                string newBase64 = File.ReadAllText(Application.persistentDataPath + "savegame.json");
                serializedData = System.Convert.FromBase64String(newBase64);
            }
            SaveGameData m_CurrentSavedData = SerializationUtility.DeserializeValue<SaveGameData>(serializedData, DataFormat.JSON);
            Debug.Log("AQUI");
            AGUSMOVEMENT.Instance.transform.position = m_CurrentSavedData.player.position;
            AGUSMOVEMENT.Instance.transform.rotation = m_CurrentSavedData.player.rotation;
            Debug.Log("AQUI2");
            InventoryManager.Instance.Items = new List<ObjectItem>();
            EquipmentManager.Instance.currentEquipment = new List<ObjectEquip>();
            foreach (ObjectItem currentObject in m_CurrentSavedData.equipment)
            {
                Debug.Log("EquipmentLoad-Referencia DID=" + currentObject?.DictionarySOID + " ID=" + currentObject?.itemID);

                if (currentObject?.DictionarySOID >= -1)
                {
                    currentObject.itemModel = m_ObjectsDatabase.Get(currentObject.DictionarySOID);
                    Debug.Log("EquipmentLoad-Referencia :" + currentObject.itemModel.ItemName);
                    InventoryManager.Instance.AddItem(currentObject);
                }
                else
                {
                    Debug.Log("EquipmentLoad-Referencia :nula");
                }
            }
            Debug.Log("AQUI3");
            foreach (ObjectItem currentObject in m_CurrentSavedData.inventory)
            {
                Debug.Log("InventoryLoad-Referencia DID=" + currentObject?.DictionarySOID + " ID=" + currentObject?.itemID);
                if (currentObject?.DictionarySOID >= -1)
                {
                    currentObject.itemModel = m_ObjectsDatabase.Get(currentObject.DictionarySOID);
                    Debug.Log("InventoryLoad-Referencia :" + currentObject.itemModel.ItemName);
                    InventoryManager.Instance.AddItem(currentObject);
                }
                else
                {
                    Debug.Log("InventoryLoad-Referencia :nula");
                }
            }
            Debug.Log("AQUI4");
            GameManager.contadorIdObjeto = m_CurrentSavedData.contadorMaxIdObjeto;

            

            InventoryManager.Instance.Items = m_CurrentSavedData.inventory;
            EquipmentManager.Instance.currentEquipment = m_CurrentSavedData.equipment;
            EquipmentManager.Instance.onEquipmentChanged.Invoke();
            InventoryManager.Instance.onInventoryChanged.Invoke();
            //SceneManager.LoadScene(m_CurrentSavedData.currentScene);

            //subscribe to the scene loaded
            //SceneManager.sceneLoaded += LoadGameSceneLoaded;
            //change scene to the specified one
            //Debug.Log("Canviant a escena: " + m_CurrentSavedData.currentScene);
            //SceneManager.LoadScene(m_CurrentSavedData.currentScene);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    private void LoadGameSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Debug.Log("Escena carregada en mode LOAD : " + scene.name);
        //we are called because we are loading and the scene has been loaded
        //load values to the player
        //FindObjectOfType<AGUSMOVEMENT>().Load(m_CurrentSavedData);
        //unsubscribe from the onloadscene
        SceneManager.sceneLoaded -= LoadGameSceneLoaded;
    }
}

