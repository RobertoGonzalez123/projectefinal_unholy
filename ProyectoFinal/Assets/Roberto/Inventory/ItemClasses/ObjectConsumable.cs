using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectConsumable : ObjectItem
{
    public int currentQuantity;

    public ObjectConsumable(ConsumableSO modelPasat)
    {
        this.DictionarySOID = modelPasat.DictionaryID;
        this.itemModel = modelPasat;
        this.currentQuantity = 1;
        GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        this.itemID = gameManager.GetNewItemID();
    }

    public override ObjectItem newObject(ItemSO itemPassat)
    {
        return new ObjectConsumable((ConsumableSO)itemPassat);
    }

    public override void Use()
    {
        currentQuantity--;
        ((ConsumableSO)itemModel).MethodToRunOnUse.Invoke();
        if(currentQuantity == 0)
        {
            InventoryManager.Instance.RemoveItem(this);
        }
        InventoryManager.Instance.onInventoryChanged.Invoke();
    }
}
