using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectWeapon : ObjectEquip
{
    public ObjectWeapon(WeaponSO model)
    {
        this.DictionarySOID = model.DictionaryID;
        this.itemModel = model;
        this.currentDurability = model.durability;
        this.currentLevel = 1;
        GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        this.itemID = gameManager.GetNewItemID();
    }

    public override ObjectItem newObject(ItemSO itemModel)
    {
        return new ObjectWeapon((WeaponSO)itemModel);
    }


}
