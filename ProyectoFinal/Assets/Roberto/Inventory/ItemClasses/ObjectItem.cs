using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class ObjectItem : MonoBehaviour
{
    public int itemID;
    public int DictionarySOID;
    public ItemSO itemModel;
    public abstract void Use();

    public abstract ObjectItem newObject(ItemSO itemPassat);

}
