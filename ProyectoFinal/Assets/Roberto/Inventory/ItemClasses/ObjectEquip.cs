using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectEquip : ObjectItem
{
    public int currentLevel;
    public int currentDurability;

    public override void Use()
    {
        print("EQUIPANDO");
        EquipmentManager.Instance.AddEquip(this);
    }
    public void LevelUp()
    {
        this.currentLevel++;
    }

}
