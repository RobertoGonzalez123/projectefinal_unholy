using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMount : ObjectEquip
{
    public ObjectMount(MountSO model)
    {
        this.DictionarySOID = model.DictionaryID;
        this.itemModel = model;
        this.currentDurability = model.durability;
        this.currentLevel = 1;
        this.itemID = GameManager.Instance.GetNewItemID();
    }

    public override ObjectItem newObject(ItemSO itemModel)
    {
        return new ObjectMount((MountSO)itemModel);
    }
}