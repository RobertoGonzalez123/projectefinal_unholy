using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSimpleItem : ObjectItem
{
    public int currentQuantity;

    public ObjectSimpleItem(SimpleItemSO modelPasat)
    {
        this.DictionarySOID = modelPasat.DictionaryID;
        this.itemModel = modelPasat;
        this.currentQuantity = 1;
        GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        this.itemID = gameManager.GetNewItemID();
    }

    public override ObjectItem newObject(ItemSO itemPassat)
    {
        return new ObjectSimpleItem((SimpleItemSO)itemPassat);
    }

    public override void Use()
    {
        Debug.Log("This item does nothing");
    }
}
