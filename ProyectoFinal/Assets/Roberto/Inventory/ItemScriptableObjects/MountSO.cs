using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewMount", menuName = "Inventory/Item/Mount")]
public class MountSO : EquipSO
{
    public float hp;
    public float speed;
    public float stamina;

}