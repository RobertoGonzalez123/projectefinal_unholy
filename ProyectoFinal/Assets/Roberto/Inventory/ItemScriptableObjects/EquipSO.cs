using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EquipSO : ItemSO
{
    public EquipmentType equipmentType;
    public int level;
    public int durability;
    public enum EquipmentType { Helmet, Chest, Pants, Boots, Weapon, Mount }

}


