using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewWeapon", menuName = "Inventory/Item/Weapon")]
public class WeaponSO : EquipSO
{
    public int dmgNormalAttack;
    public int dmgHeavyAttack;
    public int normalAttackStaminaCost;
    public int heavyAttackStaminaCost;
    public float normalAttackTime;
    public float heavyAttackTime;
    public float normalAttackStun;
    public float heavyAttackStun;
    public int criticalHitChance;
    public WeaponSwitchingSystem.WeaponType weaponType;

    public List<Estado.Estados> estadosBase;

    /*
    public override void LevelUP()
    {
        level++;
        dmgNormalAttack =+ (int)(dmgNormalAttack * 0.25f);
        dmgHeavyAttack =+ (int)(dmgHeavyAttack * 0.25f);
    }*/
}
