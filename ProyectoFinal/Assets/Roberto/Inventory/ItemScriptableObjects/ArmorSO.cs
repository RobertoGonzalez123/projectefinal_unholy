using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewArmor", menuName = "Inventory/Item/Armor")]
public class ArmorSO : EquipSO
{
    public float hp;
    public float defense;
    

    /*/public override void LevelUP()
    {
        level++;
        hp += (int)(hp * 0.15f);
        defense += (int)(defense * 0.05f);
    }/*/
}