using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemSO : ScriptableObject
{
    public int DictionaryID;
    public string ItemName;
    public int BuyPrice;
    public bool Sellable;
    public int SellPrice;
    public string ItemDescription;
    public Rarity Rarity;
    public Sprite SlotIcon;

}
public enum Rarity { Common, Uncommon, Rare, Epic, Legendary }






