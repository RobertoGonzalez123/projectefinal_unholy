using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(fileName = "NewConsumable", menuName = "Inventory/Item/Consumable")]
public class ConsumableSO : ItemSO
{
    public int cooldownSeconds;
    public UnityEvent MethodToRunOnUse;
}