using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateInventory : MonoBehaviour
{

    public InventorySlot[] slots;
    [SerializeField] GameObject slotsContainer;

    void Start()
    {

        InventoryManager.Instance.onInventoryChanged += UpdateInventoryUI;
        slots = slotsContainer.GetComponentsInChildren<InventorySlot>();
        UpdateInventoryUI();
    }

    public void UpdateInventoryUI()
    {
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].RemoveItemFromUI();
            if (i < InventoryManager.Instance.Items.Count)
            {
                slots[i].AddItemToUI(InventoryManager.Instance.Items[i]);
            }
        }
    }



}