using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{

    public static InventoryManager Instance;



    public List<ObjectItem> Items = new List<ObjectItem>();
    public int InventoryMaxCapacity;

    [SerializeField] TextMeshProUGUI inventoryFullLabel;
    public GameObject prefabSack;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public delegate void OnInventoryChanged();
    public OnInventoryChanged onInventoryChanged;

    public delegate void OnInventoryFull();
    public OnInventoryFull onInventoryFull;

    private void Start()
    {
        onInventoryFull += ShowInventoryFullText;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            string linea = " ";
            Items.ForEach(e => linea += e?.itemModel?.ItemName + "(" + e?.itemID + ")");
            Debug.Log("CurrentUInventory->" + linea);
        }
    }

    public bool HasItem(ItemSO item)
    {
        foreach (ObjectItem itemInInventory in Items)
        {
            print("itemInInventory " + itemInInventory?.itemModel?.DictionaryID);
            print("item.dictionaryID " + item?.DictionaryID);
            if (itemInInventory?.itemModel?.DictionaryID == item?.DictionaryID) return true;

        }

        return false;
    }

    public bool AddItem(ObjectItem item)
    {
        if (item is ObjectConsumable)
        {
            bool flagExistentePeviamente = false;
            int index = 0;
            foreach (ObjectItem item2 in Items)
            {
                if (item2.itemModel.DictionaryID == item.itemModel.DictionaryID)
                {
                    flagExistentePeviamente = true;
                    break;
                }
                index++;
            }
            if (flagExistentePeviamente)
            {
                ((ObjectConsumable)Items.ElementAt(index)).currentQuantity += ((ObjectConsumable)item).currentQuantity;
            }
            else
            {
                if (Items.Count >= InventoryMaxCapacity)
                {
                    onInventoryFull?.Invoke();
                    return false;
                }
                Items.Add(item);
            }
        }
        else if (item is ObjectSimpleItem)
        {
            bool flagExistentePeviamente = false;
            int index = 0;
            foreach (ObjectItem item2 in Items)
            {
                if (item2.itemModel.DictionaryID == item.itemModel.DictionaryID)
                {
                    flagExistentePeviamente = true;
                    break;
                }
                index++;
            }
            if (flagExistentePeviamente)
            {
                ((ObjectSimpleItem)Items.ElementAt(index)).currentQuantity += ((ObjectSimpleItem)item).currentQuantity;
            }
            else
            {
                if (Items.Count >= InventoryMaxCapacity)
                {
                    onInventoryFull?.Invoke();
                    return false;
                }
                Items.Add(item);
            }
        }
        else
        {
            if (Items.Count >= InventoryMaxCapacity)
            {
                onInventoryFull?.Invoke();
                return false;
            }
            Items.Add(item);
        }
        onInventoryChanged?.Invoke();
        return true;
    }

    public void RemoveItem(ObjectItem item)
    {
        string itemsTotals = "MIRAR ACTUAL ANTES->";
        foreach (ObjectItem item2 in Items) { itemsTotals += item2.itemModel.ItemName + ","; }
        Debug.Log(itemsTotals);

        Debug.Log("MIRAR ITEM" + item.itemModel.ItemName);

        int index = 0;
        foreach (ObjectItem item2 in Items)
        {
            if (item2.itemID == item.itemID) break;
            index++;
        }

        Items.RemoveAt(index);

        itemsTotals = "MIRAR ACTUAL DESPUES->";
        foreach (ObjectItem item2 in Items) { itemsTotals += item2.itemModel.ItemName + ","; }
        Debug.Log(itemsTotals);

        onInventoryChanged?.Invoke();
    }

    public void RemoveItem(ItemSO item)
    {
        string itemsTotals = "MIRAR ACTUAL ANTES->";
        foreach (ObjectItem item2 in Items) { itemsTotals += item2?.itemModel?.ItemName + ","; }
        Debug.Log(itemsTotals);

        //Debug.Log("MIRAR ITEM" + item.itemModel.ItemName);

        int index = 0;
        foreach (ObjectItem item2 in Items)
        {
            if (item2?.itemModel?.DictionaryID == item?.DictionaryID) break;
            index++;
        }

        Items.RemoveAt(index);

        itemsTotals = "MIRAR ACTUAL DESPUES->";
        foreach (ObjectItem item2 in Items) { itemsTotals += item2?.itemModel?.ItemName + ","; }
        Debug.Log(itemsTotals);

        onInventoryChanged?.Invoke();
    }

    //  TO-DO Change transform position
    public void DropItem(ObjectItem item)
    {
        GameObject tmpDroppedItem = Instantiate(prefabSack, this.transform);
        tmpDroppedItem.GetComponent<ItemInteractuableObject>().item = item;
        RemoveItem(item);
    }

    public void ShowInventoryFullText()
    {
        inventoryFullLabel.gameObject.SetActive(true);
        StartCoroutine(HideText());
    }

    IEnumerator HideText()
    {
        yield return new WaitForSeconds(2);
        inventoryFullLabel.gameObject.SetActive(false);
    }
}
