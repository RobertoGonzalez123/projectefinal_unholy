using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MovementValues", menuName = "Player/Movement")]
public class MovementValues : ScriptableObject
{
    [SerializeField] float staminaRecoveryTime;
    [SerializeField] float staminaConsumerTime;
    [SerializeField] float jumpCooldown;
    [SerializeField] float cancelSprintCooldown;
    [SerializeField] float slerpVelocity;
    [SerializeField] float groundPerceptionSensor;

    [SerializeField] float manaRecoveryTime;
    [SerializeField] float cancelManaCooldown;


    public float StaminaRecoveryTime { get => staminaRecoveryTime; }
    public float StaminaConsumerTime { get => staminaConsumerTime; }
    public float JumpCooldown { get => jumpCooldown; }
    public float CancelSprintCooldown { get => cancelSprintCooldown; }
    public float SlerpVelocity { get => slerpVelocity; }
    public float GroundPerceptionSensor { get => groundPerceptionSensor; }
    public float ManaRecoveryTime { get => manaRecoveryTime; set => manaRecoveryTime = value; }
    public float CancelManaCooldown { get => cancelManaCooldown; set => cancelManaCooldown = value; }
}
