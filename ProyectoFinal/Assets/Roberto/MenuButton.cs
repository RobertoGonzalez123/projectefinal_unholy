using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour
{
    MenuManager menuManager;
    Button btnComponent;

    void Start()
    {
        menuManager = GetComponentInParent<MenuManager>();
        btnComponent = GetComponent<Button>();
        btnComponent.onClick.AddListener(ChangeMenu);
    }

    public void ChangeMenu()
    {

        print("ejecutando " + transform.name);
        menuManager.OpenMenu(transform.name);

    }


}
