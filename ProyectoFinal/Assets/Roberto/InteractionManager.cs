using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InteractionManager : MonoBehaviour
{
    public List<Interactuable> Interactuables = new List<Interactuable>();
    public Interactuable currentInteractuable;
    public Transform playerTransform;

    public static InteractionManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

    }

    private void Start()
    {
        playerTransform = GetComponentInParent<Transform>();
    }

    private void Update()
    {
        if (Interactuables.Count > 0)
        {
            RefreshInteractuable();

        }
    }

    public void RefreshInteractuable()
    {
        if (Interactuables.Count != 0)
        {
            Interactuable itemMasCerca = Interactuables[0];
            if (itemMasCerca)
            {
                float distanciaItemMasCerca = Vector3.Distance(playerTransform.position, Interactuables[0].transform.position);
                for (int i = 0; i < Interactuables.Count; i++)
                {
                    float currentDistance = Vector3.Distance(playerTransform.position, Interactuables[i].transform.position);
                    if (currentDistance < distanciaItemMasCerca)
                    {
                        distanciaItemMasCerca = currentDistance;
                        itemMasCerca = Interactuables[i];
                    }
                }
                currentInteractuable = itemMasCerca;
                for (int i = 0; i < Interactuables.Count; i++)
                {
                    if (itemMasCerca == Interactuables[i])
                    {
                        Interactuables[i].transform.GetComponent<Outline>().enabled = true;
                    }
                    else
                    {
                        Interactuables[i].transform.GetComponent<Outline>().enabled = false;
                    }
                }
            }
        }
        if (currentInteractuable == null)
        {
            foreach (Interactuable interactuable in Interactuables)
            {
                interactuable.transform.GetComponent<Outline>().enabled = false;
            }
        }
    }

    // registra un nou interactuable a la llista y el fica com el interactuable actual.
    public void RegisterInteractuable(Interactuable interactuable)
    {
        Interactuables.Add(interactuable);
    }

    // quita un interactuable de la llista y si es el que estaba com interactuable actual fica l'ultim interactuable de la llista com a interactuable actual.
    public void UnregisterInteractuable(Interactuable interactuable)
    {
        interactuable.transform.GetComponent<Outline>().enabled = false;
        Interactuables.Remove(interactuable);
        if (Interactuables.Count > 0 && currentInteractuable == interactuable)
        {
            currentInteractuable = Interactuables[Interactuables.Count - 1];
        }
        else
        {
            currentInteractuable = null;
        }
    }

    // llama a la funcion interactuar del interactuable.
    public void InvokeCurrentInteractuable()
    {
        if (currentInteractuable != null)
        {
            bool result = currentInteractuable.Interact();
            print(result);
            if (result)
            {
                Destroy(currentInteractuable.gameObject);
                UnregisterInteractuable(currentInteractuable);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Item" || other.tag == "NPC" || other.tag == "Chest" || other.tag == "Waypoint" || other.tag == "TamingHorse")
        {
            RegisterInteractuable(other.GetComponent<Interactuable>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Item" || other.tag == "NPC" || other.tag == "Chest" || other.tag == "Waypoint" || other.tag == "TamingHorse")
        {
            UnregisterInteractuable(other.GetComponent<Interactuable>());
        }
    }

    public void OnInteract(InputAction.CallbackContext context)
    {
        InvokeCurrentInteractuable();
    }
}
