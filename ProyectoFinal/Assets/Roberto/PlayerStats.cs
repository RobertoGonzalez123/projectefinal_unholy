using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PlayerStats", menuName = "Player/PlayerStats")]
public class PlayerStats : ScriptableObject
{
    // variables de movimiento
    [SerializeField] float _WALK_SPEED;
    [SerializeField] float _RUN_SPEED;
    [SerializeField] float _STRAFE_SPEED;

    [SerializeField] float _MAX_STAMINA;
    [SerializeField] float _STAMINA_CONSUME_MODIFIER;
    [SerializeField] float _STAMINA_CONSUMED_RUNNING;
    [SerializeField] float _STAMINA_CONSUMED_JUMPING;
    [SerializeField] float _STAMINA_CONSUMED_DASHING;


    [SerializeField] int _JUMP_FORCE;
    [SerializeField] float _MAX_HP;
    [SerializeField] float _MAX_DEXTERITY;



    public float WALK_SPEED { get => _WALK_SPEED; set => _WALK_SPEED = value; }
    public float RUN_SPEED { get => _RUN_SPEED; set => _RUN_SPEED = value; }
    public float MAX_STAMINA { get => _MAX_STAMINA; set => _MAX_STAMINA = value; }
    public float STAMINA_CONSUMED_RUNNING { get => _STAMINA_CONSUMED_RUNNING; set => _STAMINA_CONSUMED_RUNNING = value; }
    public float STAMINA_CONSUMED_JUMPING { get => _STAMINA_CONSUMED_JUMPING; set => _STAMINA_CONSUMED_JUMPING = value; }
    public float STAMINA_CONSUMED_DASHING { get => _STAMINA_CONSUMED_DASHING; set => _STAMINA_CONSUMED_DASHING = value; }
    public int JUMP_FORCE { get => _JUMP_FORCE; set => _JUMP_FORCE = value; }
    public float MAX_HP { get => _MAX_HP; set => _MAX_HP = value; }
    public float MAX_DEXTERITY { get => _MAX_DEXTERITY; set => _MAX_DEXTERITY = value; }
    public float STAMINA_CONSUME_MODIFIER { get => _STAMINA_CONSUME_MODIFIER; set => _STAMINA_CONSUME_MODIFIER = value; }
    public float STRAFE_SPEED { get => _STRAFE_SPEED; set => _STRAFE_SPEED = value; }
}
