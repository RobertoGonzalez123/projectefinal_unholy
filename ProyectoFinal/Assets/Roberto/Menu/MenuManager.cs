using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField] GameObject TopHud;
    [SerializeField] GameObject menuContent;
    [SerializeField] GameObject menuNavigationNames;

    public GameObject[] menus;
    public Button[] buttonsNavigation;
    public List<string> buttonNames;

    public string currentIndex;

    [SerializeField] EventSystem eventSystem;

    [SerializeField] GameObject MinimapGroup;

    [SerializeField] InputSystemUIInputModule inputModule;
    private void Awake()
    {
        //menuContent = transform.GetChild(0).gameObject;
        buttonsNavigation = menuNavigationNames.GetComponentsInChildren<Button>();
        SetNavigationNames();
    }

    public void SetNavigationNames()
    {
        for (int i = 0; i < buttonsNavigation.Length; i++)
        {
            GameObject buttonName = buttonsNavigation[i].gameObject;
            TextMeshProUGUI buttonText = buttonName.GetComponentInChildren<TextMeshProUGUI>();
            string nameToBoth = buttonNames[i];

            buttonName.name = nameToBoth;
            buttonText.text = nameToBoth;
        }
    }

    public void CloseCurrentMenu()
    {
        foreach (var menu in menus)
        {
            eventSystem.SetSelectedGameObject(null);
            menu.gameObject.SetActive(false);
        }
    }

    public void CloseMenu()
    {
        MinimapGroup.SetActive(true);
        menuContent.SetActive(false);
        currentIndex = buttonNames[1];
        GameManager.Instance.menuOpened = false;
        GameManager.Instance.MainMenuOpened = false;
        TopHud.SetActive(true);
        MinimapGroup.SetActive(true);
        CloseCurrentMenu();
    }

    public void OpenMenu(string menuName)
    {
        //if (GameManager.Instance.ConsoleOpened)
        //{

        foreach (var menu in menus)
        {
            if (menu.transform.name == menuName)
            {
                CloseCurrentMenu();
                menu.gameObject.SetActive(true);
                GameManager.Instance.MainMenuOpened = false;
                GameManager.Instance.menuOpened = true;
                currentIndex = menuName;
                eventSystem.SetSelectedGameObject(menu);
                //GameManager.Instance.CanConsoleBeOpened = false;

                foreach (var button in buttonsNavigation)
                {
                    if (button.transform.name == menu.transform.name)
                    {
                        button.GetComponent<Button>().Select();
                        button.GetComponentInChildren<TextMeshProUGUI>().color = new Color(255f, 215f, 0f);
                    }
                    else
                    {
                        button.GetComponentInChildren<TextMeshProUGUI>().color = new Color(255f, 255f, 255f);
                    }
                }
            }
        }
        //}
    }



    public void NavigateLeftInMenu(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            if (menuContent.gameObject.activeSelf)
            {
                int currentIndex = GetCurrentMenuIndexOpened();
                currentIndex -= 2;
                if (currentIndex < 0)
                {
                    currentIndex = transform.childCount - 2;
                }
                OpenMenu(buttonNames[currentIndex]);
            }
        }
    }

    public void NavigateRightInMenu(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            if (menuContent.gameObject.activeSelf)
            {
                int currentIndex = GetCurrentMenuIndexOpened();

                if (currentIndex == transform.childCount - 1)
                {
                    currentIndex = 0;
                }
                OpenMenu(buttonNames[currentIndex]);
            }
        }
    }

    public void OnClose(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            TopHud.SetActive(true);
            MinimapGroup.SetActive(true);
            int currentIndex = GetCurrentMenuIndexOpened();
            OpenMenu(buttonNames[currentIndex]);
        }
    }

    public void OnOpenMainMenu(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            if (!GameManager.Instance.menuOpened)
            {
                OpenMenuWithName(1);
            }
        }
    }
    public void ToggleMap(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            if (!GameManager.Instance.menuOpened)
                OpenMenuWithName(0);
        }
    }
    public void ToggleInventory(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            if (!GameManager.Instance.menuOpened)
                OpenMenuWithName(1);
        }
    }
    public void ToggleQuests(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            if (!GameManager.Instance.menuOpened)
                OpenMenuWithName(2);
        }
    }
    public void ToggleAbilities(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            if (!GameManager.Instance.menuOpened)
                OpenMenuWithName(3);
        }
    }

    int GetCurrentMenuIndexOpened()
    {
        // Iterate through all childs in the MenuContent object, without the first one
        // If the object is active it means it is the current panel opened. Return the index
        for (int i = 1; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf) return i;
        }
        return 1;
    }

    public void OpenMenuWithName(int index)
    {
        if (!menuContent.activeSelf)
        {
            MinimapGroup.SetActive(false);
            TopHud.SetActive(false);
            menuContent.SetActive(true);
            OpenMenu(buttonNames[index]);

        }
        else if (menuContent.activeSelf && currentIndex == buttonNames[index])
        {
            MinimapGroup.SetActive(true);
            TopHud.SetActive(true);
            menuContent.SetActive(false);
            CloseCurrentMenu();
        }
        else
        {
            MinimapGroup.SetActive(false);
            TopHud.SetActive(false);
            menuContent.SetActive(true);
            OpenMenu(buttonNames[index]);
        }
    }



}