using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameDefaultStates", menuName = "GameStates")]
public class GameStatesDefaultDurations : ScriptableObject
{
    [SerializeField] float fireStateDuration;
    [SerializeField] float poisonStateDuration;
    [SerializeField] float corrosionStateDuration;
    [SerializeField] float electricityStateDuration;
    [SerializeField] float stunStateDuration;

    public float FireStateDuration { get => fireStateDuration; }
    public float PoisonStateDuration { get => poisonStateDuration; }
    public float CorrosionStateDuration { get => corrosionStateDuration; }
    public float ElectricityStateDuration { get => electricityStateDuration; }
    public float StunStateDuration { get => stunStateDuration; }
}
