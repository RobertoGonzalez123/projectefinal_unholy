using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static AudioClip jumpSound, gangnamSound, amongusSound, amongusSound0, mainSong;
    static AudioSource audioSrc;
    // Start is called before the first frame update
    void Start()
    {
        //jumpSound = Resources.Load<AudioClip>("jumpsound");
        //gangnamSound = Resources.Load<AudioClip>("gangnamsound");
        //amongusSound = Resources.Load<AudioClip>("amongussound");
        //amongusSound0 = Resources.Load<AudioClip>("amongussound0");
        audioSrc = GetComponent<AudioSource>();
        mainSong = Resources.Load<AudioClip>("mainSong");
        audioSrc.Play();
    }

    public static void PlaySound(string sound)
    {
        switch (sound)
        {
            /*
            case "jumpsound":
                audioSrc.PlayOneShot(jumpSound);
                break;
            case "gangnamsound":
                audioSrc.PlayOneShot(gangnamSound);
                break;
            case "amongussound":
                audioSrc.PlayOneShot(amongusSound);
                break;
            case "amongussound0":
                audioSrc.PlayOneShot(amongusSound0);
                break;
            */
            default:
                break;
        }
    }
}
