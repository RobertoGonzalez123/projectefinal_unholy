using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Estado;

public class EstadoTemporal
{
    public Estados tipoEstado;
    public float timeRemaing;

    public EstadoTemporal(Estados tipoEstado, float timeRemaing)
    {
        this.tipoEstado = tipoEstado;
        this.timeRemaing = timeRemaing;
    }

}
