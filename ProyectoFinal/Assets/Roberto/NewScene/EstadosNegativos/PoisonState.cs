using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonState : Estado
{

    public PoisonState(Estados estado, float dmg, float defaultDamageTickRate, float timeRemaning) : base(estado, dmg, defaultDamageTickRate, timeRemaning)
    {
        stateDamageModifier = 0.1f;
    }
    public PoisonState(Estados estado, float dmg, float timeRemaning) : base(estado, dmg, timeRemaning)
    {
        stateDamageModifier = 0.1f;
    }

    public override void AplicarEstado(Damageable gameObj)
    {
        Debug.Log("DMG: " + dmg + " " + this.GetType());
        if (damageTickRate <= 0)
        {
            gameObj.ReceiveDmgToHealth(GetEstadoDamage());
            damageTickRate = defaultDamageTickRate;
        }
    }

}