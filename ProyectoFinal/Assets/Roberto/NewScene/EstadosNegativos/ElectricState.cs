using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ElectricState : Estado
{

    float applyStunTime = 30f;
    float stunDurationTime = 1.5f;
    bool stunAplicado = false;

    public ElectricState(Estados estado, float dmg, float defaultDamageTickRate, float timeRemaning) : base(estado, dmg, defaultDamageTickRate, timeRemaning)
    {
        stateDamageModifier = 0.08f;
    }
    public ElectricState(Estados estado, float dmg, float timeRemaning) : base(estado, dmg, timeRemaning)
    {
        stateDamageModifier = 0.08f;
    }

    public override void AplicarEstado(Damageable gameObj)
    {
        if (damageTickRate <= 0)
        {
            gameObj.ReceiveDmgToHealth(GetEstadoDamage());
            damageTickRate = defaultDamageTickRate;
        }
        if (timeRemaining <= applyStunTime && !stunAplicado)
        {
            stunAplicado = true;
            gameObj.Stun(stunDurationTime);
        }
    }
}