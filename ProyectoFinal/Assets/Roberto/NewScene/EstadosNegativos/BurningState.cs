using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurningState : Estado
{
    public BurningState(Estados estado, float dmg, float defaultDamageTickRate, float timeRemaning) : base(estado, dmg, defaultDamageTickRate, timeRemaning)
    {
        stateDamageModifier = 0.15f;
    }
    public BurningState(Estados estado, float dmg, float timeRemaning) : base(estado, dmg, timeRemaning)
    {
        stateDamageModifier = 0.15f;
    }

    public override void AplicarEstado(Damageable gameObj)
    {
        if (damageTickRate <= 0)
        {
            gameObj.ReceiveDmgToHealth(GetEstadoDamage());
            damageTickRate = defaultDamageTickRate;
        }
    }
}
