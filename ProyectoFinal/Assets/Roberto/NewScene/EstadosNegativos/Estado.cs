using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class Estado
{
    public float dmg;
    public Estados tipoEstado;
    public float timeRemaining = 100f;
    public float damageTickRate = 0.5f;
    public float defaultDamageTickRate = 0.5f;
    public float stateDamageModifier = 0.1f;


    public Estado(Estados estado, float dmg, float defaultDamageTickRate, float timeRemaning)
    {
        this.tipoEstado = estado;
        this.timeRemaining = timeRemaning;
        this.dmg = dmg;
        this.defaultDamageTickRate = defaultDamageTickRate;
        this.damageTickRate = defaultDamageTickRate;
    }
    public Estado(Estados estado, float dmg, float timeRemaning)
    {
        this.tipoEstado = estado;
        this.timeRemaining = timeRemaning;
        this.dmg = dmg;
        this.damageTickRate = defaultDamageTickRate;
    }

    public enum Estados
    {
        Poison, Electric, Hemorrhage, Corrosion, Burning
    }

    public virtual void AplicarEstado(Damageable gameObj) { }

    public static Estado GetEstado(Estados tipoEstado, float dmg)
    {
        switch (tipoEstado)
        {
            case Estados.Poison:
                return new PoisonState(Estados.Poison, dmg, 0.3f, 100);
            case Estados.Electric:
                return new ElectricState(Estados.Electric, dmg, 0.2f, 100);
            case Estados.Hemorrhage:
                return new HemorrhageState(Estados.Hemorrhage, dmg, 0.5f, 100);
            case Estados.Corrosion:
                return new CorrosionState(Estados.Corrosion, dmg, 0.1f, 100);
            case Estados.Burning:
                return new BurningState(Estados.Burning, dmg, 0.2f, 100);
            default:
                break;
        }
        return null;
    }

    protected float GetEstadoDamage()
    {
        return dmg * stateDamageModifier;
    }
}
