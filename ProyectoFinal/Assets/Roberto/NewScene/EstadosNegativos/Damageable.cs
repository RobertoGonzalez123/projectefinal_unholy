using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using static Hurtbox;

public class Damageable : MonoBehaviour
{
    [SerializeField] protected Image healthBar;
    protected float currentHP;
    protected DoDamageStats myCurrentDamageStats;

    public virtual float CurrentHP { get => currentHP; set => currentHP = value; }

    private void Start()
    {
        healthBar.fillAmount = 1f;
    }
    public virtual void ReceiveDmgToHealth(float dmg) { }

    public virtual void Stun(float time) { }


    public virtual void ReceiveDamage(float damageReceived)
    {
        ReceiveDmgToHealth(damageReceived);
    }
    public virtual void ReceiveDamage(DoDamageStats damageStats)
    {
        Debug.Log("ENTRA EN EL DAMAGEABLE Y YHO QUIERO QUE ENTRE EN EL ENEMYBEHAVIOUR(OVERRIDE)");
    }
    public virtual void ReceiveDamageFromEntity(Damageable entity, DoDamageStats doDamageStats) { }

    public virtual DoDamageStats DoDamage() { return myCurrentDamageStats; }


    //ESTADOS CON VARIABLES Y FUNCIONES COMUNES DE ESTADOS
    [SerializeField] public List<Estado> currentStates = new List<Estado>();

    public float tickRate = 0.1f;

    IEnumerator tickRateEstadosCoroutine = null;
    IEnumerator aplicarEstadosCoroutine = null;

    IEnumerator TickRateEstados()
    {
        while (currentStates.Count > 0)
        {
            yield return new WaitForSeconds(tickRate);
            List<Estado> currentStatesTmp = new List<Estado>(currentStates);
            foreach (Estado estado in currentStates)
            {
                estado.timeRemaining--;
                if (estado.timeRemaining <= 0)
                {
                    currentStatesTmp.Remove(estado);

                }
            }
            currentStates = currentStatesTmp;
        }
    }

    IEnumerator AplicarCurrentStates()
    {
        while (currentStates.Count > 0)
        {
            yield return new WaitForSeconds(tickRate);
            foreach (Estado estado in currentStates)
            {
                estado.damageTickRate -= tickRate;
                estado.AplicarEstado(this);

            }
        }
    }


    public void CheckApplyStates()
    {
        if (aplicarEstadosCoroutine == null)
        {
            aplicarEstadosCoroutine = AplicarCurrentStates();
            StartCoroutine(aplicarEstadosCoroutine);
        }
        else
        {
            StopCoroutine(aplicarEstadosCoroutine);
            aplicarEstadosCoroutine = AplicarCurrentStates();
            StartCoroutine(aplicarEstadosCoroutine);
        }

        if (tickRateEstadosCoroutine == null)
        {
            tickRateEstadosCoroutine = TickRateEstados();
            StartCoroutine(tickRateEstadosCoroutine);
        }
        else
        {
            StopCoroutine(tickRateEstadosCoroutine);
            tickRateEstadosCoroutine = TickRateEstados();
            StartCoroutine(tickRateEstadosCoroutine);
        }
    }
}
