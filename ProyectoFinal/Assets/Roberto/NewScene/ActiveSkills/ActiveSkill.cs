using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ActiveSkill : MonoBehaviour
{
    [SerializeField] public SkillPropertie properties;
    [Header("Commun Skills")]
    public float defaultCooldown;
    public float skillDuration;
    public float currentCooldown;
    [Header("Mana")]
    public float manaConsumed;



    //public ActiveSkill(float defaultCooldown)
    //{
    //    this.defaultCooldown = defaultCooldown;
    //    currentCooldown = 0;
    //}
    public virtual void Use()
    {
        print("Usando " + this.GetType() + "");
    }

    //public void HabilityCooldown()
    //{
    //    Debug.Log("lo del overrride funciona perfecto");
    //    currentCooldown = defaultCooldown;
    //    StartCoroutine(CooldownSkillCoroutine());
    //}

    //public IEnumerator CooldownSkillCoroutine()
    //{
    //    yield return new WaitForSeconds(0.1f);
    //    if (currentCooldown > 0)
    //    {
    //        currentCooldown -= Time.deltaTime;
    //        print("Habilidad " + this.GetType() + "en cooldown" + currentCooldown);
    //    }

    //}
    public float Cooldown
    {
        get { return currentCooldown; }
        set
        {
            if (value < 0)
            {
                currentCooldown = 0;
            }
            else
            {
                currentCooldown = value;
            }
        }
    }

}

