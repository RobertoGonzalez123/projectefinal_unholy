using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SkillSlot : MonoBehaviour, IDeselectHandler
{
    public ActiveSkill skill;
    public Button boton;
    public EquipedSkillsManager equipedSkillsManager;

    public void ChangeImage()
    {
        if (skill?.properties.sprite)
        {
            this.GetComponent<Image>().sprite = skill.properties.sprite;
            this.GetComponent<Image>().color = skill.properties.spriteColor;
        }
    }
    private void Awake()
    {
        equipedSkillsManager = GetComponentInParent<EquipedSkillsManager>();
    }

    public void OnDeselect(BaseEventData eventData)
    {
        equipedSkillsManager.ClearDescription();
    }
}
