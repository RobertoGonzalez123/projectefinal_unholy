using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SkillPropertie : ScriptableObject
{

    public string skillName;
    public string skillDescription;
    public Sprite sprite;
    public Color spriteColor = Color.white;

}
