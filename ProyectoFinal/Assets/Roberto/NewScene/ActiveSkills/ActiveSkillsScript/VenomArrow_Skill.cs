using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenomArrow_Skill : ActiveSkill
{
    [SerializeField] GameObject venomArrowPrefab;
    AGUSMOVEMENT m_PlayerBehaviour;
    [SerializeField] float speed;
    [SerializeField] float pisonZoneDuration;

    private void Start()
    {
        m_PlayerBehaviour = GetComponentInParent<AGUSMOVEMENT>();
        currentCooldown = 0;
    }

    public override void Use()
    {
        base.Use();

        GameObject newArrow = Instantiate(venomArrowPrefab, m_PlayerBehaviour.transform.position + new Vector3(0, 0, 0) + (m_PlayerBehaviour.playerObj.transform.forward * 3), m_PlayerBehaviour.playerObj.rotation);
        newArrow.GetComponent<VenomArrow>().EnableVenomArrow(m_PlayerBehaviour.DoDamage().dmg, speed, skillDuration, pisonZoneDuration, m_PlayerBehaviour);

    }
}
