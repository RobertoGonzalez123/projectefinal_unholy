using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTornado_Skill : ActiveSkill
{
    [SerializeField] GameObject tornadoPrefab;
    AGUSMOVEMENT m_PlayerBehaviour;
    [SerializeField] float speed;

    private void Start()
    {
        m_PlayerBehaviour = GetComponentInParent<AGUSMOVEMENT>();
        currentCooldown = 0;
    }

    public override void Use()
    {
        base.Use();

        GameObject newTornado = Instantiate(tornadoPrefab, m_PlayerBehaviour.transform.position + new Vector3(0, -0.75f, 0) + (m_PlayerBehaviour.playerObj.transform.forward * 3), m_PlayerBehaviour.playerObj.rotation);
        newTornado.GetComponent<FireTornado>().EnableFireTornado(m_PlayerBehaviour.DoDamage().dmg, skillDuration, speed);

    }
}
