using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorBarrier_Skill : ActiveSkill
{
    [SerializeField] GameObject sphereVisualEffect;
    AGUSMOVEMENT m_PlayerBehaviour;

    private void Start()
    {
        m_PlayerBehaviour = GetComponentInParent<AGUSMOVEMENT>();
        currentCooldown = 0;
    }

    public override void Use()
    {
        base.Use();
        m_PlayerBehaviour.ActivateReflectDamage(skillDuration);
        StartCoroutine(VisualEffectCoroutine());
    }

    IEnumerator VisualEffectCoroutine()
    {
        sphereVisualEffect.SetActive(true);
        yield return new WaitForSeconds(skillDuration);
        sphereVisualEffect.SetActive(false);
    }


}
