using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectrifiedCoating_Skill : ActiveSkill
{
    AGUSMOVEMENT m_PlayerBehaviour;
    private void Start()
    {
        m_PlayerBehaviour = GetComponentInParent<AGUSMOVEMENT>();
        currentCooldown = 0;
    }

    public override void Use()
    {
        base.Use();
        ObjectWeapon weapon = m_PlayerBehaviour.GetCurrentWeapon();
        if (weapon?.itemModel)
        {
            bool estaAplicadoElEstadoActualmente = false;
            foreach (EstadoTemporal estado in m_PlayerBehaviour.estadosTemporales)
            {
                if (estado.tipoEstado == Estado.Estados.Electric)
                {
                    estaAplicadoElEstadoActualmente = true;
                }
            }
            if (!estaAplicadoElEstadoActualmente)
            {
                m_PlayerBehaviour.ApplyState(Estado.Estados.Electric, skillDuration);
            }
        }
    }
}
