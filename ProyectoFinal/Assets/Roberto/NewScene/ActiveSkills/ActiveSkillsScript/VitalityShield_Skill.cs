using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VitalityShield_Skill : ActiveSkill
{
    AGUSMOVEMENT m_PlayerBehaviour;
    [SerializeField] float healingTick;
    [SerializeField] float immunityDuration;
    [SerializeField] float healPercentageMaxHp;
    float timeRemaning;
    private void Start()
    {
        m_PlayerBehaviour = GetComponentInParent<AGUSMOVEMENT>();
        currentCooldown = 0;
    }
    public override void Use()
    {
        base.Use();
        timeRemaning = skillDuration;
        m_PlayerBehaviour.dmgInvulnerable = true;
        StartCoroutine(ImmunityTimeRemaning());
        StartCoroutine(HealingCoroutine());
    }
    private void Update()
    {
        timeRemaning -= Time.deltaTime;
    }

    IEnumerator ImmunityTimeRemaning()
    {
        yield return new WaitForSeconds(immunityDuration);
        m_PlayerBehaviour.dmgInvulnerable = false;
    }
    IEnumerator HealingCoroutine()
    {
        while (timeRemaning > 0)
        {
            yield return new WaitForSeconds(healingTick);
            m_PlayerBehaviour.GetComponent<Damageable>().CurrentHP += PlayerMaxStatsRuntime.Instance.MAX_HP * 0.05f;
        }
    }
}
