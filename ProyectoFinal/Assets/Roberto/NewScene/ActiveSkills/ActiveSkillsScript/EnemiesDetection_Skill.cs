using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemiesDetection_Skill : ActiveSkill

{
    public Collider detectionRange;
    public IEnumerator corrutinaDetectarEnemigos = null;
    public List<GameObject> enemigosRevelados = new List<GameObject>();

    private void Start()
    {
        currentCooldown = 0;
    }


    public override void Use()
    {
        base.Use();

        if (corrutinaDetectarEnemigos != null)
        {
            StopCoroutine(corrutinaDetectarEnemigos);
            corrutinaDetectarEnemigos = DetenerDetectarEnemigos();
            StartCoroutine(corrutinaDetectarEnemigos);
            detectionRange.enabled = true;
        }
        else if (corrutinaDetectarEnemigos == null)
        {
            corrutinaDetectarEnemigos = DetenerDetectarEnemigos();
            StartCoroutine(corrutinaDetectarEnemigos);
            detectionRange.enabled = true;
        }


    }

    public IEnumerator DetenerDetectarEnemigos()
    {
        yield return new WaitForSeconds(skillDuration);
        detectionRange.enabled = false;
        foreach (GameObject enemy in enemigosRevelados)
        {
            if (enemy != null)
            {
                enemy.GetComponent<Outline>().enabled = false;
            }

        }
        enemigosRevelados.Clear();
    }

}
