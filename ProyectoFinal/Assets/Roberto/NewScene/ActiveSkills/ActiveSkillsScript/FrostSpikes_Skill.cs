using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class FrostSpikes_Skill : ActiveSkill
{

    [SerializeField] GameObject frostSpikes;
    [SerializeField] AGUSMOVEMENT m_PlayerBehaviour;
    private void Start()
    {

        m_PlayerBehaviour = GetComponentInParent<AGUSMOVEMENT>();
        currentCooldown = 0;
    }

    public override void Use()
    {
        base.Use();
        InstanciateFrostSpikes();
    }

    private void InstanciateFrostSpikes()
    {
        GameObject newFrostSpikes = Instantiate(frostSpikes, m_PlayerBehaviour.transform.position + new Vector3(0, -0.75f, 0) + (m_PlayerBehaviour.playerObj.transform.forward * 3), m_PlayerBehaviour.playerObj.rotation);
        newFrostSpikes.GetComponent<FrostSpikes>().EnableFrostSpikes(m_PlayerBehaviour.DoDamage().dmg);
    }



}
