using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using static EquipedSlotSkill;
using static Hurtbox;

public class EquipedSkillsManager : MonoBehaviour
{
    public EquipedSlotSkill currentSelected;
    public EquipedSlotSkill[] canvasCurrentEquipedSkills;
    [SerializeField] SkillManager skillManager;
    [SerializeField] GameObject skillsSlots;
    [SerializeField] TextMeshProUGUI skillName;
    [SerializeField] TextMeshProUGUI skillDescription;
    [SerializeField] Button removeSkillButton;


    private void Start()
    {
        int index = 0;
        canvasCurrentEquipedSkills = GetComponentsInChildren<EquipedSlotSkill>();
        foreach (EquipedSlotSkill equipedSlot in canvasCurrentEquipedSkills)
        {
            equipedSlot.index = index;
            equipedSlot.boton.onClick.AddListener(() => this.ChangeSelected(equipedSlot));
            index++;
        }
        foreach (SkillSlot slot in skillsSlots.GetComponentsInChildren<SkillSlot>())
        {
            slot.boton.onClick.AddListener(() => this.EquipSkill(slot));
            slot.boton.onClick.AddListener(() => this.CurrentSelectedSkillSlot(slot));
            slot.equipedSkillsManager = this;
        }
        removeSkillButton.onClick.AddListener(RemoveSkill);
    }
    public void ChangeSelected(EquipedSlotSkill slot)
    {
        if (currentSelected == slot)
        {
            RemoveCurrentSelected();
        }
        else
        {
            RemoveCurrentSelected();
            currentSelected = slot;
            currentSelected.ChangeState(EquipedButtonActiveSkillState.SELECTED);
        }




    }

    public void RemoveCurrentSelected()
    {
        if (currentSelected != null)
        {
            currentSelected.ChangeState(EquipedButtonActiveSkillState.NONE);
            currentSelected = null;
        }
    }



    public void EquipSkill(SkillSlot skillSlot)
    {
        if (currentSelected?.estado == EquipedButtonActiveSkillState.SELECTED && skillSlot.skill != null)
        {
            ReplaceIfExist(skillSlot.skill);
            skillManager.CurrentEquipedSkills[currentSelected.index] = skillSlot.skill;
            UpdateCanvas();


        }
    }

    public void UpdateCanvas()
    {
        foreach (EquipedSlotSkill canvasEquipedSkill in canvasCurrentEquipedSkills)
        {
            if (skillManager.CurrentEquipedSkills[canvasEquipedSkill.index] != null)
            {
                canvasEquipedSkill.EquipActiveSkill(skillManager.CurrentEquipedSkills[canvasEquipedSkill.index]);
            }
            else
            {
                canvasEquipedSkill.UnEquipActiveSkill();
            }
        }
    }

    public bool ReplaceIfExist(ActiveSkill skill)
    {
        for (int i = 0; i < skillManager.CurrentEquipedSkills.Length; i++)
        {
            if (skillManager.CurrentEquipedSkills[i]?.GetType() == skill?.GetType())
            {
                skillManager.CurrentEquipedSkills[i] = null;
            }
        }
        return true;
    }

    public void RemoveSkill()
    {
        if (currentSelected.skill != null)
        {
            currentSelected.skill = null;
            skillManager.CurrentEquipedSkills[currentSelected.index] = null;
            UpdateCanvas();
        }
    }

    public void CurrentSelectedSkillSlot(SkillSlot slot)
    {
        skillName.text = slot.skill?.properties.skillName;
        skillDescription.text = slot.skill?.properties.skillDescription;
    }


    private void OnDisable()
    {
        ClearDescription();
    }

    public void ClearDescription()
    {
        skillName.text = "";
        skillDescription.text = "";
    }
}
