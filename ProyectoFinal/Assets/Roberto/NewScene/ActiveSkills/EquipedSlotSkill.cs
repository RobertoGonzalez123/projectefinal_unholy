using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class EquipedSlotSkill : MonoBehaviour
{
    public ActiveSkill skill;
    public EquipedButtonActiveSkillState estado;
    [SerializeField] Image border;
    public Button boton;
    [SerializeField] Color selectedColor;
    [SerializeField] Color normalColor;
    EquipedSkillsManager equipedSkillsManager;
    public int index = 0;
    public void EquipActiveSkill(ActiveSkill Skill)
    {
        skill = Skill;
        this.GetComponent<Image>().sprite = skill.properties.sprite;
        this.GetComponent<Image>().color = skill.properties.spriteColor;
    }
    public void UnEquipActiveSkill()
    {
        skill = null;
        this.GetComponent<Image>().color = Color.white;
        this.GetComponent<Image>().sprite = null;
    }

    public void OnSelected()
    {
        if (estado == EquipedButtonActiveSkillState.SELECTED)
        {
            border.color = selectedColor;
        }
        else
        {
            border.color = normalColor;
        }
    }

    public void ChangeState(EquipedButtonActiveSkillState state)
    {
        estado = state;
        OnSelected();
    }

    public enum EquipedButtonActiveSkillState
    {
        NONE, SELECTED
    }

    private void OnDisable()
    {
        estado = EquipedButtonActiveSkillState.NONE;
        ChangeState(EquipedButtonActiveSkillState.NONE);
    }

}
