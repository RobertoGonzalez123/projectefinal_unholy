using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.VFX;

public class SkillManager : MonoBehaviour
{
    [SerializeField] AGUSMOVEMENT m_PlayerBehaviour;
    ActiveSkill[] currentEquipedSkills = new ActiveSkill[4];

    ActiveSkill[] allSkills = new ActiveSkill[16];

    public GameObject canvasSkillSlotsContainer;
    SkillSlot[] canvasSkillSlots;

    public static SkillManager Instance;    

    public ActiveSkill[] CurrentEquipedSkills { get => currentEquipedSkills; set => currentEquipedSkills = value; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

    }

    private void Start()
    {


        allSkills = GetComponents<ActiveSkill>();
        canvasSkillSlots = canvasSkillSlotsContainer.GetComponentsInChildren<SkillSlot>();

        for (int i = 0; i < allSkills.Length; i++)
        {
            canvasSkillSlots[i].skill = allSkills[i];
        }
        foreach (SkillSlot skill in canvasSkillSlots)
        {
            skill.ChangeImage();
        }
    }

    public void SkillAlpha1(InputAction.CallbackContext context)
    {
        CastSkill(0);
    }
    public void SkillAlpha2(InputAction.CallbackContext context)
    {
        CastSkill(1);
    }
    public void SkillAlpha3(InputAction.CallbackContext context)
    {
        CastSkill(2);
    }
    public void SkillAlpha4(InputAction.CallbackContext context)
    {
        CastSkill(3);
    }

    public void CastSkill(int index)
    {
        if (currentEquipedSkills[index]?.Cooldown <= 0)
        {
            m_PlayerBehaviour.ConsumeMana(currentEquipedSkills[index].manaConsumed);
            HabilityCooldown(currentEquipedSkills[index]);
            currentEquipedSkills[index].Use();
        }
    }


    public void HabilityCooldown(ActiveSkill skill)
    {
        skill.Cooldown = skill.defaultCooldown;
        StartCoroutine(CooldownSkillCoroutine(skill));
    }

    public IEnumerator CooldownSkillCoroutine(ActiveSkill skill)
    {
        while (skill.Cooldown > 0)
        {
            yield return new WaitForSeconds(0.1f);
            skill.Cooldown -= 0.1f;
        }

    }

    public enum SkillName
    {

    }
}
