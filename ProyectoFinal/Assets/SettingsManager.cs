using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour
{
    [SerializeField]
    SettingsScriptableObject settingsSO;
    [SerializeField]
    GameObject BindGroupPrefab;
    [SerializeField]
    GameObject BindContent;

    [Header("ALL PANELS")]
    [SerializeField] GameObject GraphicsPanel;
    [SerializeField] GameObject AudioPanel;
    [SerializeField] GameObject ControlsPanel;

    private bool waitingForInput = false;
    public InputActionAsset inputActionAsset;

    // Start is called before the first frame update
    void Start()
    {
        //loadRebinds(inputActionAsset);
        //SetAllBindsLabels();
    }

    /*public void SetAllBindsLabels()
    {
        //GameObject child = transform.GetChild(0).gameObject;
        foreach (KeyBinding kb in settingsSO.KeyBindings)
        {
            GameObject newBind = Instantiate(BindGroupPrefab, BindContent.transform);
            newBind.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = kb.KeyCodeName;
            newBind.transform.GetChild(1).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = kb.keyCode.ToString();
            newBind.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() => SetNewKey(kb, newBind));
        }
    }

    public void StopSettingNewKey()
    {
        StopAllCoroutines();
    }

    private void SetNewKey(KeyBinding kb, GameObject button)
    {
        // Visual change to acknowledge the user that is changing the bind
        button.transform.GetChild(1).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = ">  <";
        print("trying setting new key");

        string text = button.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text;
        InputActionMap playerActionMap = inputActionAsset.FindActionMap("Player");
        if (playerActionMap != null)
        {
            print("player action map is not null");
            InputAction actionToRebind = playerActionMap.FindAction(text);
            print("action to rebind = " + actionToRebind.name + " old key is = " + actionToRebind.bindings[0].ToString());
            actionToRebind.Disable();
            var rebindOperation = actionToRebind.PerformInteractiveRebinding()
                       // To avoid accidental input from mouse motion
                       .WithControlsExcluding("Mouse")
                       .OnMatchWaitForAnother(0.1f)
                       .OnComplete(operation =>
                       {
                           actionToRebind.Enable();
                           print("operation new bind " + operation.action.bindings[0]);
                           actionToRebind.ApplyBindingOverride(0, operation.action.bindings[0]);
                           button.transform.GetChild(1).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = operation.action.bindings[0].ToString();
                       })
                       .Start();
        }
        else
        {
            Debug.LogError("InputActionMap named 'Player' not found in the InputActionAsset.");
        }
    }

    //StartCoroutine(WaitForKey(kb, button));

    IEnumerator WaitForKey(KeyBinding kb, GameObject button)
    {
        bool keySet = false;
        while (!keySet)
        {
            foreach (KeyCode vKey in System.Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKey(vKey))
                {
                    keySet = true;
                    ChangeInputSystem(kb, vKey);
                    button.transform.GetChild(1).transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = vKey.ToString();
                    print("HE ENTRADO AQUI");
                    break;
                }
            }
            yield return null;
        }
    }

    void ChangeInputSystem(KeyBinding kb, KeyCode vKey)
    {
        var oldKeyCode = kb.keyCode;
        var newKeyCode = vKey;
        //var newBinding = new InputBinding("<Keyboard>/" + oldKeyCode);
        inputActionAsset.FindAction(kb.KeyCodeName).ChangeBinding(0).WithPath("<Keyboard>/" + newKeyCode);
        kb.keyCode = vKey;
        saveRebindins(inputActionAsset);
    }

    private void saveRebindins(InputActionAsset inputActionAsset)
    {
        string rebindsJson = inputActionAsset.SaveBindingOverridesAsJson();
        PlayerPrefs.SetString("rebinds", rebindsJson);
        loadRebinds(inputActionAsset);
    }

    private void loadRebinds(InputActionAsset inputActionAsset)
    {
        var rebinds = PlayerPrefs.GetString("rebinds");
        inputActionAsset.LoadBindingOverridesFromJson(rebinds);
    }*/

    public void CloseAllPanels()
    {
        GraphicsPanel.SetActive(false);
        AudioPanel.SetActive(false);
        ControlsPanel.SetActive(false);
    }

    public void Toggle(GameObject objectToToggle)
    {
        if (objectToToggle.activeSelf) objectToToggle.SetActive(false);
        else objectToToggle.SetActive(true);
    }

    public void OpenSettingsPanel(GameObject panelToOpen)
    {
        GraphicsPanel.SetActive(false);
        AudioPanel.SetActive(false);
        ControlsPanel.SetActive(false);

        panelToOpen.SetActive(true);
    }

}
