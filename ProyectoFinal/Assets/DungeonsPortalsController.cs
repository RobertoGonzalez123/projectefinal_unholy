using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonsPortalsController : MonoBehaviour
{
    public GameObject EasyDungeonEntrancePortal;
    public GameObject MediumDungeonEntrancePortal;
    public GameObject HardDungeonEntrancePortal;

    public Vector3 EasyDungeonEntrancePortalPosition;
    public Vector3 MediumDungeonEntrancePortalPosition;
    public Vector3 HardDungeonEntrancePortalPosition;

    public static DungeonsPortalsController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        EasyDungeonEntrancePortalPosition = EasyDungeonEntrancePortal.transform.position;
        MediumDungeonEntrancePortalPosition = MediumDungeonEntrancePortal.transform.position;
        HardDungeonEntrancePortalPosition = HardDungeonEntrancePortal.transform.position;
    }
}
