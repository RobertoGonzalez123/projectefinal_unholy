using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HemorrhageCoating_Skill : ActiveSkill
{
    AGUSMOVEMENT m_PlayerBehaviour;
    private void Start()
    {
        m_PlayerBehaviour = GetComponentInParent<AGUSMOVEMENT>();
        currentCooldown = 0;
    }

    public override void Use()
    {
        base.Use();
        if (m_PlayerBehaviour.CheckIfTemporalStateIsActive(Estado.Estados.Hemorrhage))
        {
            m_PlayerBehaviour.ApplyState(Estado.Estados.Hemorrhage, skillDuration);
        }

    }
}
