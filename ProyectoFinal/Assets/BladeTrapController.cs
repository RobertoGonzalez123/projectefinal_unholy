using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeTrapController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<AGUSMOVEMENT>().ReceiveDamage(20);
        }
    }
}
