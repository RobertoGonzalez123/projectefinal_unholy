using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using FifisSaver.OdinSerializer;
using UnityEngine;

public class PortalToOverworldController : MonoBehaviour
{
    public Vector3 spawnPosition = new Vector3(0, 1, 0);

    private void OnCollisionEnter(Collision collision)
    {
        print("COLLISION " + collision.gameObject.name);
        if (GameManager.Instance.DungeonInteractedDifficulty == GameManager.DungeonDifficulty.EASY)
            spawnPosition += DungeonsPortalsController.Instance.EasyDungeonEntrancePortalPosition;
        else if (GameManager.Instance.DungeonInteractedDifficulty == GameManager.DungeonDifficulty.MEDIUM)
            spawnPosition += DungeonsPortalsController.Instance.MediumDungeonEntrancePortalPosition;
        else if (GameManager.Instance.DungeonInteractedDifficulty == GameManager.DungeonDifficulty.HARD)
            spawnPosition += DungeonsPortalsController.Instance.HardDungeonEntrancePortalPosition;

        // Check if the player has quest of completing a dungeon
        // If the quest is the same difficulty as the dungeon, complete it
        if (QuestManager.Instance.MisionActiva is MisionDungeonSO)
        {
            MisionDungeonSO quest = (MisionDungeonSO)QuestManager.Instance.MisionActiva;
            print("the quest is misionDungeonSO and the name is " + quest.QuestName);
            // Parse both enum values to string. Since both have the same name, can be compared as strings and compared
            if (quest.QuestDifficulty.ToString() == GameManager.Instance.DungeonInteractedDifficulty.ToString())
            {
                //GameManager.Instance.ChangeTutDungeonCompleted();
                quest.CompletarMision();
            }
        }
        try
        {
            string newBase64 = File.ReadAllText(Application.persistentDataPath + "lastGameState.json");
            byte[] serializedData = System.Convert.FromBase64String(newBase64);

            LastGameState lastGameState = SerializationUtility.DeserializeValue<LastGameState>(serializedData, DataFormat.JSON);
            lastGameState.wasInDungeon = true;
            byte[] serializedData2 = SerializationUtility.SerializeValue<LastGameState>(lastGameState, DataFormat.JSON);
            string base64 = System.Convert.ToBase64String(serializedData2);
            File.WriteAllText(Application.persistentDataPath + "lastGameState.json", base64, Encoding.UTF8);
        }
        catch (System.Exception e)
        {
            Debug.Log(e.Message);
        }

        GameManager.Instance.GetComponent<GameController>().SaveItemsToRestore();
        GameManager.Instance.ChangeToScene("MapCreation", spawnPosition);
    }
}
