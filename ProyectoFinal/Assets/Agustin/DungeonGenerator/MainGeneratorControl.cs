using System;
using System.Collections.Generic;
using UnityEngine;
using static Room;

public class MainGeneratorControl : MonoBehaviour
{
    [Header("Rooms Prefabs")]
    [SerializeField] GameObject LootRoomPrefab;
    [SerializeField] GameObject BossRoomPrefab;
    [SerializeField] GameObject PreBossRoomPrefab;

    [Header("Other Room Types Prefabs")]
    [SerializeField] GameObject PrefabRoomEnemies;
    [SerializeField] GameObject PrefabRoomEnemiesTraps;
    [SerializeField] GameObject PrefabRoomParkour;
    [SerializeField] GameObject PrefabRoomMiniBoss;
    [SerializeField] GameObject PrefabRoomPuzzleTraps;

    [Header("Room Type Chance")]
    [SerializeField] float RoomEnemiesChance;
    [SerializeField] float RoomEnemiesTrapsChance;
    [SerializeField] float RoomParkourChance;
    [SerializeField] float RoomMiniBossChance;
    [SerializeField] float RoomPuzzleTrapsChance;


    [Header("Settings")]
    [SerializeField] float m_RoomSize = 50;
    public int m_NumberOfRoomsToGenerate = 10;
    [SerializeField] int m_CurrentX = 25; [SerializeField] int m_CurrentY = 25;
    [SerializeField] bool m_MustWalkUntilRoomNumberReached = true;
    [SerializeField] int m_Seed = 1234567890;
    GameObject[,] m_MapDistribution = new GameObject[50, 50];

    [Header("Further POS")]
    public int m_CurrentFurX = 25;
    public int m_CurrentFurY = 25;


    [Header("DIFFICULTIES")]
    public int Easy_RoomsToGenerate;
    public int Medium_RoomsToGenerate;
    public int Hard_RoomsToGenerate;

    void Start()
    {
        // ToGenerate();
        GenerateRandomSeed();
        LoadSceneDifficulty();
        UnityEngine.Random.InitState(m_Seed);
        ToGenerateUpgraded();
    }

    void GenerateRandomSeed()
    {
        this.m_Seed = UnityEngine.Random.Range(1, Int32.MaxValue);
    }

    void LoadSceneDifficulty()
    {
        if (GameManager.Instance.DungeonInteractedDifficulty == GameManager.DungeonDifficulty.EASY)
        {
            // Load the easy difficulty
            m_NumberOfRoomsToGenerate = Easy_RoomsToGenerate;
        }
        else if (GameManager.Instance.DungeonInteractedDifficulty == GameManager.DungeonDifficulty.HARD)
        {
            // Load the hard difficulty
            m_NumberOfRoomsToGenerate = Hard_RoomsToGenerate;
        }
        // Either if the dungeon is medium difficulty or there is any error detecting the dungeon difficulty, set it to medium
        else
        {
            // Load the medium difficulty
            m_NumberOfRoomsToGenerate = Medium_RoomsToGenerate;
        }
    }

    void MakeTheDrunkManWalk()
    {
        bool Positive = UnityEngine.Random.Range(0, 2) == 0 ? false : true;
        bool Horizontal = UnityEngine.Random.Range(0, 2) == 0 ? false : true;
        if (Positive && Horizontal) m_CurrentX += 1;
        else if (!Positive && Horizontal) m_CurrentX -= 1;
        else if (Positive && !Horizontal) m_CurrentY += 1;
        else if (!Positive && !Horizontal) m_CurrentY -= 1;
        CheckIfTheFurthest();
    }

    void CheckIfTheFurthest()
    {

        if ((Mathf.Abs(m_CurrentX - 25) + Mathf.Abs(m_CurrentY - 25)) > (Mathf.Abs(m_CurrentFurX - 25) + Mathf.Abs(m_CurrentFurY - 25)))
        {
            m_CurrentFurX = m_CurrentX;
            m_CurrentFurY = m_CurrentY;
        }
    }
    Vector3 GetCurrentVectorPosition()
    {
        return new Vector3(m_CurrentX * m_RoomSize, 0, m_CurrentY * m_RoomSize);
    }


    [SerializeField]GameObject InitialRoomPrefab;

    void ToGenerateUpgraded()
    {
        m_MapDistribution[m_CurrentX, m_CurrentY] = Instantiate(InitialRoomPrefab, GetCurrentVectorPosition(), Quaternion.identity);
        MakeTheDrunkManWalk();

        for (int i = 1; i < m_NumberOfRoomsToGenerate; i++)
        {
            if (m_MapDistribution[m_CurrentX, m_CurrentY] == null)
            {
                m_MapDistribution[m_CurrentX, m_CurrentY] = Instantiate(GetRoomPrefabByType(GetRandomRoomType()), GetCurrentVectorPosition(), Quaternion.identity);
                CheckNearbyRooms();
            }
            else if (m_MustWalkUntilRoomNumberReached) i--;
            if(i!=m_NumberOfRoomsToGenerate-1)
                MakeTheDrunkManWalk();
        }
        Debug.Log("enumINT 1");
        Directions DirectionToFollow = WhereShouldIGenerateEnd();
        Debug.Log("enumINT 2");
        m_MapDistribution[m_CurrentX, m_CurrentY] = Instantiate(GetRoomPrefabByType(RoomType.MINIBOSS), new Vector3(m_CurrentX * m_RoomSize, 0, m_CurrentY * m_RoomSize), Quaternion.identity);
        Debug.Log("enumINT 4");
        Room lastMiniBossRoom = m_MapDistribution[m_CurrentX, m_CurrentY].GetComponent<Room>();
        Debug.Log("enumINT 5");
        CheckNearbyRooms();
        Debug.Log("enumINT 6");

        int currentOffset = 0;
        GameObject LootRoom = Instantiate(LootRoomPrefab, new Vector3(0, 0, 30), Quaternion.identity);
        LootRoom m_LootRoom = LootRoom.GetComponent<LootRoom>();
        currentOffset -= m_LootRoom.m_RoomSize;
        Debug.Log("enumINT ");
        GameObject BossRoom = Instantiate(BossRoomPrefab, new Vector3(0, 0, currentOffset), Quaternion.identity);
        BossRoomScript m_BossRoom = BossRoom.GetComponent<BossRoomScript>();
        currentOffset -= m_BossRoom.m_RoomSize;
        Debug.Log("enumINT ");
        GameObject PreBossRoom = Instantiate(PreBossRoomPrefab, new Vector3(0, 0, currentOffset), Quaternion.identity);
        MerchanderRoom m_PreBossRoom = PreBossRoom.GetComponent<MerchanderRoom>();
        currentOffset -= m_PreBossRoom.m_RoomSize;
        Debug.Log("enumINT ");
        //TO-DO DOOR TO OVER WORLD OR CINEMATIC TO EXIT....
        //m_LootRoom.m_PortalToOverWorld = 
        Debug.Log("enumINT ");
        m_BossRoom.m_MyDoorToLootRoom.m_DoorWhereToTeleport = m_LootRoom.m_ThisRoomPortal;
        Debug.Log("enumINT ");
        m_PreBossRoom.m_PortalToBossRoom.m_DoorWhereToTeleport = m_BossRoom.m_ThisRoomPortal;

        // 0 = LEFT      1 = UP      2 = RIGHT      3 = DOWN
        Debug.Log("enumINT " + DirectionToFollow);
        int enumINT = (int)DirectionToFollow;
        Debug.Log("enumINT " + enumINT);
        lastMiniBossRoom.DoorList[enumINT].ActivateDoor();
        lastMiniBossRoom.DoorList[enumINT].m_DoorPortal.m_DoorWhereToTeleport = m_PreBossRoom.m_ThisRoomPortal;



    }
    enum Directions { LEFT, UP, RIGHT, DOWN }
    Directions WhereShouldIGenerateEnd()
    {
        Debug.Log("EL END DEBERIA IR AQUI");
        int XDifferenceFurthestToOrigin = m_CurrentFurX - 25;
        int YDifferenceFurthestToOrigin = m_CurrentFurY - 25;

        if (XDifferenceFurthestToOrigin == 0 || YDifferenceFurthestToOrigin == 0)
        {
            if (YDifferenceFurthestToOrigin > 0)
            {
                //abajo
                m_CurrentX = m_CurrentFurX;
                m_CurrentY = m_CurrentFurY + 1;
                Debug.Log(" aqui");
                return Directions.UP;
            }
            else if (YDifferenceFurthestToOrigin < 0)
            {
                //arriba
                m_CurrentX = m_CurrentFurX;
                m_CurrentY = m_CurrentFurY - 1;
                return Directions.DOWN;
            }
            else if (XDifferenceFurthestToOrigin > 0)
            {
                //derecha
                m_CurrentX = m_CurrentFurX + 1;
                m_CurrentY = m_CurrentFurY;
                return Directions.RIGHT;
            }
            else
            {
                //izquierda
                m_CurrentX = m_CurrentFurX - 1;
                m_CurrentY = m_CurrentFurY;
                return Directions.LEFT;
            }
        }
        else
        {
            print("RESTA = X" + Mathf.Abs(XDifferenceFurthestToOrigin) + " OTRA Y" + Mathf.Abs(YDifferenceFurthestToOrigin));
            if (Mathf.Abs(XDifferenceFurthestToOrigin) >= Mathf.Abs(YDifferenceFurthestToOrigin))
            {
                //HORIZONTAL
                if (XDifferenceFurthestToOrigin > 0)
                {
                    //derecha
                    m_CurrentX = m_CurrentFurX + 1;
                    m_CurrentY = m_CurrentFurY;
                    return Directions.RIGHT;
                }
                else
                {
                    //izquierda
                    m_CurrentX = m_CurrentFurX - 1;
                    m_CurrentY = m_CurrentFurY;
                    return Directions.LEFT;
                }
            }
            else
            {
                //VERTICAL
                if (YDifferenceFurthestToOrigin > 0)
                {
                    //abajo
                    m_CurrentX = m_CurrentFurX;
                    m_CurrentY = m_CurrentFurY + 1;
                    Debug.Log(" aqui");
                    return Directions.UP;
                }
                else
                {
                    //arriba
                    m_CurrentX = m_CurrentFurX;
                    m_CurrentY = m_CurrentFurY - 1;
                    return Directions.DOWN;
                }
            }
        }
    }

    void CheckNearbyRooms()
    {
        Room CurrentRoom = m_MapDistribution[m_CurrentX, m_CurrentY].GetComponent<Room>();
        // TOP OF ME
        if (m_MapDistribution[m_CurrentX, m_CurrentY + 1] != null)
        {
            IndividualDoorControl myTopDoor = CurrentRoom.DoorList[1];
            IndividualDoorControl theirBottomDoor = m_MapDistribution[m_CurrentX, m_CurrentY + 1].GetComponent<Room>().DoorList[3];

            myTopDoor.ActivateDoor();
            theirBottomDoor.ActivateDoor();

            myTopDoor.m_DoorPortal.m_DoorWhereToTeleport = theirBottomDoor.m_DoorPortal;
            theirBottomDoor.m_DoorPortal.m_DoorWhereToTeleport = myTopDoor.m_DoorPortal;
        }
        // LEFT SIDE OF ME
        if (m_MapDistribution[m_CurrentX - 1, m_CurrentY] != null)
        {
            IndividualDoorControl myLeftDoor = CurrentRoom.DoorList[0];
            IndividualDoorControl theirRightDoor = m_MapDistribution[m_CurrentX - 1, m_CurrentY].GetComponent<Room>().DoorList[2];

            myLeftDoor.ActivateDoor();
            theirRightDoor.ActivateDoor();

            myLeftDoor.m_DoorPortal.m_DoorWhereToTeleport = theirRightDoor.m_DoorPortal;
            theirRightDoor.m_DoorPortal.m_DoorWhereToTeleport = myLeftDoor.m_DoorPortal;
        }
        // RIGHT SIDE OF ME
        if (m_MapDistribution[m_CurrentX + 1, m_CurrentY] != null)
        {
            IndividualDoorControl myRightDoor = CurrentRoom.DoorList[2];
            IndividualDoorControl theirLeftDoor = m_MapDistribution[m_CurrentX + 1, m_CurrentY].GetComponent<Room>().DoorList[0];

            myRightDoor.ActivateDoor();
            theirLeftDoor.ActivateDoor();

            myRightDoor.m_DoorPortal.m_DoorWhereToTeleport = theirLeftDoor.m_DoorPortal;
            theirLeftDoor.m_DoorPortal.m_DoorWhereToTeleport = myRightDoor.m_DoorPortal;
        }
        // UNDER ME
        if (m_MapDistribution[m_CurrentX, m_CurrentY - 1] != null)
        {
            IndividualDoorControl myBottomDoor = CurrentRoom.DoorList[3];
            IndividualDoorControl theirTopDoor = m_MapDistribution[m_CurrentX, m_CurrentY - 1].GetComponent<Room>().DoorList[1];

            myBottomDoor.ActivateDoor();
            theirTopDoor.ActivateDoor();

            myBottomDoor.m_DoorPortal.m_DoorWhereToTeleport = theirTopDoor.m_DoorPortal;
            theirTopDoor.m_DoorPortal.m_DoorWhereToTeleport = myBottomDoor.m_DoorPortal;
        }
    }



    void ToGenerate()
    {
        int currentOffset = 0;
        GameObject LootRoom = Instantiate(LootRoomPrefab, new Vector3(0, 0, 30), Quaternion.identity);
        LootRoom m_LootRoom = LootRoom.GetComponent<LootRoom>();
        currentOffset -= m_LootRoom.m_RoomSize;


        GameObject BossRoom = Instantiate(BossRoomPrefab, new Vector3(0, 0, currentOffset), Quaternion.identity);
        BossRoomScript m_BossRoom = BossRoom.GetComponent<BossRoomScript>();
        currentOffset -= m_BossRoom.m_RoomSize;

        GameObject PreBossRoom = Instantiate(PreBossRoomPrefab, new Vector3(0, 0, currentOffset), Quaternion.identity);
        MerchanderRoom m_PreBossRoom = PreBossRoom.GetComponent<MerchanderRoom>();
        currentOffset -= m_PreBossRoom.m_RoomSize;

        //TO-DO DOOR TO OVER WORLD OR CINEMATIC TO EXIT....
        //m_LootRoom.m_PortalToOverWorld = 

        m_BossRoom.m_MyDoorToLootRoom.m_DoorWhereToTeleport = m_LootRoom.m_ThisRoomPortal;

        m_PreBossRoom.m_PortalToBossRoom.m_DoorWhereToTeleport = m_BossRoom.m_ThisRoomPortal;


        List<GameObject> listOfRooms = new List<GameObject>();


        currentOffset -= 45;
        Room.RoomType SelectedRoomType = GetRandomRoomType();
        GameObject firstRoom = Instantiate(GetRoomPrefabByType(SelectedRoomType), new Vector3(0, 0, currentOffset), Quaternion.identity);
        firstRoom.GetComponent<Room>().m_ThisRoomType = SelectedRoomType;


        listOfRooms.Add(firstRoom);
        currentOffset -= 45;


        for (int i = 0; i < m_NumberOfRoomsToGenerate; i++)
        {
            bool CanUseDoor = false;
            IndividualDoorControl DoorUsedToTeleportFrom;
            while (!CanUseDoor)
            {
                int randomRoomNumber = UnityEngine.Random.Range(0, listOfRooms.Count); int randomDoorNumber = UnityEngine.Random.Range(0, 4);
                Room selectedRoom = listOfRooms[randomRoomNumber].GetComponent<Room>();
                if (!selectedRoom.DoorList[randomDoorNumber].ThisDoorIsActive)
                {
                    CanUseDoor = true;
                    //Activated the selected Door and assigned it's value to DoorUsedToTeleportFrom
                    DoorUsedToTeleportFrom = selectedRoom.DoorList[randomDoorNumber];
                    DoorUsedToTeleportFrom.ActivateDoor();

                    //Instantiate a room of randomType
                    Room.RoomType selectedRoomType = GetRandomRoomType();
                    GameObject newRoom = Instantiate(GetRoomPrefabByType(selectedRoomType), new Vector3(0, 0, currentOffset), Quaternion.identity);
                    Room newRoomScript = newRoom.GetComponent<Room>();
                    newRoomScript.m_ThisRoomType = selectedRoomType;
                    //Activate a randomDoorFromNewRoom and assign it's teleport position to DoorUsedToTeleportFrom
                    newRoomScript.DoorList[randomDoorNumber].ActivateDoor();
                    newRoomScript.DoorList[randomDoorNumber].m_DoorPortal.m_DoorWhereToTeleport = DoorUsedToTeleportFrom.m_DoorPortal;
                    //Set DoorUsedToTeleportFrom teleport to selected door from newRoom
                    DoorUsedToTeleportFrom.m_DoorPortal.m_DoorWhereToTeleport = newRoomScript.DoorList[randomDoorNumber].m_DoorPortal;

                    //Finally Add room created to RoomList to make it selectable on new iteration and update offset
                    listOfRooms.Add(newRoom);
                    currentOffset -= 45;
                }

            }
        }


        GameObject MiniBossRoom = Instantiate(GetRoomPrefabByType(RoomType.MINIBOSS), new Vector3(125, 0, -90), Quaternion.identity);
        Room miniBossRoomScript = MiniBossRoom.GetComponent<Room>();
        miniBossRoomScript.m_ThisRoomType = RoomType.MINIBOSS;

        for (int i = 0; i < 3; i++)
        {
            bool CanUseDoor = false;
            IndividualDoorControl DoorUsedToTeleportFrom;
            while (!CanUseDoor)
            {
                int randomRoomNumber = UnityEngine.Random.Range(0, listOfRooms.Count); int randomDoorNumber = UnityEngine.Random.Range(0, 4);
                Room selectedRoom = listOfRooms[randomRoomNumber].GetComponent<Room>();
                if (!selectedRoom.DoorList[randomDoorNumber].ThisDoorIsActive && selectedRoom.m_ThisRoomType != RoomType.MINIBOSS)
                {
                    CanUseDoor = true;
                    //Activated the selected Door and assigned it's value to DoorUsedToTeleportFrom
                    DoorUsedToTeleportFrom = selectedRoom.DoorList[randomDoorNumber];
                    DoorUsedToTeleportFrom.ActivateDoor();

                    //Activate a the door and assign it's teleport position to DoorUsedToTeleportFrom
                    miniBossRoomScript.DoorList[i].ActivateDoor();
                    miniBossRoomScript.DoorList[i].m_DoorPortal.m_DoorWhereToTeleport = DoorUsedToTeleportFrom.m_DoorPortal;

                    //Set DoorUsedToTeleportFrom teleport to selected door from newRoom
                    DoorUsedToTeleportFrom.m_DoorPortal.m_DoorWhereToTeleport = miniBossRoomScript.DoorList[randomDoorNumber].m_DoorPortal;

                }

            }
        }

        miniBossRoomScript.DoorList[3].ActivateDoor();
        miniBossRoomScript.DoorList[3].m_DoorPortal.m_DoorWhereToTeleport = m_PreBossRoom.m_ThisRoomPortal;





    }


    Room.RoomType GetRandomRoomType()
    {
        float total = RoomEnemiesChance + RoomEnemiesTrapsChance + RoomParkourChance + RoomMiniBossChance + RoomPuzzleTrapsChance;
        float random = UnityEngine.Random.Range(0, total);
        if (RoomEnemiesChance >= random) return Room.RoomType.ENEMIES;
        else if (RoomEnemiesChance + RoomEnemiesTrapsChance >= random) return Room.RoomType.ENEMIESTRAPS;
        else if (RoomEnemiesChance + RoomEnemiesTrapsChance + RoomParkourChance >= random) return Room.RoomType.PARKOUR;
        else if (RoomEnemiesChance + RoomEnemiesTrapsChance + RoomParkourChance + RoomMiniBossChance >= random) return Room.RoomType.MINIBOSS;
        else return Room.RoomType.PUZZLETRAPS;
    }

    GameObject GetRoomPrefabByType(RoomType whatTypeImGonnaBe)
    {
        switch (whatTypeImGonnaBe)
        {
            case RoomType.ENEMIES:
                return PrefabRoomEnemies;
            case RoomType.ENEMIESTRAPS:
                return PrefabRoomEnemiesTraps;
            case RoomType.PARKOUR:
                return PrefabRoomParkour;
            case RoomType.MINIBOSS:
                return PrefabRoomMiniBoss;
            case RoomType.PUZZLETRAPS:
                return PrefabRoomPuzzleTraps;
            default:
                Debug.Log("GetRoomPrefabByType NOT CONTROLLED PLEASE CHECK");
                return PrefabRoomEnemies;
        }

    }



}
