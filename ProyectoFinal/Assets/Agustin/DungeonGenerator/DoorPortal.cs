using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DoorPortal : MonoBehaviour
{

    public DoorPortal m_DoorWhereToTeleport;
    public bool isPortalEnabled = true;



    private void OnCollisionEnter(Collision collision)
    {
        if (isPortalEnabled)
        {
            if (collision.gameObject.GetComponent<AGUSMOVEMENT>().getCanUseDoorTeleport)
            {
                Debug.Log("TELEPORT: CollisionEnter with DoorPortal, going to teleport location");
                
                collision.gameObject.GetComponent<AGUSMOVEMENT>().StartCanUseDoorTeleportDelay();
                collision.gameObject.transform.position = m_DoorWhereToTeleport.transform.position + (0.4f*m_DoorWhereToTeleport.transform.forward.normalized )+ new Vector3(0,0.69f,0) ;
            }
            else
            {
                Debug.Log("TELEPORT: On Cooldown");
            }
            
        }else Debug.Log("CollisionEnter with DoorPortal, BUT PORTAL NOT ENABLED");
    }



}
