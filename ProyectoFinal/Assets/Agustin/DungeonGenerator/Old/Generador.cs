using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generador : MonoBehaviour
{
    // The size of the dungeon (in number of rooms)
    private int dungeonSize = 105;

    // The prefabs for the rooms and hallways
    [Header("Prefab1")]
    public GameObject roomPrefab;
    public float roomPrefabSize;

    [Header("Prefab2")]
    public GameObject roomPrefab2;
    public float roomPrefabSize2;

    public GameObject hallwayPrefab;



    // The list of rooms and hallways in the dungeon
    private List<GameObject> rooms = new List<GameObject>();
    private List<GameObject> hallways = new List<GameObject>();

    private float sizeRoomActual;
    private bool small;

    // The possible directions to move in
    private List<Vector3> directions = new List<Vector3>
    {
        Vector3.forward,
        Vector3.back,
        Vector3.left,
        Vector3.right
    };


    void Start()
    {
        small = (Random.Range(0, 2) == 0);
        sizeRoomActual = small? roomPrefabSize: roomPrefabSize2;

        // Generate the initial room
        GenerateRoom();

        // Generate the rest of the dungeon
        for (int i = 1; i < dungeonSize; i++)
        {
            small = (Random.Range(0, 2) == 0);
            sizeRoomActual = small ? roomPrefabSize : roomPrefabSize2;

            // Choose a random room to start from
            int currentRoomIndex = Random.Range(0, rooms.Count);
            GameObject currentRoom = rooms[currentRoomIndex];

            // Choose a random direction to move in
            Vector3 direction = directions[Random.Range(0, directions.Count)];

            // Check if the space in that direction is empty
            float distancia = currentRoom.GetComponent<sizeScript>().size/2 + 4 + sizeRoomActual/2;
            //if (Physics.OverlapBox(currentRoom.transform.position + direction * distancia, Vector3.one * 0.1f).Length == 0)
            if (Physics.OverlapBox(currentRoom.transform.position + direction * distancia, Vector3.one * (sizeRoomActual/2)).Length == 0)
            {
                /*  Esto es de cuando estaba hardcodeado con habitaciones de solo 6 de grande y usaba el 10 en vez de float distancia
                 *  *10 porque la habitacion son 6, partimos desde la mitad de una asi que para llegar a su extremo son 3
                 *  despues hay que a�adir el pasillo de 4 de largo 
                 *  y despues para llegar al centro de la que vamos a spawnear sumamos su mitad(3)
                 */
                GenerateRoom(currentRoom.transform.position + direction * distancia); 
                GenerateHallway(currentRoom, direction);
            }
            else
                i--;
            
        }
    }

    void GenerateHallway(GameObject room, Vector3 direction)
    {
        // Calculate the position of the hallway
        /*
         * Sumamos 5 porque partimos del centro de la habitacion, para llegar al extremo hay que sumar 3
         * luego sumamos otros 2 mas porque el pasillo mide 4 y necesitamos spawnearlo con su mitad como referencia de centro
         */
        float distancia = room.GetComponent<sizeScript>().size / 2+2;
        Vector3 hallwayPosition = room.transform.position + direction * distancia;
        // Instantiate the hallway
        GameObject hallway = Instantiate(hallwayPrefab, hallwayPosition, Quaternion.identity);

        // Calculate the rotation of the hallway
        hallway.transform.rotation = Quaternion.LookRotation(direction);

        // Set the hallway's parent to the room
        hallway.transform.parent = room.transform;
    }



    void GenerateRoom(Vector3 position = default)
    {
        // If no position is provided, generate a random position
        if (position == default)
        {
            position = new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5));
        }

        // Instantiate the room at the position
        GameObject room = small? Instantiate(roomPrefab, position, Quaternion.identity): Instantiate(roomPrefab2, position, Quaternion.identity);

        // Add the room to the list of rooms
        rooms.Add(room);
    }
}