using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GenerateMatrix : MonoBehaviour
{
    public int minWidth = 5; // Ancho m�nimo de la forma
    public int minHeight = 5; // Alto m�nimo de la forma

    private char[,] matrix;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Generate();
            Print();
        }
    }
    // Genera la matriz y la forma dentro de ella
    void Generate()
    {
        // Definir los tama�os de las filas
        List<int> rowSizes = new List<int>();
        rowSizes.Add(20);
        rowSizes.Add(20);
        rowSizes.Add(20);
        rowSizes.Add(20);
        rowSizes.Add(20);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);
        rowSizes.Add(10);

        // Inicializar la matriz con 'O' en todas las posiciones
        int numRows = rowSizes.Count;
        int numCols = rowSizes.Max();
        matrix = new char[numRows, numCols];
        for (int i = 0; i < numRows; i++)
        {
            for (int j = 0; j < numCols; j++)
            {
                matrix[i, j] = 'O';
            }
        }

        // Generar la forma
        int formWidth = Random.Range(minWidth, numCols / 2);
        int formHeight = Random.Range(minHeight, numRows / 2);
        int startX = Random.Range(0, numCols - formWidth);
        int startY = Random.Range(0, numRows - formHeight);
        for (int i = startY; i < startY + formHeight; i++)
        {
            for (int j = startX; j < startX + formWidth; j++)
            {
                if (i == startY || i == startY + formHeight - 1 || j == startX || j == startX + formWidth - 1)
                {
                    matrix[i, j] = 'X'; // Borde de la forma
                }
                else
                {
                    matrix[i, j] = 'I'; // Interior de la forma
                }
            }
        }
    }

    // Imprime la matriz en la consola
    void Print()
    {
        string output = "";
        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                output += matrix[i, j] + " ";
            }
            output += "\n";
        }
        Debug.Log(output);
    }
}
