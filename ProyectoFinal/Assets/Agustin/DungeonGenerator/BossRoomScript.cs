using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossRoomScript : MonoBehaviour
{
    public DoorPortal m_MyDoorToLootRoom;
    public DoorPortal m_ThisRoomPortal;


    public int m_RoomSize = 40;

    [SerializeField] GameObject bossObject;
    [SerializeField] GameObject canvasGameObject;
    [SerializeField] GameObject cameraBossCinematic;
    [SerializeField] AudioClip bossMusic;

    public void ActivateRoomBoss()
    {
        m_ThisRoomPortal.isPortalEnabled = false;
        m_MyDoorToLootRoom.isPortalEnabled = false;
        canvasGameObject.SetActive(true);
        bossObject.SetActive(true);
        cameraBossCinematic.SetActive(true);
        GameManager.Instance.SoundManagerGO.GetComponent<AudioSource>().clip = bossMusic;
        GameManager.Instance.SoundManagerGO.GetComponent<AudioSource>().Play();
    }
    public void OnBossDefeated()
    {
        m_MyDoorToLootRoom.isPortalEnabled = true;
        cameraBossCinematic.SetActive(false);
    }
}
