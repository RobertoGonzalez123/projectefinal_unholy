using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateRoomInParent : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        GetComponentInParent<BossRoomScript>().ActivateRoomBoss();
        Destroy(gameObject);
    }
}
