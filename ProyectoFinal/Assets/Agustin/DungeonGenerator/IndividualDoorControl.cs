using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndividualDoorControl : MonoBehaviour
{
    public bool ThisDoorIsActive = false;
    
    public DoorPortal m_DoorPortal;
    [SerializeField] GameObject m_WallObject;
    [SerializeField] GameObject m_DoorObject;
    [SerializeField] GameObject m_DoorFrameObject;
    [SerializeField] GameObject m_ClawDestinationObject;

    public void ActivateDoor()
    {
        ThisDoorIsActive = true;
        m_WallObject.SetActive(false);
        m_DoorObject.SetActive(true);
        m_DoorFrameObject.SetActive(true);
        m_DoorPortal.gameObject.SetActive(true);
    }

    public void ActivateDoor(Room.RoomType m_RoomType)
    {
        ThisDoorIsActive = true;
        m_WallObject.SetActive(false);
        m_DoorObject.SetActive(true);
        m_DoorFrameObject.SetActive(true);
        m_ClawDestinationObject.SetActive(true);
        ActivateClawItem(m_RoomType);
    }
    void ActivateClawItem(Room.RoomType m_RoomType)
    {
        switch (m_RoomType)
        {
            case Room.RoomType.ENEMIES:
                Debug.Log("ENEMIES");
                break;
            default:
                break;
        }
    }



}
