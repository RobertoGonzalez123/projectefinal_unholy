using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BushBehaviourDestroyable : MonoBehaviour
{
    [SerializeField] GameObject berriePrefab;
    [SerializeField] float healthPoints = 9f;
    [SerializeField] SphereCollider myCollider;
    private void OnTriggerEnter(Collider other)
    {
        healthPoints -= other.GetComponentInParent<Damageable>().DoDamage().dmg;
        if (healthPoints <= 0)
        {
            StartCoroutine("destroyCoroutine");
        }
    }
    IEnumerator destroyCoroutine()
    {
        myCollider.enabled = false;
        spawnItems();
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }

    void spawnItems()
    {
        int quantity = Random.Range(1, 5);
        for(int i = 0; i < quantity; i++)
        {
            Vector3 spawnPositon = new Vector3(Random.Range(-1f, 1), transform.position.y, Random.Range(-1f, 1));
            Instantiate(berriePrefab, transform.position + spawnPositon,Quaternion.identity);
        }
        Debug.Log("SPAWNEADOS BERIES");
    }
}
