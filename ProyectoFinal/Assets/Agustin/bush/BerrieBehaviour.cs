using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BerrieBehaviour : Interactuable
{

    [SerializeField] ConsumableSO itemModel;
    private void OnEnable()
    {
        transform.position = new Vector3(transform.position.x, GetGroundHeight(), transform.position.z);
    }
    public float GetGroundHeight()
    {
        // Raycast downward to find the ground height at the given position
        RaycastHit hit;
        //layer 6 = ground
        if (Physics.Raycast(transform.position + Vector3.up * 10f, Vector3.down, out hit, Mathf.Infinity,6))
        {
            return hit.point.y+0.5f;
        }

        // If no ground is found, return a default value (0)
        return 0.5f;
    }
    public override bool Interact()
    {
        if(InventoryManager.Instance.AddItem(new ObjectConsumable((ConsumableSO)itemModel)))
        {
            Destroy(gameObject);
        }
        else
            return false;
        return true;
    }
}
