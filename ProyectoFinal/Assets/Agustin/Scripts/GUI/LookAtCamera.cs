using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LookAtCamera : MonoBehaviour
{
    [SerializeField]bool preventYRotation = false;   
    private void LateUpdate()
    {
        transform.LookAt(GameManager.Instance.mainCamera.transform);
        if(preventYRotation) transform.Rotate(Vector3.up, 180f);
    }
}
