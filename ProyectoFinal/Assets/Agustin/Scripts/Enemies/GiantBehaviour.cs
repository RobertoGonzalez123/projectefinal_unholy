using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem.LowLevel;
using UnityEngine.SocialPlatforms;
using Random = UnityEngine.Random;
using UnityEngine.UI;
using UnityEngine.Events;
using Unity.VisualScripting;
using static InterfaceDamageable;
using System.Linq;

public class GiantBehaviour : Damageable, InterfaceDamageable
{
    [SerializeField] public List<Estado> myBasicStates = new List<Estado>();

    NavMeshAgent agent;
    Transform target;
    Animator animator;

    [SerializeField]
    EnemyBehaviourStats EnemyStats;
    public float dmgReceivedMultiplier;

    [SerializeField]
    LayerMask EnemyMask;

    public List<Transform> waypointList = new List<Transform>();

    Vector3 WhereImGoing;
    RaycastHit hit;

    enum States { RESTING, PATROLING, CHASING, ATTACKING, DYING }
    States CurrentState;

    float StateDeltaTime;
    float AttackDeltaTime;

    [SerializeField]
    List<AttackSO> attacks;
    int selectedRandomAttack;

    public UnityEvent EnemyDamaged = new UnityEvent();

    public bool inRestingDetectDistance, inPatrolingDetectDistance, inChasingMaxDistance, inAttackDistance;

    // LOOT STUFF
    [System.Serializable]
    public class LootTableItem
    {
        public ItemSO item;
        public float dropRate;
    }

    public GameObject LootSack;
    public List<LootTableItem> lootTable;

    public GameObject particlesReceivedDamage;


    private void ToAttack()
    {
        AttackDeltaTime = 0;
        selectedRandomAttack = Random.Range(0, attacks.Count);

        animator.Play(attacks[selectedRandomAttack].AttackAnimationName);
    }

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        target = GameObject.Find("swordman_lv1").transform;//We'll have lots of enemies so instead of assigning the player transform manually it's better option to just use the find function
        CurrentHP = EnemyStats.health;
        healthBar = transform.Find("EnemyCanvas").GetComponent<Canvas>().GetComponentsInChildren<Image>()[1];
        this.AddComponent<Outline>();
        GetComponent<Outline>().enabled = false;
    }
    private void Start()
    {
        InitState(States.RESTING);
        dmgReceivedMultiplier = 1;
        healthBar.fillAmount = CurrentHP / EnemyStats.health;
    }

    void Update()
    {
        UpdateState(CurrentState);
    }


    private void ChangeState(States newState)
    {
        if (newState == CurrentState)
            return;

        ExitState(CurrentState);
        InitState(newState);
    }

    private void InitState(States initState)
    {
        //print("ENTERING " + initState);
        CurrentState = initState;
        StateDeltaTime = 0;

        switch (CurrentState)
        {
            case States.RESTING:
                animator.Play("Sleep");
                break;

            case States.PATROLING:
                animator.Play("WalkForward");
                agent.speed = EnemyStats.patrolingSpeed;
                GoToNewPatrolPoint();
                break;

            case States.CHASING:
                animator.Play("RunForward");
                agent.speed = EnemyStats.chasingSpeed;
                break;
            case States.ATTACKING:
                ToAttack();
                break;

            case States.DYING:
                animator.Play("Death");
                break;
            default:
                break;
        }
    }


    private void UpdateState(States updateState)
    {
        StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {

            case States.RESTING:
                if (inRestingDetectDistance)
                {
                    print("RESTING: Player inside range, checking if I have vision");
                    if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, EnemyStats.restingDetectDistance, ~EnemyMask))
                    {
                        print("RESTING: RayCast Detected object -> " + hit.transform.name);
                        if (hit.transform.gameObject.tag == "Player")
                        {
                            print("RESTING: Player detected");
                            ChangeState(States.CHASING);
                        }
                    }
                }
                break;
            case States.PATROLING:
                if (ArrivedAtDestination())
                {
                    GoToNewPatrolPoint();
                }
                else if (inPatrolingDetectDistance)
                {
                    print("PATROLING: Player inside range, checking if I have vision");
                    //Debug.DrawRay(transform.position, (target.position - transform.position).normalized * BearStats.patrolingDetectDistance, Color.magenta, 2f);
                    if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, EnemyStats.patrolingDetectDistance, ~EnemyMask))
                    {
                        //print("PATROLING: Player detected");
                        //ChangeState(States.CHASING);
                        //Debug.DrawRay(transform.position, (target.position - transform.position).normalized * hit.distance, Color.magenta, 2f);
                        if (hit.transform.gameObject.tag == "Player")
                        {
                            print("PATROLING: Player detected");
                            ChangeState(States.CHASING);
                        }


                    }
                }
                break;
            case States.CHASING:
                agent.SetDestination(target.position);
                if (!inChasingMaxDistance)
                {
                    print("CHASING: Not in chase distance, entering PATROLING");
                    ChangeState(States.PATROLING);
                }
                else if (inAttackDistance)
                {
                    ChangeState(States.ATTACKING);
                }
                break;
            case States.ATTACKING:
                AttackDeltaTime += Time.deltaTime;
                if (!inAttackDistance && AttackDeltaTime >= attacks[selectedRandomAttack].AttackCooldown)
                {
                    ChangeState(States.CHASING);
                }
                else if (inAttackDistance && AttackDeltaTime >= attacks[selectedRandomAttack].AttackCooldown)
                {
                    ToAttack();
                }

                break;
            case States.DYING:
                if (StateDeltaTime >= 3.21f)
                {
                    Destroy(this.gameObject);
                }
                break;

            default:
                break;
        }
    }




    private void ExitState(States exitState)
    {
        //print("EXITING " + exitState);
        switch (exitState)
        {
            case States.RESTING:
                break;
            case States.PATROLING:
                break;
            case States.CHASING:
                break;
            case States.ATTACKING:
                break;
            default:
                break;
        }
    }

    void GoToNewPatrolPoint()
    {
        print("Called GoToNewPatrolPoint");
        WhereImGoing = waypointList[Random.Range(0, waypointList.Count)].position;
        agent.SetDestination(WhereImGoing);
    }


    bool ArrivedAtDestination()
    {
        if (Vector3.Distance(WhereImGoing, transform.position) <= EnemyStats.arrivedAtDestinationDistance) return true;
        return false;
    }


















    /* DAMAGE */
    float currentAttackMultiplier = 1f;
    /*
    DoDamageStats InterfaceDamageable.DoDamage()
    {
        DoDamageStats doDamageStats = new DoDamageStats();
        doDamageStats.dmg = attacks[selectedRandomAttack].AttackDamage * currentAttackMultiplier;

        doDamageStats.estados = myBasicStates;
        return doDamageStats;
    }*/

    public override void ReceiveDmgToHealth(float dmg)
    {
        CurrentHP -= (int)dmg;
        healthBar.fillAmount = CurrentHP / EnemyStats.health;
        if (CurrentHP <= 0 && CurrentState != States.DYING) ToDie();
    }

    public void ReceiveDamage(List<Estado> estados, float damageReceived)
    {

        EnemyDamaged.Invoke();
        ReceiveDmgToHealth(damageReceived);

        // TO-DO AddReceiveing Damage Animations and implement ItemDropSystem
        Instantiate(particlesReceivedDamage, this.transform.position, Quaternion.identity);

        if (estados != null)
        {
            for (int i = 0; i < estados.Count; i++)
            {
                Estado newEstado = currentStates.FirstOrDefault(objecte => objecte.GetType() == estados[i].GetType());
                if (newEstado == null)
                {
                    currentStates.Add(estados[i]);
                }


            }
        }
        CheckApplyStates();

    }

    IEnumerator DestroyParticles()
    {
        yield return new WaitForSeconds(1);
        Destroy(particlesReceivedDamage.gameObject);
    }

    public void ToDie()
    {
        // TO-DO DROP LOOT + GIVE XP TO PLAYER
        print("DIED ENEMY " + this.transform.name);
        List<ItemSO> loot = new List<ItemSO>(); // Create the list with all final drops
        foreach (LootTableItem lootItem in lootTable) // Fill the previous list with drops depending on its drop rate
        {
            if (Random.value <= lootItem.dropRate)
            {
                loot.Add(lootItem.item);
            }
        }
        if (loot.Count > 0) // If the enemy has not dropped anything do not spawn the sack
        {
            List<ObjectItem> lootObjects = new List<ObjectItem>();
            foreach (ItemSO itemModel in loot)
            {
                if (itemModel is ArmorSO)
                {
                    lootObjects.Add(new ObjectArmor((ArmorSO)itemModel));
                }
                else if (itemModel is WeaponSO)
                {
                    lootObjects.Add(new ObjectWeapon((WeaponSO)itemModel));
                }
                else if (itemModel is ConsumableSO)
                {
                    lootObjects.Add(new ObjectConsumable((ConsumableSO)itemModel));
                }
                else if (itemModel is SimpleItemSO)
                {
                    lootObjects.Add(new ObjectSimpleItem((SimpleItemSO)itemModel));
                }
            }
            Instantiate(LootSack, this.transform.position, Quaternion.identity).GetComponent<SackManager>().itemsAvailable = lootObjects;
        }
        Destroy(this.gameObject);
    }
    public override void ReceiveDamage(float damageReceived)
    {
        ReceiveDmgToHealth(damageReceived);
    }





}
