using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectDistance : MonoBehaviour
{
    EnemyBehaviour EnemyBehaviour;

    enum DistanceType { restingDetectDistance, patrolingDetectDistance , chasingMaxDistance, attackDistance, rangeAttackDistance }

    [SerializeField]
    DistanceType distanceType;

    private void Start()
    {
        EnemyBehaviour = GetComponentInParent<EnemyBehaviour>();
        //print("OBTENIDO ->" + EnemyBehaviour.gameObject.name);
    }
    private void OnTriggerEnter(Collider other)
    {
        //if (other.tag == "Player")
        print("PLAYER ENCONTRADO name=>"+other.gameObject.name);
        switch (distanceType)
        {
            case DistanceType.restingDetectDistance:
                EnemyBehaviour.inRestingDetectDistance = true;
                break;
            case DistanceType.patrolingDetectDistance:
                EnemyBehaviour.inPatrolingDetectDistance = true;
                break;
            case DistanceType.chasingMaxDistance:
                EnemyBehaviour.inChasingMaxDistance = true;
                break;
            case DistanceType.attackDistance:
                EnemyBehaviour.inAttackDistance = true;
                break;
            case DistanceType.rangeAttackDistance:
                EnemyBehaviour.inRangeAttackDistance = true;
                break;
            default:
                break;
        }

    }
    private void OnTriggerExit(Collider other)
    {
        //print("PLAYER FUERA DE AREA");
        print("PLAYER FUERA name=>" + other.gameObject.name);
        switch (distanceType)
        {
            case DistanceType.restingDetectDistance:
                EnemyBehaviour.inRestingDetectDistance = false;
                break;
            case DistanceType.patrolingDetectDistance:
                EnemyBehaviour.inPatrolingDetectDistance = false;
                break;
            case DistanceType.chasingMaxDistance:
                EnemyBehaviour.inChasingMaxDistance = false;
                break;
            case DistanceType.attackDistance:
                EnemyBehaviour.inAttackDistance = false;
                break;
            case DistanceType.rangeAttackDistance:
                EnemyBehaviour.inRangeAttackDistance = false;
                break;
            default:
                break;
        }

    }

    


}
