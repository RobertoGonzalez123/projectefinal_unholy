using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2DetectDistance : MonoBehaviour
{
    Boss2Behaviour BossBehaviour;

    enum DistanceType { restingDetectDistance, patrolingDetectDistance , chasingMaxDistance, attackDistance }

    [SerializeField]
    DistanceType distanceType;

    private void Awake()
    {
        BossBehaviour = GetComponentInParent<Boss2Behaviour>();
    }
    private void OnTriggerEnter(Collider other)
    {
        //if (other.tag == "Player")
        //print("PLAYER ENCONTRADO");
        switch (distanceType)
        {
            case DistanceType.restingDetectDistance:
                BossBehaviour.inRestingDetectDistance = true;
                break;
            case DistanceType.patrolingDetectDistance:
                BossBehaviour.inPatrolingDetectDistance = true;
                break;
            case DistanceType.chasingMaxDistance:
                BossBehaviour.inChasingMaxDistance = true;
                break;
            case DistanceType.attackDistance:
                BossBehaviour.inAttackDistance = true;
                break;
            default:
                break;
        }

    }
    private void OnTriggerExit(Collider other)
    {
        //print("PLAYER FUERA DE AREA");
        switch (distanceType)
        {
            case DistanceType.restingDetectDistance:
                BossBehaviour.inRestingDetectDistance = false;
                break;
            case DistanceType.patrolingDetectDistance:
                BossBehaviour.inPatrolingDetectDistance = false;
                break;
            case DistanceType.chasingMaxDistance:
                BossBehaviour.inChasingMaxDistance = false;
                break;
            case DistanceType.attackDistance:
                BossBehaviour.inAttackDistance = false;
                break;
            default:
                break;
        }

    }

    


}
