using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyBehaviourBear : EnemyBehaviour
{
    [Header("Damage effect")]
    [SerializeField] SkinnedMeshRenderer meshRenderer;
    [SerializeField] Material[] damageReceivedMaterial;
    Material[] currentMaterials;


    [SerializeField]GameObject restingAreaShown;

    public override void ReceiveDamage(Hurtbox.DoDamageStats damageStats)
    {
        
        if (currentHP > 0)
        {
            base.ReceiveDamage(damageStats);
            StartCoroutine(DamageEffectCoroutine());
        }
        
    }

    public override void ReceiveDamage(float damageReceived)
    {
        if (currentHP > 0)
        {
            base.ReceiveDmgToHealth(damageReceived);
            StartCoroutine(DamageEffectCoroutine());
        }
    }

    public override void ReceiveDmgToHealth(float dmg)
    {
        base.ReceiveDmgToHealth(dmg);
        ShowDamageNumber(dmg);
    }
    void ShowDamageNumber(float dmg)
    {
        GameObject damageSpawned = GameManager.Instance.DamageNumberTextPrefabPool.GetElement();
        damageSpawned.transform.position = new Vector3(transform.position.x, transform.position.y + 2.3f, transform.position.z);
        damageSpawned.transform.position += damageSpawned.transform.right * Random.Range(-0.5f, 0.5f);

        damageSpawned.GetComponent<TMPro.TextMeshPro>().text = "" + dmg;
    }



    IEnumerator DamageEffectCoroutine()
    {
        meshRenderer.materials = damageReceivedMaterial;
        Instantiate(particlesReceivedDamage, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.1f);
        meshRenderer.materials = currentMaterials;
    }
    [SerializeField] Animator animatorStateAlert;

    private void Start()
    {
        InitState(States.RESTING);
        dmgReceivedMultiplier = 1;

        healthBar.fillAmount = CurrentHP / EnemyStats.health;

        // Copiar los materiales originales al arreglo currentMaterials
        currentMaterials = new Material[meshRenderer.materials.Length];
        for (int i = 0; i < meshRenderer.materials.Length; i++)
        {
            currentMaterials[i] = meshRenderer.materials[i];
        }
    }




    protected override void InitState(States initState)
    {
        //print("ENTERING " + initState);
        CurrentState = initState;
        StateDeltaTime = 0;

        switch (CurrentState)
        {
            case States.RESTING:
                animator.Play("Sleep");
                restingAreaShown.SetActive(true);
                agent.speed = 0;
                break;

            case States.PATROLING:
                animatorStateAlert.Play("Unknown");
                animator.Play("WalkForward");
                agent.speed = EnemyStats.patrolingSpeed;
                GoToNewPatrolPoint();
                break;

            case States.CHASING:
                animatorStateAlert.Play("Exclamation");
                animator.Play("RunForward");
                agent.speed = EnemyStats.chasingSpeed;
                break;
            case States.ATTACKING:
                ToMeleeAttack();
                agent.speed = 0;
                break;

            case States.DYING:
                animator.Play("Death");
                break;
            default:
                break;
        }
    }


    protected override void UpdateState(States updateState)
    {
        StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {
            case States.RESTING:
                if (inRestingDetectDistance)
                {
                    //print("RESTING: Player inside range, checking if I have vision");
                    if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, EnemyStats.restingDetectDistance, ~EnemyMask))
                    {
                        //print("RESTING: RayCast Detected object -> " + hit.transform.name);
                        if (hit.transform.gameObject.tag == "Player")
                        {
                            //print("RESTING: Player detected");
                            ChangeState(States.CHASING);
                        }
                    }
                }
                break;
            case States.PATROLING:
                if (inPatrolingDetectDistance)
                {
                    //print("PATROLING: Player inside range, checking if I have vision");
                    //Debug.DrawRay(transform.position, (target.position - transform.position).normalized * BearStats.patrolingDetectDistance, Color.magenta, 2f);
                    if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, EnemyStats.patrolingDetectDistance, ~EnemyMask))
                    {
                        //print("PATROLING: Player detected");
                        //ChangeState(States.CHASING);
                        //Debug.DrawRay(transform.position, (target.position - transform.position).normalized * hit.distance, Color.magenta, 2f);
                        if (hit.transform.gameObject.tag == "Player")
                        {
                            //print("patroling to chasing");
                            ChangeState(States.CHASING);
                        }
                    }
                }
                else if (StateDeltaTime >= 15f)
                {
                    ChangeState(States.RESTING);
                }
                else if (ArrivedAtDestination())
                {
                    GoToNewPatrolPoint();
                }
                break;
            case States.CHASING:
                agent.SetDestination(target.position);
                //print("target.position " + target.position);
                if (!inChasingMaxDistance)
                {
                    //print("CHASING: Not in chase distance, entering PATROLING");
                    ChangeState(States.PATROLING);
                }
                else if (inAttackDistance)
                {
                    ChangeState(States.ATTACKING);
                }
                break;
            case States.ATTACKING:
                AttackDeltaTime += Time.deltaTime;
                if (!inAttackDistance && AttackDeltaTime >= attacks[selectedRandomAttack].AttackCooldown)
                {
                    ChangeState(States.CHASING);
                }
                else if (inAttackDistance && AttackDeltaTime >= attacks[selectedRandomAttack].AttackCooldown)
                {
                    ToMeleeAttack();
                }

                break;
            case States.DYING:
                if (StateDeltaTime >= 3.21f)
                {
                    Destroy(this.gameObject);
                }
                break;

            default:
                break;
        }
    }


    override protected void ExitState(States exitState)
    {
        //print("EXITING " + exitState);
        switch (exitState)
        {
            case States.RESTING:
                restingAreaShown.SetActive(false);
                break;
            case States.PATROLING:
                break;
            case States.CHASING:
                break;
            case States.ATTACKING:
                break;
            default:
                break;
        }
    }






}
