using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using Unity.VisualScripting;
using System.Linq;
using static Hurtbox;

public abstract class EnemyBehaviour : Damageable
{
    //Estados default del enemigo
    [SerializeField] public List<Estado> myBasicStates = new List<Estado>();


    protected NavMeshAgent agent;
    protected Transform target;
    protected Animator animator;

    [SerializeField] protected EnemyBehaviourStats EnemyStats;
    public float dmgReceivedMultiplier;

    [SerializeField] protected LayerMask EnemyMask;

    public bool inRestingDetectDistance, inPatrolingDetectDistance, inChasingMaxDistance, inAttackDistance, inRangeAttackDistance;

    public List<Transform> waypointList = new List<Transform>();

    protected Vector3 WhereImGoing;
    protected RaycastHit hit;

    public enum States { RESTING, PATROLING, CHASING, ATTACKING, RANGEATTACKING, DYING, PHASE1, PHASE2, STUNNED, GETHITTED, IDLECOMBAT }
    public States CurrentState;

    protected float StateDeltaTime;
    protected float AttackDeltaTime;

    [SerializeField] protected List<AttackSO> attacks;
    protected float currentAttackMultiplier = 1f;
    protected int selectedRandomAttack;




    // LOOT STUFF
    [System.Serializable]
    public class LootTableItem
    {
        public ItemSO item;
        public float dropRate;
    }

    public GameObject LootSack;
    public List<LootTableItem> lootTable;

    public GameObject particlesReceivedDamage;

    IEnumerator DestroyParticles()
    {
        yield return new WaitForSeconds(1);
        Destroy(particlesReceivedDamage.gameObject);
    }

    public virtual void ToDie()
    {
        ChangeState(States.DYING);
        // Remove one enemy from the spawner, so the spawner can Instantiate another enemy
        if (this.GetComponentInParent<SpawnerSpecs>())
        {
            this.GetComponentInParent<SpawnerSpecs>().enemiesSpawned--;
        }

        // TO-DO DROP LOOT + GIVE XP TO PLAYER
        //print("DIED ENEMY " + this.transform.name);
        List<ItemSO> loot = new List<ItemSO>(); // Create the list with all final drops
        foreach (LootTableItem lootItem in lootTable) // Fill the previous list with drops depending on its drop rate
        {
            if (Random.value <= lootItem.dropRate)
            {
                loot.Add(lootItem.item);
            }
        }
        if (loot.Count > 0) // If the enemy has not dropped anything do not spawn the sack
        {
            List<ObjectItem> lootObjects = new List<ObjectItem>();
            foreach (ItemSO itemModel in loot)
            {
                if (itemModel is ArmorSO)
                {
                    lootObjects.Add(new ObjectArmor((ArmorSO)itemModel));
                }
                else if (itemModel is WeaponSO)
                {
                    lootObjects.Add(new ObjectWeapon((WeaponSO)itemModel));
                }
                else if (itemModel is ConsumableSO)
                {
                    lootObjects.Add(new ObjectConsumable((ConsumableSO)itemModel));
                }
                else if (itemModel is SimpleItemSO)
                {
                    lootObjects.Add(new ObjectSimpleItem((SimpleItemSO)itemModel));
                }
                else if (itemModel is MountSO)
                {
                    lootObjects.Add(new ObjectMount((MountSO)itemModel));
                }
            }
            Instantiate(LootSack, this.transform.position, Quaternion.identity).GetComponent<SackManager>().itemsAvailable = lootObjects;
        }
    }

    public void ToMeleeAttack()
    {
        AttackDeltaTime = 0;
        selectedRandomAttack = Random.Range(0, attacks.Count);
        animator.Play(attacks[selectedRandomAttack].AttackAnimationName);
    }

    public virtual void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        target = GameObject.Find("swordman_lv1").transform;//We'll have lots of enemies so instead of assigning the player transform manually it's better option to just use the find function
        CurrentHP = EnemyStats.health;
        GetComponent<Outline>().enabled = false;
    }

    public virtual void Update()
    {
        UpdateState(CurrentState);
    }


    public void ChangeState(States newState)
    {
        if (newState == CurrentState)
            return;

        ExitState(CurrentState);
        InitState(newState);
    }

    protected abstract void InitState(States initState);
    protected abstract void UpdateState(States updateState);
    protected abstract void ExitState(States exitState);
    public void GoToNewPatrolPoint()
    {
        //print("Called GoToNewPatrolPoint");
        WhereImGoing = waypointList[Random.Range(0, waypointList.Count)].position;
        agent.SetDestination(WhereImGoing);
    }
    public bool ArrivedAtDestination()
    {
        if (Vector3.Distance(WhereImGoing, transform.position) <= EnemyStats.arrivedAtDestinationDistance) return true;
        return false;
    }


    /* DAMAGE */
    public override DoDamageStats DoDamage()
    {
        DoDamageStats doDamageStats = new DoDamageStats();
        doDamageStats.dmg = attacks[selectedRandomAttack].AttackDamage * currentAttackMultiplier;
        foreach(Estado.Estados estadoIterado in attacks[selectedRandomAttack].estados)
        {
            if (Random.value<=0.1f) 
                doDamageStats.estados.Add(Estado.GetEstado(estadoIterado, doDamageStats.dmg));
        }
        
        return doDamageStats;
    }

    public override void ReceiveDmgToHealth(float dmg)
    {
        CurrentHP -= (int)dmg;
        healthBar.fillAmount = CurrentHP / EnemyStats.health;
        if (CurrentHP <= 0 && CurrentState != States.DYING) ToDie();
    }

    public override void ReceiveDamage(DoDamageStats damageStats)
    {
        //Debug.Log("ENTRA EN EL enemy behaviour Y Yo si QUIERO QUE ENTRE EN EL ENEMYBEHAVIOUR(OVERRIDE)");
        ReceiveDmgToHealth(damageStats.dmg);
        //print("CurrentHP -> " + this.CurrentHP);
        // TO-DO AddReceiveing Damage Animations and implement ItemDropSystem

        if (damageStats.estados != null)
        {
            for (int i = 0; i < damageStats.estados.Count; i++)
            {
                Estado newEstado = currentStates.FirstOrDefault(objecte => objecte.GetType() == damageStats.estados[i].GetType());
                if (newEstado == null)
                {
                    currentStates.Add(damageStats.estados[i]);
                }
            }
        }
        CheckApplyStates();
    }

    public override void ReceiveDamage(float damageReceived)
    {
        ReceiveDmgToHealth(damageReceived);
    }


    public bool ImAimingToPlayer()
    {
        // Calcula la direcci�n hacia el objetivo
        Vector3 direction = target.position - transform.position;
        direction.y = 0f; // Mant�n la rotaci�n solo en el plano horizontal

       
        // Verifica si el agente est� mirando hacia el objetivo dentro del margen de 10 grados
        bool isLookingAtTarget = Vector3.Angle(transform.forward, direction) <= 5f;

        if (!isLookingAtTarget)
        {
            // Si no est� en el margen, sigue rotando y devuelve false
            // Rota el NavMeshAgent hacia el objetivo
            Quaternion targetRotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * 1f);
            return false;
        }

        // Devuelve true si est� mirando hacia el objetivo dentro del margen
        return true;
    }

}
