using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyBehaviourMetalon : EnemyBehaviour
{
    [Header("Projectile Related")]
    [SerializeField] GameObject projectilePrefab;
    [SerializeField] Transform firePoint;
    [SerializeField] bool rotateOnlyY;
    [SerializeField] float projectileSpeed;
    [SerializeField] AttackSO rangeAttackSO;

    float rangeAttackDeltaTime=0f;
    bool WaitingToRangeAttackAgain=false;
    float timeToWaitBetweenRangeAttacks=7f;

    [SerializeField] Animator animatorStateAlert;

    void RotateToDestination(GameObject obj, Vector3 destination, bool onlyY)
    {
        var direction = destination - obj.transform.position;
        var rotation = Quaternion.LookRotation(direction);

        if (onlyY)
        {
            rotation.x = 0;
            rotation.z = 0;
        }

        obj.transform.localRotation = rotation;
    }

    void ToRangeAttack()
    {
        while (!ImAimingToPlayer())
        {
            agent.updateRotation=false;
            rangeAttackDeltaTime = 0f;
        }
        agent.updateRotation = true;
        StartCoroutine(waitToRangeAttackAgain());
        animator.Play(rangeAttackSO.AttackAnimationName);
        //Debug.Log("TO RANGE ATTACK");

    }
    IEnumerator waitToRangeAttackAgain() {
        WaitingToRangeAttackAgain = true;
        yield return new WaitForSeconds(timeToWaitBetweenRangeAttacks);
        WaitingToRangeAttackAgain = false;
    } 

    public void ToShootProjectile()
    {
        Vector3 destination = new Vector3(target.position.x, target.position.y, target.position.z);
        var projectileObj = Instantiate(projectilePrefab, firePoint.position, Quaternion.identity) as GameObject;
        projectileObj.GetComponent<EnemyProjectile>().doDamageStats.dmg = rangeAttackSO.AttackDamage;
        Projectile projectileScript = projectileObj.GetComponent<Projectile>();
        projectileScript.speed = projectileSpeed;
        RotateToDestination(projectileObj, destination, rotateOnlyY);
        projectileObj.GetComponent<Rigidbody>().velocity = (destination - transform.position).normalized * projectileScript.speed;
    }


    [Header("Damage effect")]
    [SerializeField] SkinnedMeshRenderer meshRenderer;
    [SerializeField] Material[] damageReceivedMaterial;
    Material[] currentMaterials;

    public override void ReceiveDamage(Hurtbox.DoDamageStats damageStats)
    {

        if (currentHP > 0)
        {
            base.ReceiveDamage(damageStats);
            StartCoroutine(DamageEffectCoroutine());
        }

    }

    public override void ReceiveDamage(float damageReceived)
    {
        if (currentHP > 0)
        {
            base.ReceiveDmgToHealth(damageReceived);
            StartCoroutine(DamageEffectCoroutine());
        }
    }

    public override void ReceiveDmgToHealth(float dmg)
    {
        base.ReceiveDmgToHealth(dmg);
        ShowDamageNumber(dmg);
    }
    void ShowDamageNumber(float dmg)
    {
        GameObject damageSpawned = GameManager.Instance.DamageNumberTextPrefabPool.GetElement();
        damageSpawned.transform.position = new Vector3(transform.position.x, transform.position.y + 2.3f, transform.position.z);
        damageSpawned.transform.position += damageSpawned.transform.right * Random.Range(-0.5f, 0.5f);

        damageSpawned.GetComponent<TMPro.TextMeshPro>().text = "" + dmg;
    }

    private void Start()
    {
        InitState(States.PATROLING);
        dmgReceivedMultiplier = 1;

        healthBar.fillAmount = CurrentHP / EnemyStats.health;

        // Copiar los materiales originales al arreglo currentMaterials
        currentMaterials = new Material[meshRenderer.materials.Length];
        for (int i = 0; i < meshRenderer.materials.Length; i++)
        {
            currentMaterials[i] = meshRenderer.materials[i];
        }
    }


    IEnumerator DamageEffectCoroutine()
    {
        meshRenderer.materials = damageReceivedMaterial;
        Instantiate(particlesReceivedDamage, transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.1f);
        meshRenderer.materials = currentMaterials;
    }


    protected override void InitState(States initState)
    {
        //print("METALON ENTERING " + initState);
        CurrentState = initState;
        StateDeltaTime = 0;

        switch (CurrentState)
        {
            case States.RESTING:
                agent.speed = 0;
                animator.Play("Sleep");
                break;

            case States.PATROLING:
                animator.Play("WalkForward");
                agent.speed = EnemyStats.patrolingSpeed;
                GoToNewPatrolPoint();
                break;

            case States.CHASING:
                animator.Play("RunForward");
                agent.speed = EnemyStats.chasingSpeed;
                break;
            case States.ATTACKING:
                agent.speed = 0;
                ToMeleeAttack();
                break;
            case States.RANGEATTACKING:
                agent.speed = 0;
                ToRangeAttack();
                break;

            case States.DYING:
                animator.Play("Death");
                break;
    
            default:
                break;
        }
    }


    protected override void UpdateState(States updateState)
    {
        StateDeltaTime += Time.deltaTime;

        switch (updateState)
        {

            case States.RESTING:
                if (inRestingDetectDistance)
                {
                    //print("RESTING: Player inside range, checking if I have vision");
                    if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, EnemyStats.restingDetectDistance, ~EnemyMask))
                    {
                        //print("RESTING: RayCast Detected object -> " + hit.transform.name);
                        if (hit.transform.gameObject.tag == "Player")
                        {
                            //print("RESTING: Player detected");
                            ChangeState(States.CHASING);
                        }
                    }
                }
                break;
            case States.PATROLING:
                if (ArrivedAtDestination())
                {
                    GoToNewPatrolPoint();
                }
                else if (inPatrolingDetectDistance)
                {
                    //print("PATROLING: Player inside range, checking if I have vision");
                    //Debug.DrawRay(transform.position, (target.position - transform.position).normalized * BearStats.patrolingDetectDistance, Color.magenta, 2f);
                    if (Physics.Raycast(transform.position, (target.position - transform.position).normalized, out hit, EnemyStats.patrolingDetectDistance, ~EnemyMask))
                    {
                        //print("PATROLING: Player detected");
                        //ChangeState(States.CHASING);
                        //Debug.DrawRay(transform.position, (target.position - transform.position).normalized * hit.distance, Color.magenta, 2f);
                        if (hit.transform.gameObject.tag == "Player")
                        {
                            //print("PATROLING: Player detected");
                            ChangeState(States.CHASING);
                        }

                    }
                }
                break;
            case States.CHASING:
                agent.SetDestination(target.position);
                if (!inChasingMaxDistance)
                {
                    //print("CHASING: Not in chase distance, entering PATROLING");
                    ChangeState(States.PATROLING);
                }
                else if (inAttackDistance) ChangeState(States.ATTACKING); 
                else if (inRangeAttackDistance && !WaitingToRangeAttackAgain && !inAttackDistance) ChangeState(States.RANGEATTACKING);
                break;
            case States.ATTACKING:
                AttackDeltaTime += Time.deltaTime;
                if (inAttackDistance && AttackDeltaTime >= attacks[selectedRandomAttack].AttackCooldown)
                {
                    ToMeleeAttack();
                }
                else if (!inAttackDistance && AttackDeltaTime >= attacks[selectedRandomAttack].AttackCooldown 
                    && inRangeAttackDistance && !WaitingToRangeAttackAgain)
                {
                    ChangeState(States.RANGEATTACKING);
                }
                else if (!inAttackDistance && AttackDeltaTime >= attacks[selectedRandomAttack].AttackCooldown)
                {
                    ChangeState(States.CHASING);
                }
                //TO-DO A lo mejor un wait para que no este tan OP
                break;
            case States.RANGEATTACKING:
                rangeAttackDeltaTime+=Time.deltaTime;
                if (inAttackDistance && rangeAttackDeltaTime >= rangeAttackSO.AttackCooldown)
                {
                    ChangeState(States.ATTACKING);
                }
                else if (WaitingToRangeAttackAgain && rangeAttackDeltaTime >= rangeAttackSO.AttackCooldown && inChasingMaxDistance)
                {
                    ChangeState(States.CHASING);
                }
                else if(rangeAttackDeltaTime >= rangeAttackSO.AttackCooldown && !inChasingMaxDistance)
                    ChangeState(States.PATROLING);
                else if (!WaitingToRangeAttackAgain && rangeAttackDeltaTime >= rangeAttackSO.AttackCooldown && inRangeAttackDistance)
                    ToRangeAttack();
                break;
            case States.DYING:
                if (StateDeltaTime >= 3.21f)
                {
                    Destroy(this.gameObject);
                }
                break;

            default:
                break;
        }
    }


    override protected void ExitState(States exitState)
    {
        print("METALON EXITING " + exitState);
        switch (exitState)
        {
            case States.RESTING:
                break;
            case States.PATROLING:
                break;
            case States.CHASING:
                break;
            case States.ATTACKING:
                break;
            case States.RANGEATTACKING:
                break;
            default:
                break;
        }
    }





 
}
