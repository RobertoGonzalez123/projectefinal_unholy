using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyReturnToPoolTime : MonoBehaviour
{
	[SerializeField] float timeToReturnToPool = 0.5f;
	void OnEnable()
	{
		StartCoroutine("CheckIfTime");
	}

	IEnumerator CheckIfTime()
	{

		while (true)
		{
			yield return new WaitForSeconds(timeToReturnToPool);
			this.GetComponent<Poolable>().ReturnToPool();
			break;
		}
	}
}
