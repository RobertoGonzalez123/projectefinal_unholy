using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualEffectManager : MonoBehaviour
{
    public List<GameObject> slashes;

    void Start()
    {
        DisableSlashes();
    }

   
    void DisableSlashes()
    {
        for (int i = 0; i < slashes.Count; i++)
            slashes[i].SetActive(false);
    }
}


