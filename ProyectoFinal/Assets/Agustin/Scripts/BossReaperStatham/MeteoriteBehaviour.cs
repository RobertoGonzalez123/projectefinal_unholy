using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteoriteBehaviour : MonoBehaviour
{
    public GameObject bordeCirculoObject;
    public GameObject circuloRojoObject;
    public GameObject meteoritoPrefab;
    float targetScale = 0.5f;
    [SerializeField]float growthTime = 3f;

    Vector3 initialChildPosition;
    Vector3 targetChildPosition;
    float growthTimer = 0f;
    bool settingsSetted = false;

    public Transform playerTransform;

    public void LoadSettings(AttackSO attackSettings,Transform player)
    {
        meteoritoPrefab.GetComponent<ProjectileMoveScript>().LoadDamage(attackSettings);
        playerTransform = player;
        initialChildPosition = meteoritoPrefab.transform.position;
        settingsSetted = true;
    }
    


    private void Update()
    {
        if (settingsSetted)
        {
            growthTimer += Time.deltaTime;
            if (growthTimer < growthTime * 0.75f)
            {
                bordeCirculoObject.transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, playerTransform.position.z);
                circuloRojoObject.transform.position = new Vector3(playerTransform.position.x, playerTransform.position.y, playerTransform.position.z);
            }
            if (growthTimer < growthTime && meteoritoPrefab)
            {
                // Incrementar el temporizador de crecimiento
                growthTimer += Time.deltaTime;

                // Calcular el factor de escala actual en funci�n del temporizador
                float scaleProgress = Mathf.Clamp01(growthTimer / growthTime);
                float currentScale = Mathf.Lerp(0.05f, targetScale, scaleProgress);

                // Aplicar la escala al objeto padre
                circuloRojoObject.transform.localScale = Vector3.one * currentScale;
                // Mover el objeto hijo hacia la posici�n objetivo
                meteoritoPrefab.transform.position = Vector3.Lerp(initialChildPosition, circuloRojoObject.transform.position, scaleProgress);
                meteoritoPrefab.transform.LookAt(circuloRojoObject.transform.position);
            }
            else if (growthTime + 1.7f > growthTimer)
            {
                Destroy(gameObject);
            }
          
        }
    }
}
