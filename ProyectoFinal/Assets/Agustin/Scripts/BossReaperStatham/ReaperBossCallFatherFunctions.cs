using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReaperBossCallFatherFunctions : MonoBehaviour
{
    ReaperBossBehaviour bossBehaviour;
    void Start()
    {
        bossBehaviour = GetComponentInParent<ReaperBossBehaviour>();
    }

    /* -------------------------------------------------------------------------------------
     * Script used just to call BossBehaviour(father) functions from the Animator Component 
     ---------------------------------------------------------------------------------------*/
    // SETTING ATTACKS SO
    public void SetAttackScytheLight()
    {
        bossBehaviour.SetAttackScytheLight();
    }
    public void SetAttackScytheHeavy()
    {
        bossBehaviour.SetAttackScytheHeavy();
    }
    public void SetAttackSpellRing()
    {
        bossBehaviour.SetAttackSpellRing();
    }
    public void SetAttackSpellThrow()
    {
        bossBehaviour.SetAttackSpellThrow();
    }

    // CALLING CAST INSTANTIATIONS
    public void ToCastRingNormal()
    {
        bossBehaviour.ToCastRingNormal();
    }
    public void ToCastRingFast()
    {
        bossBehaviour.ToCastRingFast();
    }
    public void ToCastThrowable()
    {
        bossBehaviour.ToCastThrowable();
    }
}
