using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InMeleeAttackArea : MonoBehaviour
{
    [SerializeField]ReaperBossBehaviour myScript;

    private void OnTriggerEnter(Collider other)
    {
        myScript.inMeleeAttackRange = true;
    }
    private void OnTriggerExit(Collider other)
    {
        myScript.inMeleeAttackRange = false;
    }

}
