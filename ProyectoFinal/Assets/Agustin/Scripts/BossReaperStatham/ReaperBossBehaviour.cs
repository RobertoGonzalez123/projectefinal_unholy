using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
using static Hurtbox;
using System.Linq;
using UnityEngine.UI;

public class ReaperBossBehaviour : Damageable
{
    [Header("Damage effect Render Related")]
    [SerializeField] SkinnedMeshRenderer meshRenderer;
    [SerializeField] Material[] damageReceivedMaterial;
    Material[] currentMaterials;


    [Header("Own Utils")]
    [SerializeField] CapsuleCollider bossHurtbox;
    [SerializeField] GameObject buffEffectGameObject;
    [SerializeField] Image stunFillR;
    [SerializeField] Image stunFillL;
    IEnumerator spawnMeteoritesCoroutine;
    NavMeshAgent agent;
    Transform playerTransform;
    Animator animator;
    

    [Header("Settings")]
    [SerializeField] LayerMask EnemyMask;


    [Header("Stats")]
    [SerializeField] float normalSpeed = 6f;
    [SerializeField] float fastSpeed = 10f;
    [SerializeField] float maxHealth = 1500f;
    [SerializeField] float stunnedMax = 500f;



    [Header("Current Used Params")]
    [SerializeField] float dmgReceivedMultiplier = 1;
    [SerializeField] float currentDamageDoneMultiplier = 1f;
    [SerializeField] float stunnedRecovery = 1f;
    [SerializeField] float miniumDistanceToTeleport = 20f;
    [SerializeField] float miniumDistanceToPlayer = 4f;
    [SerializeField] float maximumDistanceToPlayer = 25f;
    public bool inMeleeAttackRange;
    States CurrentState;
    States lastState;
    AttackSO currentAttack;
    float currentStunned = 0f;
    float StateDeltaTime = 0f;
    

    [Header("Attacks")]
    [SerializeField] AttackSO scytheAttackLight;
    [SerializeField] AttackSO scytheAttackHeavy;
    [SerializeField] AttackSO spellAttackRing;
    [SerializeField] AttackSO spellAttackThrow;
    [SerializeField] AttackSO meteoriteAttack;
    [SerializeField] float throwableSpeed = 10f;

    [Header("Tranforms List")]
    [SerializeField] Transform ringInstantiatePoint;
    [SerializeField] Transform throwInstantiatePoint;
    [SerializeField] Transform[] teleportList;

    [Header("Prefabs")]
    [SerializeField] GameObject ringShockwavePrefabPurple;
    [SerializeField] GameObject ringShockwavePrefabRed;
    [SerializeField] GameObject throwablePrefab;
    [SerializeField] GameObject meteoritePackPrefab;

    [Header("Saved State Times")]
    [SerializeField] float spellAttackNormalRingDuration = 10f;
    [SerializeField] float spellAttackFastRingDuration = 5f;
    [SerializeField] float spellAttackThrowableDuration = 5f;
    [SerializeField] float stunnedDuration = 3f;
    [SerializeField] float getInGroundDuration = 2f;
    [SerializeField] float getOutGroundDuration = 3f;
    [SerializeField] float comboADuration = 2.93f;
    [SerializeField] float comboBDuration = 2.93f;
    [SerializeField] float comboCDuration = 3.6f;
    override public float CurrentHP
    {
        get => currentHP;
        set
        {
            currentHP = value;
            float healthPercentage = currentHP / maxHealth;
            healthBar.fillAmount = healthPercentage;

            // Por debajo del 25% de vida
            if (healthPercentage <= 0.25f)
            {
                dmgReceivedMultiplier = 0.25f;
                currentDamageDoneMultiplier = 2f;
            }
            // Entre el 25% y el 50% de vida
            else if (healthPercentage <= 0.5f)
            {
                dmgReceivedMultiplier = 0.5f;
                currentDamageDoneMultiplier = 1.5f;
                if (spawnMeteoritesCoroutine == null)
                {
                    spawnMeteoritesCoroutine = MeteoriteRainCoroutine();
                    StartCoroutine(spawnMeteoritesCoroutine);
                }
            }
        }
    }

    public float CurrentStunned
    {
        get => currentStunned;
        set
        {
            currentStunned = value;
            if(CurrentState != States.DYING)
            {
                stunFillL.fillAmount = currentStunned / stunnedMax;
                stunFillR.fillAmount = currentStunned / stunnedMax;
                if (CurrentStunned > stunnedMax)
                {
                    CurrentStunned = stunnedMax;
                    ChangeState(States.STUNNED);
                }
            }
        }
    }

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        
    }
    private void Start()
    {
        playerTransform = AGUSMOVEMENT.Instance.playerObj.transform;
        InitState(States.IDLE);
        dmgReceivedMultiplier = 1;
        currentHP = maxHealth;
        CopyInitialMaterials();
    }

    private void Update()
    {
        UpdateState(CurrentState);
    }

    void CopyInitialMaterials() { 
        currentMaterials = new Material[meshRenderer.materials.Length];
        for (int i = 0; i < meshRenderer.materials.Length; i++)
        {
            currentMaterials[i] = meshRenderer.materials[i];
        }
    }


    enum States { STUNNED,DYING,IDLE, CASTRINGNORMAL, CASTRINGFAST,CASTTHROW,GETINGROUND,GETOUTOFGROUND,COMBOA,COMBOB,COMBOC }
    

    public override DoDamageStats DoDamage()
    {
        DoDamageStats doDamageStats = new DoDamageStats();
        doDamageStats.dmg = currentAttack.AttackDamage * currentDamageDoneMultiplier;
        return doDamageStats;
    }


    public override void ReceiveDamage(DoDamageStats damageStats)
    {
        ReceiveDmgToHealth(damageStats.dmg);
        if (damageStats.estados != null)
        {
            for (int i = 0; i < damageStats.estados.Count; i++)
            {
                Estado newEstado = currentStates.FirstOrDefault(objecte => objecte.GetType() == damageStats.estados[i].GetType());
                if (newEstado == null) currentStates.Add(damageStats.estados[i]);
            }
        }
        CheckApplyStates();
    }

    public override void ReceiveDamage(float damageReceived)
    {
        ReceiveDmgToHealth(damageReceived);
    }

    public override void ReceiveDmgToHealth(float dmg)
    {
        StartCoroutine(DamageEffectCoroutine());
        if (CurrentState != States.STUNNED) CurrentStunned += dmg*dmgReceivedMultiplier;
        CurrentHP -= dmg * dmgReceivedMultiplier;
        if (CurrentHP <= 0 && CurrentState != States.DYING) ToDie();
    }

    public virtual void ToDie()
    {
        ChangeState(States.DYING);
    }

    IEnumerator DamageEffectCoroutine()
    {
        meshRenderer.materials = damageReceivedMaterial;
        yield return new WaitForSeconds(0.16f);
        meshRenderer.materials = currentMaterials;
    }
    IEnumerator MeteoriteRainCoroutine()
    {
        yield return new WaitForSeconds(1f);
        int numToSpawn = Random.Range(4, 7);
        for (int i = 0; i < numToSpawn; i++)
        {
            yield return new WaitForSeconds(3);
            Instantiate(meteoritePackPrefab, playerTransform.position, Quaternion.identity).GetComponent<MeteoriteBehaviour>().LoadSettings(meteoriteAttack, playerTransform);
        }
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(20,40));
            numToSpawn = Random.Range(4, 7);
            for (int i = 0; i < numToSpawn; i++)
            {
                yield return new WaitForSeconds(3);
                Instantiate(meteoritePackPrefab, playerTransform.position, Quaternion.identity).GetComponent<MeteoriteBehaviour>().LoadSettings(meteoriteAttack, playerTransform);
            }
        }
        
        
    }
    void WhatToDoNext()
    {
        int randomAction = Random.Range(1,7);
        if (randomAction==1 && lastState!=States.CASTRINGNORMAL && lastState != States.GETOUTOFGROUND && CurrentState != States.CASTRINGNORMAL)
        {
            ChangeState(States.CASTRINGNORMAL);
        }
        else if (randomAction == 2 && lastState != States.CASTRINGFAST && lastState != States.GETOUTOFGROUND && CurrentState != States.CASTRINGFAST)
        {
            ChangeState(States.CASTRINGFAST);
        }
        else if (randomAction == 3 && lastState != States.CASTTHROW  && CurrentState != States.CASTTHROW)
        {
            ChangeState(States.CASTTHROW);
        }
        else if (randomAction == 4 && lastState != States.GETINGROUND && lastState != States.GETOUTOFGROUND && CurrentState != States.GETINGROUND)
        {
            ChangeState(States.GETINGROUND);
        }
        else if (randomAction == 5 && !lastStateWasMeleeAttack())
        {
            ChangeState(States.COMBOA);
        }
        else if (randomAction == 6 && !lastStateWasMeleeAttack())
        {
            ChangeState(States.COMBOB);
        }
        else if (randomAction == 7 && !lastStateWasMeleeAttack())
        {
            ChangeState(States.COMBOC);
        }
        else
        {
            WhatToDoNext();
        }
    }

    bool lastStateWasMeleeAttack()
    {
        if (lastState == States.COMBOA || lastState == States.COMBOB || lastState == States.COMBOC)
            return true;
        if (CurrentState == States.COMBOA || CurrentState == States.COMBOB || CurrentState == States.COMBOC)
            return true;
        return false;

    }


    public void TeleportToAnotherPlace()
    {
        Transform randomTransform = teleportList[Random.Range(0, teleportList.Length)];
        if (Vector3.Distance(transform.position, randomTransform.position) >= miniumDistanceToTeleport)
            agent.Warp(randomTransform.position);
        else
            TeleportToAnotherPlace();
    }

    public bool TeleportToNearestPosition()
    {
        if(Vector3.Distance(transform.position,playerTransform.position)< maximumDistanceToPlayer) return false;

        // Buscar la posici�n m�s cercana al jugador
        float closestDistance = Mathf.Infinity;
        Vector3 closestPosition = Vector3.zero;

        foreach (Transform teleportPosition in teleportList)
        {
            float distance = Vector3.Distance(teleportPosition.position, playerTransform.position);
            if (distance >= miniumDistanceToPlayer && distance < closestDistance)
            {
                closestDistance = distance;
                closestPosition = teleportPosition.position;
            }
        }

        // Teletransportar el agente a la posici�n m�s cercana
        agent.Warp(closestPosition);
        return true;
    }

    void ChangeState(States newState)
    {
        if (newState == CurrentState)
            return;

        ExitState(CurrentState);
        lastState = CurrentState;
        InitState(newState);
    }


    bool isWaitingToAttack;
    #region Init/Update/Exit State
    void InitState(States initState)
    {
        print("ENTERING " + initState);
        CurrentState = initState;
        StateDeltaTime = 0;
        isWaitingToAttack = false;
        switch (CurrentState)
        {
            case States.COMBOA:
                if (!inMeleeAttackRange)
                {
                    animator.SetTrigger("ToFloatForward");
                    isWaitingToAttack = true;
                }
                else
                    animator.SetTrigger("ToComboA");
                break;
            case States.COMBOB:
                if (!inMeleeAttackRange)
                {
                    animator.SetTrigger("ToFloatForward");
                    isWaitingToAttack = true;
                }
                else
                    animator.SetTrigger("ToComboB");
                break;
            case States.COMBOC:
                if (!inMeleeAttackRange)
                {
                    animator.SetTrigger("ToFloatForward");
                    isWaitingToAttack = true;
                }
                else
                    animator.SetTrigger("ToComboC");
                break;
            case States.STUNNED:
                agent.speed = 0f;
                animator.SetTrigger("ToStunned");
                break;
            case States.CASTTHROW:
                agent.speed = 0f;
                ForceRotation();
                animator.SetTrigger("ToSpellThrow");
                break;
            case States.DYING:
                agent.speed = 0f;
                this.GetComponentInParent<BossRoomScript>().OnBossDefeated();
                this.bossHurtbox.enabled = false;
                animator.SetTrigger("ToDeath");
                StopCoroutine(spawnMeteoritesCoroutine);
                break;
            case States.CASTRINGNORMAL:
                agent.speed = 0f;
                ForceRotation();
                animator.SetTrigger("ToRingNormal");
                break;
            case States.CASTRINGFAST:
                agent.speed = 0f;
                ForceRotation();
                animator.SetTrigger("ToRingFast");
                break;
            case States.GETINGROUND:
                animator.SetTrigger("ToGetInGround");
                agent.speed = 0f;
                break;
            case States.GETOUTOFGROUND:
                animator.SetTrigger("ToGetOutGround");
                TeleportToAnotherPlace();
                agent.speed = 0f;
                break;
            default:
                break;
        }
    }
    void ForceRotation()
    {
        Vector3 targetDirection = playerTransform.position - transform.position;
        targetDirection.y = 0f; 
        Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
        Quaternion newRotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 22f * Time.deltaTime);
        transform.rotation = newRotation;
    }
    void GoToPlayer()
    {
        // Obtener la direcci�n y la distancia entre el agente y el jugador
        Vector3 direction = playerTransform.position - agent.transform.position;
        float distance = direction.magnitude;

        // Calcular la posici�n de destino con el margen de 1.5 unidades
        Vector3 targetPosition = playerTransform.position - direction.normalized * (distance - 1.5f);

        // Establecer la posici�n de destino al agente
        agent.speed = normalSpeed;
        agent.SetDestination(targetPosition);
    }

    void UpdateState(States updateState)
    {
        if(!isWaitingToAttack) StateDeltaTime += Time.deltaTime;
        if (updateState != States.STUNNED && CurrentStunned>0) CurrentStunned -= Time.deltaTime * stunnedRecovery;

        switch (updateState)
        {
            case States.COMBOA:
                if (isWaitingToAttack)
                {
                    if (inMeleeAttackRange)
                    {
                        isWaitingToAttack = false;
                        animator.SetTrigger("ToComboA");
                    }
                    else
                        if(!TeleportToNearestPosition())
                            GoToPlayer();
                }
                else if (StateDeltaTime >= comboADuration)
                        WhatToDoNext();
                break;
            case States.COMBOB:
                if (isWaitingToAttack)
                {
                    if (inMeleeAttackRange)
                    {
                        isWaitingToAttack = false;
                        animator.SetTrigger("ToComboB");
                    }
                    else
                        if (!TeleportToNearestPosition())
                        GoToPlayer();
                }
                else if (StateDeltaTime >= comboBDuration)
                    WhatToDoNext();
                break;
            case States.COMBOC:
                if (isWaitingToAttack)
                {
                    if (inMeleeAttackRange)
                    {
                        isWaitingToAttack = false;
                        animator.SetTrigger("ToComboC");
                    }
                    else
                        if (!TeleportToNearestPosition())
                        GoToPlayer();
                }
                else if (StateDeltaTime >= comboCDuration)
                    WhatToDoNext();
                break;
            case States.STUNNED:
                CurrentStunned = Mathf.Lerp(stunnedMax, 0f, StateDeltaTime / stunnedDuration);
                if (StateDeltaTime >=stunnedDuration)
                {
                    ChangeState(States.GETINGROUND);
                    agent.speed = 0;
                }
                break;
            case States.CASTTHROW:
                ForceRotation();
                if (StateDeltaTime >= spellAttackThrowableDuration)
                {
                    WhatToDoNext();
                }
                break;
            
            case States.CASTRINGNORMAL:
                ForceRotation();
                if (StateDeltaTime >= spellAttackNormalRingDuration)
                {
                    WhatToDoNext();
                }
                break;
            case States.CASTRINGFAST:
                ForceRotation();
                if (StateDeltaTime >= spellAttackFastRingDuration)
                {
                    WhatToDoNext();
                }
                break;
            case States.DYING:
                if (StateDeltaTime >= 6f)
                {
                    Destroy(this.gameObject);
                }
                break;
            case States.GETINGROUND:
                if (StateDeltaTime >= getInGroundDuration)
                {
                    ChangeState(States.GETOUTOFGROUND);
                }
                break;
            case States.GETOUTOFGROUND:
                if (StateDeltaTime >= getOutGroundDuration)
                {
                    if(CurrentHP>=maxHealth*0.75f)
                        ChangeState(States.CASTRINGNORMAL);
                    else
                        ChangeState(States.CASTRINGFAST);
                }
                break;
            case States.IDLE:
                if (StateDeltaTime >= 7f)
                    WhatToDoNext();
                    
                break;
            default:
                break;
        }
    }


    void ExitState(States exitState)
    {
        print("EXITING " + exitState);
        switch (exitState)
        {
            case States.STUNNED:
                CurrentStunned = 0f;
                agent.speed = normalSpeed;
                break;
            case States.DYING:
                break;
            default:
                break;
        }
    }
    #endregion

    #region CalledFromChildAnimationController
    /*------------------------------------
     * START SET CURRENT ATTACK SO  */
    public void SetAttackScytheLight()
    {
        this.currentAttack = scytheAttackLight;
    }
    public void SetAttackScytheHeavy()
    {
        this.currentAttack = scytheAttackHeavy;
    }
    public void SetAttackSpellRing()
    {
        this.currentAttack = spellAttackRing;
    }
    public void SetAttackSpellThrow()
    {
        this.currentAttack = spellAttackThrow;
    }
    /* END SET CURRENT ATTACK SO 
     ------------------------------------*/


    /*------------------------------------
     * START CALL SPAWN CASTS */
    public void ToCastRingNormal()
    {
        Instantiate(ringShockwavePrefabPurple, ringInstantiatePoint.transform.position - new Vector3(0, 0.15f,0), ringShockwavePrefabPurple.transform.rotation).GetComponent<RingBehaviour>().LoadDamage(spellAttackRing);
    }
    public void ToCastRingFast()
    {
        Instantiate(ringShockwavePrefabRed, ringInstantiatePoint.transform.position - new Vector3(0, 0.15f, 0), ringShockwavePrefabRed.transform.rotation).GetComponent<RingBehaviour>().LoadDamage(spellAttackRing);
    }
    public void ToCastThrowable()
    {
        Instantiate(throwablePrefab, throwInstantiatePoint.transform.position, throwablePrefab.transform.rotation).GetComponent<ProjectileMoveScript>().GoToPoint(playerTransform.position+ new Vector3(0, 1, 0), throwableSpeed,spellAttackThrow);
    }
    /* START CALL SPAWN CASTS 
     ------------------------------------*/
    #endregion

}
