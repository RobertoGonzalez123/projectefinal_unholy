using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingBehaviour : OnlyDamaging
{
    public void LoadDamage(AttackSO attackSettings)
    {
        doDamageStats.dmg = attackSettings.AttackDamage;
        foreach(Estado.Estados unEstado in attackSettings.estados)
        {
            doDamageStats.estados.Add(new Estado(unEstado, attackSettings.AttackDamage / 10, 10f));
        }
        
    }
    private void OnEnable()
    {
        StartCoroutine("destroyCoroutine");
    }
    IEnumerator destroyCoroutine()
    {
        yield return new WaitForSeconds(7f);
        Destroy(gameObject);
    }



}
