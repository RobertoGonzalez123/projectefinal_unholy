using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSwitchingSystem : MonoBehaviour
{
    public enum WeaponType { PUNCH,BIGSWORD,SWORD,AXE,HAMMER, CLUB}


    public WeaponType m_CurrentWeapon;

    [Header("List of Weapon GameObjects to enable")]
    [SerializeField] GameObject punchGO;
    [SerializeField] GameObject BigSwordGO;
    [SerializeField] GameObject SwordGO;
    [SerializeField] GameObject AxeGO;
    [SerializeField] GameObject HammerGO;
    [SerializeField] GameObject ClubGO;

    private void Start()
    {
        EquipmentManager.Instance.onEquipmentChanged += ActivateWeaponGO;
    }
    void ActivateWeaponGO()
    {
        if (EquipmentManager.Instance.currentEquipment[4]?.itemModel?.name != null)
            m_CurrentWeapon = ((WeaponSO)EquipmentManager.Instance.currentEquipment[4]?.itemModel).weaponType;
        else
            m_CurrentWeapon = WeaponType.PUNCH;
        
        BigSwordGO.SetActive(false);
        SwordGO.SetActive(false);
        AxeGO.SetActive(false);
        HammerGO.SetActive(false);
        punchGO.SetActive(false);
        ClubGO.SetActive(false);
        switch (m_CurrentWeapon)
        {
            case WeaponType.PUNCH:
                punchGO.SetActive(true);
                break;
            case WeaponType.BIGSWORD:
                BigSwordGO.SetActive(true);
                break;
            case WeaponType.SWORD:
                SwordGO.SetActive(true);
                break;
            case WeaponType.AXE:
                AxeGO.SetActive(true);
                break;
            case WeaponType.HAMMER:
                HammerGO.SetActive(true);
                break;
            case WeaponType.CLUB:
                ClubGO.SetActive(true);
                break;
            default:
                break;
        }
        
    }


}
