using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyReturnToPool : MonoBehaviour
{
	[SerializeField] Poolable thisPooleable;
	[SerializeField] float timeToReturnToPool = 0.5f;
	void OnEnable()
	{
		StartCoroutine("CheckIfAlive");
	}

	IEnumerator CheckIfAlive()
	{
		ParticleSystem ps = this.GetComponent<ParticleSystem>();

		while (true && ps != null)
		{
			yield return new WaitForSeconds(timeToReturnToPool);
			if (!ps.IsAlive(true))
			{
				thisPooleable.ReturnToPool();
				break;
			}
		}
	}
}
