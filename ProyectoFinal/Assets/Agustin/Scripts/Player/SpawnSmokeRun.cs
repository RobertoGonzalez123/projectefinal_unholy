using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSmokeRun : MonoBehaviour
{
    AGUSMOVEMENT playerScript;
   
    [SerializeField] Pool pool;
    [SerializeField] float timeBetweenSpawn = 0.3f;
    IEnumerator currentCoroutine;
    private void Awake()
    {
        playerScript = GetComponentInParent<AGUSMOVEMENT>();
       
    }
   

    private void Update()
    {
        if (playerScript.grounded)
        {
            if (playerScript.getCurrentMoveSpeed >= 5)
            {
                if (currentCoroutine == null)
                {
                    currentCoroutine = spawnDust();
                    StartCoroutine(currentCoroutine);
                }
            }
            else if (currentCoroutine != null)
            {
                StopCoroutine(currentCoroutine);
                currentCoroutine = null;
            }

        }
        else if (currentCoroutine != null)
        {
            StopCoroutine(currentCoroutine);
            currentCoroutine = null;
        }
            
    }
    IEnumerator spawnDust()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeBetweenSpawn);
            GameObject dust = pool.GetElement();
            dust.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
        }
    }
   
}
