using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSounds : MonoBehaviour
{
    AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayFootstep1()
    {
        AudioClip footstep1 = Resources.Load<AudioClip>("Foot1");
        audioSource.PlayOneShot(footstep1);
    }

    public void PlayFootstep2()
    {
        AudioClip footstep2 = Resources.Load<AudioClip>("Foot2");
        audioSource.PlayOneShot(footstep2);
    }

    public void PlayFootstep3()
    {
        AudioClip footstep3 = Resources.Load<AudioClip>("Foot3");
        audioSource.PlayOneShot(footstep3);
    }

}
