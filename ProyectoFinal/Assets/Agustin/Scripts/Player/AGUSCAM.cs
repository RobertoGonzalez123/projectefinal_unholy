using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AGUSCAM : MonoBehaviour
{
    [Header("References")]
    public Transform orientation;
    public Transform playerObj;
    public GameObject ponercamera; 

    [Header("Speed")]
    public float rotationSpeed;

    Rigidbody rb;
    AGUSMOVEMENT scriptPlayer;

    
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        scriptPlayer = GetComponent<AGUSMOVEMENT>();
    }

    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
       
        // rotate orientation
        Vector3 viewDir = this.transform.position - new Vector3(ponercamera.transform.position.x, this.transform.position.y, ponercamera.transform.position.z);
        orientation.forward = viewDir.normalized;

        // roate player object
        Vector3 MO = scriptPlayer.m_InputMap.Player.Move.ReadValue<Vector3>();
        Vector3 movementtmp = Vector3.zero;
        if (MO.x > 0.5f)// D
            movementtmp.x += 1;
        else if (MO.x < -0.5f) // A 
            movementtmp.x += -1;
        if (MO.z > 0.5f)// W
            movementtmp.z += 1;
        else if (MO.z < -0.5f)// S
            movementtmp.z += -1;

        
        Vector3 inputDir = orientation.forward * movementtmp.z + orientation.right * movementtmp.x;
        if (inputDir != Vector3.zero)
            playerObj.forward = Vector3.Slerp(playerObj.forward, inputDir.normalized, Time.deltaTime * rotationSpeed);
    }

   
}
