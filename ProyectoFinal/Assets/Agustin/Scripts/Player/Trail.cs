using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Trail : MonoBehaviour
{
    public float DestroyTime = 1f;
    [Header("Mesh Related")]
    public float meshRefreshRate = 0.1f;
    public Transform positionToSpawn;
    public Material materialSelected;

    private bool isTrailActive;
    private SkinnedMeshRenderer[] skinnedMeshRenderers;
    
    [Header("VFX Related")]
    public VisualEffect vfxGraphParticlesTrail;


    private void Start()
    {
        vfxGraphParticlesTrail.Stop();
    }

    public void ToTrail(float time)
    {
        if (!isTrailActive)
        {
            isTrailActive = true;
            StartCoroutine(ActivateTrail(time));
        }
    }

    IEnumerator ActivateTrail(float timeActive)
    {
        vfxGraphParticlesTrail.Play();

        yield return new WaitForSeconds(0.2f);
        while (timeActive > 0)
        {
            timeActive -= meshRefreshRate;
            if (skinnedMeshRenderers == null)
                skinnedMeshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
            for (int i = 0; i < skinnedMeshRenderers.Length; i++)
            {
                GameObject gObj = new GameObject();

                gObj.transform.SetPositionAndRotation(skinnedMeshRenderers[i].transform.position, skinnedMeshRenderers[i].transform.rotation);
                MeshRenderer mr = gObj.AddComponent<MeshRenderer>();
                MeshFilter mf = gObj.AddComponent<MeshFilter>();

                Mesh mesh = new Mesh();
                skinnedMeshRenderers[i].BakeMesh(mesh);
                mf.mesh = mesh;
                mr.material = materialSelected;
                Destroy(gObj, DestroyTime);
            }
            yield return new WaitForSeconds(meshRefreshRate);
        }
        vfxGraphParticlesTrail.Stop();

        isTrailActive = false;
    }
}
