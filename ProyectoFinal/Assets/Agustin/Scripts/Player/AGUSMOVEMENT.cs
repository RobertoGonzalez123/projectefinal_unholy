using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FiniteStateMachine;
using UnityEngine.UI;
using System.Linq;
using static Hurtbox;
using UnityEngine.SceneManagement;

public class AGUSMOVEMENT : Damageable
{
    /*
    * 
    *  HORSE START
    * 
    */
    [Header("HORSE RELATED")]
    public bool isRidingHorse = false;
    [SerializeField] public GameObject HorseGameO;
    [SerializeField] public Animator HorseAnimator;
    public void ToggleHorse()
    {
        isRidingHorse = !isRidingHorse;
        GameManager.Instance.ExplosionHorseSpawn.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        GameManager.Instance.ExplosionHorseSpawn.SetActive(true);
        m_FSM.ChangeState<Idle>();
        playerObj.gameObject.SetActive(!isRidingHorse);
        HorseGameO.SetActive(isRidingHorse);
        if (isRidingHorse) HorseGameO.transform.rotation = playerObj.transform.rotation;
        else playerObj.transform.rotation = HorseGameO.transform.rotation;
    }

    /*
     * 
     * HORSE END
     * 
     */

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow) && EquipmentManager.Instance.currentEquipment[5]?.itemModel?.ItemName != null)
            ToggleHorse();
        if (shouldPlayerObjectRotate) RotateOrientation();

        if (!isRidingHorse)
        {
            m_FSM.Update();
        }

    }
    [Header("PLAYER RELATED")]
    public bool dmgInvulnerable = false;
    public SkillManager equipableSkills;

    [Header("Damage Received Related")]
    [SerializeField] GameEvent receivedDamageEvent;
    [SerializeField] SkinnedMeshRenderer[] meshRenderers;
    [SerializeField] Material[] damageReceivedMaterial;
    Material[] currentMaterials;

    [Header("References")]
    public Transform orientation;
    public Transform playerObj;
    public GameObject ponercamera;
    [SerializeField] MovementValues movementValues;


    [Header("Speed")]
    public float rotationSpeed;


    [Header("Movement")]
    [Tooltip("Current Movement Speed it's Value's updated on entering state")] public float moveSpeed;
    //public float walkSpeed;
    //public float runSpeed;

    [Header("Jump")]
    public float jumpForce;
    public float jumpCooldown;
    public float airMultiplier;
    public bool readyToJump;

    [Header("Ground Check")]
    public float playerHeight;
    public LayerMask whatIsGround;
    public bool grounded = true;

    [Header("Player Stats")]
    public PlayerStats playerStats;
    public bool sprint = false;
    IEnumerator staminaRecoveryInExecution;
    IEnumerator manaRecoveryInExecution;

    WeaponSwitchingSystem m_WeaponSwitchingSystem;

    Animator m_animator;
    Rigidbody rb;
    public FSM m_FSM;

    public PlayerController m_InputMap;


    public List<EstadoTemporal> estadosTemporales = new List<EstadoTemporal>();


    Vector3 moveDirection;

    /* INTERFACE DAMAGEABLE */
    private float currentAttackMultiplier = 1f;
    private float currentArmor;

    public float CurrentArmor { get => currentArmor; set => currentArmor = value; }

    [Header("Skills")]
    public PlayerSkillsSO playerSkills;

    [Header("GUI_SLIDERS")]
    [SerializeField] protected Image staminaBar;
    [SerializeField] protected Image manaBar;

    [Header("NPC Interactions")]
    public bool isPlayerTalking;
    public bool isPlayerAbleToMove;
    public QuestSO activeQuest;
    public GameObject DialogPanel;
    public GameObject NPCSellPanel;
    public GameObject AllGUICloseable;

    [Header("Blend Times")]
    public float blendTimeIdle;
    public float blendTimeWalk;
    public float blendTimeStrafe;
    public float blendTimeRun;
    public float blendTimeJump;
    public float blendTimeFall;
    public float blendTimeRoll;

    public float blendTimeAttack1;

    public bool shouldPlayerObjectRotate = true;
    public bool shouldPlayerCameraRotate = true;

    [Header("Movement")]
    [Tooltip("Current Movement Speed it's Value's updated on entering state")]
    [SerializeField] private float CurrentMoveSpeed = 0;
    public float getCurrentMoveSpeed { get => CurrentMoveSpeed; }


    private bool m_CanUseDoorTeleport = true;
    public bool getCanUseDoorTeleport { get => m_CanUseDoorTeleport; }

    [Header("Deat related stuff")]
    public int deathFee;
    public GameObject DeadPanel;
    public GameObject LastWaypointUsed;

    public bool ConsoleOpen = false;

    public void StartCanUseDoorTeleportDelay()
    {
        m_CanUseDoorTeleport = false;
        Invoke("ToSetCanUseDoorTeleport", 4f);
    }
    private void ToSetCanUseDoorTeleport() { m_CanUseDoorTeleport = true; }

    IEnumerator DamageEffectCoroutine()
    {
        foreach (SkinnedMeshRenderer meshRenderer in meshRenderers)
        {
            meshRenderer.materials = damageReceivedMaterial;
        }
        yield return new WaitForSeconds(0.2f);
        foreach (SkinnedMeshRenderer meshRenderer in meshRenderers)
        {
            meshRenderer.materials = currentMaterials;
        }
    }

    public static AGUSMOVEMENT Instance;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
        m_animator = GetComponentInChildren<Animator>();
        moveDirection = Vector3.zero;
        m_InputMap = new PlayerController();
        m_InputMap.Player.Enable();
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        readyToJump = true;
        m_FSM = new FSM(gameObject);
        m_FSM.AddState(new Idle(m_FSM, "Great Sword Idle", blendTimeIdle));
        m_FSM.AddState(new Jump(m_FSM, CurrentMoveSpeed, "Jump", jumpForce, blendTimeJump));
        m_FSM.AddState(new Walk(m_FSM, "GreatSwordRun", playerStats.WALK_SPEED, blendTimeWalk));
        m_FSM.AddState(new Falling(m_FSM, "Falling", CurrentMoveSpeed, blendTimeFall));
        m_FSM.AddState(new Run(m_FSM, "Sprint", playerStats.RUN_SPEED, blendTimeRun));
        m_FSM.AddState(new Fly(m_FSM, blendTimeWalk));

        m_FSM.AddState(new LeftAttack1(m_FSM, "LeftSwordAttack1", 8, 1f, 0.45f));
        m_FSM.AddState(new LeftAttack2(m_FSM, "LeftSwordAttack2", 8, 1f, 0.5f));
        m_FSM.AddState(new LeftAttack3(m_FSM, "LeftSwordAttack3", 8, 1f, 0.4f));
        m_FSM.AddState(new LeftAttack4(m_FSM, "LeftSwordAttack4", 8, 1.6f, 1.5f));


        m_FSM.AddState(new DashInvencible(m_FSM, "Sprint", 30, 0.1f, 0.1f, 0.1f));

        m_FSM.AddState(new Stunned(m_FSM, "Stun", 2.5f, blendTimeIdle));

        m_FSM.AddState(new Kicked(m_FSM, "Kicked"));

        m_FSM.AddState(new Dead(m_FSM, "Dead"));

        playerHeight = this.GetComponent<CapsuleCollider>().height;
        equipableSkills = GetComponentInChildren<SkillManager>();


    }

    IEnumerator CurrentVelocityCoroutine = null;


    IEnumerator SetCurrentVelocityCoroutine(float desiredVelocity, float timeToReachVelocity)
    {
        // Current transition point from 0f to 1f
        float currentPoint = 0.0f;
        float StartingMoveSpeed = CurrentMoveSpeed;
        float currentCoroutineDeltaTime = 0.0f;

        //While the absolute difference between CurrentMoveSpeed and the desired speed is greater than 0.005f
        while (Mathf.Abs(CurrentMoveSpeed - desiredVelocity) > 0.005f)
        {
            currentPoint = currentCoroutineDeltaTime / timeToReachVelocity;
            CurrentMoveSpeed = Mathf.SmoothStep(StartingMoveSpeed, desiredVelocity, currentPoint);
            //currentCoroutineDeltaTime += Time.deltaTime;
            //yield return new WaitForSeconds(Time.deltaTime);
            yield return new WaitForSeconds(Time.fixedDeltaTime);
            currentCoroutineDeltaTime += Time.fixedDeltaTime;
        }
        yield return null;
    }

    public void SetCurrentVelocity(float desiredVelocity, float timeToReachVelocity)
    {
        // TO-DO Implementar sistema para variar la velocidad de animacion y velocidad de movimiento multiplicandola por una posible pocion de velocidad
        if (CurrentVelocityCoroutine != null) StopCoroutine(CurrentVelocityCoroutine);
        CurrentVelocityCoroutine = SetCurrentVelocityCoroutine(desiredVelocity, timeToReachVelocity);
        StartCoroutine(CurrentVelocityCoroutine);
    }
    public void SetCurrentVelocityInstant(float desiredVelocity)
    {
        if (CurrentVelocityCoroutine != null) StopCoroutine(CurrentVelocityCoroutine);
        moveSpeed = desiredVelocity;
    }

    private void Start()
    {
        // Copiar los materiales originales al arreglo currentMaterials
        currentMaterials = new Material[meshRenderers[0].materials.Length];
        for (int i = 0; i < meshRenderers[0].materials.Length; i++)
        {
            currentMaterials[i] = meshRenderers[0].materials[i];
        }

        if (PlayerMaxStatsRuntime.Instance.CURRENT_STAMINA < PlayerMaxStatsRuntime.Instance.MAX_STAMINA && staminaRecoveryInExecution == null)
        {
            staminaRecoveryInExecution = StaminaRecovery();
            StartCoroutine(staminaRecoveryInExecution);
        }
        if (PlayerMaxStatsRuntime.Instance.CURRENT_MANA < PlayerMaxStatsRuntime.Instance.MAX_MANA && manaRecoveryInExecution == null)
        {
            manaRecoveryInExecution = ManaRecovery();
            StartCoroutine(manaRecoveryInExecution);
        }
        CurrentHP = PlayerMaxStatsRuntime.Instance.CURRENT_HP;
        Mana = PlayerMaxStatsRuntime.Instance.CURRENT_MANA;
        Stamina = PlayerMaxStatsRuntime.Instance.CURRENT_STAMINA;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        m_WeaponSwitchingSystem = GetComponent<WeaponSwitchingSystem>();
        m_FSM.ChangeState<Idle>();

    }

    public bool m_playerIsDashing = false;
    public void SetDashBoolAndLayer()
    {
        float invencibleDashTime = 0.7f;
        if (!m_playerIsDashing)
        {
            gameObject.layer = 9;
            m_playerIsDashing = true;
            Invoke("IsDashBool", invencibleDashTime);
        }
    }
    private void IsDashBool() { m_playerIsDashing = false; gameObject.layer = 7; }



    void RotateOrientation()
    {

        // rotate orientation object to have the diff orientation from CAMERA TRASNFORM
        Vector3 viewDir = this.transform.position - new Vector3(ponercamera.transform.position.x, this.transform.position.y, ponercamera.transform.position.z);
        orientation.forward = viewDir.normalized;

        // rotate the player object to match the orientation forward with a Slerp
        Vector3 MO = m_InputMap.Player.Move.ReadValue<Vector3>();
        Vector3 movementtmp = Vector3.zero;
        if (MO.x > 0.5f)// D
            movementtmp.x += 1;
        else if (MO.x < -0.5f) // A 
            movementtmp.x += -1;
        if (MO.z > 0.5f)// W
            movementtmp.z += 1;
        else if (MO.z < -0.5f)// S
            movementtmp.z += -1;

        Vector3 inputDir = orientation.forward * movementtmp.z + orientation.right * movementtmp.x;
        if (inputDir != Vector3.zero && shouldPlayerObjectRotate)
            if (isRidingHorse) HorseGameO.transform.forward = Vector3.Slerp(HorseGameO.transform.forward, inputDir.normalized, Time.deltaTime * rotationSpeed);
            else playerObj.forward = Vector3.Slerp(playerObj.forward, inputDir.normalized, Time.deltaTime * rotationSpeed);


    }



    private void FixedUpdate()
    {
        m_FSM.FixedUpdate();
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + movementValues.GroundPerceptionSensor, whatIsGround);
        if (!grounded)
        {
            ControlFallSpeed();
        }

        //ObjectWeapon weapon = (ObjectWeapon)EquipmentManager.Instance.currentEquipment[(int)EquipSO.EquipmentType.Weapon];
        //if (weapon?.itemModel)
        //{
        //    print("Entrando a printear estados");
        //    WeaponSO weaponSO = (WeaponSO)weapon?.itemModel;
        //    foreach (EstadoTemporal estado in weapon.estadosTemporales)
        //    {
        //        print("El estado " + estado.tipoEstado + estado.timeRemaing + estado);
        //    }
        //    foreach (Estado.Estados estado in weaponSO.estadosBase)
        //    {
        //        print("estado base del arma " + estado);
        //    }
        //}


    }

    public IEnumerator checkTemporalStatesCoroutine = null;
    public void ApplyState(Estado.Estados estado, float time)
    {
        if (EquipmentManager.Instance.currentEquipment[(int)EquipSO.EquipmentType.Weapon]?.DictionarySOID != null)
        {
            estadosTemporales.Add(new EstadoTemporal(estado, time));
            if (checkTemporalStatesCoroutine == null)
            {
                checkTemporalStatesCoroutine = CheckStateEstadosTemporales();
                StartCoroutine(CheckStateEstadosTemporales());
            }
            else
            {
                StopCoroutine(checkTemporalStatesCoroutine);
                checkTemporalStatesCoroutine = CheckStateEstadosTemporales();
                StartCoroutine(CheckStateEstadosTemporales());
            }
        }

    }
    public void StopCheckTemporalStates()
    {
        if(checkTemporalStatesCoroutine!=null)
            StopCoroutine(checkTemporalStatesCoroutine);
    }
    IEnumerator CheckStateEstadosTemporales()
    {
        while (estadosTemporales.Count > 0)
        {
            yield return new WaitForSeconds(1);
            List<EstadoTemporal> currentestadosTemporalesTmp = new List<EstadoTemporal>(estadosTemporales);
            foreach (EstadoTemporal estado in estadosTemporales)
            {
                estado.timeRemaing--;
                if (estado.timeRemaing <= 0)
                {
                    currentestadosTemporalesTmp.Remove(estado);
                }

            }
            estadosTemporales = currentestadosTemporalesTmp;
        }
    }
    public bool CheckIfTemporalStateIsActive(Estado.Estados checkEstado)
    {
        foreach (EstadoTemporal estadoTemporal in estadosTemporales)
        {
            if (estadoTemporal.tipoEstado == checkEstado)
            {
                return false;
            }
        }
        return true;
    }


    void ControlFallSpeed()
    {
        if (rb.velocity.y < 0)
        {
            rb.AddForce(14.8f * Vector3.down);
        }
    }

    public void UpdateReadyToJump()
    {
        readyToJump = false;
        Invoke(nameof(ResetJump), jumpCooldown);
    }
    private void ResetJump()
    {
        readyToJump = true;
    }

    #region StaminaAndMana
    public void ConsumeStamina(float initialStaminaToConsume)
    {
        Stamina -= initialStaminaToConsume * PlayerMaxStatsRuntime.Instance.STAMINA_GENERAL_CONSUMED_MODIFIER;
        EndActionConsumeStamina();
    }
    public bool ConsumeMana(float initialManaToConsume)
    {
        if ((Mana - (initialManaToConsume * PlayerMaxStatsRuntime.Instance.MANA_GENERAL_CONSUMED_MODIFIER)) >= 0)
        {
            Mana -= initialManaToConsume * PlayerMaxStatsRuntime.Instance.MANA_GENERAL_CONSUMED_MODIFIER;
            EndActionConsumeMana();
            return true;
        }
        return false;


    }
    public float Stamina
    {
        get { return PlayerMaxStatsRuntime.Instance.CURRENT_STAMINA; }
        set
        {
            if (value < 0)
            {
                PlayerMaxStatsRuntime.Instance.CURRENT_STAMINA = 0;
                EndActionConsumeStamina();
            }
            else if (value > PlayerMaxStatsRuntime.Instance.MAX_STAMINA)
                PlayerMaxStatsRuntime.Instance.CURRENT_STAMINA = PlayerMaxStatsRuntime.Instance.MAX_STAMINA;
            else
            {
                PlayerMaxStatsRuntime.Instance.CURRENT_STAMINA = value;
            }
            staminaBar.fillAmount = PlayerMaxStatsRuntime.Instance.CURRENT_STAMINA / PlayerMaxStatsRuntime.Instance.MAX_STAMINA;
        }
    }
    public float Mana
    {
        get { return PlayerMaxStatsRuntime.Instance.CURRENT_MANA; }
        set
        {
            if (value < 0)
            {
                PlayerMaxStatsRuntime.Instance.CURRENT_MANA = 0;
                EndActionConsumeMana();
            }
            else if (value > PlayerMaxStatsRuntime.Instance.MAX_MANA)
                PlayerMaxStatsRuntime.Instance.CURRENT_MANA = PlayerMaxStatsRuntime.Instance.MAX_MANA;
            else
            {
                PlayerMaxStatsRuntime.Instance.CURRENT_MANA = value;
            }
            manaBar.fillAmount = PlayerMaxStatsRuntime.Instance.CURRENT_MANA / PlayerMaxStatsRuntime.Instance.MAX_MANA;
        }
    }


    public void EndActionConsumeStamina()
    {
        if (Stamina < PlayerMaxStatsRuntime.Instance.MAX_STAMINA && staminaRecoveryInExecution == null)
        {
            staminaRecoveryInExecution = StaminaRecovery();
            StartCoroutine(staminaRecoveryInExecution);
        }
        else if (Stamina < PlayerMaxStatsRuntime.Instance.MAX_STAMINA && staminaRecoveryInExecution != null)
        {
            StopCoroutine(staminaRecoveryInExecution);
            staminaRecoveryInExecution = StaminaRecovery();
            StartCoroutine(staminaRecoveryInExecution);
        }
    }
    public void EndActionConsumeMana()
    {
        if (Mana < PlayerMaxStatsRuntime.Instance.MAX_MANA && manaRecoveryInExecution == null)
        {
            manaRecoveryInExecution = ManaRecovery();
            StartCoroutine(manaRecoveryInExecution);
        }
        else if (Mana < PlayerMaxStatsRuntime.Instance.MAX_MANA && manaRecoveryInExecution != null)
        {
            StopCoroutine(manaRecoveryInExecution);
            manaRecoveryInExecution = ManaRecovery();
            StartCoroutine(manaRecoveryInExecution);
        }
    }


    IEnumerator StaminaRecovery()
    {
        yield return new WaitForSeconds(movementValues.CancelSprintCooldown);
        while (CheckStaminaRecoveryPossible())
        {
            yield return new WaitForSeconds(movementValues.StaminaRecoveryTime);
            Stamina += PlayerMaxStatsRuntime.Instance.STAMINA_RECOVERING_MODIFIER;
        }
    }
    IEnumerator ManaRecovery()
    {
        yield return new WaitForSeconds(movementValues.CancelManaCooldown);
        while (CheckManaRecoveryPossible())
        {
            yield return new WaitForSeconds(movementValues.ManaRecoveryTime);
            Mana += PlayerMaxStatsRuntime.Instance.MANA_RECOVERING_MODIFIER;
        }
    }

    bool CheckStaminaRecoveryPossible()
    {
        if (Stamina < PlayerMaxStatsRuntime.Instance.MAX_STAMINA && !sprint || Stamina < PlayerMaxStatsRuntime.Instance.MAX_STAMINA && sprint && GetComponent<Rigidbody>().velocity == Vector3.zero)
            return true;
        else
            return false;
    }
    bool CheckManaRecoveryPossible()
    {
        if (Mana < PlayerMaxStatsRuntime.Instance.MAX_MANA)
            return true;
        else
            return false;
    }


    public IEnumerator StaminaConsumedRunning()
    {
        while (sprint)
        {
            yield return new WaitForSeconds(movementValues.StaminaConsumerTime);
            Stamina -= playerStats.STAMINA_CONSUMED_RUNNING * PlayerMaxStatsRuntime.Instance.STAMINA_RUN_CONSUMED_MODIFIER;
        }
        EndActionConsumeStamina();
    }

    #endregion

    public void SetAllChildObjectsActive(GameObject parentObject)
    {
        // Loop through all child objects
        foreach (Transform child in parentObject.transform)
        {
            // Set child object to active
            child.gameObject.SetActive(true);

            // Recursively call function to set child object's children to active
            SetAllChildObjectsActive(child.gameObject);
        }
    }
    public void SetAllChildObjectsInactive(GameObject parentObject)
    {
        // Loop through all child objects
        foreach (Transform child in parentObject.transform)
        {
            if (child.gameObject.activeSelf)
            {

                // Set child object to inactive
                child.gameObject.SetActive(false);

                // Recursively call function to set child object's children to inactive
                SetAllChildObjectsInactive(child.gameObject);
            }
        }
    }
    /*
        public void CloseNPCPanels()
        {
            try
            {
                for (int i = 0; i < DialogPanel.transform.childCount; i++)
                {
                    // Set all childs inactive
                    if (DialogPanel.transform.GetChild(i).gameObject.activeSelf)
                    {
                        SetAllChildObjectsInactive(DialogPanel.transform.GetChild(i).gameObject);
                        DialogPanel.transform.GetChild(i).gameObject.SetActive(false);
                    }
                }
            }
            catch (NullReferenceException nre)
            {
                print("NullReferenceException on CLoseNPCPanels() method. " + nre.Message);
            }
            // Set the whole panel inactive
            DialogPanel.gameObject.SetActive(false);
            if (NPCSellPanel.activeInHierarchy) NPCSellPanel.SetActive(false);
        }
    */

    /* DAMAGE */
    #region RelacionadoConRecibirHacerDaņo_Actualizar_Morir
    override public Hurtbox.DoDamageStats DoDamage()
    {
        Hurtbox.DoDamageStats doDamageStats = new Hurtbox.DoDamageStats();

        // Devuelve 5 si no hay arma equipada
        float damage = EquipmentManager.Instance.getWeaponNormalAttack();
        doDamageStats.dmg = ((int)(damage * currentAttackMultiplier));

        ObjectWeapon weapon = (ObjectWeapon)EquipmentManager.Instance.currentEquipment[(int)EquipSO.EquipmentType.Weapon];
        if (weapon?.itemModel != null)
        {
            WeaponSO weaponSO = (WeaponSO)weapon.itemModel;

            // agregar los estados base del arma a la lista de estados a aplicar al objetivo
            foreach (Estado.Estados estado in weaponSO.estadosBase)
            {
                doDamageStats.estados.Add(Estado.GetEstado(estado, doDamageStats.dmg));
            }
            // agregar los estados temporales a la lista de estados a aplicar al objetivo
            foreach (EstadoTemporal estado in estadosTemporales)
            {
                doDamageStats.estados.Add(Estado.GetEstado(estado.tipoEstado, doDamageStats.dmg));
            }
        }

        return doDamageStats;
    }


    public override float CurrentHP
    {
        get => currentHP;
        set
        {
            currentHP = value;

            if (value > PlayerMaxStatsRuntime.Instance.MAX_HP)
            {
                CurrentHP = PlayerMaxStatsRuntime.Instance.MAX_HP;
            }
            PlayerMaxStatsRuntime.Instance.CURRENT_HP = currentHP;
            UpdateHealthBar();
        }
    }
    public void UpdateHealthBar()
    {
        healthBar.fillAmount = (float)CurrentHP / (float)PlayerMaxStatsRuntime.Instance.MAX_HP;
    }
    public override void ReceiveDmgToHealth(float dmg)
    {
        if (!dmgInvulnerable)
        {
            CurrentHP -= dmg;
            receivedDamageEvent.Raise();
            StartCoroutine(DamageEffectCoroutine());
            if (this.CurrentHP <= 0) ToDie();
            GetComponent<playerSkillScript>().addExperience(playerSkills.GetSkill("HP"));
        }

    }

    public override void ReceiveDamage(DoDamageStats damageStats)
    {
        ReceiveDmgToHealth(damageStats.dmg);
        // Add experience on HP Skill
        // TO-DO AddReceiveing Damage Animations



        for (int i = 0; i < damageStats.estados.Count; i++)
        {
            Estado newEstado = currentStates.FirstOrDefault(objecte => objecte.GetType() == damageStats.estados[i].GetType());
            if (newEstado == null)
            {
                currentStates.Add(damageStats.estados[i]);
            }
        }
        CheckApplyStates();

    }
    public override void ReceiveDamage(float damageReceived)
    {
        ReceiveDmgToHealth(damageReceived);
    }


    public ObjectWeapon GetCurrentWeapon()
    {
        ObjectWeapon weapon = (ObjectWeapon)EquipmentManager.Instance.currentEquipment[(int)EquipSO.EquipmentType.Weapon];
        return weapon;
    }



    public override void Stun(float time)
    {
        m_FSM.ChangeState<Stunned>();
    }

    public void ToDie()
    {
        m_FSM.ChangeState<Dead>();
        gameObject.layer = 9;
        // Open a panel telling the player that has died
        // Two buttons where appear like try again and quit to main menu
        DeadPanel.SetActive(true);

        // Change all enemy states to patroling
        List<GameObject> enemiesInScene = GetObjectsWithTag("Enemy");
        foreach (GameObject enemy in enemiesInScene)
        {
            enemy.GetComponent<EnemyBehaviour>().ChangeState(EnemyBehaviour.States.RESTING);
            print("enemy changed to state " + enemy.GetComponent<EnemyBehaviour>().CurrentState);
        }
    }

    public void PlayerRespawn()
    {
        GameManager.Instance.ShowOrHideCursor(false);
        gameObject.layer = 7;
        Vector3 RespawnPosition;
        if (LastWaypointUsed == null)
            RespawnPosition = new Vector3(-241.5f, 3.5f, -245); // This is the initial spawn point in the minicave over there
        else
            RespawnPosition = new Vector3(LastWaypointUsed.transform.position.x + 2, LastWaypointUsed.transform.position.y, LastWaypointUsed.transform.position.z);

        // If the player dies in the Dungeon, take him back tot he overworld
        if (!SceneManager.GetActiveScene().name.Contains("MapCreation"))
        {
            GameManager.Instance.SimpleChangeScene("MapCreation");
        }

        this.transform.position = RespawnPosition;
        this.m_FSM.ChangeState<Idle>();
        if (PlayerMaxStatsRuntime.Instance.CURRENT_GOLD >= deathFee)
        {
            PlayerMaxStatsRuntime.Instance.CURRENT_GOLD -= deathFee;
        }
        CurrentHP = PlayerMaxStatsRuntime.Instance.MAX_HP;
        Stamina = PlayerMaxStatsRuntime.Instance.MAX_STAMINA;
        Mana = PlayerMaxStatsRuntime.Instance.MAX_MANA;
    }

    bool reflectDmg = false;
    public override void ReceiveDamageFromEntity(Damageable entity, DoDamageStats doDamageStats)
    {
        if (reflectDmg)
        {
            entity.ReceiveDmgToHealth(doDamageStats.dmg);
        }
    }

    IEnumerator reflectDamageActiveCoroutine = null;
    public void ActivateReflectDamage(float seconds)
    {
        if (reflectDamageActiveCoroutine == null)
        {
            reflectDamageActiveCoroutine = ReflectDamageActive(seconds);
            StartCoroutine(reflectDamageActiveCoroutine);
        }
        else
        {
            StopCoroutine(reflectDamageActiveCoroutine);
            reflectDamageActiveCoroutine = ReflectDamageActive(seconds);
            StartCoroutine(reflectDamageActiveCoroutine);
        }
    }

    IEnumerator ReflectDamageActive(float seconds)
    {
        reflectDmg = true;
        yield return new WaitForSeconds(seconds);
        reflectDmg = false;
    }

    #endregion

    public static List<GameObject> GetObjectsWithTag(string tag)
    {
        List<GameObject> objectsWithTag = new List<GameObject>();

        GameObject[] sceneObjects = FindObjectsOfType<GameObject>();
        foreach (GameObject obj in sceneObjects)
        {
            if (obj.CompareTag(tag))
            {
                objectsWithTag.Add(obj);
            }
        }

        return objectsWithTag;
    }

}