using FiniteStateMachine;
using UnityEngine;

public class Dash : State
{
    protected AGUSMOVEMENT m_PlayerBehaviour;
    protected Rigidbody m_RigidBody;
    protected Animator m_Animator;

    protected float currentDeltaTime;

    protected string m_AnimationName;

    protected float m_DashTime;
    protected float m_MiniumTimeInState;
    protected float m_MovementSpeed;
    protected float m_TimeToReachVelocity;
    protected float m_CurrentMovementSpeed;

    protected Vector3 WhereToAddForce;

    protected Vector3 m_MovimentFinal = Vector3.zero;
    protected Vector3 InputMovement;

    private enum DashDirection { Forward, Backward, Left, Right }

    private DashDirection m_CurrentDashDirection;

    private float m_DashDistance;

    public Dash(FSM fSM, string movementAnimation, float movementSpeed, float TimeToReachVelocity, float DashTimeDuration, float MiniumTimeInState,float DashDistance)
       : base(fSM)
    {
        m_PlayerBehaviour = fSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody>();
        m_Animator = m_FSM.Owner.GetComponentInChildren<Animator>();

        m_AnimationName = movementAnimation;
        m_MovementSpeed = movementSpeed;
        m_DashTime = DashTimeDuration;
        m_TimeToReachVelocity = TimeToReachVelocity;
        m_MiniumTimeInState = MiniumTimeInState;

        m_DashDistance = DashDistance;
    }

    RaycastHit hit;
    RaycastHit hitSec;
    public override void Init()
    {
        base.Init(); 
        WhereToAddForce = Vector3.zero;
        m_RigidBody.velocity = new Vector3(0, m_RigidBody.velocity.y, 0);
        currentDeltaTime = 0f;
        SetDashDirection(); 
        m_Animator.CrossFade("Dash", m_TimeToReachVelocity, 0, 0.2f, 0.2f);
        m_PlayerBehaviour.SetCurrentVelocity(m_MovementSpeed, m_TimeToReachVelocity);
       // m_PlayerBehaviour.isDashing = true;
        Vector3 whereImChecking = m_PlayerBehaviour.playerObj.position + (m_MovimentFinal.normalized * m_DashDistance)+new Vector3(0,0.89f,0);
        if (!Physics.SphereCast(whereImChecking, 0.9f, m_MovimentFinal, out hit, m_DashDistance))
        {
            m_FSM.Owner.transform.position = whereImChecking;
            Debug.Log("RAY1 TELEPORTADO A -> "+whereImChecking);
            
        }
        else
        {

            Debug.Log("RAY 1 CHOCADO CON -> " + hit.transform.name + "\nEN POSICION -> " + hit.point);
            // Physics.Raycast(m_PlayerBehaviour.playerObj.position,m_PlayerBehaviour.playerObj.position-hit.point, out hitSec,m_DashDistance);
            // Debug.DrawRay(m_PlayerBehaviour.playerObj.position, m_PlayerBehaviour.playerObj.position - hit.point,Color.magenta,4f);
            Vector3 diff = Vector3.zero;
            if (hit.point.y < m_FSM.Owner.transform.position.y) diff.y = 0.5f;
            else
            {
                Vector3 directionToPlayer = m_FSM.Owner.transform.position-hit.point ;
                directionToPlayer = directionToPlayer.normalized;
                diff.x= directionToPlayer.x * 0.5f;
                diff.z = directionToPlayer.z * 0.5f;
            }

           // m_FSM.Owner.transform.position = hit.point+diff;
        }
        m_Animator.Play("Great Sword Idle");
    } 
      
    public override void Exit()
    {
        base.Exit();
        //m_PlayerBehaviour.isDashing = false;
    }


    //SETS ROLL DIRECTION AND WHERE THE IMPULSE HAS TO APPLY BASED ON INPUT
    private void SetDashDirection()
    {
        Vector3 vectorInput = m_PlayerBehaviour.m_InputMap.Player.Move.ReadValue<Vector3>();
        m_MovimentFinal = Vector3.zero;

        if (vectorInput.x > 0.5f)// D
        {
            m_CurrentDashDirection = DashDirection.Right;
            m_MovimentFinal += m_PlayerBehaviour.orientation.right;
        }
        else if (vectorInput.x < -0.5f) // A 
        {
            m_CurrentDashDirection = DashDirection.Left;
            m_MovimentFinal += -m_PlayerBehaviour.orientation.right;
        }
        else if (vectorInput.z > 0.5f)// W
        {
            m_CurrentDashDirection = DashDirection.Forward;
            m_MovimentFinal += m_PlayerBehaviour.orientation.forward;
        }
        else if (vectorInput.z < -0.5f)// S
        {
            m_CurrentDashDirection = DashDirection.Backward;
            m_MovimentFinal += -m_PlayerBehaviour.orientation.forward;
        }
        else
        {
            m_CurrentDashDirection = DashDirection.Forward;
            m_MovimentFinal += m_PlayerBehaviour.orientation.forward;
        }
        Debug.Log("SET ROLL DIRECTION->" + m_CurrentDashDirection);

    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        currentDeltaTime += Time.deltaTime;
        //Once MiniumTime in State is reached
        if (currentDeltaTime >= m_MiniumTimeInState)
        {

            CheckMovementInput();
            if (m_MovimentFinal != Vector3.zero)
                m_FSM.ChangeState<Walk>();
            else
                m_FSM.ChangeState<Idle>();
        }
        /*
        else
        {
            ApplyVelocityAndLimit();

            m_PlayerBehaviour.playerObj.forward = Vector3.Slerp(m_PlayerBehaviour.playerObj.forward, m_MovimentFinal.normalized, Time.deltaTime * 8);

        }*/

    }

    //Applies m_MovimentFinal and limits the velocity if surpassed max
    protected void ApplyVelocityAndLimit()
    {
        // Velocidad a traves de addforce con un limitador
        m_RigidBody.AddForce(m_MovimentFinal.normalized * m_PlayerBehaviour.getCurrentMoveSpeed * 8, ForceMode.Force);
        Vector3 velocitat = new Vector3(m_RigidBody.velocity.x, 0, m_RigidBody.velocity.z);
        if (velocitat.magnitude > m_PlayerBehaviour.getCurrentMoveSpeed)
        {
            Vector3 velocitatMax = velocitat.normalized * m_PlayerBehaviour.getCurrentMoveSpeed;
            m_RigidBody.velocity = new Vector3(velocitatMax.x, m_RigidBody.velocity.y, velocitatMax.z);
        }
    }


    //Updates m_MovimentFinal vector based on input and orientation
    protected void CheckMovementInput()
    {
        InputMovement = m_PlayerBehaviour.m_InputMap.Player.Move.ReadValue<Vector3>();
        m_MovimentFinal = Vector3.zero;
        if (InputMovement.x > 0.5f)// D
            m_MovimentFinal += m_PlayerBehaviour.orientation.right;
        else if (InputMovement.x < -0.5f) // A 
            m_MovimentFinal += -m_PlayerBehaviour.orientation.right;
        if (InputMovement.z > 0.5f)// W
            m_MovimentFinal += m_PlayerBehaviour.orientation.forward;
        else if (InputMovement.z < -0.5f)// S
            m_MovimentFinal += -m_PlayerBehaviour.orientation.forward;
    }



}
