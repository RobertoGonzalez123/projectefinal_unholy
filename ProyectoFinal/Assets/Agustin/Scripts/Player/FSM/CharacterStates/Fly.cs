using FiniteStateMachine;
using UnityEngine;
using UnityEngine.InputSystem;


public class Fly : State
{
    AGUSMOVEMENT m_PlayerBehaviour;
    Rigidbody m_RigidBody;
    float m_TimeToCompletelyStop;
    protected Animator m_Animator;

    protected Vector3 MO;
    protected Vector3 m_MovimentFinal = Vector3.zero;

    public Fly(FSM fsm, float TimeToStop) : base(fsm)
    {
        m_PlayerBehaviour = m_FSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_RigidBody = m_FSM.Owner.GetComponent<Rigidbody>();
        m_TimeToCompletelyStop = TimeToStop;
        m_Animator = m_FSM.Owner.GetComponentInChildren<Animator>();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();

    }

    public override void Init()
    {
        base.Init();
        Debug.Log("Init de fly");
        m_PlayerBehaviour.m_InputMap.Player.Move.performed += StartedMoving;

        // Remove collider from the player
        m_PlayerBehaviour.gameObject.GetComponent<CapsuleCollider>().enabled = false;
        m_PlayerBehaviour.gameObject.GetComponent<Rigidbody>().constraints =
            RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionY;

    }
    public override void Exit()
    {
        base.Exit();
        Debug.Log("Exit de fly");
        m_PlayerBehaviour.m_InputMap.Player.Move.performed -= StartedMoving;

        m_PlayerBehaviour.gameObject.GetComponent<CapsuleCollider>().enabled = true;
        m_PlayerBehaviour.gameObject.GetComponent<Rigidbody>().constraints =
           RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationY;
    }

    public override void Update()
    {
        base.Update();
        Debug.Log("update de fly");
        CheckMovementInput();
        // If current move speed is greater than 0, apply it to the player
        if (m_PlayerBehaviour.isPlayerAbleToMove) ApplyVelocityAndLimit();
        //m_PlayerBehaviour.getCurrentMoveSpeed >= 0.5f && 
        if (Input.GetKey(KeyCode.Z))
        {
            Debug.Log("should go down");
            m_RigidBody.gameObject.transform.position += new Vector3(0, -0.25f, 0);
        }
        if (Input.GetKey(KeyCode.X))
        {
            Debug.Log("should go up");
            m_RigidBody.gameObject.transform.position += new Vector3(0, 0.25f, 0);
        }
    }

    protected void CheckMovementInput()
    {
        MO = m_PlayerBehaviour.m_InputMap.Player.Move.ReadValue<Vector3>();
        m_MovimentFinal = Vector3.zero;
        if (MO.x > 0.5f)// D
            m_MovimentFinal += m_PlayerBehaviour.orientation.right;
        else if (MO.x < -0.5f) // A 
            m_MovimentFinal += -m_PlayerBehaviour.orientation.right;
        if (MO.z > 0.5f)// W
            m_MovimentFinal += m_PlayerBehaviour.orientation.forward;
        else if (MO.z < -0.5f)// S
            m_MovimentFinal += -m_PlayerBehaviour.orientation.forward;

        Debug.Log("FINAL MOVEMENT " + m_MovimentFinal);

    }

    protected void ApplyVelocityAndLimit()
    {
        if (m_PlayerBehaviour.isPlayerAbleToMove)
        {
            Debug.Log("MOVEMENT FLYING MACHJINE " + m_PlayerBehaviour.getCurrentMoveSpeed + " MOVIMENT FINAL " + m_MovimentFinal);

            // Velocidad a traves de addforce con un limitador
            Debug.Log("ADDFORCE " + m_MovimentFinal * 500);
            m_RigidBody.AddForce(m_MovimentFinal * 500);

            Vector3 velocitat = new Vector3(m_RigidBody.velocity.x, 0, m_RigidBody.velocity.z);
            if (velocitat.magnitude > m_PlayerBehaviour.getCurrentMoveSpeed)
            {
                Vector3 velocitatMax = velocitat.normalized * m_PlayerBehaviour.getCurrentMoveSpeed;
                m_RigidBody.velocity = new Vector3(velocitatMax.x, m_RigidBody.velocity.y, velocitatMax.z);
            }
        }
    }

    private void StartedMoving(InputAction.CallbackContext context)
    {
        Debug.Log("StartedMoving in Fly state. Isabletomove? " + m_PlayerBehaviour.isPlayerAbleToMove);
        if (m_PlayerBehaviour.isPlayerAbleToMove)
        {
            CheckMovementInput();
            /*if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.m_playerIsDashing) m_RigidBody.AddForce(Vector3.down * 30, ForceMode.Impulse);
            if (m_PlayerBehaviour.grounded && m_MovimentFinal == Vector3.zero)
            {
                m_FSM.ChangeState<Idle>();
            }
            else if (m_PlayerBehaviour.m_CameraFocusEnemy.isFocusingEnemy && m_PlayerBehaviour.grounded && m_MovimentFinal != Vector3.zero)
            {
                m_FSM.ChangeState<Strafe>();
            }*/
        }
    }
}
