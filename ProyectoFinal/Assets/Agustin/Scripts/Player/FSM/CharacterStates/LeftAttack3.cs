using FiniteStateMachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class LeftAttack3 : Attack
{
    protected AGUSMOVEMENT m_PlayerBehaviour;
    protected string m_AnimationName;
    protected float forceToApply;
    protected Rigidbody m_RigidBody;

    protected float currentDeltaTime;
    protected float AttackingTime;
    protected float ComboTime;
    protected Animator m_Animator;
    protected Transform playerObj;

    protected Vector3 MO;

    public LeftAttack3(FSM fSM, string movementAnimation, float force, float AttackTime, float ComboTime)
       : base(fSM)
    {
        m_PlayerBehaviour = fSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody>();

        playerObj = fSM.Owner.GetComponent<AGUSMOVEMENT>().playerObj;
        m_AnimationName = movementAnimation;
        forceToApply = force;
        AttackingTime = AttackTime;
        this.ComboTime = ComboTime;
        m_Animator = m_FSM.Owner.GetComponentInChildren<Animator>();
    }



    public override void FixedUpdate()
    {
        base.FixedUpdate();
        currentDeltaTime += Time.deltaTime;

        MO = m_PlayerBehaviour.m_InputMap.Player.Move.ReadValue<Vector3>();
        // Impulse the player at the start
        //if(currentDeltaTime < AttackingTime/2) m_RigidBody.AddForce(m_PlayerBehaviour.playerObj.transform.forward.normalized * forceToApply * 2f);

        // If on air with negative Y velocity change to Falling state
        if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.GetComponent<Rigidbody>().velocity.y < 0) m_FSM.ChangeState<Falling>();
        // If player is grounded and state exceeded the AttackingTime change to Idle state
        else if (currentDeltaTime >= AttackingTime && m_PlayerBehaviour.grounded && MO == Vector3.zero) m_FSM.ChangeState<Idle>();
        else if (currentDeltaTime >= AttackingTime && m_PlayerBehaviour.grounded) m_FSM.ChangeState<Walk>();

    }

    public override void Init()
    {
        base.Init();
        currentDeltaTime = 0f;

        m_PlayerBehaviour.SetCurrentVelocity(0, 0.1f);

        m_PlayerBehaviour.m_InputMap.Player.Jump.performed += StartedJumping;
        m_PlayerBehaviour.m_InputMap.Player.Move.performed += StartedMoving;
        m_PlayerBehaviour.m_InputMap.Player.Attack1.performed += Attack;
        m_PlayerBehaviour.m_InputMap.Player.Roll.performed += StartDashInvencible;
        m_PlayerBehaviour.shouldPlayerObjectRotate = false;
        m_Animator.SetTrigger(m_AnimationName);
        /* CROSSFADE PARAMS */
        // -target animation's name string
        // -normalizedTransitionDuration : TRANSITION TIME
        // -LAYER PONER -> 0
        // -normalizedTimeOffset : CURRENT ANIMATION OFFSET
        // -normalizedTransitionTime: TARGET OFFSET

        m_RigidBody.velocity = new Vector3(0, m_RigidBody.velocity.y, 0) + playerObj.forward * 4;


    }
    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.m_InputMap.Player.Jump.performed -= StartedJumping;
        m_PlayerBehaviour.m_InputMap.Player.Move.performed -= StartedMoving;
        m_PlayerBehaviour.m_InputMap.Player.Attack1.performed -= Attack;
        m_PlayerBehaviour.m_InputMap.Player.Roll.performed -= StartDashInvencible;
        m_PlayerBehaviour.shouldPlayerObjectRotate = true;
    }

    private void StartedMoving(InputAction.CallbackContext context)
    {
        //On presing a
        if (currentDeltaTime >= ComboTime)
        {
            if (m_PlayerBehaviour.grounded)
                m_FSM.ChangeState<Walk>();
        }
    }
    private void StartedJumping(InputAction.CallbackContext context)
    {

        Debug.Log("STARTED JUMPING CALLED -> SPACE INPUT PERFORMED");



        if (currentDeltaTime >= ComboTime)
        {
            if (m_PlayerBehaviour.readyToJump && m_PlayerBehaviour.grounded)
                m_FSM.ChangeState<Jump>();
        }

    }

    private void Attack(InputAction.CallbackContext context)
    {
        // TO-DO  PONER CONTROL DE STAMINA
        if (m_PlayerBehaviour.grounded && currentDeltaTime >= ComboTime)
            m_FSM.ChangeState<LeftAttack4>();

    }


    public override void Update()
    {
        base.Update();
    }


    private void StartDashInvencible(InputAction.CallbackContext context)
    {
        if (!m_PlayerBehaviour.m_playerIsDashing && currentDeltaTime >= ComboTime && m_PlayerBehaviour.ConsumeMana(PlayerMaxStatsRuntime.Instance.MANA_DASH_CONSUMED_MODIFIER)) m_FSM.ChangeState<DashInvencible>();
    }
}
