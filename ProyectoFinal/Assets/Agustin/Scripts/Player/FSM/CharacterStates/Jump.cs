using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : Movement
{
    string m_Animation;
    float jumpForce;


    public Jump(FSM fsm, float movementSpeed, string animation, float jumpForce, float TimeToReachVelocity)
        : base(fsm, animation, movementSpeed, TimeToReachVelocity)
    {
        m_Animation = animation;
        this.jumpForce = jumpForce;
        m_PlayerBehaviour = m_FSM.Owner.GetComponent<AGUSMOVEMENT>();
    }
    public override void Init()
    {
        //Debug.Log("Entering state: " + GetType());
        if (m_PlayerBehaviour.isRidingHorse)
        {
            m_RigidBody.AddForce(m_FSM.Owner.transform.up * jumpForce * 1.4f, ForceMode.Impulse);
            m_PlayerBehaviour.HorseAnimator.CrossFade("horse_jump", m_TimeToReachVelocity, 0, 0.2f, 0.2f);
        }
        else
        {
            m_RigidBody.AddForce(m_FSM.Owner.transform.up * jumpForce, ForceMode.Impulse);
            if (m_RigidBody.velocity.x != 0 || m_RigidBody.velocity.z != 0)
            {
                m_FSM.Owner.GetComponentInChildren<Animator>().CrossFade("JumpRunning", m_TimeToReachVelocity, 0, 0.2f, 0.2f);

            }
            else
            {
                m_FSM.Owner.GetComponentInChildren<Animator>().CrossFade(m_Animation, m_TimeToReachVelocity, 0, 0.2f, 0.2f);
            }
        }


        SoundManager.PlaySound("jumpsound");
        m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, 0.01f, m_RigidBody.velocity.z);

        m_PlayerBehaviour.UpdateReadyToJump();
        m_PlayerBehaviour.ConsumeStamina(m_PlayerBehaviour.playerSkills.GetSkill("JUMP").StaminaConsumed);
        m_PlayerBehaviour.GetComponent<playerSkillScript>().addExperience(m_PlayerBehaviour.playerSkills.GetSkill("jump"));

    }
    public override void Exit()
    {
        base.Exit();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_PlayerBehaviour.grounded && m_PlayerBehaviour.GetComponent<Rigidbody>().velocity.y == 0)
            m_FSM.ChangeState<Idle>();



    }

}


