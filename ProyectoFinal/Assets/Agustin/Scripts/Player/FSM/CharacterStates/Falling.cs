using FiniteStateMachine;
using UnityEngine;

public class Falling : Movement
{

    public Falling(FSM fSM, string movementAnimation, float movementSpeed, float TimeToReachVelocity)
      : base(fSM, movementAnimation, movementSpeed, TimeToReachVelocity)
    {
    }

    public override void Init()
    {
        if(!m_PlayerBehaviour.isRidingHorse) m_Animator.CrossFade(m_MovementAnimation, 1f, 0, 0.4f);
    }
    public override void Exit()
    {
        base.Exit();
    }
    public override void FixedUpdate()
    {
        CheckMovementInput();

        //m_RigidBody.AddForce(m_MovimentFinal.normalized * m_MovementSpeed *3, ForceMode.Force);
        m_RigidBody.velocity = m_MovimentFinal.normalized * m_PlayerBehaviour.getCurrentMoveSpeed + new Vector3(0, m_RigidBody.velocity.y, 0);

        //If player is on air with a Y velocity greater than -0.1f -> para prevenir que no quede en falling pillado
        if (m_PlayerBehaviour.grounded && m_RigidBody.velocity.y > -0.1f)
        {
            // If no input 
            if ((m_MovimentFinal.x != 0 || m_MovimentFinal.z != 0))
                m_FSM.ChangeState<Walk>();
            else
                m_FSM.ChangeState<Idle>();
        }



    }
}
