using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Stunned : State
{
    AGUSMOVEMENT m_PlayerBehaviour;
    Rigidbody m_RigidBody;
    string m_Animation;
    float m_TimeToCompletelyStop;
    protected Animator m_Animator;
    float currentDeltaTime;

    protected float TimeOnState;
    public Stunned(FSM fsm, string animation, float stateTime, float TimeToStop) : base(fsm)
    {
        m_Animation = animation;
        m_PlayerBehaviour = m_FSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_RigidBody = m_FSM.Owner.GetComponent<Rigidbody>();
        m_TimeToCompletelyStop = TimeToStop;
        m_Animator = m_FSM.Owner.GetComponentInChildren<Animator>();
        TimeOnState = stateTime;
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    public override void Init()
    {
        base.Init();
        currentDeltaTime = 0;
        m_RigidBody.velocity = new Vector3(0, m_RigidBody.velocity.y, 0);
        // Sets the player current velocity to the state's one using it's Time
        // Also changes the playing animation crossfading to the state's one

        m_Animator.CrossFade(m_Animation, m_TimeToCompletelyStop, 0, 0.2f, 0.1f);
        /* CROSSFADE PARAMS
         -target animation's name string
         -normalizedTransitionDuration : TRANSITION TIME
         -LAYER PONER -> 0
         -normalizedTimeOffset : CURRENT ANIMATION OFFSET
         -normalizedTransitionTime: TARGET OFFSET
        */


    }


    public override void Exit()
    {
        base.Exit();

    }


    public override void Update()
    {
        base.Update();
        currentDeltaTime += Time.deltaTime;
        Debug.Log("Ha terminado? :" + currentDeltaTime);
        if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.GetComponent<Rigidbody>().velocity.y < 0) m_FSM.ChangeState<Falling>();
        else if (m_PlayerBehaviour.grounded && currentDeltaTime > TimeOnState) m_FSM.ChangeState<Idle>();

    }
}
