using FiniteStateMachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class Idle : State
{
    AGUSMOVEMENT m_PlayerBehaviour;
    Rigidbody m_RigidBody;
    string m_Animation;
    float m_TimeToCompletelyStop;
    protected Animator m_Animator;
    public Idle(FSM fsm, string animation, float TimeToStop) : base(fsm)
    {
        m_Animation = animation;
        m_PlayerBehaviour = m_FSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_RigidBody = m_FSM.Owner.GetComponent<Rigidbody>();
        m_TimeToCompletelyStop = TimeToStop;
        m_Animator = m_FSM.Owner.GetComponentInChildren<Animator>();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.m_playerIsDashing) m_RigidBody.AddForce(Vector3.down * 30, ForceMode.Impulse);
    }

    public override void Init()
    {
        base.Init();

        // Sets the player current velocity to the state's one using it's Time
        // Also changes the playing animation crossfading to the state's one

        m_PlayerBehaviour.SetCurrentVelocity(0, m_TimeToCompletelyStop);
        if (m_PlayerBehaviour.isRidingHorse) m_PlayerBehaviour.HorseAnimator.CrossFade("horse_Idle", m_TimeToCompletelyStop, 0, 0.2f, 0.1f);
        else if(m_FSM.Owner.GetComponent<WeaponSwitchingSystem>().m_CurrentWeapon == WeaponSwitchingSystem.WeaponType.PUNCH)
            m_Animator.CrossFade("IdleHand", m_TimeToCompletelyStop, 0, 0.2f, 0.1f);
        else
            m_Animator.CrossFade(m_Animation, m_TimeToCompletelyStop, 0, 0.2f, 0.1f);
        /* CROSSFADE PARAMS
         -target animation's name string
         -normalizedTransitionDuration : TRANSITION TIME
         -LAYER PONER -> 0
         -normalizedTimeOffset : CURRENT ANIMATION OFFSET
         -normalizedTransitionTime: TARGET OFFSET
        */

        m_PlayerBehaviour.m_InputMap.Player.Jump.performed += StartedJumping;
        m_PlayerBehaviour.m_InputMap.Player.Move.performed += StartedMoving;
        m_PlayerBehaviour.m_InputMap.Player.Attack1.performed += Attack1;
        m_PlayerBehaviour.m_InputMap.Player.Roll.performed += StartDashInvencible;


        m_PlayerBehaviour.m_InputMap.Player.Interact.performed += InteractionManager.Instance.OnInteract;

        m_PlayerBehaviour.m_InputMap.Player.Alpha1.performed += m_PlayerBehaviour.equipableSkills.SkillAlpha1;
        m_PlayerBehaviour.m_InputMap.Player.Alpha2.performed += m_PlayerBehaviour.equipableSkills.SkillAlpha2;
        m_PlayerBehaviour.m_InputMap.Player.Alpha3.performed += m_PlayerBehaviour.equipableSkills.SkillAlpha3;
        m_PlayerBehaviour.m_InputMap.Player.Alpha4.performed += m_PlayerBehaviour.equipableSkills.SkillAlpha4;

    }
    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.m_InputMap.Player.Jump.performed -= StartedJumping;
        m_PlayerBehaviour.m_InputMap.Player.Move.performed -= StartedMoving;
        m_PlayerBehaviour.m_InputMap.Player.Roll.performed -= StartDashInvencible;
        m_PlayerBehaviour.m_InputMap.Player.Attack1.performed -= Attack1;



        m_PlayerBehaviour.m_InputMap.Player.Interact.performed -= InteractionManager.Instance.OnInteract;

        m_PlayerBehaviour.m_InputMap.Player.Alpha1.performed -= m_PlayerBehaviour.equipableSkills.SkillAlpha1;
        m_PlayerBehaviour.m_InputMap.Player.Alpha2.performed -= m_PlayerBehaviour.equipableSkills.SkillAlpha2;
        m_PlayerBehaviour.m_InputMap.Player.Alpha3.performed -= m_PlayerBehaviour.equipableSkills.SkillAlpha3;
        m_PlayerBehaviour.m_InputMap.Player.Alpha4.performed -= m_PlayerBehaviour.equipableSkills.SkillAlpha4;
    }
    private void StartDashInvencible(InputAction.CallbackContext context)
    {
        if (!m_PlayerBehaviour.m_playerIsDashing && m_PlayerBehaviour.ConsumeMana(PlayerMaxStatsRuntime.Instance.MANA_DASH_CONSUMED_MODIFIER)) m_FSM.ChangeState<DashInvencible>();
    }

    private void ApplyVelocityAndLimit()
    {
        // Velocidad a traves de addforce con un limitador
        Vector3 CurrentDirection = new Vector3(m_RigidBody.velocity.x, 0, m_RigidBody.velocity.z).normalized;
        m_RigidBody.AddForce(CurrentDirection * m_PlayerBehaviour.getCurrentMoveSpeed * 8, ForceMode.Force);
        Vector3 velocitat = new Vector3(m_RigidBody.velocity.x, 0, m_RigidBody.velocity.z);
        if (velocitat.magnitude > m_PlayerBehaviour.getCurrentMoveSpeed)
        {
            Vector3 velocitatMax = velocitat.normalized * m_PlayerBehaviour.getCurrentMoveSpeed;
            m_RigidBody.velocity = new Vector3(velocitatMax.x, m_RigidBody.velocity.y, velocitatMax.z);
        }
    }

    public override void Update()
    {
        base.Update();
        // If current move speed is greater than 0, apply it to the player
        if (m_PlayerBehaviour.getCurrentMoveSpeed >= 0.5f && m_PlayerBehaviour.isPlayerAbleToMove) ApplyVelocityAndLimit();


        // If player is on air with negative Y velocity changes to Falling state
        if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.GetComponent<Rigidbody>().velocity.y < 0 && m_PlayerBehaviour.isPlayerAbleToMove) m_FSM.ChangeState<Falling>();

    }
    
    private void Attack1(InputAction.CallbackContext context)
    {
        if (m_PlayerBehaviour.isPlayerAbleToMove && !m_PlayerBehaviour.isRidingHorse)
        {
            // TO-DO  PONER CONTROL DE STAMINA
            if (m_PlayerBehaviour.grounded)
                m_FSM.ChangeState<LeftAttack1>();
        }

    }

    private void StartedMoving(InputAction.CallbackContext context)
    {
        if (m_PlayerBehaviour.isPlayerAbleToMove)
        {
            if (m_PlayerBehaviour.grounded)
                m_FSM.ChangeState<Walk>();
        }
    }
    private void StartedJumping(InputAction.CallbackContext context)
    {
        if (m_PlayerBehaviour.isPlayerAbleToMove)
        {
            //Debug.Log("STARTED JUMPING CALLED -> SPACE INPUT PERFORMED");
            if (m_PlayerBehaviour.readyToJump && m_PlayerBehaviour.grounded && PlayerMaxStatsRuntime.Instance.CURRENT_STAMINA >= m_PlayerBehaviour.playerSkills.GetSkillStamina("JUMP") * m_PlayerBehaviour.GetComponent<AGUSMOVEMENT>().playerStats.STAMINA_CONSUME_MODIFIER)
                m_FSM.ChangeState<Jump>();
        }
    }
}
