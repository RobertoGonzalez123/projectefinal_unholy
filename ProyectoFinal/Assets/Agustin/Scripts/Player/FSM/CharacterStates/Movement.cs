using FiniteStateMachine;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.InputSystem;

public class Movement : State
{

    protected Rigidbody m_RigidBody;
    protected AGUSMOVEMENT m_PlayerBehaviour;
    protected string m_MovementAnimation;
    protected float m_MovementSpeed;

    protected Vector3 m_MovimentFinal = Vector3.zero;
    protected Vector3 MO;
    protected bool heavyAttack;

    protected Animator m_Animator;
    protected float m_TimeToReachVelocity;

    public Movement(FSM fSM, string movementAnimation, float movementSpeed, float TimeToReachVelocity)
        : base(fSM)
    {
        m_PlayerBehaviour = fSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody>();
        m_MovementAnimation = movementAnimation;
        m_MovementSpeed = movementSpeed;
        m_TimeToReachVelocity = TimeToReachVelocity;
        m_Animator = m_FSM.Owner.GetComponentInChildren<Animator>();
    }

    public override void Init()
    {
        base.Init();
        m_PlayerBehaviour = m_FSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_PlayerBehaviour.m_InputMap.Player.Jump.performed += StartedJumping;
        m_PlayerBehaviour.m_InputMap.Player.Attack1.performed += Attack1;
        //if (m_PlayerBehaviour.equipableSkills.hurracaneStateIsActive)
        //m_PlayerBehaviour.m_InputMap.Player.Alpha3.performed += HurricaneKick;

        m_PlayerBehaviour.m_InputMap.Player.Interact.performed += InteractionManager.Instance.OnInteract;

        m_PlayerBehaviour.m_InputMap.Player.Alpha1.performed += m_PlayerBehaviour.equipableSkills.SkillAlpha1;
        m_PlayerBehaviour.m_InputMap.Player.Alpha2.performed += m_PlayerBehaviour.equipableSkills.SkillAlpha2;
        m_PlayerBehaviour.m_InputMap.Player.Alpha3.performed += m_PlayerBehaviour.equipableSkills.SkillAlpha3;
        m_PlayerBehaviour.m_InputMap.Player.Alpha4.performed += m_PlayerBehaviour.equipableSkills.SkillAlpha4;
    }

    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.m_InputMap.Player.Jump.performed -= StartedJumping;
        m_PlayerBehaviour.m_InputMap.Player.Attack1.performed -= Attack1;


        m_PlayerBehaviour.m_InputMap.Player.Interact.performed -= InteractionManager.Instance.OnInteract;

        m_PlayerBehaviour.m_InputMap.Player.Alpha1.performed -= m_PlayerBehaviour.equipableSkills.SkillAlpha1;
        m_PlayerBehaviour.m_InputMap.Player.Alpha2.performed -= m_PlayerBehaviour.equipableSkills.SkillAlpha2;
        m_PlayerBehaviour.m_InputMap.Player.Alpha3.performed -= m_PlayerBehaviour.equipableSkills.SkillAlpha3;
        m_PlayerBehaviour.m_InputMap.Player.Alpha4.performed -= m_PlayerBehaviour.equipableSkills.SkillAlpha4;
    }


    private void StartedJumping(InputAction.CallbackContext context)
    {
        // If the player is ready to jump
        // If the player is touching the ground
        // If the player has more or equal current stamina than the JUMP Skill plus the stamina modifier, in case any potion or effect is affecting the stamina consumption
        if (m_PlayerBehaviour.isPlayerAbleToMove)
        {
            if (m_PlayerBehaviour.readyToJump && m_PlayerBehaviour.grounded && PlayerMaxStatsRuntime.Instance.CURRENT_STAMINA >= m_PlayerBehaviour.playerSkills.GetSkillStamina("JUMP") * m_PlayerBehaviour.GetComponent<AGUSMOVEMENT>().playerStats.STAMINA_CONSUME_MODIFIER)
            {
                m_FSM.ChangeState<Jump>();
            }
        }
    }

    public override void FixedUpdate()
    {
        CheckMovementInput();
        ApplyVelocityAndLimit();

        if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.GetComponent<Rigidbody>().velocity.y < -0.2f)
        {
            //velocity.y<-0.2f para que al bajar una rampa no cambie
            m_FSM.ChangeState<Falling>();
        }

    }

    protected void ApplyVelocityAndLimit()
    {
        if (m_PlayerBehaviour.isPlayerAbleToMove)
        {
            if (m_PlayerBehaviour.grounded)
            {
                // Velocidad a traves de addforce con un limitador
                m_RigidBody.AddForce(m_MovimentFinal.normalized * m_PlayerBehaviour.getCurrentMoveSpeed * 8, ForceMode.Force);
            }
            Vector3 velocitat = new Vector3(m_RigidBody.velocity.x, 0, m_RigidBody.velocity.z);
            if (velocitat.magnitude > m_PlayerBehaviour.getCurrentMoveSpeed)
            {
                Vector3 velocitatMax = velocitat.normalized * m_PlayerBehaviour.getCurrentMoveSpeed;
                m_RigidBody.velocity = new Vector3(velocitatMax.x, m_RigidBody.velocity.y, velocitatMax.z);
            }
        }
    }

    protected void CheckMovementInput()
    {
        MO = m_PlayerBehaviour.m_InputMap.Player.Move.ReadValue<Vector3>();
        m_MovimentFinal = Vector3.zero;
        if (MO.x > 0.5f)// D
            m_MovimentFinal += m_PlayerBehaviour.orientation.right;
        else if (MO.x < -0.5f) // A 
            m_MovimentFinal += -m_PlayerBehaviour.orientation.right;
        if (MO.z > 0.5f)// W
            m_MovimentFinal += m_PlayerBehaviour.orientation.forward;
        else if (MO.z < -0.5f)// S
            m_MovimentFinal += -m_PlayerBehaviour.orientation.forward;
        
    }

    private void Attack1(InputAction.CallbackContext context)
    {
        // TO-DO  PONER CONTROL DE STAMINA
        if (m_PlayerBehaviour.isPlayerAbleToMove && !m_PlayerBehaviour.isRidingHorse)
        {
            if (m_PlayerBehaviour.grounded)
                m_FSM.ChangeState<LeftAttack1>();
        }
    }


}