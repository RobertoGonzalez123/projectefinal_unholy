using FiniteStateMachine;
using UnityEngine;

public class Dead : State
{
    AGUSMOVEMENT m_PlayerBehaviour;
    Rigidbody m_RigidBody;
    string m_Animation;
    protected Animator m_Animator;
    private float time;

    public Dead(FSM fsm, string animation) : base(fsm)
    {
        m_Animation = animation;
        m_PlayerBehaviour = m_FSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_RigidBody = m_FSM.Owner.GetComponent<Rigidbody>();
        m_Animator = m_FSM.Owner.GetComponentInChildren<Animator>();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        //if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.m_playerIsDashing) m_RigidBody.AddForce(Vector3.down * 30, ForceMode.Impulse);
    }

    public override void Init()
    {
        base.Init();
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.Confined;
        time = 0.0f;
        Debug.Log("Dead State Started");
        // Get the player position
        // Add an explosion force to push the player backwards and play the animation
        m_Animator.Play("Dead");
        m_Animator.CrossFade(m_Animation, 0.1f, 0, 0.2f, 0.1f);
        /* CROSSFADE PARAMS
         -target animation's name string
         -normalizedTransitionDuration : TRANSITION TIME
         -LAYER PONER -> 0
         -normalizedTimeOffset : CURRENT ANIMATION OFFSET
         -normalizedTransitionTime: TARGET OFFSET
        */
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void Update()
    {
        base.Update();
    }
}
