using FiniteStateMachine;
using UnityEngine;

public class DashInvencible : State
{
    protected AGUSMOVEMENT m_PlayerBehaviour;
    protected Rigidbody m_RigidBody;
    protected Animator m_Animator;

    protected float currentDeltaTime;

    protected string m_AnimationName;

    protected float m_DashTime;
    protected float m_MiniumTimeInState;
    protected float m_MovementSpeed;
    protected float m_TimeToReachVelocity;

    protected Vector3 m_MovimentFinal = Vector3.zero;
    protected Vector3 InputMovement;

    private enum RollDirection { Forward, Backward, Left, Right }
    private RollDirection m_CurrentRollDirection;


    public DashInvencible(FSM fSM, string movementAnimation, float movementSpeed, float TimeToReachVelocity, float DashDuration, float MiniumTimeInState) : base(fSM)
    {
        m_PlayerBehaviour = fSM.Owner.GetComponent<AGUSMOVEMENT>();
        m_RigidBody = fSM.Owner.GetComponent<Rigidbody>();
        m_Animator = m_FSM.Owner.GetComponentInChildren<Animator>();

        m_AnimationName = movementAnimation;
        m_MovementSpeed = movementSpeed;
        m_DashTime = DashDuration;
        m_TimeToReachVelocity = TimeToReachVelocity;
        m_MiniumTimeInState = MiniumTimeInState;
    }

    public override void Init()
    {
        base.Init();
        m_PlayerBehaviour.SetDashBoolAndLayer();
        //m_RigidBody.velocity = new Vector3(0, m_RigidBody.velocity.y, 0);
        currentDeltaTime = 0f;

        //CHECK PLAYER INPUT, IF NONE MOVEMENT IS SETTED TO FORWARD
        CheckMovementInput();
        if (m_MovimentFinal == Vector3.zero) m_MovimentFinal += m_PlayerBehaviour.orientation.forward;

        /*if (!m_PlayerBehaviour.m_InputMap.Player.Roll.inProgress)
        {
            m_Animator.CrossFade("GreatSwordRun", m_TimeToReachVelocity, 0, 0.2f, 0.3f); // -> PlayMoveAnimationBasedOnDirection();

        }*/
        if (m_PlayerBehaviour.isRidingHorse)
        {
            m_PlayerBehaviour.HorseAnimator.CrossFade("horse_galloping", m_TimeToReachVelocity, 0, 0.2f, 0.3f);
            m_PlayerBehaviour.HorseGameO.GetComponent<Trail>().ToTrail(m_MiniumTimeInState * 0.9f);
        }
        else
        {
            m_Animator.CrossFade("GreatSwordRun", m_TimeToReachVelocity, 0, 0.2f, 0.3f);
            m_PlayerBehaviour.playerObj.GetComponent<Trail>().ToTrail(m_MiniumTimeInState * 0.9f);
        }

       
        m_PlayerBehaviour.SetCurrentVelocity(m_MovementSpeed, m_TimeToReachVelocity);

    }

    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.SetCurrentVelocityInstant(0f);
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        currentDeltaTime += Time.deltaTime;

        //m_RigidBody.AddForce(Vector3.down * 15f, ForceMode.Impulse);

        //if(!m_PlayerBehaviour.grounded) m_RigidBody.AddForce(Vector3.down * 15f, ForceMode.Impulse);

        //Once MiniumTime in State is reached
        if (currentDeltaTime >= m_MiniumTimeInState)
        {
            //CheckMovementInput();

            if (m_PlayerBehaviour.m_InputMap.Player.Move.ReadValue<Vector3>() != Vector3.zero)
                m_FSM.ChangeState<Walk>();
            else
                m_FSM.ChangeState<Idle>();
        }
        else
        {
            ApplyVelocityAndLimit();
            m_PlayerBehaviour.playerObj.forward = Vector3.Slerp(m_PlayerBehaviour.playerObj.forward, m_MovimentFinal.normalized, Time.deltaTime * 8);
        }


    }

    //Applies m_MovimentFinal and limits the velocity if surpassed max
    protected void ApplyVelocityAndLimit()
    {
        // Velocidad a traves de addforce con un limitador
        m_RigidBody.AddForce(m_MovimentFinal.normalized * m_PlayerBehaviour.getCurrentMoveSpeed * 8, ForceMode.Force);
        //m_RigidBody.velocity = new Vector3(m_MovimentFinal.normalized.x * m_PlayerBehaviour.getCurrentMoveSpeed, m_RigidBody.velocity.y, m_MovimentFinal.normalized.z * m_PlayerBehaviour.getCurrentMoveSpeed);
        Vector3 velocitat = new Vector3(m_RigidBody.velocity.x, 0, m_RigidBody.velocity.z);
        if (velocitat.magnitude > m_PlayerBehaviour.getCurrentMoveSpeed)
        {
            Vector3 velocitatMax = velocitat.normalized * m_PlayerBehaviour.getCurrentMoveSpeed;
            m_RigidBody.velocity = new Vector3(velocitatMax.x, m_RigidBody.velocity.y, velocitatMax.z);
        }
    }


    //Updates m_MovimentFinal vector based on input and orientation
    protected void CheckMovementInput()
    {
        InputMovement = m_PlayerBehaviour.m_InputMap.Player.Move.ReadValue<Vector3>();
        m_MovimentFinal = Vector3.zero;
        if (InputMovement.x > 0.5f)// D
            m_MovimentFinal += m_PlayerBehaviour.orientation.right;
        else if (InputMovement.x < -0.5f) // A 
            m_MovimentFinal += -m_PlayerBehaviour.orientation.right;
        if (InputMovement.z > 0.5f)// W
            m_MovimentFinal += m_PlayerBehaviour.orientation.forward;
        else if (InputMovement.z < -0.5f)// S
            m_MovimentFinal += -m_PlayerBehaviour.orientation.forward;
    }




}
