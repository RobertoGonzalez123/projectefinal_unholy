using FiniteStateMachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class Run : Movement
{
    public Run(FSM fSM, string movementAnimation, float movementSpeed, float TimeToReachVelocity)
      : base(fSM, movementAnimation, movementSpeed, TimeToReachVelocity)
    {
    }

    public override void Init()
    {
        base.Init();
        // Sets the player current velocity to the state's one using it's Time
        // Also changes the playing animation crossfading to the state's one
        m_PlayerBehaviour.SetCurrentVelocity(m_MovementSpeed, m_TimeToReachVelocity);
        if (m_PlayerBehaviour.isRidingHorse)
            m_PlayerBehaviour.HorseAnimator.CrossFade("horse_galloping", m_TimeToReachVelocity, 0, 0.2f, 0.2f);
        else 
            m_Animator.CrossFade(m_MovementAnimation, m_TimeToReachVelocity, 0, 0.2f, 0.2f);
        /* CROSSFADE PARAMS
         -target animation's name string
         -normalizedTransitionDuration : TRANSITION TIME
         -LAYER PONER -> 0
         -normalizedTimeOffset : CURRENT ANIMATION OFFSET
         -normalizedTransitionTime: TARGET OFFSET
        */
        m_PlayerBehaviour.m_InputMap.Player.Running.canceled += StopRunning;
        m_PlayerBehaviour.m_InputMap.Player.Roll.performed += StartDashInvencible;
        m_PlayerBehaviour.SetCurrentVelocity(m_MovementSpeed, m_TimeToReachVelocity);

        m_PlayerBehaviour.sprint = true;
        m_PlayerBehaviour.StartCoroutine(m_PlayerBehaviour.StaminaConsumedRunning());

    }

    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.sprint = false;
        m_PlayerBehaviour.m_InputMap.Player.Running.canceled -= StopRunning;
        m_PlayerBehaviour.m_InputMap.Player.Roll.performed -= StartDashInvencible;
    }

    private void StopRunning(InputAction.CallbackContext context)
    {
        if (context.canceled)
        {
            m_FSM.ChangeState<Walk>();
        }
    }

    private void StartDashInvencible(InputAction.CallbackContext context)
    {
        if (!m_PlayerBehaviour.m_playerIsDashing && m_PlayerBehaviour.ConsumeMana(PlayerMaxStatsRuntime.Instance.MANA_DASH_CONSUMED_MODIFIER)) m_FSM.ChangeState<DashInvencible>();
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (m_PlayerBehaviour.grounded && m_MovimentFinal == Vector3.zero)
        {
            m_FSM.ChangeState<Idle>();
        }
        if (m_PlayerBehaviour.Stamina == 0)
        {
            m_FSM.ChangeState<Walk>();
        }
        //else if (m_PlayerBehaviour.grounded
        //   && m_MovimentFinal != Vector3.zero && !m_PlayerBehaviour.sprint)
        //{
        //    m_FSM.ChangeState<Walk>();
        //}
    }

}
