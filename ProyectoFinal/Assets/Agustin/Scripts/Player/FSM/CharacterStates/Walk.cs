using FiniteStateMachine;
using UnityEngine;
using UnityEngine.InputSystem;

public class Walk : Movement
{
    public Walk(FSM fSM, string movementAnimation, float movementSpeed, float TimeToReachVelocity)
      : base(fSM, movementAnimation, movementSpeed, TimeToReachVelocity)
    {
    }
    public override void Init()
    {
        base.Init();
        //Debug.Log("WALK currentmovespeed-> "+m_MovementSpeed+" timetoreach -> "+m_TimeToReachVelocity);
        // Sets the player current velocity to the state's one using it's Time
        // Also changes the playing animation crossfading to the state's one
        m_PlayerBehaviour.SetCurrentVelocity(m_MovementSpeed, m_TimeToReachVelocity);
        if (m_PlayerBehaviour.isRidingHorse) 
            m_PlayerBehaviour.HorseAnimator.CrossFade("horse_walk", m_TimeToReachVelocity, 0, 0.2f, 0.2f);
        else if (m_FSM.Owner.GetComponent<WeaponSwitchingSystem>().m_CurrentWeapon == WeaponSwitchingSystem.WeaponType.PUNCH)
            m_Animator.CrossFade("GreatSwordRunHand", m_TimeToReachVelocity, 0, 0.2f, 0.2f);
        else
            m_Animator.CrossFade(m_MovementAnimation, m_TimeToReachVelocity, 0, 0.2f, 0.2f);
        /* CROSSFADE PARAMS
         -target animation's name string
         -normalizedTransitionDuration : TRANSITION TIME
         -LAYER PONER -> 0
         -normalizedTimeOffset : CURRENT ANIMATION OFFSET
         -normalizedTransitionTime: TARGET OFFSET
        */

        m_PlayerBehaviour.m_InputMap.Player.Running.performed += StartRunning;
        m_PlayerBehaviour.m_InputMap.Player.Roll.performed += StartDashInvencible;
        if (m_PlayerBehaviour.m_InputMap.Player.Running.inProgress && m_PlayerBehaviour.Stamina > 0)
        {
            m_FSM.ChangeState<Run>();
        }
    }

    public override void Exit()
    {
        base.Exit();
        m_PlayerBehaviour.m_InputMap.Player.Running.performed -= StartRunning;
        m_PlayerBehaviour.m_InputMap.Player.Roll.performed -= StartDashInvencible;
    }

    private void StartRunning(InputAction.CallbackContext context)
    {
        m_FSM.ChangeState<Run>();
    }

    private void StartDashInvencible(InputAction.CallbackContext context)
    {
        if (!m_PlayerBehaviour.m_playerIsDashing && m_PlayerBehaviour.ConsumeMana(PlayerMaxStatsRuntime.Instance.MANA_DASH_CONSUMED_MODIFIER)) m_FSM.ChangeState<DashInvencible>();
    }


    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (!m_PlayerBehaviour.grounded && m_PlayerBehaviour.m_playerIsDashing) m_RigidBody.AddForce(Vector3.down * 30, ForceMode.Impulse);
        if (m_PlayerBehaviour.grounded && m_MovimentFinal == Vector3.zero)
        {
            m_FSM.ChangeState<Idle>();
        }

    }



}
