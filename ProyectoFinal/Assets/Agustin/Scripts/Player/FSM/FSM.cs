using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FiniteStateMachine
{
    public class State
    {
        protected FSM m_FSM;
        public State(FSM fSM)
        {
            m_FSM = fSM;
        }

        public virtual void Init() { /*Debug.Log("Entering state: " + GetType() );*/ }
        public virtual void Update() { }
        public virtual void FixedUpdate() { }
        public virtual void Exit() { /*Debug.Log("Exiting state: " + GetType() );*/ }
        public virtual void OnCollisionEnter(Collision collision) { }
        public virtual void OnCollisionStay(Collision collision) { }
        public virtual void OnCollisionExit(Collision collision) { }
        public virtual void OnTriggerEnter(Collider collider) { }
        public virtual void OnTriggerStay(Collider collider) { }
        public virtual void OnTriggerExit(Collider collider) { }

    }

    public class FSM
    {
        private GameObject m_Owner;
        public GameObject Owner
        {
            get { return m_Owner; }
        }
        private State m_CurrentState = null;
        private List<State> m_States = new List<State>();

        public FSM(GameObject owner)
        {
            m_Owner = owner;
        }

        public void AddState(State state)
        {
            if(!m_States.Contains(state))
                m_States.Add(state);
        }

        public T GetState<T>() where T : State
        {
            return m_States.First(state => state.GetType() == typeof(T)) as T;
        }

        public bool ChangeState<T>() where T : State
        {
            T state = GetState<T>();
            if(state != null)
            {
                m_CurrentState?.Exit();

                m_CurrentState = state;
                m_CurrentState.Init();

                return true;
            }
            return false;
        }

        public void Update()
        {
            m_CurrentState?.Update();
        }
        public void FixedUpdate()
        {
            m_CurrentState?.FixedUpdate();
        }

        public void OnCollisionEnter(Collision collision)
        {
            m_CurrentState?.OnCollisionEnter(collision);
        }
        public void OnCollisionStay(Collision collision)
        {
            m_CurrentState?.OnCollisionStay(collision);
        }
        public void OnCollisionExit(Collision collision)
        {
            m_CurrentState?.OnCollisionExit(collision);
        }
        public void OnTriggerEnter(Collider collider)
        {
            m_CurrentState?.OnTriggerEnter(collider);
        }
        public void OnTriggerStay(Collider collider)
        {
            m_CurrentState?.OnTriggerStay(collider);
        }
        public void OnTriggerExit(Collider collider)
        {
            m_CurrentState?.OnTriggerExit(collider);
        }
    }
}

