using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BloodyScreenEffect : MonoBehaviour
{
    Image bloodyScreen;
    IEnumerator m_currentEnumerator;

    void Awake()
    {
        this.bloodyScreen = GetComponent<Image>();
    }

    public void ShowDamageEffect()
    {
        if (m_currentEnumerator != null) StopCoroutine(m_currentEnumerator);
        m_currentEnumerator = BlinkBloody();
        StartCoroutine(m_currentEnumerator);
    }



    IEnumerator BlinkBloody()
    {
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.3f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.25f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.2f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.15f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.1f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.05f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0f);
        /*yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.1f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.2f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.3f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.4f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.5f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.6f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.7f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.8f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.9f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 1f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.9f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.8f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.7f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.6f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.5f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.4f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.3f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.2f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0.1f);
        yield return new WaitForSeconds(0.1f);
        this.bloodyScreen.color = new Color(this.bloodyScreen.color.r, this.bloodyScreen.color.g, this.bloodyScreen.color.b, 0f);
        */
    }



}
