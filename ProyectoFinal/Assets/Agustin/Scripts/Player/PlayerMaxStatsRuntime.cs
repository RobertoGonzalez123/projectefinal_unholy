using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerMaxStatsRuntime : MonoBehaviour
{
    public PlayerStats playerStats;
    public TextMeshProUGUI levelUpText;

    // variables de economia
    [SerializeField] int _CURRENT_GOLD;
    [SerializeField] int _CURRENT_GEMS;

    // variables de movimiento
    [SerializeField] float _MOVEMENT_SPEED_MODIFIER;
    [SerializeField] float _STAMINA_JUMP_CONSUMED_MODIFIER;
    [SerializeField] float _STAMINA_RUN_CONSUMED_MODIFIER;
    [SerializeField] float _STAMINA_DASH_CONSUMED_MODIFIER;
    [SerializeField] float _STAMINA_GENERAL_CONSUMED_MODIFIER;
    [SerializeField] float _STAMINA_RECOVERING_MODIFIER;
    [SerializeField] float _MAX_STAMINA;
    [SerializeField] float _CURRENT_STAMINA;
    [SerializeField] float _STRENGTH_MODIFIER;

    // variables de nivel de habilidades
    [SerializeField] int _HP_SKILL_LEVEL;
    [SerializeField] int _JUMP_SKILL_LEVEL;
    [SerializeField] int _RUN_SKILL_LEVEL;
    [SerializeField] int _DASH_SKILL_LEVEL;
    [SerializeField] int _STRENGTH_SKILL_LEVEL;
    [SerializeField] int _CRIT_SKILL_LEVEL;

    // variables de estadisticas del player
    [SerializeField] float _MAX_HP;
    [SerializeField] float _CURRENT_HP;
    [SerializeField] float _MAX_DEXTERITY;
    [SerializeField] float _CURRENT_DEXTERITY;
    [SerializeField] float _CURRENT_ARMOR;

    [SerializeField] float _MAX_MANA;
    [SerializeField] float _CURRENT_MANA;
    [SerializeField] float _MANA_GENERAL_CONSUMED_MODIFIER;
    [SerializeField] float _MANA_RECOVERING_MODIFIER;
    [SerializeField] float _MANA_DASH_CONSUMED_MODIFIER;

    public static PlayerMaxStatsRuntime Instance;
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    IEnumerator HideLevelUpText()
    {
        yield return new WaitForSeconds(2.5f);
        levelUpText.gameObject.SetActive(false);
    }

    public void LevelUpHP()
    {
        if (_HP_SKILL_LEVEL < 10)
        {
            _HP_SKILL_LEVEL++;
            if (CURRENT_HP == MAX_HP)
            {
                MAX_HP += playerStats.MAX_HP * 0.05f;
                CURRENT_HP = MAX_HP;
            }
            else
            {
                MAX_HP += playerStats.MAX_HP * 0.05f;
            }
            levelUpText.text = "HP SKILL INCREASED";
            levelUpText.gameObject.SetActive(true);
            StartCoroutine(HideLevelUpText());
        }
    }

    public void SetHpBasedOnArmor()
    {
        // Get the base HP Points based on HP Skill Level
        float tempMaxHP = playerStats.MAX_HP + (playerStats.MAX_HP * 0.05f) * (_HP_SKILL_LEVEL - 1);
        // Get the armor hp to add
        float hpFromArmor = EquipmentManager.Instance.getArmorHP();

        tempMaxHP += hpFromArmor;

        if (CURRENT_HP == MAX_HP)
        {
            CURRENT_HP = tempMaxHP;
        }
        MAX_HP = tempMaxHP;
    }

    public void LevelUpJump()
    {
        if (_JUMP_SKILL_LEVEL < 10)
        {
            _JUMP_SKILL_LEVEL++;
            _STAMINA_JUMP_CONSUMED_MODIFIER *= 0.96f;
        }
        levelUpText.text = "JUMP SKILL INCREASED";
        levelUpText.gameObject.SetActive(true);
        StartCoroutine(HideLevelUpText());
    }
    public void LevelUpRun()
    {
        if (_RUN_SKILL_LEVEL < 10)
        {
            _RUN_SKILL_LEVEL++;
            _STAMINA_RUN_CONSUMED_MODIFIER *= 0.98f;
        }
        levelUpText.text = "RUN SKILL INCREASED";
        levelUpText.gameObject.SetActive(true);
        StartCoroutine(HideLevelUpText());
    }
    public void LevelUpDash()
    {
        if (_DASH_SKILL_LEVEL < 10)
        {
            _DASH_SKILL_LEVEL++;
            _STAMINA_DASH_CONSUMED_MODIFIER *= 0.95f;
        }
        levelUpText.text = "DASH SKILL INCREASED";
        levelUpText.gameObject.SetActive(true);
        StartCoroutine(HideLevelUpText());
    }

    public void LevelUpStrength()
    {
        if (_STRENGTH_SKILL_LEVEL < 10)
        {
            _STRENGTH_SKILL_LEVEL++;
            _STRENGTH_MODIFIER *= 1.15f;
        }
        levelUpText.text = "STRENGTH SKILL INCREASED";
        levelUpText.gameObject.SetActive(true);
        StartCoroutine(HideLevelUpText());
    }

    public void LevelUpCrit()
    {
        if (_CRIT_SKILL_LEVEL < 10)
        {
            _CRIT_SKILL_LEVEL++;
        }
        levelUpText.text = "CRIT SKILL INCREASED";
        levelUpText.gameObject.SetActive(true);
        StartCoroutine(HideLevelUpText());
    }

    public float MAX_STAMINA { get => _MAX_STAMINA; set => _MAX_STAMINA = value; }
    public float CURRENT_STAMINA { get => _CURRENT_STAMINA; set => _CURRENT_STAMINA = value; }
    public float STAMINA_RECOVERING_MODIFIER { get => _STAMINA_RECOVERING_MODIFIER; set => _STAMINA_RECOVERING_MODIFIER = value; }
    public float MAX_HP { get => _MAX_HP; set => _MAX_HP = value; }
    public float CURRENT_HP { get => _CURRENT_HP; set => _CURRENT_HP = value; }
    public int CURRENT_GOLD { get => _CURRENT_GOLD; set => _CURRENT_GOLD = value; }
    public float MAX_DEXTERITY { get => _MAX_DEXTERITY; set => _MAX_DEXTERITY = value; }
    public float CURRENT_DEXTERITY { get => _CURRENT_DEXTERITY; set => _CURRENT_DEXTERITY = value; }
    public float STAMINA_JUMP_CONSUMED_MODIFIER { get => _STAMINA_JUMP_CONSUMED_MODIFIER; set => _STAMINA_JUMP_CONSUMED_MODIFIER = value; }
    public float STAMINA_RUN_CONSUMED_MODIFIER { get => _STAMINA_RUN_CONSUMED_MODIFIER; set => _STAMINA_RUN_CONSUMED_MODIFIER = value; }
    public float STRENGTH_MODIFIER { get => _STRENGTH_MODIFIER; set => _STRENGTH_MODIFIER = value; }
    public int CRIT_SKILL_LEVEL { get => _CRIT_SKILL_LEVEL; set => _CRIT_SKILL_LEVEL = value; }
    public float STAMINA_GENERAL_CONSUMED_MODIFIER { get => _STAMINA_GENERAL_CONSUMED_MODIFIER; set => _STAMINA_GENERAL_CONSUMED_MODIFIER = value; }
    public float CURRENT_MANA { get => _CURRENT_MANA; set => _CURRENT_MANA = value; }
    public float MAX_MANA { get => _MAX_MANA; set => _MAX_MANA = value; }
    public float MANA_GENERAL_CONSUMED_MODIFIER { get => _MANA_GENERAL_CONSUMED_MODIFIER; set => _MANA_GENERAL_CONSUMED_MODIFIER = value; }
    public float MANA_RECOVERING_MODIFIER { get => _MANA_RECOVERING_MODIFIER; set => _MANA_RECOVERING_MODIFIER = value; }
    public float MANA_DASH_CONSUMED_MODIFIER { get => _MANA_DASH_CONSUMED_MODIFIER; set => _MANA_DASH_CONSUMED_MODIFIER = value; }
}
