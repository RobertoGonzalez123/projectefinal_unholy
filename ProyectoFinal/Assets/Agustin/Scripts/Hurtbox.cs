using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting.Antlr3.Runtime.Misc;
using UnityEngine;
using static Hurtbox;
using static InterfaceDamageable;

public class Hurtbox : MonoBehaviour
{
    Damageable mySelf;

    private void Awake()
    {
        mySelf = this.GetComponentInParent<Damageable>();
    }
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("He colisionado con :" + other.transform.name);
        if (other.GetComponentInParent<Damageable>())
        {
            DoDamageStats doDamageStats = other.GetComponentInParent<Damageable>().DoDamage();
            mySelf.ReceiveDamage(doDamageStats);
            mySelf.ReceiveDamageFromEntity(other.GetComponentInParent<Damageable>(), doDamageStats);
        }
        else if (other.GetComponentInParent<OnlyDamaging>())
        {
            Debug.Log("PRINT0");
            mySelf.ReceiveDamage(other.GetComponentInParent<OnlyDamaging>().doDamageStats);
            if (other.GetComponentInParent<OnlyDamaging>().bossOrbApplyForce)
            {
                Debug.Log("PRINT1");
                AGUSMOVEMENT playerScript = GetComponentInParent<AGUSMOVEMENT>();
                if (playerScript != null)
                {
                    float explosionForce = 500f; // Ajusta la fuerza de la explosi�n seg�n tus necesidades
                    float explosionRadius = 3f; // Ajusta el radio de la explosi�n seg�n tus necesidades
                    Vector3 explosionPosition = other.transform.position-new Vector3(0,1,0);
                    playerScript.GetComponent<Rigidbody>().AddExplosionForce(explosionForce, explosionPosition, explosionRadius,0.1f);
                    if(playerScript.CurrentHP>0)
                        playerScript.GetComponent<AGUSMOVEMENT>().m_FSM.ChangeState<Kicked>();
                }
            }
        }
    }
    public class DoDamageStats
    {
        public List<Estado> estados;
        public float dmg;

        public DoDamageStats()
        {
            estados = new List<Estado>();
        }
    }
}
