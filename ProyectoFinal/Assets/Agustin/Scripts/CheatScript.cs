using System.Collections.Generic;
using QFSW.QC;
using UnityEngine;

public class CheatScript : MonoBehaviour
{
    [SerializeField] Light DirectionalLight;
    [SerializeField] List<WeaponSO> weaponsList;
    [SerializeField] GameObject PauseSection;



    [Command]
    string GetWeapons()
    {
        foreach (WeaponSO itemModel in weaponsList) { InventoryManager.Instance.AddItem(new ObjectWeapon((WeaponSO)itemModel)); }
        return "You got all weapons";
    }

    [SerializeField] List<ArmorSO> armorList;
    [Command]
    void GetArmor()
    {
        foreach (ArmorSO itemModel in armorList) { InventoryManager.Instance.AddItem(new ObjectArmor((ArmorSO)itemModel)); }
    }

    [SerializeField] List<ConsumableSO> consumableList;
    [Command]
    void GetConsumables(int quantityToGet = 5)
    {
        foreach (ConsumableSO itemModel in consumableList)
        {
            ObjectConsumable consumable = new ObjectConsumable((ConsumableSO)itemModel);
            consumable.currentQuantity = quantityToGet;
            InventoryManager.Instance.AddItem(consumable);
        }
    }

    [Command]
    string GodMode(bool enabled = true)
    {
        AGUSMOVEMENT.Instance.dmgInvulnerable = enabled;
        if (AGUSMOVEMENT.Instance.dmgInvulnerable) return "naaashe";
        else return "not naaashe anyomre :(";
    }

    [Command]
    string Fly(bool enable = true)
    {
        if (enable) AGUSMOVEMENT.Instance.m_FSM.ChangeState<Fly>();
        else AGUSMOVEMENT.Instance.m_FSM.ChangeState<Idle>();



        return "u a pigeon?";
    }

    [Command]
    string Give(string item_name, int quantity = 1)
    {
        if (item_name == "GOLD")
        {
            PlayerMaxStatsRuntime.Instance.CURRENT_GOLD += quantity;
            return "u got " + quantity + " Gold Coins";
        }

        ObjectItem item = GameManager.Instance.SearchItemInGame(item_name);

        for (int i = 0; i < quantity; i++) InventoryManager.Instance.AddItem(item);
        return "u got " + quantity + " " + item.itemModel.ItemName;
    }

    [Command]
    string Give(int idItem, int quantity = 1)
    {
        ObjectItem item = GameManager.Instance.SearchItemInGame(idItem);

        for (int i = 0; i < quantity; i++) InventoryManager.Instance.AddItem(item);
        return "u got " + quantity + " " + item.itemModel.ItemName;
    }

    [Command]
    string CompleteActiveQuest()
    {
        if (QuestManager.Instance.HasActiveMission())
        {
            QuestSO quest = QuestManager.Instance.MisionActiva;
            quest.ChangeQuestState(QuestSO.QuestState.DOING_FINISHED);
        }

        return "Are you really that lazy man?";
    }

    [Command]
    string Lights()
    {
        if (DirectionalLight.gameObject.activeInHierarchy)
            DirectionalLight.gameObject.SetActive(false);
        else
            DirectionalLight.gameObject.SetActive(true);

        return "shiny shine shining";
    }

    [Command]
    string OpenPauseMenu()
    {
        PauseSection.SetActive(PauseSection.activeInHierarchy);
        GameManager.Instance.ShowOrHideCursor(true);
        return "opened pause menu";
    }
}
