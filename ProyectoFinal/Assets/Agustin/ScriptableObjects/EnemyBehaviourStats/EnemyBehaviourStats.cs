using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Enemy/EnemyBehaviourStats")]
public class EnemyBehaviourStats : ScriptableObject
{
    [Header("BaseStats")]
    public int health;

    [Header("Speed")]
    public float chasingSpeed;
    public float patrolingSpeed;


    [Header("Distance")]
    public float restingDetectDistance;
    public float patrolingDetectDistance;
    public float chasingMaxDistance;
    public float attackDistance;
    public float rangeAttackDistance;
    public float arrivedAtDestinationDistance;

    public List<Estado.Estados> estadosBase;


}
