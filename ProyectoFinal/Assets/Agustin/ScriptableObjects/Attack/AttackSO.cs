using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Enemy/AttackSO")]
public class AttackSO : ScriptableObject
{
    public string AttackAnimationName;
    public int AttackDamage;
    public float AttackCooldown;
    public int AttackPenetration; //TO DISCUSS: Used to calculate shield penetration 

    public List<Estado.Estados> estados;
}
