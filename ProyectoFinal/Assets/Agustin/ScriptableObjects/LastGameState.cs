using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct LastGameState
{

    public QuestSO.QuestState questdungeonTutorial;
    public QuestSO.QuestState questrecolectaItems;
    public QuestSO.QuestState questtutorialPosition;

    public bool isNewGame;
    public bool wasInDungeon;

}


