
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface InteractuableInterface
{
    public bool Interact();
}
