using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss1Controller : MonoBehaviour
{

    int initialBossHP;
    public Animator animator;
    public GameObject MeteoritePrefab;
    public GameObject RedAreaPrefab;

    private void Start()
    {
        initialBossHP = 100;
        //GetComponent<EnemyBehaviour>().EnemyDamaged.AddListener(CheckHealthPoints);
        StartCoroutine(DropMeteorites());
    }

    IEnumerator DropMeteorites()
    {
        yield return new WaitForSeconds(Random.Range(5, 10));
        animator.Play("ScreamMeteorites");
        for (int i = 0; i < 10; i++)
        {
            float randomX = Random.Range(-15, 15);
            float randomZ = Random.Range(6, -26);
            Quaternion randomRotation = Quaternion.Euler(Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f), Random.Range(0.0f, 360.0f));

            GameObject newRedArea = Instantiate(RedAreaPrefab, new Vector3(randomX, 0.5f, randomZ), Quaternion.Euler(-90, 0, 0));
            StartCoroutine(SpawnRedArea(newRedArea));
            GameObject newMeteorite = Instantiate(MeteoritePrefab, new Vector3(randomX, 50, randomZ), randomRotation);
            yield return new WaitForSeconds(1);
        }
    }

    IEnumerator SpawnRedArea(GameObject redArea)
    {
        while (true)
        {
            yield return null;
            redArea.transform.localScale += new Vector3(0.002f, 0.002f, 0.002f);
            if (redArea.transform.localScale == new Vector3(0.5f, 0.5f, 0.5f)) { Destroy(redArea); break; }
        }
    }

    public void CheckHealthPoints()
    {
        // If the boss has less than 40% of hp, receive 70% of the damage. If the boss has less than 20% of hp, receive half of the dmg
        if (GetComponent<EnemyBehaviour>().CurrentHP <= initialBossHP * 0.6f)
        {
            GetComponent<EnemyBehaviour>().dmgReceivedMultiplier = 0.7f;
            animator.Play("Roar");
        }
        if (GetComponent<EnemyBehaviour>().CurrentHP <= initialBossHP * 0.3f)
        {
            GetComponent<EnemyBehaviour>().dmgReceivedMultiplier = 0.5f;
            animator.speed = .75f;
            animator.Play("Roar");
            animator.speed = 1;
        }
    }
}
