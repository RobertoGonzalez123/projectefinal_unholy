using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.gameObject.GetComponent<AGUSMOVEMENT>().ReceiveDamage(25);
            other.gameObject.GetComponent<AGUSMOVEMENT>().m_FSM.ChangeState<Kicked>();

        }
    }
}
