using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteoriteController : MonoBehaviour
{
    public GameObject PlayerCollisionParticles;
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            print("Player collision detected");
            Destroy(this.gameObject);
            // Hacer 40 de daño al jugador si el meteorito impacta con el player
            other.gameObject.GetComponent<AGUSMOVEMENT>().ReceiveDamage(10);
            other.gameObject.GetComponent<AGUSMOVEMENT>().m_FSM.ChangeState<Kicked>();
            Instantiate(PlayerCollisionParticles, new Vector3(this.transform.position.x, this.transform.position.y - 1, this.transform.position.z), Quaternion.identity);
        }
        if (other.gameObject.tag == "Floor")
        {
            // Instanciar efectos y particulas y demas
            Destroy(this.gameObject);
        }
    }
}
