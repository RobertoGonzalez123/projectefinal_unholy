using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VenomArrow : OnlyDamaging
{
    [SerializeField] Rigidbody rigidBodyCollider;
    [SerializeField] float time = 10;
    IEnumerator arrowCoroutine = null;
    [SerializeField] GameObject poisonZonePrefab;
    AGUSMOVEMENT m_PlayerBehaviour;
    float poisonZoneSkillDuration = 10;

    public void EnableVenomArrow(float dmg, float speed, float skillDuration, float poisonZoneSkillDuration, AGUSMOVEMENT player)
    {
        doDamageStats.dmg = dmg;
        this.time = skillDuration;
        this.poisonZoneSkillDuration = poisonZoneSkillDuration;
        m_PlayerBehaviour = player;
        arrowCoroutine = DisableObject();
        StartCoroutine(arrowCoroutine);
        rigidBodyCollider.velocity = transform.forward * speed;
    }

    private void OnCollisionEnter(Collision collision)
    {
        SpawnPoisonZone();
    }

    IEnumerator DisableObject()
    {
        yield return new WaitForSeconds(time);
        SpawnPoisonZone();

    }

    private void SpawnPoisonZone()
    {
        StopCoroutine(arrowCoroutine);
        GameObject newPoisonZone = Instantiate(poisonZonePrefab, transform.position + new Vector3(0, 0, 0), transform.rotation);
        newPoisonZone.GetComponent<PoisonZone>().StartPoisonZone(m_PlayerBehaviour.DoDamage().dmg, poisonZoneSkillDuration);
        Destroy(gameObject);
    }
}
