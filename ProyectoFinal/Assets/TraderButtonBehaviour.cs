using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;// Required when using Event data.
using TMPro;

public class TraderButtonBehaviour : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] GameObject descriptionPanelGO;
    [SerializeField] TextMeshProUGUI itemName;
    [SerializeField] TextMeshProUGUI itemDescription;
    [SerializeField] TextMeshProUGUI itemPrice;
    [SerializeField] Image imageToShow;
    [SerializeField] TextMeshProUGUI quantityText;
    [SerializeField] Image rarityGlow;
    IEnumerator OpenPanelCoroutine()
    {
        print("start coroutine item i think");
        yield return new WaitForSeconds(1.0f);
        this.OpenPanel();
    }

    //Used to load player Items to sell
    public void LoadInfo(ObjectItem playerItem)
    {
        imageToShow.sprite = playerItem.itemModel.SlotIcon;
        
        itemName.text = playerItem.itemModel.ItemName;
        itemDescription.text = playerItem.itemModel.ItemDescription;
        itemPrice.text = playerItem.itemModel.SellPrice.ToString() + " GOLD";
        rarityGlow.color = GetRarityColor(playerItem.itemModel.Rarity);

        if (playerItem is ObjectConsumable)
            quantityText.text = "" + ((ObjectConsumable)playerItem).currentQuantity;
        else if (playerItem is ObjectSimpleItem)
            quantityText.text = "" + ((ObjectSimpleItem)playerItem).currentQuantity;
        else
            quantityText.text = "";
    }
    //Used to load NPC Items to buy
    public void LoadInfo(ItemSO itemModel)
    {
        imageToShow.sprite = itemModel.SlotIcon;

        itemName.text = itemModel.ItemName;
        itemDescription.text = itemModel.ItemDescription;
        itemPrice.text = itemModel.BuyPrice.ToString() + " GOLD";
        rarityGlow.color = GetRarityColor(itemModel.Rarity);

        if (itemModel is ConsumableSO || itemModel is SimpleItemSO)
            quantityText.text = "1";
        else
            quantityText.text = "";
    }

    void OpenPanel()
    {
        print("opens panel i think");
        descriptionPanelGO.SetActive(true);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        print("HAS ENTRADO EN EL BOTON");
        this.StartCoroutine(OpenPanelCoroutine());
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("The cursor exited the selectable UI element.");
        this.StopAllCoroutines();
        descriptionPanelGO.SetActive(false);
    }

    private Color GetRarityColor(Rarity rarity)
    {
        Color color;

        switch (rarity)
        {
            case Rarity.Common:
                color = Color.white;
                break;
            case Rarity.Uncommon:
                color = Color.green;
                break;
            case Rarity.Rare:
                color = Color.blue;
                break;
            case Rarity.Epic:
                color = Color.magenta;
                break;
            case Rarity.Legendary:
                color = Color.yellow;
                break;
            default:
                color = Color.white;
                break;
        }
        color.a = 0.1008f;
        return color;
    }
}
