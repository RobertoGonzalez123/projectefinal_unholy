using Cinemachine;
using UnityEngine;

public class DungeonSceneManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        TeleportPlayerToStart();
        ChangeCameraSensibilityAndDistance();
        //GameManager.Instance.StartCoroutineFadeLoadingScreen(1, 1, 0);
    }

    void ChangeCameraSensibilityAndDistance()
    {
        CinemachineFreeLook camera = GameObject.Find("CM FreeLook1").gameObject.GetComponent<CinemachineFreeLook>();
        camera.m_Orbits[0].m_Radius = 1.5f;
        camera.m_Orbits[1].m_Radius = 3.5f;
        camera.m_Orbits[2].m_Radius = 1.5f;

        //camera.m_XAxis.m_MaxSpeed = 300;
        //camera.m_YAxis.m_MaxSpeed = 2;

    }

    void TeleportPlayerToStart()
    {
        GameObject player = GameObject.Find("Player");
        if (player)
        {
            player.transform.localScale = new Vector3(0.75f, 0.75f, 0.75f);
            player.GetComponent<Transform>().position = new Vector3(1232, 2, 1250);
            player.GetComponent<Transform>().eulerAngles = new Vector3(0, -223, 0);
        }
        
    }
}
