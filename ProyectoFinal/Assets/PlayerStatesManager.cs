/*
using FiniteStateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatesManager : States
{
    public AGUSMOVEMENT player;
    List<float> currentStates = new List<float>();
    public GameStatesDefaultDurations gameStatesDurations;
    void Start()
    {
        player = GetComponentInParent<AGUSMOVEMENT>();
        StartCoroutine(DizzyState());
    }

    public void GetState(Estado state, float modifier, int dmg)
    {
        currentStates[(int)state] += modifier;
        if (currentStates[(int)state] >= 100)
        {
            currentStates[(int)state] = 100;
            StartState(state, dmg);
        }

    }
    public void RemoveState(Estado state, float modifier)
    {
        currentStates[(int)state] -= modifier;
        if (currentStates[(int)state] <= 0)
        {
            currentStates[(int)state] = 0;
        }
    }

    public void StartState(Estado state, int dmg)
    {
        switch (state)
        {
            case Estado.Fire:
                StartCoroutine(FireState(dmg));
                break;
            case Estado.Poison:
                StartCoroutine(PoisonState(dmg));
                break;
            case Estado.Corrosion:
                StartCoroutine(CorrosionState(dmg));
                break;
            case Estado.Electricity:
                StartCoroutine(ElectricState(dmg));
                break;
            case Estado.Stun:
                StartCoroutine(StunState());
                break;
            case Estado.Dizzy:
                StartCoroutine(DizzyState());
                break;
        }
    }

    IEnumerator FireState(int dmg)
    {
        for (int i = 0; i < gameStatesDurations.FireStateDuration; i++)
        {
            yield return new WaitForSeconds(1f);
            player.ReceiveDamage(dmg);
        }
    }
    IEnumerator PoisonState(int dmg)
    {
        for (int i = 0; i < gameStatesDurations.PoisonStateDuration; i++)
        {
            yield return new WaitForSeconds(0.2f);
            player.ReceiveDamage(dmg);
        }
    }
    IEnumerator CorrosionState(int dmg)
    {
        for (int i = 0; i < gameStatesDurations.CorrosionStateDuration; i++)
        {
            yield return new WaitForSeconds(0.1f);
            player.ReceiveDamage(dmg);
        }

    }
    IEnumerator ElectricState(int dmg)
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < gameStatesDurations.CorrosionStateDuration; i++)
        {
            yield return new WaitForSeconds(1f);
            player.ReceiveDamage(dmg);
        }

    }
    IEnumerator StunState()
    {
        yield return new WaitForSeconds(1f);
        //stun player
    }


    IEnumerator DizzyState()
    {
        yield return new WaitForSeconds(1f);
        for (int i = 0; i < gameStatesDurations.CorrosionStateDuration; i++)
        {
            yield return new WaitForSeconds(1f);
            player.Stamina = player.Stamina - player.playerStats.MAX_STAMINA * 0.05f;
        }

    }

}
*/