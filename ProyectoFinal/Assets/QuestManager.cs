using TMPro;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    public static QuestManager Instance;
    public GameObject QuestCanvasMenu;

    public GameObject NoQuestAcceptedText;
    public GameObject QuestsUIGroup;

    [SerializeField] TextMeshProUGUI UI_QuestTitle;
    [SerializeField] TextMeshProUGUI UI_QuestDescription;
    [SerializeField] TextMeshProUGUI UI_QuestTasks;
    [SerializeField] TextMeshProUGUI UI_QuestRewards;


    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        InventoryManager.Instance.onInventoryChanged += UpdateQuestItemsRequisites;
    }

    void UpdateQuestItemsRequisites()
    {
        if (MisionActiva is MisionRecolectaSO)
        {
            int progresoTemp = 0;
            MisionRecolectaSO mision = misionActiva as MisionRecolectaSO;
            foreach (ObjectItem item in InventoryManager.Instance.Items) // for each item in the inventory
            {
                if (item.itemModel.ItemName == mision.itemObjetivo.ItemName)
                {
                    if (item is ObjectConsumable)
                    {
                        ObjectConsumable item2 = (ObjectConsumable)item;
                        progresoTemp += item2.currentQuantity;
                    }
                    else if (item is ObjectSimpleItem)
                    {
                        ObjectSimpleItem item2 = (ObjectSimpleItem)item;
                        progresoTemp += item2.currentQuantity;
                    }
                }
            }
            mision.progreso = progresoTemp; // Set the progress of the quest
            if (mision.progreso == mision.cantidadObjetivo) // If the progress and the requirements are the same value change the state of the quest
            {
                mision.ChangeQuestState(QuestSO.QuestState.DOING_FINISHED);
            }
        }
    }

    private QuestSO misionActiva;

    public QuestSO MisionActiva { get => misionActiva; set => misionActiva = value; }

    public void AgregarMisionActiva(QuestSO mision)
    {
        if (MisionActiva == null)
        {
            MisionActiva = mision;

            // Change QuestMenu in Canvas with information about the quest
            NoQuestAcceptedText.SetActive(false);
            QuestsUIGroup.SetActive(true);
            UI_QuestTitle.text = MisionActiva.QuestName;
            UI_QuestDescription.text = MisionActiva.QuestDescription;
        }
    }

    public bool HasActiveMission()
    {
        if (MisionActiva == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void EliminarMisionActiva()
    {
        MisionActiva.ChangeQuestState(QuestSO.QuestState.NOT_TAKEN);
        MisionActiva = null;
    }

    public void ActualizarMisiones()
    {
        if (MisionActiva != null)
        {
            MisionActiva.CompletarMision();
            if (MisionActiva.GetState() == QuestSO.QuestState.FINISHED)
            {
                EliminarMisionActiva();
            }
        }
    }

}
