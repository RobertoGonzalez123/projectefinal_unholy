using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowThrowerInTheWallController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine(ThrowArrowAnimation());
    }

    IEnumerator ThrowArrowAnimation()
    {
        while (true)
        {
            GetComponent<Animator>().Play("ThrowArrow");
            int timeToWaitForTheNextArrowShoot = (int)Random.Range(3f, 6f);
            yield return new WaitForSeconds(timeToWaitForTheNextArrowShoot);
        }
    }

}
