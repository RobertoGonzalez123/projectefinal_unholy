using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointController : Interactuable
{
    public GameController gameController;

    public override bool Interact()
    {
        print("saving with a waypoint");
        gameController.SaveGame();
        AGUSMOVEMENT.Instance.LastWaypointUsed = this.gameObject;
        return false; // Roberto's things to not destroy the object I think
    }
}
