using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowTrapController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            print("ARROW COLLIDES WITH PLAYER");
            other.GetComponent<AGUSMOVEMENT>().ReceiveDamage(10);
        }
    }
}
