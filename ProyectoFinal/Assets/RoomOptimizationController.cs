using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomOptimizationController : MonoBehaviour
{
    private GameObject[] objectsToLoad;

    // Call this method to enable all objects inside the room
    public void LoadAllObjects()
    {
        if (objectsToLoad == null)
        {
            // Cache all child objects in the room for faster access
            objectsToLoad = GetComponentsInChildren<GameObject>();
        }

        // Enable all objects
        foreach (var obj in objectsToLoad)
        {
            obj.SetActive(true);
        }
    }

    // Call this method to disable all objects inside the room
    public void UnloadAllObjects()
    {
        if (objectsToLoad == null)
        {
            // Cache all child objects in the room for faster access
            objectsToLoad = GetComponentsInChildren<GameObject>();
        }

        // Disable all objects
        foreach (var obj in objectsToLoad)
        {
            obj.SetActive(false);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
