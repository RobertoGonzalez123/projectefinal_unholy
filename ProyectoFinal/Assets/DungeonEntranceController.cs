using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DungeonEntranceController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (transform.parent.name.Contains("Easy"))
                GameManager.Instance.DungeonInteractedDifficulty = GameManager.DungeonDifficulty.EASY;
            else if (transform.parent.name.Contains("Hard"))
                GameManager.Instance.DungeonInteractedDifficulty = GameManager.DungeonDifficulty.HARD;
            else
                GameManager.Instance.DungeonInteractedDifficulty = GameManager.DungeonDifficulty.MEDIUM;
            
            GameManager.Instance.ChangeToScene("Mazmorras");
        }
    }
}
