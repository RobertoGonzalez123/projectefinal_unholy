using System.Collections;
using UnityEngine;

public class StartingMenuManager : MonoBehaviour
{
    public CanvasGroup Canvas;
    public Animator GeneralButtonsAnimator;

    public GameObject CanvasSkipAnimation;

    public Animator CameraAnimator;
    public Animator CameraLookAtAnimator;

    private bool isPlayOptionsDisplayed = false;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        // Hide the canvas
        StartCoroutine(HideCanvas());
    }

    IEnumerator HideCanvas()
    {
        Canvas.alpha = 0f;
        yield return new WaitForSeconds(25);
        CanvasSkipAnimation.SetActive(false); // We no longer want to see the 'SkipAnimationButton'
        StartCoroutine(FadeInCanvas());
    }

    public void SkipAnimation()
    {
        print("Skipping animation");
        StopAllCoroutines();
        StartCoroutine(FadeInCanvas());
        CameraAnimator.Play("StartingCameraAnimation", -1, 24);
        CameraLookAtAnimator.Play("StartingCameraAimAnimation", -1, 24);
        CanvasSkipAnimation.SetActive(false);
    }

    IEnumerator FadeInCanvas()
    {
        float timer = 0f;
        float currentAlpha = 0f;
        float targetAlpha = 1;

        while (timer < 3)
        {
            // Calculate the normalized time value
            float normalizedTime = timer / 3;

            // Update the alpha value based on the normalized time
            currentAlpha = Mathf.Lerp(0f, 1f, normalizedTime);

            // Set the alpha value of the CanvasGroup
            Canvas.alpha = currentAlpha;

            // Increase the timer by the elapsed time since the last frame
            timer += Time.deltaTime;

            // Wait for the next frame
            yield return null;
        }

        // Ensure that the final alpha value is set correctly
        Canvas.alpha = 1;

        Canvas.interactable = true;

        // Disable or destroy the script if needed
        // gameObject.SetActive(false);
        // Destroy(this);

    }

    public void PlayAnimationOnPlayButtonPressed()
    {
        isPlayOptionsDisplayed = true;
        GeneralButtonsAnimator.Play("StartingMenuPlayAnimation");
    }

    public void PlayAnimationOnPlayButtonUnPressed()
    {
        if (isPlayOptionsDisplayed)
        {
            isPlayOptionsDisplayed = false;
            GeneralButtonsAnimator.Play("StartingMenuClosePlayAnimation");
        }
    }

    public void ToggleGameObject(GameObject objectToToggle)
    {
        objectToToggle.SetActive(!objectToToggle.activeInHierarchy);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

}
